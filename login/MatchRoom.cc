#include "MatchRoom.h"
#include "LoginServer.h"
#include <functional>
#include "net/ProtobufClient.h"
#include "user/UserManager.h"
#include "RoomServerPoolManager.h"
#include <algorithm>

using namespace fsk;
using namespace fsk::net;

typedef shared_ptr<ProtobufClient> ProtobufClientPtr;

MatchRoom::MatchRoom(LoginServer* server, shared_ptr<RoomServerPoolManager> roomServerPool)
		:server_(server),
		roomServerPool_(roomServerPool)
{
}

int32_t MatchRoom::init(){
	using namespace std::placeholders;
	server_->registerMessageCallback(pbmsg::LoginRoom_Message, pbmsg::SSRoomInfo_Response, bind(&MatchRoom::RoomInfoCallback, this, _1, _2, _3, _4 ));
	server_->registerMessageCallback(pbmsg::LoginRoom_Message, pbmsg::SSEnterRoom_Response, bind(&MatchRoom::SSEnterRoomCallback, this, _1, _2, _3, _4 ));
	return 0;
}

int32_t MatchRoom::getRoomById(int32_t id, RoomUnitInfoPtr& info){
	if(roomsById_.find(id) == roomsById_.end()){
		LOG_ERROR <<"no such roomid:"<<id;
		return -1;
	}
	
	info = roomsById_[id];
	return 0;

}

int32_t MatchRoom::getRoomInfoByType(int32_t type, RoomInfo& roomInfo){
	if(rooms_.find(type) == rooms_.end()){
		LOG_ERROR <<"no such roomtype:"<<type;
		return -1;
	}
	
	roomInfo = rooms_[type];
	return 0;
}

int32_t MatchRoom::loadMatchRoom() {
	fstream fin(tmp::ROOM_FILE_NAME, ios::in | ios::binary);
	ROOM_INFO_ARRAY infoArr;
	infoArr.ParseFromIstream(&fin);
	for (int32_t ii = 0; ii < infoArr.items_size(); ii++) {
		ROOM_INFO info = infoArr.items(ii);
		int32_t roomTypeId = info.roomid();
		if (rooms_.find(roomTypeId) != rooms_.end()) {
			LOG_ERROR << "have two same room id:" << info.roomid();
			return -1;
		}
		rooms_[roomTypeId] = make_pair(info, EXT_ROOM_INFO_PTR(new EXT_ROOM_INFO()));
	}
	return 0;
}

void MatchRoom::RoomInfoCallback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp timestamp){
	
	uint16_t port = conn->peerAddress().toPort();
	string ip = conn->peerAddress().toIp();
	int32_t serverId;
    int32_t ret = server_->getServerIdByConnection(conn, serverId);
	if(ret != 0){
		LOG_ERROR << "no server with conn";
		return ;
	}	
	
	shared_ptr<LoginRoomMessage> message = 
			std::dynamic_pointer_cast<LoginRoomMessage>(_message);
	SSRoomInfoResponse rsp = message->response().roominforesponse();
	int32_t roomType = rsp.roomtype();
	if(rooms_.find(roomType) == rooms_.end()){
		LOG_ERROR << "room type:"<< roomType;
		return;
	}
	EXT_ROOM_INFO_PTR info = rooms_[roomType].second;
	info->roomNum_ = rsp.infos_size();
	info->availRooms_.clear();
	for(int i = 0; i < info->roomNum_; i++){
		const SSRoomInfoResponse_SSRoomInfo& rspRoom = rsp.infos(i);
		
		RoomUnitInfoPtr unit(new RoomUnitInfo());
		unit->campNum = rspRoom.campnum();
		unit->roomId = rspRoom.roomid();
		unit->playerNumByCamp.clear();
		unit->roomType = roomType;
		unit->serverId = serverId;
		for(int32_t j = 0; j < unit->campNum; j++){
			unit->playerNumByCamp.push_back(rspRoom.counts(j));	
		}
		info->availRooms_.push_back(unit);	
		roomsById_[unit->roomId] = unit;	
	}
	info->last_update = timestamp;
}

void MatchRoom::SSEnterRoomCallback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp timestamp){
	
	shared_ptr<LoginRoomMessage> message = 
			std::static_pointer_cast<LoginRoomMessage>(_message);
	SSEnterRoomResponse ssrsp = message->response().enterroomresponse();
	
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}

	Ret ssRet = ssrsp.ret();
	pbmsg::Ret csRet;

	if(ssRet == Ret::Success){
		//fixme
		csRet = Ret::Success;
	}else if(ssRet == Ret::Error){
		//fixme
		csRet = Ret::Error;
	}

	vector<int32_t> uids;

	for(size_t ii = 0; ii < ssrsp.uids_size(); ++ii){
		uids.push_back(ssrsp.uids(ii));
	}

	
    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
   	msg->set_msgtype(pbmsg::RoomEnter_Response);
	msg->set_seq(message->seq());        
    pbmsg::login::Response *response = msg->mutable_response();
    pbmsg::login::RoomEnterResponse *rsp = response->mutable_roomenter();

	rsp->set_ret(csRet);
	if(csRet == Ret::Success){
		int32_t roomId= ssrsp.roomid();
		int32_t camp = ssrsp.camp();

		RoomUnitInfoPtr unitInfo;
		int32_t ret = getRoomById(roomId, unitInfo);
		if(ret != 0){
			LOG_ERROR <<"get room info failed, roomid:"<<roomId;
			return;
		}
		SSConnectionInfoPtr info = roomServerPool_->getConnectionInfoById(unitInfo->serverId);
		if(info.get() == NULL){
			LOG_ERROR <<"get room connection failed";
			return;
		}
		rsp->set_roomid(roomId);
		rsp->set_ip(info->ip_);
		rsp->set_port(info->port_);

		for_each(uids.begin(), uids.end(), [&](int32_t &uid){
				UserInfoPtr info = UserManager::getInstance().getById(uid);
				do{
					if(info.get() == NULL){
						break;
					}
					info->setInRoom(roomId, camp);

				}while(0);

		});
		

	}
	server->sendMessageByIdSet(uids, std::static_pointer_cast<google::protobuf::Message>(msg));
}

int32_t MatchRoom::ResetRoomInfo(){
    return 0;
}

int32_t MatchRoom::getAllRoomInfoRequest(){
	for(auto it = rooms_.begin(); it != rooms_.end(); ++it){
		int32_t roomTypeId = it->first;
		getRoomInfoRequestByType(roomTypeId);
	}	
	return 0;
}	

int32_t MatchRoom::getRoomInfoRequestByType(int32_t roomType){
	shared_ptr<LoginRoomMessage> msg(new LoginRoomMessage());
	msg->set_msgtype(pbmsg::SSRoomInfo_Request);
	pbmsg::loginroom::Request* req = msg->mutable_request();
	SSRoomInfoRequest* roomReq = req->mutable_roominforequest();
	roomReq->set_roomtype(roomType);
	roomServerPool_->sendBroadCast(msg);
	return 0;
}

int32_t MatchRoom::tryEnterRoomByType(int32_t uid, int32_t roomTypeId, RoomUnitInfoPtr& roomUnitInfo, int32_t& camp){
	UserInfoPtr info = UserManager::getInstance().getById(uid);
	if(info->isInRoom_){
		LOG_ERROR << "user already in room:"<<info->roomId_ << "uid:"<<uid << "roomtypeid:" <<roomTypeId;
		return -1;
	}

	if(rooms_.find(roomTypeId) == rooms_.end()){

		LOG_ERROR <<"no such roomtype:"<<roomTypeId<<" uid:"<<uid;  //fixme kick off
		return -1;
	}

	/*RoomInfo roomInfo = rooms_[roomTypeId];
	
	if(roomInfo.second->playerNum >= roomInfo.first.maxnum()){
		LOG_ERROR <<"no avail ";		//fixme apply for create new
		return -1;
	}*/

	int32_t ret = TakeUpRoomByType(roomTypeId, vector<int32_t>{uid}, roomUnitInfo, camp); 
	if(ret != 0){
		LOG_ERROR << "takeup room failed";
		return -1;
	}

	return ret;
}

int32_t MatchRoom::tryEnterRoomByRoomId(int32_t uid, int32_t roomId, int32_t campId, RoomUnitInfoPtr& roomInfo){
	UserInfoPtr info = UserManager::getInstance().getById(uid);
	if(info->isInRoom_){
		LOG_ERROR << "user already in room:"<<info->roomId_ << "uid:"<<uid << "campid:" <<campId;
		return -1;
	}

	if(roomsById_.find(roomId) == roomsById_.end()){

		LOG_ERROR <<"no such roomtype:"<<roomId<<" uid:"<<uid;  //fixme kick off
		return -1;
	}

	roomInfo = roomsById_[roomId];

	if(campId < 0 || campId >= roomInfo->playerNumByCamp.size()){
		LOG_ERROR <<"camp id exceeds limit, campId:"<<campId;
	}

	roomInfo->playerNumByCamp[campId] += 1;
	roomInfo->playerNum += 1;
    return 0;

}


int32_t MatchRoom::sendRoomServerEnterRequest(vector<int32_t>&& uid, const RoomUnitInfoPtr& info, int32& camp, int32_t seq){
	/*
	ProtobufClientPtr client;
    ret = server_->roomServerPool_->getClientById(unitInfo.serverId, client);
    if(ret != 0){
        LOG_ERROR << "no such client";
        return -1;
    }*/
	
	shared_ptr<LoginRoomMessage> msg(new LoginRoomMessage());
	msg->set_msgtype(pbmsg::SSEnterRoom_Request);
	msg->set_seq(seq);
	pbmsg::loginroom::Request* req = msg->mutable_request();
	SSEnterRoomRequest* enterReq = req->mutable_enterroomrequest();
	enterReq->set_roomid(info->roomId);
	enterReq->set_camp(camp);
	for(auto it = uid.begin(); it != uid.end(); ++it){
		int32_t userId = *it;
		UserInfoPtr userInfo = UserManager::getInstance().getById(userId);	
		pbmsg::common::UserBasicInfo *user = enterReq->add_users();
		user->set_ip(userInfo->ip_);
		user->set_photo(userInfo->basicData_->photo());
		user->set_name(userInfo->basicData_->name());
		user->set_lv(userInfo->basicData_->lv());
		user->set_exp(userInfo->basicData_->exp());
		user->set_uid(userId);
		user->set_token(userInfo->basicData_->token());
	}	

	roomServerPool_->sendByServerId(info->serverId, msg);
	return 0;
}

bool MatchRoom::readjust(int32_t roomId, vector<int32_t>&& numByCamp){
	
	RoomUnitInfoPtr info;
	if(getRoomById(roomId, info)!= 0){
		LOG_ERROR<<"get room failed, id:"<<roomId;
		return false;
	}

	if(info->playerNumByCamp.size() != numByCamp.size()){
		LOG_ERROR<<"camp num not matched";
		return false;
	}

	for(int32_t ii = 0; ii < numByCamp.size(); ii++){
		info->playerNumByCamp[ii] = numByCamp[ii];
	}
	return true;
}

bool MatchRoom::takeUpCount(vector<int32_t>& id, RoomUnitInfoPtr& info, int32_t& camp){
	int32_t count = id.size();
	RoomInfo roomInfo;
	int32_t ret = getRoomInfoByType(info->roomType, roomInfo);
	if(ret != 0){
		LOG_ERROR <<"no room for id:"<<info->roomId;
		return false;
	}

	int32_t campMax = roomInfo.first.maxnum(); 

	auto min = std::min_element(std::begin(info->playerNumByCamp), std::end(info->playerNumByCamp));

	if(min == info->playerNumByCamp.end()){
		LOG_ERROR <<"no min";
		return false;
	}

	camp = std::distance(std::begin(info->playerNumByCamp), min);

	if(campMax - *min < count){
		return false;
	}
	*min += count;
	//info.playerNumByCamp[camp] = 5;

	info->playerNum += count;

	return true;

}



int32_t MatchRoom::TakeUpRoomByType(int32_t roomTypeId, vector<int32_t>&& id, RoomUnitInfoPtr& unitInfo, int32_t& camp){
	if(rooms_.find(roomTypeId) == rooms_.end()){
    	LOG_ERROR <<"no such room type:"<<roomTypeId;
		return -1;
	}
	RoomInfo allInfo = rooms_[roomTypeId];
	ROOM_INFO basicInfo = allInfo.first;
	EXT_ROOM_INFO_PTR extInfo = allInfo.second;

	int32_t availIdx = -1;

	for(int32_t ii = 0; ii < extInfo->availRooms_.size(); ii++){
		bool check = true;
		availIdx = ii;
		unitInfo = extInfo->availRooms_[ii];

		for(int32_t jj = 0; jj < id.size(); jj++){
			int32_t uid = id[jj];
			UserInfoPtr uinfo = UserManager::getInstance().getById(uid);
			if(!(uinfo->basicData_->lv() <= basicInfo.requiredlv()
				 && uinfo->basicData_->exp() <= basicInfo.requiredexp())){
				check = false;
				break;		
			}
		}
		if(!check){
			continue;
		}

		check = takeUpCount(id, unitInfo, camp);
		if(check){
			break;
		}

	}		

	//is no room fixme
	if(availIdx <0){
		LOG_ERROR << "found none room";//fixme
		return -1;
	}
	return 0;

}	

