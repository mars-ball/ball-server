#pragma once
#include "net/ProtobufServer.h"
#include "user/UserManager.h"
#include "hredis/RedConnection.h"
#include "MessageRegister.h"
#include "protobuf/HMessage.pb.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "protobuf/LoginLoginMessage.pb.h"
#include "net/MessageDefine.h"
#include "net/Dispatcher.h"
#include "RoomServerPoolManager.h"
#include "MatchRoom.h"
#include "LoginSp.h"
#include "common/bimap.h"
#include "common/Logging.h"
#include "FriendManager.h"
#include "NotifHandler.h"
#include "common/ServerList.h"
#include "store/Store.h"

using namespace std;
using namespace codeproject::bimap_detail;
	

namespace fsk{
namespace net{


class LoginServer:public ProtobufServer{
public:
    
    LoginServer(string name, EventLoop* loop, const InetAddress& listenAddr)
			:ProtobufServer(name, loop, listenAddr),
			msgRegister_(this),
			roomPool_( new RoomServerPoolManager(this)),
			matchRoom_(new MatchRoom(this, roomPool_)),
			loginsp_(new LoginSp(this)),
			loginPool_(new LoginServerPoolManager(this)),
			friendManager_(new FriendManager(this, loginPool_)),
			notifHandler_(new NotifHandler(this)),
			serverList_(new ServerList()),
			store_(new Store())
			
	{
		using namespace std::placeholders;
		red_ = RedConnection::create();
		UserManager::getInstance().setupRed(red_);
		UserManager::getInstance().registerCacheByIdNorm(bind(&LoginServer::isUserBelongToHost, this, placeholders::_1));
		dispatchers_[pbmsg::H_Message] = ProtobufDispatcherBase::ptr_t(new 
					ProtobufDispatcher<pbmsg::login::HMessage>(this, 
						bind(&ProtobufServer::onUnkownMessage, this, 
						placeholders::_1, placeholders::_2, placeholders::_3)));	
		dispatchers_[pbmsg::LoginRoom_Message] = ProtobufDispatcherBase::ptr_t(new 
					ProtobufDispatcher<pbmsg::loginroom::LoginRoomMessage>(this, 
						bind(&ProtobufServer::onUnkownMessage, this, 
						placeholders::_1, placeholders::_2, placeholders::_3)));	
		dispatchers_[pbmsg::LoginLogin_Message] = ProtobufDispatcherBase::ptr_t(new 
					ProtobufDispatcher<pbmsg::loginlogin::LoginLoginMessage>(this, 
						bind(&ProtobufServer::onUnkownMessage, this, 
						placeholders::_1, placeholders::_2, placeholders::_3)));	
		roomPool_->registClientConnectCompleteCallback(bind(&LoginServer::clientConnectCompleteCallback, this, _1, _2));

		msgRegister_.registerMessages();
		friendManager_->init();

		serverList_->loadAllList();
		if(!serverList_->getLoginSidByName(name, sid_)){
			LOG_ERROR <<"no sid with name:" + name;
		}
	}

	void start(){
		ProtobufServer::start();
		loginsp_->start();

	}

	RedConnection::ptr_t getRedConnection(){
		return red_;
	}

	NotifHandlerPtr getNotifHandler(){
		return notifHandler_;
	}

	FriendManagerPtr getFriendManager(){
		return friendManager_;
	}

	bool isUserBelongToHost(int32_t uid){
		int32_t uIndex = friendManager_->getFriendServerIndex(uid);
		int32_t sIndex = loginPool_->getHostIndex();
		if(uIndex == sIndex){
			return true;
		}
		return false;
	}

	void clientConnectCompleteCallback(int32_t sid, const TcpConnectionPtr& conn){
		shared_ptr<pbmsg::loginroom::LoginRoomMessage> msg(new pbmsg::loginroom::LoginRoomMessage());
		msg->set_msgtype(pbmsg::Connect_Request);
		pbmsg::loginroom::Request* req = msg->mutable_request();
		ConnectRequest* connReq = req->mutable_connectrequest();
		connReq->set_sid(sid);
		codec_->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));
	}


private:

    RedConnection::ptr_t red_;
    MessageRegister msgRegister_;
	shared_ptr<LoginSp> loginsp_;

public:
	shared_ptr<RoomServerPoolManager> roomPool_;
	shared_ptr<MatchRoom> matchRoom_;
	
	LoginServerPoolManagerPtr loginPool_;
	FriendManagerPtr friendManager_;
	NotifHandlerPtr notifHandler_;
	ServerListPtr serverList_;
	StorePtr store_;
};
}
}
