#include"CSBasicBuyRequestProcess.h"
#include"user/UserManager.h"
#include"database/tnt_deploy_store.pb.h"
#include"store/Store.h"

using namespace localSave;
using namespace pbmsg::login;

void CSBasicBuyRequestProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	
	if(!message->has_request() || !message->request().has_basicbuy()){
		LOG_ERROR <<"no request or no notif list";
		return;
	}	

	login::CSBasicBuyRequest req = message->request().basicbuy();
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
	
	int32_t uid;
	if(server->getUserIdByConnection(conn, uid) != 0){
		LOG_ERROR <<"get user id failed:"<<uid;
		return ;
	}

	pbmsg::Ret csRet = Ret::Failed;

	//STOREPtr store = NULL;
	int32_t itemId = 0;
	PLATFORM_TYPE platformType = PLATFORM_TYPE_INVALID;
	string platformUid = "";

	do{
		if(!req.has_itemid() || /*!req.has_platformtype() ||*/ !req.has_receipt()){
			LOG_ERROR <<"buy request is incomplete";
			break;
		}

		Receipt receipt;

		itemId = req.itemid();
		platformType = req.platformtype();
		//platformUid = req.platformuserid();
		//receipt = req.receipt();

		receipt.platformType = platformType;

		if(platformType == PLATFORM_TYPE_ANDROID){
			if(!req.has_payload() || req.payload().length() <= 0){
				LOG_ERROR <<"android platform not has payload, uid:"<<uid;
				return;
			}
			if(!req.has_signature() || req.signature().length() <= 0){
				LOG_ERROR <<"android no signature, uid:"<<uid;
				return;
			}
			receipt.payLoad = req.payload();
			receipt.signature = req.signature();
		}


		if(!loginServer->store_->buy(uid, itemId, receipt)){
			LOG_ERROR <<"buy failed";
			break;
		}
		csRet = Ret::Success;
	}while(0);

	shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::CSBasicBuy_Response); 
    msg->set_seq(message->seq());        
    login::Response *response = msg->mutable_response();
	login::CSBasicBuyResponse* buyRsp = response->mutable_basicbuy();
	buyRsp->set_ret(csRet);

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));


	LOG_INFO << "user:"<<uid<<"buy item:";
}














