#include "LoginServerPoolManager.h"
#include "common/CfgReader.h"
#include "net/InetAddress.h"
#include "google/protobuf/message.h"
#include "LoginServer.h"


using namespace google::protobuf;
using namespace fsk;
using namespace fsk::net;

const string LoginServerPoolManager::cfgFileName_ = "roomCfg.xml";

int32_t LoginServerPoolManager::StartFromCfg(){

	CfgReader reader(cfgFileName_);
	reader.loadFile();
	reader.locateNode(2, "list", "login");

	do{
		string name;
	    int32_t ret = reader.readSubString("name", name);
		if(ret != 0){
			LOG_ERROR << "readString error";
			return -1;
		}

		int32_t sid;
		ret = reader.readSubInt32("id", sid);
		if(ret != 0){
			LOG_ERROR << "read id failed ";
			return -1;
		}
		
		string ip;
		ret = reader.readSubString("ip", ip);
		if(ret != 0){
			LOG_ERROR <<"read string failed";
			return -1;
		}
		
		int32_t port;
		ret = reader.readSubInt32("port", port);
		if(ret != 0){
			LOG_ERROR <<"read port failed";
			return -1;
		}
		
		startNewConnection(sid, name, ip, port);

	}while(reader.nextSibling("login"));	

	connectionNum_ = serverIdBySort_.size();

	return 0;
}
