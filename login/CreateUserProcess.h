#ifndef KUN_SERVER_CREATEUSER_PROCESS
#define KUN_SERVER_CREATEUSER_PROCESS
#include "net/CommonInc.h"
#include "net/ProcessBase.h"
#include "net/ProtobufServer.h"
#include "user/UserManager.h"
#include "protobuf/HMessage.pb.h"
#include "common/TmpDefine.h"
#include "net/TcpConnection.h"
#include "hredis/RedConnection.h"

using namespace fsk;
using namespace fsk::net;

class CreateUserProcess : public ProcessBase{

public:
	typedef shared_ptr<google::protobuf::Message> MessagePtr;

	CreateUserProcess():ProcessBase(pbmsg::Login_Request){
	}


	void callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& message, Timestamp);
	string generateToken();
};


#endif
