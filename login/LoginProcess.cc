#include"LoginProcess.h"
#include"database/UserBasicData.pb.h"

using namespace localSave;

void LoginProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){
	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	pbmsg::login::LoginRequest req = message->request().login();
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}

	
	pbmsg::Ret csRet = pbmsg::Ret::Error;

	int64_t uid = req.uid();
	int32_t id = req.id();
   	string token = req.token();
	string password = req.password();
	
	UserInfoPtr userInfo = UserManager::getInstance().getById(id);

	do{
		if(userInfo.get() == NULL){
			LOG_ERROR << "not created id:" <<id;
			csRet = pbmsg::Ret::NotExist;
			break;
			//fixme;
		}	

		if(userInfo->basicData_->token() == token || userInfo->basicData_->token() == password){
			csRet = pbmsg::Ret::Success;	
		}else{
			csRet = pbmsg::Ret::PasswordFailed;
			break;
		}
		userInfo->login();
		loginServer->updateUserConnection(id, conn);

	}while(0);
	//UserManager::getInstance().updateConnectionById(req.id(), conn);

    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::Login_Response); 
    msg->set_seq(message->seq());        
    pbmsg::login::Response *response = msg->mutable_response();
	pbmsg::login::LoginResponse* loginResponse = response->mutable_login();
	loginResponse->set_ret (csRet);

	if(csRet == pbmsg::Ret::Success){
		pbmsg::common::UserBasicInfo *info = loginResponse->mutable_info();
		info->set_uid(uid);
		info->set_photo(userInfo->basicData_->photo());
		info->set_name(userInfo->basicData_->name());
		info->set_lv(userInfo->basicData_->lv());
		info->set_gold(userInfo->basicData_->gold());
		info->set_language(userInfo->basicData_->language());
		info->set_rmads(userInfo->basicData_->rmads());

		if(userInfo->notifData_.get() == NULL){
			LOG_ERROR <<"user pack data is null";
		}else{
			if(userInfo->notifData_->items_size() > 0){
				loginResponse->set_hasnotif(userInfo->notifData_->items_size());
			}else{
				loginResponse->set_hasnotif(0);
			}
		}
	}

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	LOG_INFO << "login process end";
}
