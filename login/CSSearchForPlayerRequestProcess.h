#pragma once
#include "net/CommonInc.h"
#include "net/ProcessBase.h"
#include "user/UserManager.h"
#include "protobuf/HMessage.pb.h"
#include "common/TmpDefine.h"
#include "net/TcpConnection.h"
#include <string>
#include "LoginServer.h"

using namespace std;
using namespace fsk::net;

class CSSearchForPlayerRequestProcess : public ProcessBase{

public:

	CSSearchForPlayerRequestProcess():ProcessBase(pbmsg::CSSearchForPlayer_Request){
	}


	void callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp);
};





