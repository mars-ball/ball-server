#include"CSGetNotifListRequestProcess.h"
#include"user/UserManager.h"
#include"database/UserNotifData.pb.h"

using namespace localSave;
using namespace login;

void CSGetNotifListRequestProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	
	if(!message->has_request() || !message->request().has_notiflist()){
		LOG_ERROR <<"no request or no notif list";
		return;
	}	

	login::CSGetNotifListRequest req = message->request().notiflist();
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
	int32_t uid;
	if(server->getUserIdByConnection(conn, uid) != 0){
		LOG_ERROR <<"get user id failed:"<<uid;
		return ;
	}

	pbmsg::Ret csRet = Ret::Failed;
	UserNotifDataPtr notifData(NULL);
	do{

		notifData = UserManager::getInstance().getNotifDataById(uid);
		if(notifData.get() == NULL){
			LOG_ERROR <<"notifData null:"<<uid;
			break;
		}

		csRet = Ret::Success;
	}while(0);

    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::Login_Response); 
    msg->set_seq(message->seq());        
    login::Response *response = msg->mutable_response();
	login::CSGetNotifListResponse* notifRsp = response->mutable_notiflist();
	notifRsp->set_ret (csRet);

	if(csRet == Ret::Success && notifData.get() != NULL){
		for(int ii = 0; ii < notifData->items_size(); ii++){
			CSNotifItem* csItem = notifRsp->add_items();
			const UserNotifDataItem& item = notifData->items(ii);
			csItem->set_seqno(item.seqno());
			if(item.has_addfriend()){
				CSNotifAddFriend *addFriend = csItem->mutable_addfriend();
				CSFriendInfo *fInfo = addFriend->mutable_info();

				int friendId = item.addfriend().followerid();
				UserInfoPtr info = UserManager::getInstance().getById(friendId);
				if(info.get() == NULL){
					LOG_ERROR<<"no user info, uid:"<<friendId;
					return;
				}
				FriendManager::fillFriendByUserInfo(fInfo, info);
			}
			if(item.has_addfriendsuccess()){
				CSNotifAddFriendSuccess *addFriend = csItem->mutable_addfriendsuccess();
				CSFriendInfo *fInfo = addFriend->mutable_info();

				int friendId = item.addfriendsuccess().toid();
				UserInfoPtr info = UserManager::getInstance().getById(friendId);
				if(info.get() == NULL){
					LOG_ERROR<<"no user info, uid:"<<friendId;
					return;
				}
				FriendManager::fillFriendByUserInfo(fInfo, info);
			}
			

		}

	}

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	LOG_INFO << "search process end";
}














