#include "FriendManager.h"
#include "protobuf/LoginLoginMessage.pb.h"
#include <memory>
#include "net/ProtobufServer.h"
#include "LoginServer.h"
#include "user/UserManager.h"
#include "database/UserBasicData.pb.h"

using namespace std;
using namespace fsk;
using namespace fsk::net;
using namespace google::protobuf;
using namespace pbmsg::loginlogin;
using namespace localSave;

void FriendManager::init(){
	using namespace std::placeholders;
	server_->registerMessageCallback(pbmsg::LoginLogin_Message, pbmsg::SSAddFriendSuccess_Response, bind(&FriendManager::SSAddFriendCallback, this, _1, _2, _3, _4 ));
	server_->registerMessageCallback(pbmsg::LoginLogin_Message, pbmsg::SSFollowBattle_Request, bind(&FriendManager::SSFollowInstanceBattleRequestCallback, this, _1, _2, _3, _4 ));
	server_->registerMessageCallback(pbmsg::LoginLogin_Message, pbmsg::SSFollowBattle_Response, bind(&FriendManager::SSFollowInstanceBattleResponseCallback, this, _1, _2, _3, _4 ));
	
	loginPool_->StartFromCfg();
}

//-1 if error
int32_t FriendManager::getFriendServerId(int32_t uid){
	int32_t snum = loginPool_->connectionNum_;
	int32_t sidx = uid%snum;
	return loginPool_->getServerIdByIndex(sidx);
}

int32_t FriendManager::getFriendServerIndex(int32_t uid){
	int32_t snum = loginPool_->connectionNum_;
	int32_t sidx = uid%snum;
	return sidx;
}

void FriendManager::sendByUid(int32_t uid, MessagePtr msg){
	int32_t snum = loginPool_->connectionNum_;
	int32_t sidx = uid%snum;
	loginPool_->sendByServerIndex(sidx, msg);
}

void FriendManager::addFriendRequest(int32_t fromId, int32_t toId, int32_t seq){

	shared_ptr<LoginLoginMessage> msg(new LoginLoginMessage());
	msg->set_msgtype(pbmsg::SSAddFriend_Request);
	msg->set_seq(seq);
	pbmsg::loginlogin::Request* req = msg->mutable_request();
	SSAddFriendRequest* add = req->mutable_addfriend();
	add->set_fromuid(fromId);
	add->set_touid(toId);

	sendByUid(toId, msg);

}

bool FriendManager::addFriendRequestHandler(int32_t fromId, int32_t toId){

	UserFriendDataPtr friendData = UserManager::getInstance().getFriendDataById(toId);

	if(friendData.get() == NULL){
		LOG_ERROR<<"get user friend data failed";
		return false;
	}

	for(int32_t ii = 0; ii < friendData->friend__size(); ii++){
		int32_t uid = friendData->friend_(ii);
		if(fromId == uid){
			LOG_ERROR<<"already has friend, from:"<<fromId <<" to:"<<toId;//fixme
			return false;
		}
	}

	//if(server_->isUserOnline(toId)){
		//sendAddFriendRequestNotif(fromId, toId);
	//}else{
		addNotifOfAddFriend(fromId, toId);
	//}
    return 0;
}

bool FriendManager::addFriendSuccessHandler(int32_t fromId, int32_t toId){

	//if(server_->isUserOnline(toId)){
		//sendAddFriendSuccessNotif(fromId, toId);
	//}else{
		makeFriend(fromId, toId);

		addNotifOfAddFriendSuccess(fromId, toId);
	//}
    return 0;
}



void FriendManager::sendAddFriendRequestNotif(int32_t fromId, int32_t toId){

}

void FriendManager::addNotifOfAddFriend(int32_t fromId, int32_t toId){
	UserNotifDataPtr notifData = UserManager::getInstance().getNotifDataById(toId);
	if(notifData.get() == NULL){
		LOG_ERROR <<"get notif data failed";
		return;
	}

	for(int ii = 0; ii < notifData->items_size(); ii++){
		const UserNotifDataItem& item = notifData->items(ii);
		if(item.has_addfriend() && item.addfriend().followerid() == fromId){
			LOG_ERROR <<" add friend is resent, from"<<fromId<<", "<<toId;
			return;
		}
	}
	UserNotifDataItem* item = notifData->add_items();
	item->set_seqno(notifData->seqnomax());
	notifData->set_seqnomax(notifData->seqnomax() + 1);
	UserNotifAddFriend* newNotif = item->mutable_addfriend();
	newNotif->set_followerid(fromId);
	
}

bool FriendManager::addNotifOfAddFriendSuccess(int32_t fromId, int32_t toId){
	UserNotifDataPtr notifData = UserManager::getInstance().getNotifDataById(toId);
	if(notifData.get() == NULL){
		LOG_ERROR <<"get notif data failed";
		return false;
	}

	for(int ii = 0; ii < notifData->items_size(); ii++){
		const UserNotifDataItem& item = notifData->items(ii);
		if(item.has_addfriendsuccess() && item.addfriendsuccess().toid() == toId){
			LOG_ERROR <<" add friend success is resent, from"<<fromId<<", "<<toId;
			return false;
		}
	}

	UserNotifDataItem* item = notifData->add_items();
	UserNotifAddFriendSuccess* newNotif = item->mutable_addfriendsuccess();
	newNotif->set_toid(toId);
	return true;
}



void FriendManager::SSAddFriendCallback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp timestamp){
	
	shared_ptr<LoginLoginMessage> message = 
			std::static_pointer_cast<LoginLoginMessage>(_message);
	SSAddFriendResponse ssrsp = message->response().addfriend();
	
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}

	Ret ssRet = ssrsp.ret();
	pbmsg::Ret csRet;

	int32_t fromId;
	int32_t toId;
	if(ssRet == Ret::Success){
		csRet = Ret::Success;
		fromId  = ssrsp.fromuid();
		toId = ssrsp.touid();

		makeFriend(fromId, toId);
		makeFriend(toId, fromId);

	}else if(ssRet == Ret::Error){
		//fixme
		csRet = Ret::Error;
	}

	sendAddFriendSuccessNotif(fromId, toId, message->seq());
}

void FriendManager::sendAddFriendSuccessNotif(int32_t uid, int32_t toId, int32_t seq){
    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
   	msg->set_msgtype(pbmsg::CSAddFriendSuccess_Notif);
	msg->set_seq(seq);        
    pbmsg::login::Notification *notif = msg->mutable_notification();
    pbmsg::login::CSAddFriendSuccessNotif *addNotif = notif->mutable_addfriendsuccess();


	UserInfoPtr info = UserManager::getInstance().getById(toId);
	if(info.get() == NULL){
		LOG_ERROR<<"no user info, uid:"<<toId;
		return;
	}
	CSFriendInfo* friendInfo = addNotif->add_friends();
	fillFriendByUserInfo(friendInfo, info);

	server_->sendMessageById(uid, std::static_pointer_cast<google::protobuf::Message>(msg));
}

void FriendManager::fillFriendByUserInfo(CSFriendInfo* friendInfo, UserInfoPtr userInfo){
	friendInfo->set_uid(userInfo->uid_);
	friendInfo->set_photo(userInfo->basicData_->photo());
	friendInfo->set_name(userInfo->basicData_->name());
	friendInfo->set_succnum(userInfo->basicData_->succnum());
	friendInfo->set_lv(userInfo->basicData_->lv());
	friendInfo->set_exp(userInfo->basicData_->exp());
	friendInfo->set_srctype(userInfo->basicData_->srctype());
	friendInfo->set_srcname(userInfo->basicData_->srcname());
}



void FriendManager::makeFriend(int32_t fromId, int32_t toId){
	UserFriendDataPtr friendData = UserManager::getInstance().getFriendDataById(fromId);

	if(friendData.get() == NULL){
		LOG_ERROR<<"get user friend data failed";
		return;
	}

	for(int32_t ii = 0; ii < friendData->friend__size(); ii++){
		int32_t uid = friendData->friend_(ii);
		if(toId == uid){
			LOG_ERROR<<"already has friend, from:"<<fromId <<" to:"<<toId;//fixme
			return;
		}
	}
	friendData->add_friend_(toId);
}



void FriendManager::sendFollowInstanceBattleRequest(int32_t fromId, int32_t toId, int32_t index = 0){
	shared_ptr<LoginLoginMessage> msg(new LoginLoginMessage());
	msg->set_msgtype(pbmsg::SSFollowBattle_Request);
	msg->set_seq(index);
	pbmsg::loginlogin::Request* req = msg->mutable_request();
	SSFollowInstantBattleRequest* add = req->mutable_followbattle();
	add->set_fromuid(fromId);
	add->set_touid(toId);

	sendByUid(toId, msg);

}

void FriendManager::SSFollowInstanceBattleRequestCallback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp timestamp){
	shared_ptr<LoginLoginMessage> message = 
			std::static_pointer_cast<LoginLoginMessage>(_message);
	SSFollowInstantBattleRequest ssrsp = message->request().followbattle();
	
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}

	int32_t fromId = ssrsp.fromuid();
	int32_t toId = ssrsp.touid();

	UserInfoPtr info = UserManager::getInstance().getById(toId);
	if(info.get() == NULL){
		LOG_ERROR<<"no user info, uid:"<<toId;
		return;
	}

	Ret ssRet = Ret::Failed;
	int32_t roomId;
	int32_t campId;
	string roomIp;
	int32_t roomPort;

	do{

		if(info->isLogin_ && info->isInRoom_){
			roomId = info->roomId_;
			campId = info->campId_;
		}

		ssRet = Ret::Success;
	}while(0);

    shared_ptr<LoginLoginMessage> msg(new LoginLoginMessage());
   	msg->set_msgtype(SSFollowBattle_Response);
	msg->set_seq(message->seq());
    pbmsg::loginlogin::Response *response = msg->mutable_response();
    response->set_ret(ssRet);
    SSFollowInstantBattleResponse *rsp = response->mutable_followbattle();
	rsp->set_ret(ssRet);
	rsp->set_fromuid(fromId);
	rsp->set_touid(toId); 
    if(ssRet == Ret::Success){
    	rsp->set_roomid(roomId);
    	rsp->set_campid(campId);
    }

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));


}

/*
void FriendManager::SSFollowInstanceBattleRequestCallback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp timestamp){
	shared_ptr<LoginLoginMessage> message = 
			std::static_pointer_cast<LoginLoginMessage>(_message);
	SSFollowInstantBattleRequest ssrsp = message->request().followbattle();
	
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}

	int32_t fromId = ssrsp->fromid();
	int32_t toId = ssrsp->toid();

	UserInfoPtr info = UserManager::getInstance().getById(toId);
	if(info.get() == NULL){
		LOG_ERROR<<"no user info, uid:"<<toId;
		return;
	}

	Ret ssRet = Ret::Failed;
	int32_t roomId;
	int32_t campId;

	if(info->isLogin_ && info->isInRoom_){
		roomId = info->roomId_;
		campId = info->campId_;
	}

    shared_ptr<LoginLoginMessage> msg(new LoginLoginMessage());
   	msg->set_msgtype(SSFollowBattle_Response);
	msg->set_seq(message->seq());
    Response *response = msg->mutable_response();
    response->set_ret(ssRet);
    SSFollowInstantBattleResponse *rsp = response->mutable_followbattle();
	rsp->set_ret(ssRet);
	rsp->set_fromuid(fromId);
	rsp->set_touid(toId); 
    if(ssRet == Ret::Success){   	
    	rsp->set_roomid(roomId);
    	rsp->set_campId(campId);
    }

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));


}*/


void FriendManager::SSFollowInstanceBattleResponseCallback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp timestamp){
	shared_ptr<LoginLoginMessage> message = 
			std::static_pointer_cast<LoginLoginMessage>(_message);
	SSFollowInstantBattleResponse ssrsp = message->response().followbattle();
	
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}

	int32_t fromId = ssrsp.fromuid();
	int32_t toId = ssrsp.touid();

    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
   	msg->set_msgtype(pbmsg::CSFollowFriendInstantBattle_Response);
	msg->set_seq(message->seq());        
    pbmsg::login::Response *response = msg->mutable_response();
    pbmsg::login::CSFollowFriendInstantBattleResponse *followBattle = response->mutable_followfriendinstantbattle();

    Ret csRet = Ret::Failed;

	int32_t roomId;
	int32_t campId;

    do{

    	if(ssrsp.ret() == Ret::Failed){
    		break;
    	}

    	roomId = ssrsp.roomid();
		campId = ssrsp.campid();

		/*
		RoomUnitInfoPtr roomInfo;

		if(loginServer->matchRoom_->getRoomById(roomId, roomInfo) != 0){
			ssRet == Ret::Failed;
			break;
		}

		int32_t sid = info->serverId;
		SSConnectionInfoPtr sInfo = getConnectionInfoById(sid);
		if(sInfo.get() == NULL){
			LOG_ERROR <<"get connection info failed";
			break;
		}*/


		csRet = Ret::Success;
    }while(0);

	if(ssrsp.ret() == Ret::Success){

		followBattle->set_roomid(roomId);
		followBattle->set_campid(campId);
	}

    followBattle->set_ret(ssrsp.ret());


	loginServer->sendMessageById(fromId, std::static_pointer_cast<google::protobuf::Message>(msg));
}













