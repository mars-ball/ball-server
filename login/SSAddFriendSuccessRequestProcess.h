#pragma once


#include "net/ProcessBase.h"
#include "net/ProtobufServer.h"
#include "user/UserManager.h"
#include "common/TmpDefine.h"
#include "net/TcpConnection.h"
#include "protobuf/LoginLoginMessage.pb.h"

using namespace fsk;
using namespace fsk::net;

class SSAddFriendSuccessRequestProcess : public ProcessBase{

public:
	typedef shared_ptr<google::protobuf::Message> MessagePtr;

	SSAddFriendSuccessRequestProcess():ProcessBase(pbmsg::SSAddFriendSuccess_Request){
	}


	void callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& message, Timestamp);
};
