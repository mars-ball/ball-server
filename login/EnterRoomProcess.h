#pragma once

#include "net/CommonInc.h"
#include "net/ProcessBase.h"
#include "net/ProtobufServer.h"
#include "user/UserManager.h"
#include "protobuf/HMessage.pb.h"
#include "common/TmpDefine.h"
#include "net/TcpConnection.h"

using namespace fsk;
using namespace fsk::net;

class EnterRoomProcess : public ProcessBase{

public:
	typedef shared_ptr<google::protobuf::Message> MessagePtr;

	EnterRoomProcess():ProcessBase(pbmsg::Login_Request){
	}


	void callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& message, Timestamp);
};

