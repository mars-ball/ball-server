#include "LoginSp.h"
#include <functional>
#include "common/Logging.h"
#include "common/TimerId.h"
#include "LoginServer.h"
using namespace std;
using namespace fsk;



LoginSp::LoginSp(LoginServer* loginServer)
		:loginServer_(loginServer),
		updateTimer_(new TimerId())
{
}


void LoginSp::start(){
	using namespace std::placeholders;
	state_ = INIT_ROOM;
	*updateTimer_ = loginServer_->getEventLoop()->runEvery(updateTimeGap, bind(&LoginSp::update, this));//*updateTimer_ = 

}

void LoginSp::update(){
	int ret;
	switch(state_){
	case INIT_ROOM: 
		ret = initRoom();
		state_ = ret==0 ? OPEN_ROOM: ERROR; 
		break;
	case OPEN_ROOM:
		ret = openRoom();
		state_ = ret==0 ? FINISH: ERROR;
		break;
	case FINISH:
		finish();
		break;
	case ERROR:
		LOG_ERROR << "error, curr state:"<<std::to_string(state_);
	default:
		LOG_ERROR << "unkown state:" <<std::to_string(state_);	
	}

}

void LoginSp::finish(){
	loginServer_->getEventLoop()->cancel(*updateTimer_);
}

int32_t LoginSp::initRoom(){
	int32_t ret = loginServer_->roomPool_->StartFromCfg();
	if(ret != 0){
		LOG_ERROR<< "load cfg failed";
		return -1;
	}

	ret = loginServer_->matchRoom_->init();
	if(ret != 0){
		LOG_ERROR <<"login server init failed";
		return -1;
	}

	ret = loginServer_->matchRoom_->loadMatchRoom();
	if(ret != 0){
		LOG_ERROR << "load match room failed";
		return -1;
	}
	return 0;
}

int32_t LoginSp::openRoom(){
	return loginServer_->matchRoom_->getAllRoomInfoRequest();	
	return 0;
}










