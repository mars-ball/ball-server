#include"CSGetFriendListRequestProcess.h"
#include"user/UserManager.h"
#include"database/UserNotifData.pb.h"
#include "database/UserBasicData.pb.h"
#include"login/FriendManager.h"

using namespace localSave;
using namespace login;

void CSGetFriendListRequestProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	
	if(!message->has_request() || !message->request().has_getfriendlist()){
		LOG_ERROR <<"no request or no handle notif";
		return;
	}	

	login::CSGetFriendListRequest req = message->request().getfriendlist();
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
	int32_t uid;
	if(server->getUserIdByConnection(conn, uid) != 0){
		LOG_ERROR <<"get user id failed:"<<uid;
		return ;
	}

	pbmsg::Ret csRet = Ret::Failed;
	UserFriendDataPtr friendData(NULL);
	do{

		friendData = UserManager::getInstance().getFriendDataById(uid);

		if(friendData.get() == NULL){
			LOG_ERROR<<"get user friend data failed";
			return;
		}

		csRet = Ret::Success;

	}while(0);

    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::CSGetFriendList_Response); 
    msg->set_seq(message->seq());        
    login::Response *response = msg->mutable_response();
	login::CSGetFriendListResponse* addRsp = response->mutable_getfriendlist();
	//addRsp->set_ret (csRet);
	if(csRet == Ret::Success && friendData.get() != NULL){
		for(int32_t ii = 0; ii < friendData->friend__size(); ii++){
			int32_t friendId = friendData->friend_(ii);
			UserInfoPtr info = UserManager::getInstance().getById(friendId);
			if(info.get() == NULL){
				LOG_ERROR<<"no user info, uid:"<<friendId;
				return;
			}
			CSFriendInfo *newFriend = addRsp->add_friends();
			if(newFriend == NULL){
				LOG_ERROR <<"no friend alloced";
				return;
			}
			FriendManager::fillFriendByUserInfo(newFriend, info);
		}
	}

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	LOG_INFO << "search process end";
}














