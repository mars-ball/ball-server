#pragma once
#include "database/UserNotifData.pb.h"
#include "protobuf/HMessage.pb.h"
#include <memory>

namespace fsk{
	namespace net{
		class LoginServer;
	}
}

using namespace std;
using namespace fsk;
using namespace fsk::net;
using namespace localSave;
using namespace pbmsg;
using namespace pbmsg::login;


class NotifHandler{

public:
	NotifHandler(LoginServer* server)
	:loginServer(server)
	{
	}

	bool handleAcceptRequest(int32_t uid, const UserNotifDataItem& notifItem);

	bool handleNotif(int32_t uid, CSHandleNotifRequest& req);

	bool handleAcceptFriendRequest(int32_t uid, const UserNotifDataItem& notifItem);

	bool sendSSAddFriendSuccess(int32_t fromId, int32_t toId);

private:
	LoginServer* loginServer;
	


};

typedef shared_ptr<NotifHandler> NotifHandlerPtr;

