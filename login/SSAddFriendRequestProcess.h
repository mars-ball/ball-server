#pragma once


#include "net/ProcessBase.h"
#include "net/ProtobufServer.h"
#include "user/UserManager.h"
#include "common/TmpDefine.h"
#include "net/TcpConnection.h"
#include "protobuf/LoginLoginMessage.pb.h"

using namespace fsk;
using namespace fsk::net;

class SSAddFriendRequestProcess : public ProcessBase{

public:
	typedef shared_ptr<google::protobuf::Message> MessagePtr;

	SSAddFriendRequestProcess():ProcessBase(pbmsg::SSAddFriend_Request){
	}


	void callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& message, Timestamp);
};
