//
//  main.cpp
//  KunServer
//
//  Created by meryn on 15/12/6.
//  Copyright (c) 2015年 meryn. All rights reserved.
//


#include "common/Logging.h"
#include "net/InetAddress.h"
#include "login/LoginServer.h" 
#include "database/PhotoData.h"
#include "database/StoreData.h"

using namespace fsk;
using namespace fsk::net;

void foo(){
	LOG_INFO << "HAHA";
}

static const string hostCfgFileName = "serverCfg.xml";
static string hostIp;
static int32 hostPort;
static string serverName;

static int32_t loadHostCfg(){

	CfgReader reader(hostCfgFileName);
	reader.loadFile();

	if(reader.readString("name", serverName) != 0){
		LOG_ERROR<<"read server name failed";
	}

	if(reader.readInt("port", hostPort) != 0){
		LOG_ERROR<<"read server name failed";
	}

	return 0;
}

static int32_t loadTntData(){

	PhotoData::getInstance().loadData();
	StoreData::getInstance().loadData();
    return 0;
}


int main(int argc, char* argv[]){

	loadTntData();

	if(argc){
        
		loadHostCfg();

		EventLoop loop;
		uint16_t port;

		if(argc >= 1){
			port = static_cast<uint16_t>(atoi(argv[1]));
		}else{
			port = static_cast<uint16_t>(hostPort);
		}

		if(argc >= 2){
			serverName = argv[2];
		}
		
		LOG_INFO <<"start server:"<<serverName<<" on port:"<<port;

		InetAddress serverAddr(port);
		LoginServer server(serverName, &loop, serverAddr);
		server.start();

		loop.loop();

	}else{
		printf("usage: %s port\n", argv[0]);

	}
}
