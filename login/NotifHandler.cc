#include "NotifHandler.h"
#include "FriendManager.h"
#include "LoginServer.h"
#include "protobuf/LoginLoginMessage.pb.h"
#include "common/Util.h"

using namespace google::protobuf;
using namespace pbmsg::login;
using namespace pbmsg::loginlogin;


bool NotifHandler::handleNotif(int32_t uid, login::CSHandleNotifRequest& req){

	if(!req.has_notifseq()){
		return false;
	}
	int32_t notfSeq = req.notifseq();

	if(!req.has_handletype()){
		return false;
	}

	HandleNotifType handleType = req.handletype();

	UserNotifDataPtr notifData = UserManager::getInstance().getNotifDataById(uid);
	if(notifData.get() == NULL){
		LOG_ERROR<<"notif data is null";
		return false;
	}

	int notifIndex = -1;
	const UserNotifDataItem* notifItem = UserManager::getInstance().getUserNotifDataItemBySeqNo(uid, notfSeq, notifIndex);

	if(notifItem == NULL && notifIndex < 0){
		LOG_ERROR<<"notif item is null";
		return false;
	}

	if(handleType == HANDLE_NOTIF_TYPE_ACCEPT){
		if(!handleAcceptRequest(uid, *notifItem)){
			LOG_ERROR<<"handle notif failed, uid:"<<uid;
			return false;
		}
	}

	if(ProtoRemoveByIdx(notifData->mutable_items(), notifIndex)!= 0){
		LOG_ERROR<<"remove notif failed";
		return false;
	}


	return true;



}


bool NotifHandler::handleAcceptRequest(int32_t uid, const UserNotifDataItem& notifItem){
	
	if(notifItem.has_addfriend()){
		return handleAcceptFriendRequest(uid, notifItem);
	}
    return true;
}


bool NotifHandler::handleAcceptFriendRequest(int32_t uid, const UserNotifDataItem& notifItem){

	if(!notifItem.has_addfriend()){
		LOG_ERROR << "no addfriend module";
		return false;
	}

	const UserNotifAddFriend& addFriend = notifItem.addfriend();

	int32_t fromId = addFriend.followerid();
	FriendManagerPtr friendManager = loginServer->getFriendManager();
	if(friendManager.get() == NULL){
		LOG_ERROR <<"get friend manager null";
		return false;
	}

	//friendManager->makeFriend(fromId, uid);
	friendManager->makeFriend(uid, fromId);

	sendSSAddFriendSuccess(fromId, uid);

	return true;
}



bool NotifHandler::sendSSAddFriendSuccess(int32_t fromId, int32_t toId){

	FriendManagerPtr friendManager = loginServer->getFriendManager();
	if(friendManager.get() == NULL){
		LOG_ERROR <<"get friend manager null";
		return false;
	}

	shared_ptr<LoginLoginMessage> msg(new LoginLoginMessage());
	msg->set_msgtype(pbmsg::SSAddFriendSuccess_Request);
	msg->set_seq(0);
	pbmsg::loginlogin::Request* req = msg->mutable_request();
	SSAddFriendSuccessRequest* add = req->mutable_addfriendsuccess();
	add->set_fromuid(fromId);
	add->set_touid(toId);

	friendManager->sendByUid(fromId, msg);
	return true;

}

















