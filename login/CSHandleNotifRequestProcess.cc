#include"CSHandleNotifRequestProcess.h"
#include"user/UserManager.h"
#include"database/UserNotifData.pb.h"

using namespace localSave;
using namespace login;

void CSHandleNotifRequestProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	
	if(!message->has_request() || !message->request().has_handlenotif()){
		LOG_ERROR <<"no request or no handle notif";
		return;
	}	

	login::CSHandleNotifRequest req = message->request().handlenotif();
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
	int32_t uid;
	if(server->getUserIdByConnection(conn, uid) != 0){
		LOG_ERROR <<"get user id failed:"<<uid;
		return ;
	}

	pbmsg::Ret csRet = Ret::Failed;

	do{


		NotifHandlerPtr notifHandler = loginServer->getNotifHandler();
		if(notifHandler.get() == NULL){
			LOG_ERROR <<"notif handler is null";
			break;
		}

		if(!notifHandler->handleNotif(uid, req)){
			LOG_ERROR <<"handle accept friend failed";
			break;
		}

		csRet = Ret::Success;

	}while(0);

    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::CSHandleNotif_Response); 
    msg->set_seq(message->seq());        
    login::Response *response = msg->mutable_response();
	login::CSHandleNotifResponse* notifRsp = response->mutable_handlenotif();
	notifRsp->set_ret (csRet);

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	LOG_INFO << "search process end";
}














