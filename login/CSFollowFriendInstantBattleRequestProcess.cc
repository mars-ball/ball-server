#include"CSFollowFriendInstantBattleRequestProcess.h"
#include"user/UserManager.h"
#include"database/UserNotifData.pb.h"
#include"database/UserBasicData.pb.h"
#include"login/FriendManager.h"

using namespace localSave;
using namespace login;

void CSFollowFriendInstantBattleRequestProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	
	if(!message->has_request() || !message->request().has_followfriendinstantbattle()){
		LOG_ERROR <<"no request or no handle notif";
		return;
	}	

	login::CSFollowFriendInstantBattleRequest req = message->request().followfriendinstantbattle();
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
	int32_t uid;
	if(loginServer->getUserIdByConnection(conn, uid) != 0){
		LOG_ERROR <<"get user id failed:"<<uid;
		return ;
	}

	FriendManagerPtr friendManager = loginServer->getFriendManager();
	if(friendManager.get() == NULL){
		LOG_ERROR <<"get friend manager null";
		return ;
	}

	pbmsg::Ret csRet = Ret::Failed;
	UserFriendDataPtr friendData(NULL);
	do{
		if(!req.has_friendid()){
			LOG_ERROR <<"friend id empty";
			break;
		}

		int32_t friendId = req.friendid();

		friendData = UserManager::getInstance().getFriendDataById(friendId);

		if(friendData.get() == NULL){
			LOG_ERROR<<"get user friend data failed";
			break;
		}

		friendManager->sendFollowInstanceBattleRequest(uid, friendId, message->seq());

		csRet = Ret::Success;

	}while(0);


	LOG_INFO << "follow end";
}














