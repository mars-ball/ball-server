#pragma once
#include "LoginServerPoolManager.h"


using namespace fsk;
using namespace fsk::net;
namespace pbmsg{
	namespace login{
		class CSFriendInfo;
	}
}
using namespace pbmsg::login;

class FriendManager{
public:
	FriendManager(ProtobufServer* server, LoginServerPoolManagerPtr loginServerPool)
			:server_(server),
			loginPool_(loginServerPool)
	{
	}

	void init();
	void addFriendRequest(int32_t fromId, int32_t toId, int32_t seq);
	void makeFriend(int32_t fromId, int32_t toId);

	bool addFriendRequestHandler(int32_t fromId, int32_t toId);
	bool addFriendSuccessHandler(int32_t fromId, int32_t toId);

	void addNotifOfAddFriend(int32_t fromId, int32_t toId);
	bool addNotifOfAddFriendSuccess(int32_t fromId, int32_t toId);
	void sendAddFriendRequestNotif(int32_t fromId, int32_t toId);
	void sendAddFriendSuccessNotif(int32_t uid, int32_t toId, int32_t seq = 0);

	void sendFollowInstanceBattleRequest(int32_t fromId, int32_t toId, int32_t index);

	int32_t getFriendServerIndex(int32_t uid);
	int32_t getFriendServerId(int32_t uid);

	void sendByUid(int32_t uid, MessagePtr msg);

	static void fillFriendByUserInfo(CSFriendInfo* friendInfo, UserInfoPtr userInfo);

	void SSAddFriendCallback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp timestamp);
	void SSFollowInstanceBattleRequestCallback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp timestamp);
	void SSFollowInstanceBattleResponseCallback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp timestamp);

private:
	LoginServerPoolManagerPtr loginPool_;		
	ProtobufServer* server_;
	
};

typedef shared_ptr<FriendManager> FriendManagerPtr;