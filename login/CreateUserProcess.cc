#include "CreateUserProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "protobuf/HMessage.pb.h"

using namespace fsk;
using namespace fsk::net;

void CreateUserProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

		shared_ptr<pbmsg::login::HMessage> message = 
				std::static_pointer_cast<pbmsg::login::HMessage>(_message);

		if(!message->has_request() || !message->request().has_createuser()){
			LOG_ERROR << "create message is not complete";
			return;
		}

		pbmsg::login::CreateUserRequest req = message->request().createuser();
		int64_t uuid = req.uuid();
		string name = req.name();
		string token = generateToken();
		string deviceId = req.deviceid();

		int32_t uid;
		int32_t ret = UserManager::getInstance().createUser(uuid, name, uid, token, deviceId);

		if(ret != 0){
			cout << "create failed uuid:" +uuid <<endl;
		}


		server->updateUserConnection(uid, conn);
        shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
       	msg->set_msgtype(pbmsg::CreateUser_Response); 
		msg->set_seq(message->seq());        
        pbmsg::login::Response *response = msg->mutable_response();
        pbmsg::login::CreateUserResponse *rsp = response->mutable_createuser();
        rsp->set_id(uid);
        rsp->set_token(token);
        rsp->set_msg("created");

		server->sendMessageById(uid, std::static_pointer_cast<google::protobuf::Message>(msg));



}


string CreateUserProcess::generateToken(){
	int len = 10;
	char code[len];

    char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        code[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    string res(code);

    return res;

}