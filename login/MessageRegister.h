#ifndef KUN_SERVER_MESSAGEREGISTER_H
#define KUN_SERVER_MESSAGEREGISTER_H
#include "net/ProtobufServer.h"
#include "protobuf/HMessage.pb.h"
#include <functional>
#include "login/CreateUserProcess.h"
#include <vector>
#include <memory>
#include <functional>
#include "common/Singleton.h"

using namespace fsk::net;
using namespace std;
using namespace std::placeholders;
using namespace pbmsg;


class MessageRegister: Singleton<MessageRegister>{
public:
	MessageRegister(ProtobufServer* server): server_(server){
	}
	
	void registerMessages();

	template <typename T>
	void registerOneProcessMessage(int32_t topType, int msgType);
	void registerSpMessage(int32_t topType, int msgType, const CallbackT::ProtobufMessageTCallback& callback);
	void registerMessageDefines();

private:
	ProtobufServer *server_;
	vector<shared_ptr<ProcessBase>> handlers_;

};




















#endif
