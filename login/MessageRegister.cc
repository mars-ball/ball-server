#include "MessageRegister.h"
#include "net/ProtobufServer.h"
#include "protobuf/HMessage.pb.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "protobuf/LoginLoginMessage.pb.h"
#include "EnterRoomProcess.h"
#include "SSPlayerGameStatusProcess.h"

#include <functional>
#include "CreateUserProcess.h"
#include "net/Dispatcher.h"
#include "LoginProcess.h"
#include "CSAddFriendRequestProcess.h"
#include "CSFollowFriendInstantBattleRequestProcess.h"
#include "CSGetFriendListRequestProcess.h"
#include "CSGetNotifListRequestProcess.h"
#include "CSHandleNotifRequestProcess.h"
#include "CSSearchForPlayerRequestProcess.h"
#include "CSBasicBuyRequestProcess.h"
#include "CSGetUserPackRequestProcess.h"
#include "CSChangeInfoProcess.h"
#include "CSWatchAdRewardProcess.h"

#include "CSRegMailProcess.h"
#include "CSLoginMailProcess.h"

#include "SSAddFriendRequestProcess.h"
#include "SSAddFriendSuccessRequestProcess.h"

using namespace fsk::net;
using namespace std;
using namespace std::placeholders;
using namespace pbmsg::login;
using namespace pbmsg::loginroom;
using namespace pbmsg::loginlogin;

template <typename T>
void MessageRegister::registerOneProcessMessage(int32_t topType, int32_t msgType){
	shared_ptr<T> tmp(new T);
	handlers_.push_back(tmp);
	server_->registerMessageCallback(topType, msgType, bind(&T::callback, tmp.get(), _1, _2, _3, _4 ));

}

void MessageRegister::registerSpMessage(int32_t topType, int msgType, const CallbackT::ProtobufMessageTCallback& callback){
	server_->registerMessageCallback(topType, msgType, callback);
}



void MessageRegister::registerMessages(){
	registerOneProcessMessage<CreateUserProcess>(pbmsg::H_Message, pbmsg::CreateUser_Request);
	registerOneProcessMessage<LoginProcess>(pbmsg::H_Message, pbmsg::Login_Request);
	registerOneProcessMessage<EnterRoomProcess>(pbmsg::H_Message, pbmsg::RoomEnter_Request);
	registerOneProcessMessage<SSPlayerGameStatusProcess>(LoginRoom_Message, SSPlayerGameStatus_Request);

	registerOneProcessMessage<CSSearchForPlayerRequestProcess>(pbmsg::H_Message, pbmsg::CSSearchForPlayer_Request);
	registerOneProcessMessage<CSAddFriendRequestProcess>(pbmsg::H_Message, pbmsg::CSAddFriend_Request);
	registerOneProcessMessage<CSGetFriendListRequestProcess>(pbmsg::H_Message, pbmsg::CSGetFriendList_Request);
	registerOneProcessMessage<CSHandleNotifRequestProcess>(pbmsg::H_Message, pbmsg::CSHandleNotif_Request);
	registerOneProcessMessage<CSGetNotifListRequestProcess>(pbmsg::H_Message, pbmsg::CSGetNotifList_Request);
	registerOneProcessMessage<CSFollowFriendInstantBattleRequestProcess>(pbmsg::H_Message, pbmsg::CSFollowFriendInstantBattle_Request);
	registerOneProcessMessage<CSBasicBuyRequestProcess>(pbmsg::H_Message, CSBasicBuy_Request);
	registerOneProcessMessage<CSGetUserPackRequestProcess>(pbmsg::H_Message, CSGetUserPack_Request);
	registerOneProcessMessage<CSChangeInfoProcess>(pbmsg::H_Message, CSChangeInfo_Request);
	registerOneProcessMessage<CSWatchAdRewardProcess>(pbmsg::H_Message, CSWatchAdEnd_Request);
	registerOneProcessMessage<CSRegMailProcess>(pbmsg::H_Message, CSRegMail_Request);
	registerOneProcessMessage<CSLoginMailProcess>(pbmsg::H_Message, CSLoginMail_Request);

	registerOneProcessMessage<SSAddFriendRequestProcess>(LoginLogin_Message, SSAddFriend_Request);
	registerOneProcessMessage<SSAddFriendSuccessRequestProcess>(LoginLogin_Message, SSAddFriendSuccess_Request);

	

	registerMessageDefines();
}


void MessageRegister::registerMessageDefines(){
	MessageDefine::getInstance().AddIdDes(pbmsg::LoginRoom_Message, pbmsg::LoginRoom_Message, LoginRoomMessage::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::LoginRoom_Message, SSEnterRoom_Request, SSEnterRoomRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::LoginRoom_Message, SSEnterRoom_Response, SSEnterRoomResponse::descriptor());
	MessageDefine::getInstance().AddIdDes(LoginRoom_Message, SSPlayerGameStatus_Request, SSPlayerGameStatusRequest::descriptor());

	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::H_Message, HMessage::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CreateUser_Request, CreateUserRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::Login_Request, LoginRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSLoginMail_Request, CSLoginMailRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSRegMail_Request, CSRegMailRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::RoomEnter_Request, RoomEnterRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSSearchForPlayer_Request, CSSearchForPlayerRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSAddFriend_Request, CSGetFriendListRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSGetFriendList_Request, CSGetFriendListRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSHandleNotif_Request, CSHandleNotifRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSGetNotifList_Request, CSGetNotifListRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSFollowFriendInstantBattle_Request, CSFollowFriendInstantBattleRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSBasicBuy_Request, CSBasicBuyRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSGetUserPack_Request, CSGetUserPackRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSChangeInfo_Request, CSChangeInfoRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSWatchAdEnd_Request, CSWatchAdEndRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(LoginLogin_Message, LoginLogin_Message, LoginLoginMessage::descriptor());
	MessageDefine::getInstance().AddIdDes(LoginLogin_Message, SSAddFriend_Request, SSAddFriendRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(LoginLogin_Message, SSAddFriendSuccess_Request, SSAddFriendSuccessRequest::descriptor());

}
