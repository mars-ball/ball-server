#pragma once
#include<fstream>
#include "stdint.h"
#include<memory>
#include"common/Logging.h"
#include"net/ProtobufClient.h"
#include"net/ProtobufServer.h"
#include"net/ServerPoolManager.h"

#include <map>
#include "common/CfgReader.h"
#include <string>
#include "net/TcpClient.h"

using namespace std;

namespace fsk{
namespace net{


class RoomServerPoolManager:public ServerPoolManager{
public:
	static const string cfgFileName_;

	RoomServerPoolManager(ProtobufServer *server):ServerPoolManager(server){}
	int32_t StartFromCfg();
	

};


}
}

