#include "SSPlayerGameStatusProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "LoginServer.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "user/UserManager.h"
#include <vector>

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::loginroom;

void SSPlayerGameStatusProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<LoginRoomMessage> message =
			std::static_pointer_cast<LoginRoomMessage>(_message);
	if(!message->has_request()){
		LOG_ERROR <<"no request module";
		return;
	}
	
	if(!message->request().has_playerroomstatus()){
		LOG_ERROR <<"NO player room status module";
		return;
	}

	const SSPlayerGameStatusRequest req = message->request().playerroomstatus();

	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}

	int32_t uid = req.uid();
	PlayerGameStatus status = req.status();

	Ret ssRet = Ret::Failed;

	do{
		UserInfoPtr userInfo = UserManager::getInstance().getById(uid);

		if(userInfo.get() == NULL){
			LOG_ERROR << "get player failed" <<uid;
			ssRet = pbmsg::Ret::NotExist;
			//fixme;
			break;
		}

		switch(status){
			case PlayerGameStatus::GAME_STATUS_ROUND_OVER:
			case PlayerGameStatus::GAME_STATUS_GAME_OVER:
			{
				if(!req.has_score()){
					LOG_ERROR<<"no score data, uid:"<<uid;
					return ;
				}

				userInfo->scoreData_->set_id(uid);
				int32_t score = req.score();
				if(score > userInfo->scoreData_->maxscore()){
					userInfo->scoreData_->set_maxscore(score);
				}				

				break;
			}
			case GAME_STATUS_OFFLINE:
			{
				if(!req.has_campid() || !req.has_roomid() || req.numbycamp_size() <= 0){
					LOG_ERROR <<"offline packet is not complete, uid:"<<uid;
					return;
				}

				int32_t camp = req.campid();
				int32_t roomId = req.roomid();
				userInfo->setInRoom(camp, roomId, false);
				vector<int32_t> numByCamp;

				for(int32_t ii = 0; ii < req.numbycamp_size(); ii++){
					numByCamp.push_back(req.numbycamp(ii));
				}

				if(!loginServer->matchRoom_->readjust(req.roomid(), std::move(numByCamp))){
					LOG_ERROR <<"match room readjuct camp num failed";
					return;
				}

				if(UserManager::getInstance().saveAllData(uid) != 0){
					LOG_ERROR <<"save user data is failed";
					return;
				}

			}

		}
		
		ssRet = Ret::Success;
	}while(0);
/*
    shared_ptr<LoginRoomMessage> msg(new LoginRoomMessage());
   	msg->set_msgtype(SSPlayerGameStatus_Response);
	msg->set_seq(message->seq());
    Response *response = msg->mutable_response();
    response->set_ret(0);
    SSPlayerGameStatusResponse *rsp = response->mutable_playergamestatus();
    rsp->set_ret(ssRet);

	ret = loginServer->matchRoom_->sendRoomServerEnterRequest(vector<int32_t>{uid}, roomUnitInfo, camp, message->seq());

	if(ret != 0){
		LOG_ERROR <<"send ss room req failed:"<<uid;
		return;
	}
*/

}
