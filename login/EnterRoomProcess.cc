#include "EnterRoomProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "protobuf/HMessage.pb.h"
#include "LoginServer.h"
#include "MatchRoom.h"

using namespace fsk;
using namespace fsk::net;

void EnterRoomProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	pbmsg::login::RoomEnterRequest req = message->request().roomenter();
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}

	int32_t uid;
	int32_t ret = loginServer->getUserIdByConnection(conn, uid);
	if(ret != 0){
		LOG_ERROR <<"get user id from conn failed";
		return;
	}

	RoomUnitInfoPtr roomUnitInfo;
	int32_t campId;

	if(req.has_roomtypeid() && req.roomtypeid() > 0){
		int32_t roomTypeId = req.roomtypeid();
		ret = loginServer->matchRoom_->tryEnterRoomByType(uid, roomTypeId, roomUnitInfo, campId);
		if(ret != 0){
			LOG_ERROR <<"try enter room failed, uid:"<<uid<<" roomtypeid:"<<roomTypeId;
			return;
		}
	}else if(req.has_roomid() && req.roomid() > 0 && req.campid() >= 0){
		int32_t roomId = req.roomid();
		campId = req.campid();

		ret = loginServer->matchRoom_->tryEnterRoomByRoomId(uid, roomId, campId, roomUnitInfo);
		if(ret != 0){
			LOG_ERROR <<"try enter room by room uid:"<<uid<<" roomid:"<<roomId<< " campid:"<<campId;
			return;
		}
	}else{
		LOG_ERROR <<"enter room msg errror, roomtype:" <<req.roomtypeid() <<" roomid:"<<req.roomid()<<" camp:"<<req.campid();
		return;

	}

	if(roomUnitInfo.get() == NULL){
		LOG_ERROR<<"room info is empty, uid:"<<uid;
		return;
	}

	ret = loginServer->matchRoom_->sendRoomServerEnterRequest(vector<int32_t>{uid}, roomUnitInfo, campId, message->seq());

	if(ret != 0){
		LOG_ERROR <<"send ss room req failed:"<<uid;
		return;
	}

/*	pbmsg::Ret csRet;
	
	loginServer->getUserIdByConnection(conn);

	UserManager::getInstance().UpdateConnectionById(uid, conn);
    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
   	msg->set_msgtype(pbmsg::CreateUser_Response); 
	msg->set_seq(message->seq());        
    pbmsg::login::Response *response = msg->mutable_response();
    pbmsg::login::CreateUserResponse *rsp = response->mutable_createuser();
    rsp->set_id(uid);
    rsp->set_token("123456");
    rsp->set_msg("created");

	server->sendMessageById(uid, std::static_pointer_cast<google::protobuf::Message>(msg));
	*/



}
