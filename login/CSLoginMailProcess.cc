#include"CSLoginMailProcess.h"
#include"database/UserBasicData.pb.h"

using namespace localSave;

void CSLoginMailProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){
	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	
	if(message == NULL || !message->has_request() || !message->request().has_loginmail()){
		LOG_ERROR <<"no login mail";
		return;
	}

	pbmsg::login::CSLoginMailRequest req = message->request().loginmail();

	string mail = req.mail();
	string pwd = req.pwd(); 

	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}	

	int32_t uid;
	if(server->getUserIdByConnection(conn, uid) != 0){
		LOG_ERROR <<"get user id failed:"<<uid;
		return ;
	}

	pbmsg::Ret csRet = pbmsg::Ret::Error;
	
	UserInfoPtr userInfo = UserManager::getInstance().getByMail(mail);

	do{
		if(userInfo.get() == NULL || userInfo->basicData_.get() == NULL){
			LOG_ERROR << "not created id:" <<uid;
			csRet = pbmsg::Ret::NotExist;
			break;
		}	

		if(!userInfo->basicData_->has_mail()){
			LOG_ERROR<<"user:"<<uid <<" not register mail";
			csRet = pbmsg::Ret::NotExist;
			break;
		}

		if(userInfo->basicData_->pwd() != pwd){
			LOG_ERROR<<"user:"<<uid <<" password not equal oldpwd:" << userInfo->basicData_->pwd() << " pwd:" <<pwd;
			csRet = pbmsg::Ret::PasswordFailed;
			break;
		}


		csRet = pbmsg::Ret::Success;
	}while(0);

    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::CSLoginMail_Response); 
    msg->set_seq(message->seq());
    pbmsg::login::Response *response = msg->mutable_response();
	pbmsg::login::CSLoginMailResponse* loginRsp = response->mutable_loginmail();
	loginRsp->set_ret (csRet);
	loginRsp->set_uid(userInfo->uid_);
	loginRsp->set_token(userInfo->basicData_->token());

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	LOG_INFO << "login process end";
}
