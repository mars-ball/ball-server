#pragma once
#include <memory>


namespace fsk{
	class TimerId;
	namespace net{
		class LoginServer;
	}
}

using namespace std;
using namespace fsk;
using namespace fsk::net;

typedef shared_ptr<TimerId> TimerIdPtr;

class LoginSp{

public:
	enum SP{ 
		INVALID,
		INIT_ROOM,
		OPEN_ROOM,
		FINISH,
		ERROR,
		MAX
	};
	
	LoginSp(LoginServer* loginServer);

	void start();
	void update();

	void finish();
	int32_t initRoom();
	int32_t openRoom();

private:
	LoginServer* loginServer_;
	int32_t state_;
	const double updateTimeGap = 0.5;
	TimerIdPtr updateTimer_;
	
};	
