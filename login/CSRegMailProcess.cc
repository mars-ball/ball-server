#include"CSRegMailProcess.h"
#include"database/UserBasicData.pb.h"

using namespace localSave;

void CSRegMailProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){
	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	
	if(message == NULL || !message->has_request() || !message->request().has_regmail()){
		LOG_ERROR <<"no reg mail";
		return;
	}

	pbmsg::login::CSRegMailRequest req = message->request().regmail();

	string mail = req.mail();
	string pwd = req.pwd(); 

	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}	

	int32_t uid;
	if(server->getUserIdByConnection(conn, uid) != 0){
		LOG_ERROR <<"get user id failed:"<<uid;
		return ;
	}


	pbmsg::Ret csRet  = pbmsg::Ret::Error;
	
	UserInfoPtr userInfo = UserManager::getInstance().getById(uid);

	do{
		if(userInfo.get() == NULL || userInfo->basicData_.get() == NULL){
			LOG_ERROR << "not created id:" <<uid;
			csRet = pbmsg::Ret::NotExist;
			break;
			//fixme;
		}	

		if(userInfo->basicData_->has_mail()){
			LOG_ERROR<<"user:"<<uid <<" already register mail";
			csRet = pbmsg::Ret::Again;
			break;
		}

		string oldPwd = userInfo->basicData_->pwd();

		userInfo->basicData_->set_mail(mail);
		userInfo->basicData_->set_pwd(pwd);

		if(UserManager::getInstance().saveUserBasicData(userInfo)!= 0){
			LOG_ERROR<<"save basic user"<<uid<<"failed";
			csRet = pbmsg::Ret::DatabaseWriteFailed;
			userInfo->basicData_->set_pwd(oldPwd);
			userInfo->basicData_->set_mail("");
			break;
		}

		csRet = pbmsg::Ret::Success;

	}while(0);

    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::Login_Response); 
    msg->set_seq(message->seq());        
    pbmsg::login::Response *response = msg->mutable_response();
	pbmsg::login::CSRegMailResponse* regRsp = response->mutable_regmail();
	regRsp->set_ret (csRet);


	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	LOG_INFO << "login process end";
}
