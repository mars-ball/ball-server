#include"CSChangeInfoProcess.h"
#include"user/UserManager.h"

using namespace localSave;
using namespace login;

void CSChangeInfoProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	
	if(!message->has_request() || !message->request().has_changeinfo()){
		LOG_ERROR <<"no request or no notif list";
		return;
	}	

	login::CSChangeInfoRequest req = message->request().changeinfo();
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
	
	int32_t uid;
	if(server->getUserIdByConnection(conn, uid) != 0){
		LOG_ERROR <<"get user id failed:"<<uid;
		return ;
	}

	pbmsg::Ret csRet = Ret::Failed;

	do{

		UserBasicDataPtr userBasicInfo = UserManager::getInstance().getUserBasicDataById(uid);
		if(userBasicInfo.get() == NULL){
			LOG_ERROR <<"get user basic info failed";
			return;
		}

		if(req.has_name() && req.name().length() > 0){
			string name = req.name();
			userBasicInfo->set_name(name);
		}else if(req.has_photoid()){
			int photoId = req.photoid();

			UserPackDataPtr packData = UserManager::getInstance().getPackDataById(uid);
			if(packData.get() == NULL){
				LOG_ERROR <<"packData null:"<<uid;
				break;
			}
			bool check = false;
			for(int32_t ii = 0; ii < packData->items_size(); ii++){
				UserItemData item = packData->items(ii);
				if(item.has_photo()){
					UserItemPhoto itemPhoto = item.photo();
					if(itemPhoto.photoid() == photoId){
						check = true;
						break;
					}
				}
			}
			if(check){
				userBasicInfo->set_photo(photoId);
			}		
		}else if(req.has_language() && req.language() > 0 && req.language() < USER_LANGUAGE_MAX){
			userBasicInfo->set_language(req.language());

		}else{
			break;
		}

		UserInfoPtr userInfo = UserManager::getInstance().getById(uid);
		if(userInfo.get() == NULL){
			LOG_ERROR << "user info cannot be found";
			return ;
		}
		UserManager::getInstance().saveUserBasicData(userInfo);

		csRet = Ret::Success;
	}while(0);

	shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::CSChangeInfo_Response); 
    msg->set_seq(message->seq());        
    login::Response *response = msg->mutable_response();
	login::CSChangeInfoResponse* infoRsp = response->mutable_changeinfo();
	infoRsp->set_ret(csRet);

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));


	LOG_INFO << "user:"<<uid<<"change item";
}














