#include"CSWatchAdRewardProcess.h"
#include"user/UserManager.h"
#include"database/tnt_deploy_store.pb.h"
#include "common/TmpDefine.h"

using namespace localSave;
using namespace pbmsg::login;
using namespace login;
using namespace tmp;

void CSWatchAdRewardProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	
	if(!message->has_request() || !message->request().has_adend()){
		LOG_ERROR <<"no request or no notif list";
		return;
	}	

	login::CSWatchAdEndRequest req = message->request().adend();
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
	
	int32_t uid;
	if(server->getUserIdByConnection(conn, uid) != 0){
		LOG_ERROR <<"get user id failed:"<<uid;
		return ;
	}

	pbmsg::Ret csRet = Ret::Failed;

	//STOREPtr store = NULL;
	int32_t itemId = 0;


	do{
		if(!req.has_type()){
			LOG_ERROR <<"buy request is incomplete, uid"<< uid;
			break;
		}

		if(req.type() != AD_TYPE_LONG){
			LOG_ERROR <<"watch type is error, uid"<<uid <<" type:"<<req.type();
		}

		UserBasicDataPtr basicData = UserManager::getInstance().getUserBasicDataById(uid);
		if(basicData.get() == NULL){
			LOG_ERROR <<"basicData null:"<<uid;
			break;
		}

		basicData->set_gold(basicData->gold() + WATCH_AD_REWARD_GOLOD);

		csRet = Ret::Success;
	}while(0);

	shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::CSWatchAdEnd_Response); 
    msg->set_seq(message->seq());        
    login::Response *response = msg->mutable_response();
	login::CSWatchAdEndResponse* buyRsp = response->mutable_adend();
	buyRsp->set_ret(csRet);
	if(csRet == Ret::Success){
		buyRsp->set_addgold(WATCH_AD_REWARD_GOLOD);
	}else{
		buyRsp->set_addgold(0);
	}

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));


	LOG_INFO << "watch ad end uid:"<<uid;
}














