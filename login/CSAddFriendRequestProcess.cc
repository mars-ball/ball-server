#include"CSAddFriendRequestProcess.h"
#include"user/UserManager.h"
#include"database/UserNotifData.pb.h"
#include "database/UserBasicData.pb.h"
#include"login/FriendManager.h"

using namespace localSave;
using namespace login;

void CSAddFriendRequestProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	
	if(!message->has_request() || !message->request().has_addfriend()){
		LOG_ERROR <<"no request or no handle notif";
		return;
	}	

	login::CSAddFriendRequest req = message->request().addfriend();
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
	int32_t uid;
	if(server->getUserIdByConnection(conn, uid) != 0){
		LOG_ERROR <<"get user id failed:"<<uid;
		return ;
	}

	pbmsg::Ret csRet = Ret::Failed;
	do{

		int32_t friendId = req.uid();

		loginServer->getFriendManager()->addFriendRequest(uid, friendId, message->seq());

		csRet = Ret::Success;

	}while(0);

    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::CSAddFriend_Response); 
    msg->set_seq(message->seq());        
    login::Response *response = msg->mutable_response();
	login::CSAddFriendResponse* addRsp = response->mutable_addfriend();
	addRsp->set_ret (csRet);
	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	LOG_INFO << "add process end";
}














