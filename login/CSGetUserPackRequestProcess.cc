#include"CSGetUserPackRequestProcess.h"
#include"user/UserManager.h"

using namespace localSave;
using namespace login;

void CSGetUserPackRequestProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	
	if(!message->has_request() || !message->request().has_userpack()){
		LOG_ERROR <<"no request or no pack";
		return;
	}	

	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
	int32_t uid;
	if(server->getUserIdByConnection(conn, uid) != 0){
		LOG_ERROR <<"get user id failed:"<<uid;
		return ;
	}

	pbmsg::Ret csRet = Ret::Failed;
	UserPackDataPtr packData = NULL;
	UserBasicDataPtr basicData = NULL;

	do{
		  packData = UserManager::getInstance().getPackDataById(uid);
		  if(packData.get() == NULL){
		    LOG_ERROR <<"packData null:"<<uid;
		    break;
		  }

		  basicData = UserManager::getInstance().getUserBasicDataById(uid);
		  if(basicData.get() == NULL){
		  	LOG_ERROR <<"basic data null:"<<uid;
		  	break;
		  }


		csRet = Ret::Success;
	}while(0);

	shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::CSGetUserPack_Response); 
    msg->set_seq(message->seq());        
    login::Response *response = msg->mutable_response();
    response->set_ret(csRet);
	login::CSGetUserPackResponse* buyRsp = response->mutable_userpack();
	UserPackData* pack = buyRsp->mutable_userpack();
	*pack = *packData;
	buyRsp->set_gold(basicData->gold());

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));
	LOG_INFO << "user:"<<uid<<"buy item:";
}














