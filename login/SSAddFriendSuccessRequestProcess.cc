#include "SSAddFriendSuccessRequestProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "LoginServer.h"
#include "protobuf/LoginLoginMessage.pb.h"
#include "user/UserManager.h"
#include <vector>

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::loginlogin;

void SSAddFriendSuccessRequestProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<LoginLoginMessage> message =
			std::static_pointer_cast<LoginLoginMessage>(_message);
	if(!message->has_request()){
		LOG_ERROR <<"no request module";
		return;
	}
	if(!message->request().has_addfriendsuccess()){
		LOG_ERROR <<"NO add friend module";
		return;
	}

	const SSAddFriendSuccessRequest req = message->request().addfriendsuccess();

	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}

	int32_t fromUid = req.fromuid();
	int32_t toUid = req.touid();

	loginServer->friendManager_->addFriendSuccessHandler(fromUid, toUid);

/*
    shared_ptr<LoginRoomMessage> msg(new LoginRoomMessage());
   	msg->set_msgtype(SSEnterRoom_Response);
	msg->set_seq(message->seq());
    Response *response = msg->mutable_response();
    response->set_ret(0);
    SSEnterRoomResponse *rsp = response->mutable_enterroomresponse();
    rsp->set_roomid(roomId);
	rsp->set_ret(Success);

	for(auto it = idset.begin(); it != idset.end(); ++it){
		rsp->add_uids(*it);
	}


	roomServer->sendServerMessageByConnection(conn, std::static_pointer_cast<google::protobuf::Message>(msg));
*/
	


}
