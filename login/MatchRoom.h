#pragma once
#include "database/tnt_deploy_room_info.pb.h"
#include <memory>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <utility>
#include "common/Logging.h"
#include "user/UserManager.h"
#include "common/TmpDefine.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "RoomServerPoolManager.h"
#include "MessageRegister.h"


namespace fsk{
	namespace net{
		class LoginServer;
	}
}

using namespace std;
using namespace fsk;
using namespace fsk::net;

using namespace tnt_deploy;
using namespace pbmsg::loginroom;


struct RoomUnitInfo{
	int32_t roomId;
	//int32_t sid;
	int32_t roomType; 	
	/*RoomServerInfo(int camp){
		playerNumByCamp = shared_ptr<int32_t>(new int32_t[camp]{ 0 });
	}*/
	int32_t campNum;
	vector<int32_t> playerNumByCamp;
	int32_t playerNum;
	int32_t serverId;

};
typedef shared_ptr<RoomUnitInfo> RoomUnitInfoPtr;

struct EXT_ROOM_INFO {
	EXT_ROOM_INFO() {
	}
	int32_t roomNum_;
	vector<RoomUnitInfoPtr> availRooms_;	
	Timestamp last_update;
		
};


class MatchRoom {

public:
	typedef shared_ptr<EXT_ROOM_INFO> EXT_ROOM_INFO_PTR;
	typedef std::pair<ROOM_INFO, shared_ptr<EXT_ROOM_INFO>> RoomInfo;

	MatchRoom(LoginServer* server, shared_ptr<RoomServerPoolManager> roomPool);

	int32_t loadMatchRoom();

	void RoomInfoCallback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp timestamp);
	void SSEnterRoomCallback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp timestamp);

	int32_t getAllRoomInfoRequest();

	int32_t getRoomInfoRequestByType(int32_t roomType);

	int32_t tryEnterRoomByType(int32_t uid, int32_t roomTypeId, RoomUnitInfoPtr& roomUnitInfo, int32_t& camp);

	int32_t tryEnterRoomByRoomId(int32_t uid, int32_t roomId, int32_t campId, RoomUnitInfoPtr& roomInfo);

	int32_t TakeUpRoomByType(int32_t roomTypeId, vector<int32_t>&& id, RoomUnitInfoPtr& unitInfo, int32_t& camp);

	bool readjust(int32_t roomId, vector<int32_t>&& numByCamp);

	int32_t sendRoomServerEnterRequest(vector<int32_t>&& uid, const RoomUnitInfoPtr& info, int32& camp, int32_t seq);

	int32_t init();

	int32_t ResetRoomInfo();

	int32_t getRoomById(int32_t id, RoomUnitInfoPtr& info);
	int32_t getRoomInfoByType(int32_t type, RoomInfo& roomInfo);

	bool takeUpCount(vector<int32_t>& id, RoomUnitInfoPtr& info, int32_t& camp);
private:
	map<int32_t, std::pair<ROOM_INFO, EXT_ROOM_INFO_PTR> > rooms_;
	map<int32_t, RoomUnitInfoPtr> roomsById_;
	map<int32_t, int32_t> serverPlayerNum_;
	shared_ptr<RoomServerPoolManager> roomServerPool_;
	LoginServer* server_;
	
};
