#include"CSSearchForPlayerRequestProcess.h"
#include"database/UserBasicData.pb.h"

void CSSearchForPlayerRequestProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){
	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	pbmsg::login::CSSearchForPlayerRequest req = message->request().searchplayer();
	LoginServer* loginServer = dynamic_cast<LoginServer*>(server);
	if(loginServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
	pbmsg::Ret csRet = Ret::Success;
	UserInfoPtr userInfo;

	do{
		if(!req.has_uid()){
			csRet = Ret::Failed;
			break;
		}

		int32_t wantedId = req.uid();
		
		userInfo = UserManager::getInstance().getById(wantedId);

		if(userInfo.get() == NULL){
			LOG_ERROR << "not created id:" <<wantedId;
			csRet = pbmsg::Ret::NotExist;
			//fixme;
			break;
		}
		
	}while(0);



    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::Login_Response); 
    msg->set_seq(message->seq());        
    pbmsg::login::Response *response = msg->mutable_response();
	pbmsg::login::CSSearchForPlayerResponse* searchRsp = response->mutable_searchplayer();
	searchRsp->set_ret (csRet);
	if(csRet == Ret::Success){
		pbmsg::login::CSFriendInfo* boy =  searchRsp->mutable_friend_();
		boy->set_uid(userInfo->uid_);
		boy->set_name(userInfo->basicData_->name());
		boy->set_lv(userInfo->basicData_->lv());
		boy->set_exp(userInfo->basicData_->exp());
		boy->set_photo(userInfo->basicData_->photo());
		boy->set_succnum(userInfo->basicData_->succnum());
		
	}

	loginServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	LOG_INFO << "search process end";
}
