#pragma once
#include<cmath>
#include<cstdlib>
#include "common/Logging.h"
#include <limits.h>

using namespace std;
using namespace fsk;

#define EPS 0.001f


struct Vec{
    float x;
    float y;
    Vec(float _x, float _y):x(_x), y(_y){}
    Vec():x(0), y(0){}

    Vec operator * (float mul)const{
        float newX = x*mul;
        float newY = y*mul;
        return Vec(newX, newY);
    }
    
    Vec operator + (const Vec& add)const{
        return Vec(x + add.x, y + add.y);
    }
    
    Vec norm() const{
        float newX;
        float newY;
        float x2 = x*x + y*y;
        if(x2 < 0.1){
            newX = 0;
            newY = 0;
        }else{
            float r = sqrt(x2);
            newX = x/r;
            newY = y/r; 
        }
        return Vec(newX, newY);
    }

    float magnitude() const{
        return static_cast<float>(sqrt(x*x + y*y));
    }
};

struct Point{
    float x;
    float y;

    Point():x(0), y(0){}
    Point(float x_, float y_): x(x_), y(y_){}
    Point operator +(const Vec& vec) const{
        float newX;
        float newY;
        newX = x + vec.x;
        newY = y + vec.y;
        return Point(newX, newY);
    }

    Vec operator -(const Point& point){
        Vec dist(x - point.x, y - point.y);
        return dist;
    }


};


Point calcAccDistance(Point initPos, Vec initSpeed, Vec accSpeed, int64_t timeMs);
Vec calcAccSpeed(Vec initSpeed, Vec accSpeed, int64_t timeMs);


enum Direction{
	INVALID,
	LEFT = 1,
	RIGHT = 2,
	TOP = 4,
	BOTTOM = 8,
    LEFT_TOP,
    LEFT_BOTTOM,
    RIGHT_TOP,
    RIGHT_BOTTOM
};

struct Rect{
    float x_;
    float y_;
    float width_;
    float height_;
	float eps_ = 0.01;

    const static float borderGap;

    Rect(float x, float y, float width, float height)
    :x_(x),y_(y), width_(width), height_(height)
    {}
    Rect(){
    	Rect(0, 0, 0, 0);
    }
	Point center(){

		return Point{x_+width_/2, y_+height_/2};
	}

    bool isContainPoint(const Point& point, int32_t cutMask = 0){
        if(cutMask == 0){
            if(point.x <= x_ || point.x >= x_ + width_ || point.y <= y_ || point.y >= y_ + height_){
            	  return false;
            }
        }else{
            if((cutMask & Direction::LEFT) == 0 && point.x <= x_ ){
                return false;
            }
            if((cutMask & Direction::RIGHT) == 0 && point.x >= x_ + width_){
                return false;
            }
            if((cutMask & Direction::TOP) == 0 && point.y >= y_ + height_){
                return false;
            }
            if((cutMask & Direction::BOTTOM) == 0 && point.y <= y_){
                return false;
            }
        }
        
        return true; 
    }


    void cutSpeed(const Point& pos, const Vec& oldV, Vec& newV, int32_t& cutMask){
        newV = oldV;
        cutMask = 0;
        if(pos.x <= x_ + borderGap && oldV.x <= 0){
            newV.x = 0;
            cutMask += Direction::LEFT;
        }

        if(pos.y <= y_ + borderGap && oldV.y <= 0){
            newV.y = 0;
            cutMask += Direction::BOTTOM;
        }

        if(pos.x >= x_ + width_ - borderGap && oldV.x >= 0){
            newV.x = 0;
            cutMask += Direction::RIGHT;
        }

        if(pos.y >= y_ + height_ - borderGap && oldV.y >= 0){
            newV.y = 0;
            cutMask += Direction::TOP;
        }
    }


	bool cutMove(const Point& pos, const Vec& v, const float a, int64_t deltaTime, Point& borderPos, int64_t& borderDelta){ //not guarantee the pos is in rect

        float deltaS = static_cast<float>(1.0f*deltaTime/1000.0);

		Point newPos = pos + v*deltaS + v.norm() * (0.5f*a*deltaS*deltaS);
        /*
		if(isContainPoint(newPos)){
			LOG_ERROR <<"new pos is in border:"<<newPos.x <<" ,y:"<<newPos.y;
			return false;	
		}*/

		Direction dir = Direction::INVALID;
		float xx = x_ + width_;
		float yy = y_ + height_;
        float x = newPos.x;
        float y = newPos.y;

        if(x < x_ && y < y ){
            dir = Direction::LEFT_BOTTOM;
        }else if(x< x_ && y){
            dir = Direction::LEFT_TOP;
        }else if(x> xx && y < y_){
            dir = Direction::RIGHT_BOTTOM;
        }else if(x > xx && y > yy){
            dir = Direction::RIGHT_TOP;
        }else if(x < x_ && v.x < -eps_){
            dir = Direction::LEFT;
        }else if(x > xx && v.x > eps_){
            dir = Direction::RIGHT;
        }else if(y < y_ && v.y < -eps_){
            dir = Direction::BOTTOM;
        }else if(y > yy && v.y > eps_){
            dir = Direction::TOP;
        }

        if(dir == Direction::INVALID){
            
            borderDelta = deltaTime;
            borderPos = newPos;
            return true;
        }

        float tmpDelta, tmpDelta1, tmpDelta2;

        switch(dir){
        case Direction::LEFT_BOTTOM:
            tmpDelta1 = fabs(1.0*(x_ - pos.x)/v.x);
            tmpDelta2 = fabs(1.0*(y_ - pos.y)/v.y);
            tmpDelta = tmpDelta1 < tmpDelta2 ? tmpDelta1: tmpDelta2;
            break;
        case Direction::LEFT_TOP:
            tmpDelta1 = fabs(1.0*(x_ - pos.x)/v.x);
            tmpDelta2 = fabs(1.0*(yy - pos.y)/v.y);
            tmpDelta = tmpDelta1 < tmpDelta2 ? tmpDelta1: tmpDelta2;
            break;
        case Direction::RIGHT_BOTTOM:
            tmpDelta1 = fabs(1.0*(xx - pos.x)/v.x);
            tmpDelta2 = fabs(1.0*(y_ - pos.y)/v.y);
            tmpDelta = tmpDelta1 < tmpDelta2 ? tmpDelta1: tmpDelta2;
            break;
        case Direction::RIGHT_TOP:
            tmpDelta1 = fabs(1.0*(xx - pos.x)/v.x);
            tmpDelta2 = fabs(1.0*(yy - pos.y)/v.y);
            tmpDelta = tmpDelta1 < tmpDelta2 ? tmpDelta1: tmpDelta2;
            break;           
        case Direction::LEFT:
            tmpDelta = fabs(1.0*(x_ - pos.x)/v.x);
			break;
		case Direction::TOP:
			tmpDelta = fabs(1.0*(yy - pos.y)/v.y);
			break;
		case Direction::RIGHT:
			tmpDelta = fabs(1.0*(xx - pos.x)/v.x);
			break;
		case Direction::BOTTOM:
			tmpDelta1 = fabs(1.0*(y_ - pos.y)/v.y);
		  	break;
		default:
			LOG_ERROR<<"dir error:"<<dir;	
        }
		
        borderDelta = static_cast<int64_t>(tmpDelta*1000);

		if(borderDelta < 0){
			LOG_ERROR <<"delta less than 0";
			return false;
		}

	   	borderPos = pos + v*tmpDelta;
		return true;
	}

};






























