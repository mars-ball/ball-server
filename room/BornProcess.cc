#include "BornProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "RoomServer.h"
#include "protobuf/RoomMessage.pb.h"
#include "user/UserManager.h"
#include <vector>
#include "Room.h"

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::room;

void BornProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<RoomMessage> message =
			std::static_pointer_cast<RoomMessage>(_message);
	const BornRequest req = message->request().born();
	pbmsg::Ret csRet = Ret::Success;

	RoomServer * roomServer = NULL;

	do{
		roomServer = dynamic_cast<RoomServer*>(server);
		if(roomServer == NULL){
			LOG_ERROR << "cast null";
			csRet = Ret::Error;
			break;
		}

		int32_t uid_cs = req.uid();
		int32_t uid;
		int32_t ret = roomServer->getUserIdByConnection(conn, uid);
		if(ret != 0 || uid != uid_cs){
			LOG_ERROR <<"get user id failed:"<<uid_cs<<" :"<<uid;
			csRet = Ret::Error;
			break;
		}	


		RoomManagerPtr roomManager = roomServer->getRoomManager();

		if(roomManager == NULL){
			LOG_ERROR <<"room server manager null";
			csRet = Ret::Error;
			break;
		}

		if(!roomManager->playerBorn(uid)){
			LOG_ERROR <<"player born failed, id:"<<uid ;
			csRet = Ret::Error;
			break;
		}
	}while(0);
	
    shared_ptr<RoomMessage> msg(new RoomMessage());
   	msg->set_msgtype(Born_Response);
	msg->set_seq(message->seq());
    Response *response = msg->mutable_response();
    response->set_ret(csRet);
    BornResponse *rsp = response->mutable_born();
    rsp->set_ret(csRet);


	roomServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	


}
