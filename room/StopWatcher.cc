#include "StopWatcher.h"
#include "common/TmpDefine.h"
#include "AI.h"

class greater_class{
public:
    bool operator()(TimerEvent a, TimerEvent b)
    {
        return a.timeStamp_ > b.timeStamp_;
    }
};

StopWatcher::StopWatcher(Room* room):room_(room)
{
	make_heap(timers_.begin(), timers_.end(), greater_class());
}

void StopWatcher::AddEvent(TimerEvent event){
	timers_.push_back(event);
	push_heap(timers_.begin(), timers_.end(), greater_class());
}

vector<TimerEvent> StopWatcher::PopEvents(int64_t nowtime){
 vector<TimerEvent> events;
 while(timers_.size() > 0 && timers_.front().timeStamp_ < nowtime){
  pop_heap(timers_.begin(), timers_.end(), greater_class());
  events.push_back(timers_.back());
  timers_.pop_back(); 
 }

 return events;
}

void StopWatcher::handleAllEvents(int64_t now){
	vector<TimerEvent> events = PopEvents(now);
	for_each(events.begin(), events.end(), [&](const TimerEvent& ev){
		handleEvent(ev);
	});
}


void StopWatcher::handleEvent(TimerEvent event){
	
	switch(event.type_){
	case TIMER_EVENT_TYPE_AI:
		handleAIEvent(event.id, event.aiPlayer_);
		break;
	default:
		LOG_ERROR << "other events";
	}


}


void StopWatcher::handleAIEvent(int32_t id, LuaAITimerParam param){
	
	AIInfoPtr ai = room_->getAIInfoById(id);

	if(ai.get() == NULL){
		LOG_ERROR << "ai info is null, id:"<<id;
		return;
	}
	
	ai->luaTimer(param);

}
