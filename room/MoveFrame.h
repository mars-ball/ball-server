#pragma once
#include<vector>
#include<string>
#include<map>
#include"Room.h"
#include"Move.h"
#include"common/Timestamp.h"


using namespace std;
using namespace fsk;

struct MoveRuntimeInfo;
typedef shared_ptr<MoveRuntimeInfo> MoveRuntimeInfoPtr;

//struct MoveCapture;
//typedef shared_ptr<MoveCapture> MoveCapturePtr;

class Room;
typedef shared_ptr<Room> RoomPtr;


struct MoveFrame{

	MoveFrame();
	int32_t updateMoveRuntimeInfo(int32_t uid, MoveRuntimeInfoPtr moveInfo);	
	MoveRuntimeInfoPtr getMoveRuntimeInfo(int32_t uid);
	MoveUnitPtr getUnitInfo(int32_t uid, int32_t unitId);

	map<int32_t, MoveRuntimeInfoPtr> moves_;	

	int64_t startTime_;
	int64_t endTime_;
	int32_t frameIndex_;

};


typedef shared_ptr<MoveFrame> MoveFramePtr;


class MoveFrameManager{		//need a clean circle   //in a frame, pos:prev pos, oldV: new speed

public:
	MoveFrameManager(RoomPtr room);
	void init(RoomPtr room);
	int32_t updateMoveRuntimeInfo(int32_t uid, MoveRuntimeInfoPtr moveInfo);
	int32_t updateMoveRuntimeInfoForward(int32_t uid, MoveRuntimeInfoPtr moveInfo);
	int32_t updateAccMoveRuntimeForward(int32_t uid, MoveUnitPtr initUnit, int64_t time, Point initPos, Vec initSpeed, Vec accSpeed, int64_t endTime);
	int32_t updateMoveRuntimeInfo(Timestamp time, int32_t uid, MoveRuntimeInfoPtr moveInfo);
	bool isFrameFull();
	MoveFramePtr getFrameByTime(int64_t time);
	bool isIndexValid(int32_t index);
	MoveFramePtr getFrameByIndex(int32_t index);
	bool getFrameIndexbyTime(int64_t time, int32_t& index);
	int32_t getPlayerCaptureByTime(int32_t uid, int64_t timestamp, MoveRuntimeInfoPtr& cap, int64_t& deltaTime );
	MoveUnitPtr getUnitCaptureByIndex(int32_t index, int32_t uid, int32_t unitId);

	bool getUnitPosByTime(int32_t uid, int32_t unitId, int64_t timestamp, Point& pos);

	bool isTimeInRange(int64_t time);
	void addNewFrame(int64_t timestamp);
	int32_t frameCount();
	void dequeue();
	bool addMoveUnitFromIndex(int32_t uid, int32_t index, MoveUnitPtr unit);
	bool editUnitFromIndex(int32_t uid, int32_t unitId, int32_t index, function<void(MoveUnitPtr& unit)> handler);
	MoveRuntimeInfoPtr getLatestMoveRuntimeInfo(int32_t uid);
	MoveUnitPtr getLatestMoveUnit(int32_t uid, int32_t unitId);

	MoveFramePtr getCurrFrame();
	MoveRuntimeInfoPtr getCurrMoveRuntime(int32_t uid);
	int32_t getPrevIndex(int32_t index);
	int32_t getNextIndex(int32_t index);

	MoveRuntimeInfoPtr getMoveRuntimeInfo(int32_t uid);
private:
	bool isStart_;
	
	RoomPtr room_;
	vector<MoveFramePtr> frames_;
	Timestamp currStartTime_;
	int32_t reserveIndex_;
	int32_t currIndex_;

	int64_t toTime_;
	//int32_t futureIndex_;
	static const int32_t kEditForwardCount_;
	static const int32_t kFrameReserveCount_;
	static const int32_t kFrameCount_;
	//static const int64_t kFrameFutureCount_;


};
