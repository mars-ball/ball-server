#include "CSSplitProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "RoomServer.h"
#include "user/UserManager.h"
#include "RoomManager.h"
#include "Room.h"
#include "protobuf/RoomMessage.pb.h"
#include "Move.h"

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::room;


void CSSplitProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){
		shared_ptr<RoomMessage> message =
				std::static_pointer_cast<RoomMessage>(_message);
		if(!message->has_request()){
			LOG_ERROR <<"no request";
			return;
		}

		room::Request req = message->request();
		if(!req.has_split()){
			LOG_ERROR <<"no split message";
			return;
		}

		const CSSplitRequest split = req.split();
		
		RoomServer* roomServer = static_cast<RoomServer*>(server);
		if(roomServer == NULL){
			LOG_ERROR << "cast null";
			return ;
		}

		int64_t serverTime = split.servertime();
		Vec dir(split.dir().x(), split.dir().y());
		if(dir.magnitude() < 0.5){
			dir = Vec(1.0f, 0);
		}

		int32_t uid;

		int32_t ret = roomServer->getUserIdByConnection(conn, uid);
		if(ret != 0){
			LOG_ERROR<<"no id conn record:";
			return;
		}

		RoomPtr room;
		ret = roomServer->getRoomManager()->getPlayerRoom(uid, room);
		if(ret != 0){
			LOG_ERROR << "get player room failed, uid:"<<uid;
			return;
		}

		if(!room->splitPlayer(uid, dir, serverTime, message->seq())){
			LOG_ERROR <<"split player failed";
			return;
		}

		/*
        shared_ptr<RoomMessage> msg(new RoomMessage());
       	msg->set_msgtype(SSSplit_Response);
		msg->set_seq(message->seq());
        Response *response = msg->mutable_response();
        CSSplitResponse *rsp = response->split();

		if(ret){
			rsp->set_ret(Ret);
			rsp->set_roomid(roomId);
			for(auto it = uids.begin(); it != uids.end(); ++it){
				rsp->add_uids(*it);
			}
		}else{
			rsp->set_ret(Error);
			rsp->set_msg("error");
		}


		roomServer->sendServerMessageByConnection(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	*/

}
