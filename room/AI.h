#pragma once
#include "Room.h"
#include "lua/LuaAIParam.h"
#include <map>


using namespace fsk;
using namespace fsk::net;

class LuaManager;

class AIManager;

class Room;

typedef shared_ptr<LuaAIParam> LuaAIParamPtr;

class AIInfo:public BattleObjectInfo{
public:
	AIInfo(int32_t id, MapRectPtr mapRect);
	
	void luaStart();
	void luaUpdate();
	void luaTimer(LuaAITimerParam param);	
	void resetParam();


	bool isStart_;

	
public:
	LuaAIParamPtr luaParam_;

private:
	string luaFileName_;

	LuaManager* luaMgr_;


};

typedef shared_ptr<AIInfo> AIInfoPtr;





class AIManager{

public:
	AIManager(Room* room);

	int32_t size();

	AIInfoPtr addAI();
	void removeAI(int32_t uid);
	void updateAIs();

	AIInfoPtr getAIInfoById(int32_t id);



	MapRectPtr mapRect_;

private:

	map<int32_t, AIInfoPtr> ais_;

	int aiId_ = tmp::AI_ID_MIN;



	

	

};



typedef shared_ptr<AIManager> AIManagerPtr;


