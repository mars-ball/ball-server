#pragma once


#include "net/ProcessBase.h"
#include "net/ProtobufServer.h"
#include "user/UserManager.h"
#include "common/TmpDefine.h"
#include "net/TcpConnection.h"
#include "protobuf/LoginRoomMessage.pb.h"

using namespace fsk;
using namespace fsk::net;

class SSEnterRoomProcess : public ProcessBase{

public:
	typedef shared_ptr<google::protobuf::Message> MessagePtr;

	SSEnterRoomProcess():ProcessBase(pbmsg::SSEnterRoom_Request){
	}


	void callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& message, Timestamp);
};


