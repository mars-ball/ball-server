//
//  main.cpp
//  KunServer
//
//  Created by meryn on 15/12/6.
//  Copyright (c) 2015年 meryn. All rights reserved.
//


#include "common/Logging.h"
#include "protobuf/ServerType.pb.h"
#include "net/InetAddress.h"
#include <string>
#include "common/CfgReader.h"
#include <map>
#include <vector>
#include "room/RoomServer.h"
#include "database/DropData.h"
#include "database/MineData.h"
#include "common/TmpDefine.h"
#include "database/PhotoData.h"
#include "database/AINameData.h"
using namespace std;
using namespace fsk;
using namespace fsk::net;


struct ServerCfg{
	int serverId;
	string serverName;
	map<pbmsg::ServerType, vector<string>> whiteList;
};


int32_t loadInServerCfg(std::string path, ServerCfg& cfg){


		CfgReader reader(path);
        reader.loadFile();

		int32_t ret = reader.readString("name", cfg.serverName);
		if(ret!=0){
			LOG_ERROR << "read name failed";
			return -1;
		}	
		
		ret = reader.readInt("id", cfg.serverId);
		if(ret != 0){
			LOG_ERROR << "read id failed";
			return -1;
		}	

		reader.locateNode(1, "white-list");	

		do{
			string whiteIp;
			ret = reader.readSubString("ip", whiteIp);
			if(ret != 0){
				LOG_ERROR << "read ip failed";
				return -1;
			}

			string whiteType;
			ret = reader.readSubString("type", whiteType);
			if(ret != 0){
				LOG_ERROR << "read type failed";
				return -1;
			}

            if(whiteType == "login"){
				if(cfg.whiteList.find(pbmsg::LoginServerType) == cfg.whiteList.end()){
					cfg.whiteList[pbmsg::LoginServerType] = vector<string>();
				}
                cfg.whiteList[pbmsg::LoginServerType].push_back(whiteIp);
            }


		}while(reader.nextSibling());
           
        return 0;
}

static int32_t loadTntData(){

	DropData::getInstance().loadData();
	MineData::getInstance().loadData();
	PhotoData::getInstance().loadData();
	AINameData::getInstance().loadData();
    return 0;
}




int main(int argc, char* argv[]){

	LOG_INFO << tmp::DROP_FILE_NAME;

	loadTntData();

	LOG_INFO << "room is starting";
	//cout << "pid = " <<getpid();

	//MessageDefine::getInstance().Add(0, "LoginRequestMessage");
	//MessageDefine::getInstance().Add(1, "LoginResponseMessage");
	//
	
	ServerCfg cfg;
	int32_t ret = loadInServerCfg("server_cfg.xml", cfg);
   	if(ret != 0){
		LOG_ERROR << "load server cfg failed";
		return 0;
	}

	if(argc){
        
		EventLoop loop;
		uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
		InetAddress serverAddr(port);
		RoomServer server(cfg.serverName, &loop, serverAddr);
		server.setServerId(cfg.serverId);
		server.SetWhiteList(std::move(cfg.whiteList));
		server.initAll();
		server.start();
		loop.loop();
	}else{
		printf("usage: %s port\n", argv[0]);

	}
}
