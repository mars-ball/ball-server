#include "SSRoomInfoProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "RoomServer.h"
#include "RoomManager.h"
#include "Room.h"
#include <vector>

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::loginroom;

void SSRoomInfoProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

		shared_ptr<LoginRoomMessage> message =
				std::static_pointer_cast<LoginRoomMessage>(_message);
		const SSRoomInfoRequest req = message->request().roominforequest();

		RoomServer* roomServer = dynamic_cast<RoomServer*>(server);
		if(roomServer == NULL){
			LOG_ERROR << "cast null";
			return ;
		}

        int32_t roomType = req.roomtype();

		RoomManagerPtr&& manager = roomServer->getRoomManager();
		vector<RoomPtr> rooms;
		int32_t ret = manager->getAllRoomStatByType(roomType, rooms);
		if(ret != 0){
			LOG_ERROR <<"room type failed";
			return;
		}
/*
		LoginRoomMessage msg;
       	msg.set_msgtype(pbmsg::SSRoomInfo_Response);
		msg.set_seq(message->seq());
        Response *response = msg.mutable_response();
        response->set_ret(0);
*/



		shared_ptr<LoginRoomMessage> msg(new LoginRoomMessage());

       	msg->set_msgtype(pbmsg::SSRoomInfo_Response);
		msg->set_seq(message->seq());
        Response *response = msg->mutable_response();
        response->set_ret(0);        
        
        SSRoomInfoResponse *rsp = response->mutable_roominforesponse();
		rsp->set_roomtype(roomType);

		for(auto it = rooms.begin(); it!= rooms.end(); ++it){
			SSRoomInfoResponse_SSRoomInfo* info = rsp->add_infos();
			info->set_roomid((*it)->getRoomId());
			info->set_campnum((*it)->getCampNum());
			for(int32_t ii = 0; ii < (*it)->getCampNum(); ii++){
				info->add_counts((*it)->getPlayerCountByCamp(ii));
			}
		}

		roomServer->sendServerMessageByConnection(conn, std::static_pointer_cast<google::protobuf::Message>(msg));



}
