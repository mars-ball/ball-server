#pragma once
#include<algorithm>
#include<fstream>
#include"google/protobuf/message.h"
#include "database/tnt_deploy_room_info.pb.h"
#include "database/Map.pb.h"
#include "common/TmpDefine.h"
#include "common/Logging.h"

using namespace google::protobuf;
using namespace tnt_deploy;
using namespace localSave;
using namespace std;
using namespace fsk;

typedef shared_ptr<ROOM_INFO> ROOM_INFOPtr;


class RoomDataInfo{

public:
    int32_t loadRoomData();

	int32_t loadMapData(string fileName, MapData& mapData);

	int32_t getRoomInfo(int32_t roomType, ROOM_INFOPtr& roomInfo);
	int32_t getMapInfo(int32_t roomType, MapData& mapData);
	int32_t getAllType(vector<int32_t>& types);

private:
	map<int32_t, std::pair<ROOM_INFOPtr, MapData> > rooms_;


};
