#include "MoveFrame.h"
#include "Room.h"
#include "common/Logging.h"
#include "HMath.h"



using namespace std;
using namespace fsk;
using namespace fsk::net;

const int32_t MoveFrameManager::kFrameReserveCount_ = 15;
const int32_t MoveFrameManager::kFrameCount_ = 20;
const int32_t MoveFrameManager::kEditForwardCount_ = 4;

MoveFrame::MoveFrame(){
	
}

int32_t MoveFrame::updateMoveRuntimeInfo(int32_t uid, MoveRuntimeInfoPtr moveInfo){

	//MoveRuntimeInfoPtr info(new MoveRuntimeInfo(moveInfo));
	moves_[uid] = MoveRuntimeInfoPtr(new MoveRuntimeInfo(moveInfo));//info->getPtr();
	moves_[uid]->resetUnitRoot();
	//moves_[uid] = info;
	return 0;
}

MoveRuntimeInfoPtr MoveFrame::getMoveRuntimeInfo(int32_t uid){
	if(moves_.find(uid) == moves_.end()){
		LOG_ERROR <<"user not exists, uid:"<<uid;
		return NULL;
	}
	return moves_[uid];
}


MoveUnitPtr MoveFrame::getUnitInfo(int32_t uid, int32_t unitId){
	MoveRuntimeInfoPtr move = getMoveRuntimeInfo(uid);
	if(move.get() == NULL){
		LOG_ERROR <<"get move runtime null";
		return NULL;
	}

	return move->getUnitById(unitId);


}


MoveFrameManager::MoveFrameManager(RoomPtr room)
:frames_(kFrameCount_ , MoveFramePtr(new MoveFrame())),
reserveIndex_(kFrameCount_ - 1),
isStart_(false),
currIndex_(0)
{
	//frames_ = MoveFramePtr(new MoveFrame[kFrameReserveCount_], std::default_delete<int>());	
	room_ = room;
}

int32_t MoveFrameManager::updateMoveRuntimeInfo(int32_t uid, MoveRuntimeInfoPtr moveInfo){
	/*if(isFrameFull()){
		LOG_ERROR << "frame is full, old_index:"<<reserveIndex_ <<" curr_index:"<<currIndex_;
		return -1;		
	}*/

	MoveFramePtr frame = frames_[currIndex_];
	
	if(frame.get() == NULL){
		LOG_ERROR <<"frame null";
		return -1;
	}

	int32_t ret = frame->updateMoveRuntimeInfo(uid, moveInfo);	
	if(ret != 0){
		LOG_ERROR <<"update failed id:"<<uid;
		return -1;
	}


	return 0;

}


int32_t MoveFrameManager::updateMoveRuntimeInfoForward(int32_t uid, MoveRuntimeInfoPtr moveInfo){


	for(int32_t ii = currIndex_, jj = 0; ii != reserveIndex_ && jj < kEditForwardCount_; (ii = getPrevIndex(ii)), jj++){

		MoveFramePtr frame = getFrameByIndex(ii);
		
		if(frame.get() == NULL){
			LOG_ERROR <<"frame null";
			return -1;
		}

		int32_t ret = frame->updateMoveRuntimeInfo(uid, moveInfo);	
		if(ret != 0){
			LOG_ERROR <<"update failed id:"<<uid;
			return -1;
		}
		//LOG_ERROR <<" move frame forward, starttime:"<<frame->startTime_<<" ,"<<frame->endTime_;

	}


	return 0;

}

int32_t MoveFrameManager::updateAccMoveRuntimeForward(int32_t uid, MoveUnitPtr initUnit, int64_t startTime, Point initPos, Vec initSpeed, Vec accSpeed, int64_t endTime = -1){

	int32_t prevIndex;
	if(!getFrameIndexbyTime(startTime, prevIndex)){
		LOG_ERROR << "get frame index failed, uid"<<uid<<" time:"<<startTime;
		return -1;
	}

	MoveFramePtr frame = getFrameByIndex(prevIndex);
	if(frame.get() == NULL){
		LOG_ERROR <<"frame null";
		return -1;
	}

	//initUnit->pos = initPos;
	//initUnit->oldV = initSpeed;


	Vec speed = initSpeed;
	int64_t prevTime = startTime;
	Point pos = initPos;

	for(int32_t ii = prevIndex; ii != getNextIndex(currIndex_); ii = getNextIndex(ii)){
		MoveFramePtr frame = getFrameByIndex(ii);
		if(frame.get() == NULL){
			LOG_ERROR <<"frame null";
			return -1;
		}

		int64_t prevTimeGap = frame->startTime_ - prevTime;

		if(ii == prevIndex){

		}

        if(endTime > 0){
            int64_t toEndGap = endTime - prevTime;
            if(prevTimeGap > toEndGap){
                prevTimeGap = toEndGap;
            }
        }
        
		if(prevTimeGap < 0){
			prevTimeGap = 0;
		}

		MoveRuntimeInfoPtr move = frame->getMoveRuntimeInfo(uid);
		if(move.get() == NULL){
			LOG_ERROR <<"move runtime is null, uid:" <<uid;
			continue;
		}

		MoveUnitPtr unit(new MoveUnit(initUnit));
		unit->pos = calcAccDistance(pos, speed, accSpeed, prevTimeGap);
		unit->oldV = speed;

		move->updateUnit(unit);	

		pos = unit->pos;

		//LOG_ERROR<<"acc frame, pos"<<pos.x <<", "<< pos.y<<" v:"<<speed.x<<", "<<speed.y << " framestart:"<<frame->startTime_ << " prevTimestamp:" << prevTime;
	

		speed = calcAccSpeed(speed, accSpeed, prevTimeGap);
        prevTime = frame->startTime_ < startTime ? startTime: frame->startTime_;
}
    
    initUnit->currV = speed;
    initUnit->pos = pos;
    
    
    return 0;
}



int32_t MoveFrameManager::updateMoveRuntimeInfo(Timestamp time, int32_t uid, MoveRuntimeInfoPtr moveInfo){
	
	MoveFramePtr frame = getFrameByTime(time.getMsEpoch()); 

	if(frame.get() == NULL){
		LOG_ERROR <<"get frame failed";
		return 0;
	}
	
	int32_t ret = frame->updateMoveRuntimeInfo(uid, moveInfo);	
	if(ret != 0){
		LOG_ERROR <<"update failed id:"<<uid;
		return -1;
	}

	return 0;
}


bool MoveFrameManager::isFrameFull(){
	if(frameCount() >= kFrameReserveCount_){
		return true;
	}else{
		return false;
	}
}


void MoveFrameManager::init(RoomPtr room){
	room_ = room;
}

MoveFramePtr MoveFrameManager::getFrameByTime(int64_t time){

	for(int32_t ii = getNextIndex(reserveIndex_); ii != getNextIndex(currIndex_); ii = getNextIndex(ii)){
		MoveFramePtr frame = getFrameByIndex(ii);
		//LOG_ERROR <<"frame:"<<ii<<" ,starttime:"<<frame->startTime_<<" ,endTime_:"<<frame->endTime_<<" time:"<<time <<" cond:"<<(time >= frame->startTime_ && time < frame->endTime_);
	}

	for(int32_t ii = getNextIndex(reserveIndex_); ii != getNextIndex(currIndex_); ii = getNextIndex(ii)){
		MoveFramePtr frame = getFrameByIndex(ii);
		if(frame.get() == NULL){
			LOG_ERROR <<"frame is null, index:"<<ii;
			return NULL;
		}
		if(time >= frame->startTime_ && time < frame->endTime_){
				return frame;
		}
	}
	return NULL;
}	

int32_t MoveFrameManager::frameCount(){
	return (currIndex_ + kFrameCount_  - reserveIndex_)%kFrameCount_;
}

void MoveFrameManager::dequeue(){
	reserveIndex_ = (reserveIndex_ + 1)%kFrameCount_;
}


void MoveFrameManager::addNewFrame(int64_t timestamp){
	if(isFrameFull()){
		dequeue();
	}

	if(!isStart_){
		isStart_ = true;
	}else{
		MoveFramePtr currFrame = getCurrFrame();
		if(currFrame.get() ==  NULL){
			LOG_ERROR <<"curr frame is null";
			return;
		}	
		currFrame->endTime_ = timestamp;
		currIndex_ = (currIndex_ + 1)%kFrameCount_;
	}

	frames_[currIndex_] = MoveFramePtr(new MoveFrame());
	frames_[currIndex_]->startTime_ = timestamp;
	frames_[currIndex_]->endTime_ = timestamp + Room::kFrameDeltaMs;
}


int32_t MoveFrameManager::getPlayerCaptureByTime(int32_t uid, int64_t timestamp, MoveRuntimeInfoPtr& cap, int64_t& deltaTime ){

	MoveFramePtr frame = getFrameByTime(timestamp);
	if(frame.get() == NULL){ 
		LOG_ERROR <<"no such frame : "<<timestamp ;
		return -1;
	}

	if(frame->moves_.find(uid) == frame->moves_.end()){
		LOG_ERROR <<"frame not contains this player : "<<uid;
		return -1;
	}

	cap = frame->moves_[uid];
	deltaTime = timestamp - frame->startTime_;
	return 0;
}

MoveUnitPtr MoveFrameManager::getUnitCaptureByIndex(int32_t index, int32_t uid, int32_t unitId){

	if(!isIndexValid(index)){
		LOG_ERROR <<"index not valid:"<<index;
		return NULL;
	}

	MoveFramePtr frame = frames_[index];

	if(frame.get() == NULL){
		LOG_ERROR <<"frame null, index:"<<index;
		return NULL;
	}

	return frame->getUnitInfo(uid, unitId);

}

bool MoveFrameManager::isTimeInRange(int64_t time){

	if(time > toTime_){
		return false;
	}
	return true;
}

bool MoveFrameManager::getFrameIndexbyTime(int64_t time, int32_t& index){

	for(int32_t ii = getNextIndex(reserveIndex_); ii != currIndex_; ii = (ii+1)%kFrameCount_){
		if(frames_[ii]->startTime_ <= time && time <= frames_[ii]->endTime_){
				index = ii;
				return true;
		}
	}

	LOG_ERROR <<"no index at time:"<< time;
	return false;
}

bool MoveFrameManager::isIndexValid(int32_t index){

	int32_t upHalf = (index + kFrameCount_ - reserveIndex_)%kFrameCount_;
	int32_t downHalf = (currIndex_ + kFrameCount_ - index)%kFrameCount_;

	if(upHalf + downHalf != (currIndex_ + kFrameCount_ - reserveIndex_)%kFrameCount_){
		return false;
	}

	return true;
}


bool MoveFrameManager::editUnitFromIndex(int32_t uid, int32_t unitId, int32_t index, function<void(MoveUnitPtr& unit)> handler){

	if(!isIndexValid(index)){		
		LOG_ERROR <<"index not valid:"<<index;
		return false;
	}

	for(int32_t ii = index; ii != currIndex_; ii=(ii+1)%kFrameCount_){
		MoveFramePtr frame = getFrameByIndex(ii);
		if(frame.get() == NULL){
			LOG_ERROR <<"to edit frame null";
			return false;
		}
		MoveUnitPtr unit = frame->getUnitInfo(uid, unitId);
		if(unit.get() == NULL){
			LOG_ERROR <<"unit is null";
			return false;
		}

		if(handler == NULL){
			LOG_ERROR <<"unit handler null";
			return false;
		}
		handler(unit);
	}
	return true;
}





bool MoveFrameManager::addMoveUnitFromIndex(int32_t uid, int32_t index, MoveUnitPtr unit){

	if(!isIndexValid(index)){		
		LOG_ERROR <<"index not valid:"<<index;
		return false;
	}

	MoveUnitPtr preUnit = unit;

	for(int32_t ii = index; ii != currIndex_; ii=(ii+1)%kFrameCount_){
		if(ii == index){ 
			MoveFramePtr startFrame = getFrameByIndex(ii);
			if(startFrame.get() == NULL){
				LOG_ERROR <<"start frame null";
				return false;
			}

			MoveRuntimeInfoPtr cap = startFrame->getMoveRuntimeInfo(uid);
			if(cap.get() == NULL){
				LOG_ERROR <<"move cap is null, uid:"<<uid;
				return false;
			}

			cap->addUnit(unit);

			continue;
		}

		MoveFramePtr frame = getFrameByIndex(ii);
		if(frame.get() == NULL){
			LOG_ERROR <<"get frame from index failed";
			return false;
		}

		MoveRuntimeInfoPtr move = frame->getMoveRuntimeInfo(uid);
		if(move.get() == NULL){
			LOG_ERROR << "get move from frame failed, uid:"<<uid;
			return false;
		}

		if(preUnit.get() == NULL){
			LOG_ERROR <<"pre unit is null, uid:"<<uid;
			return false;
		}

		MoveUnitPtr newUnit(new MoveUnit(preUnit));
	
		int64_t timeDelta = frame->endTime_ - frame->startTime_;

		newUnit->step(timeDelta);

		move->addUnit(newUnit);
			
		preUnit = newUnit;

	}
    
    return true;
}


MoveFramePtr MoveFrameManager::getFrameByIndex(int32_t index){
	if(!isIndexValid(index)){
		LOG_ERROR <<"index not valid:"<<index;
		return NULL;
	}
	return frames_[index];
}

MoveFramePtr MoveFrameManager::getCurrFrame(){
	return frames_[currIndex_];
}

MoveRuntimeInfoPtr MoveFrameManager::getCurrMoveRuntime(int32_t uid){
	MoveFramePtr frame = getCurrFrame();
	if(frame.get() == NULL){
		LOG_ERROR <<"no curr frame";
		return NULL;
	}

	return frame->getMoveRuntimeInfo(uid);
}


MoveRuntimeInfoPtr MoveFrameManager::getLatestMoveRuntimeInfo(int32_t uid){
	int32_t index = (currIndex_ - 1 + kFrameCount_)%kFrameCount_;
	if(!isIndexValid(index)){
		LOG_ERROR <<"index error";
		return NULL;
	}

	MoveFramePtr frame = getFrameByIndex(index);
	if(frame.get() == NULL){
		LOG_ERROR <<"frame is null";
		return NULL;
	}

	MoveRuntimeInfoPtr move = frame->getMoveRuntimeInfo(uid);
	if(move.get() == NULL){
		LOG_ERROR <<"move is null";
		return NULL;
	}

	return move;
}


MoveUnitPtr MoveFrameManager::getLatestMoveUnit(int32_t uid, int32_t unitId){

	MoveRuntimeInfoPtr move = getLatestMoveRuntimeInfo(uid);
	if(move.get() == NULL){
		LOG_ERROR <<"get lastest move failed";
		return NULL;
	}

	MoveUnitPtr unit = move->getUnitById(unitId);
	return unit;

}


bool MoveFrameManager::getUnitPosByTime(int32_t uid, int32_t unitId, int64_t timestamp, Point& pos){
	/*
	int32_t index;

	if(!getFrameIndexbyTime(timestamp, index)){
		LOG_ERROR << " get index failed, timestamp:"<<timestamp;
		return false;
	}

	MoveFramePtr frame = getFrameByIndex(index);
	if(frame.get() == NULL){
		LOG_ERROR << "get frame null, index:"<<index;
		return false;
	}

	int32_t nextIndex = getNextIndex(index);
	if(!isIndexValid(nextIndex)){
		LOG_ERROR <<"next index not valid, currIndex:" << index <<" nextIndex:"<<nextIndex;
		return false;
	}

	MoveUnitPtr unit0 = getUnitCaptureByIndex(index, uid, unitId);
	if(unit0.get() == NULL){
		LOG_ERROR <<"unit0 is null, uid:"<<uid<<" unitId:"<<unitId <<" index:"<<index;
		return false;
	}

	MoveUnitPtr unit1 = getUnitCaptureByIndex(nextIndex, uid, unitId);
	if(unit1.get() == NULL){
		LOG_ERROR <<"unit0 is null, uid:"<<uid<<" unitId:"<<unitId <<" index:"<<nextIndex;
		return false;
	}

	int64_t deltaMs = timestamp - frame->startTime_;
	float takeUp = 1.0f*deltaMs/(frame->endTime_ - frame->startTime_);

	
	if(takeUp > 1.0f || takeUp < .0f){
		LOG_ERROR <<"take up time gap error, starttime:"<<frame->startTime_<<" endTime_:"<<frame->endTime_<<" timestamp:"<<timestamp<<" takeup:"<<takeUp;
		return false;
	}

	Vec dis = unit1->pos - unit0->pos;

	pos = unit0->pos + dis.norm() * takeUp;*/





	return true;


}

int32_t MoveFrameManager::getNextIndex(int32_t index){
	int newIndex;
	newIndex = (index + 1)%kFrameCount_;
	return newIndex;
}
int32_t MoveFrameManager::getPrevIndex(int32_t index){
	int newIndex;
	newIndex = (index - 1 + kFrameCount_)%kFrameCount_;
	return newIndex;
}
