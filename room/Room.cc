#include "Room.h"
#include "RoomServer.h"
#include "common/Logging.h"
#include <algorithm>
#include "database/Map.pb.h"
#include "database/DropData.h"
#include "common/Util.h"
#include "MapRect.h"
#include <set>
#include "protobuf/RoomMessage.pb.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "common/TmpDefine.h"
#include "common/Timestamp.h"
#include "user/UserManager.h"
#include "LinearMath/btVector3.h"
#include "MoveCalculate.h"
#include <limits>
#include "AI.h"
#include <cmath>


#include <iostream>
using namespace std;

using namespace pbmsg;
using namespace pbmsg::room;
using namespace pbmsg::loginroom;
using namespace fsk;
using namespace tmp;

const float Room::kFrameDelta = 0.08f;
const float Room::kInitR = 100;
const float Room::kMaxR = 500;
const float Room::kInitW = 100;
const int32_t Room::kFreshMineDelta = 10000;
int64_t Room::kFrameDeltaMs;
const float Room::kAIPercent = 0.5;

const float Room::kCanSplitMinR = 80;
//const float Room::kSplitLoseR = 50;
const int64_t Room::kSplitCombineTimeMs = 6000;
const float Room::kGameWorldScale = 0.1f;

const float Room::kShrinkPercent = 0.3f;


const int32_t Room::kRadiusSortCounter = 12;
const int32_t Room::kAIThinkCounter = 4;
const int32_t Room::kMineRefreshCounter = 60;
const int32_t Room::kAIGenerateCounter = 240;
const int32_t Room::kShrinkCounter = 4;

RunTimeRoomInfo::RunTimeRoomInfo(RoomPtr room){
	room_ = room;
}
	
int32_t RunTimeRoomInfo::tryRegisterRuntimeRoomInfo(PlayerBattleInfoPtr player){
    return 0;
}

void PlayerBattleInfo::initPlayerBattleData(){
	lv_ = 0;
	score_ = 0;
}

void PlayerBattleInfo::noUnitCallback(){

	if(move_.get() == NULL){
		LOG_ERROR<< "move module is null";
		return;
	}
	if(move_->mapRect_.get() == NULL){
		LOG_ERROR<< "map rect is null";
		return;
	}

	RoomPtr room = move_->mapRect_->room_;

	if(room.get() == NULL){
		LOG_ERROR<<"room is null";
		return;
	}

	room->playerRoundOver(id, GameOverStatus::GAMEOVER_FAILURE);
	LOG_INFO <<"player round over from no unit callback";
}


PlayerBattleInfoPtr Room::getPlayerBattleInfoBy(int32_t id){
	if(players_.count(id) < 1){
		LOG_ERROR << " no such id:"<<id;
	}
	return static_pointer_cast<PlayerBattleInfo> (players_[id]);
}

AIInfoPtr Room::getAIInfoById(int32_t id){
	
	if(!tmp::isAIId(id)){
		LOG_ERROR << "is not ai id:"<<id;
		return NULL;
	}

	return aiMgr_->getAIInfoById(id);


}

BattleObjectInfoPtr Room::getBattleObjectInfoById(int32_t id){
	if(players_.count(id) < 1){
		LOG_ERROR << "no such battle object:"<<id;
		return NULL;
	}
	return players_[id];
}


Room::Room(RoomManagerPtr roomManager, int32_t roomId, ROOM_INFOPtr roomInfo, MapData& mapData)
:roomManager_(roomManager),
roomId_(roomId),
roomInfo_(roomInfo),
 mapInfo_(mapData),
 aiMgr_(new AIManager(this)),
 stopWatcher_(new StopWatcher(this)),
 playingPlayersCount_(0),
 waitingPlayersCount_(0),
 isStart_(false)
{
	int32_t campNum = roomInfo->campnum();
	campNum_ = campNum;
	playerCountByCamp_ = std::vector<int32_t>(campNum, 0);
	waitPlayerCountByCamp_ = std::vector<int32_t>(campNum, 0);
	if(campNum == 1){
		isSingleCamp_ = true;
	}

	kFrameDeltaMs = tmp::sToMs(kFrameDelta);
	campMaxCount_ = roomInfo->maxnum();

}

void Room::init(){
	using namespace std::placeholders;

	frameManager_ = MoveFrameManagerPtr(new MoveFrameManager(shared_from_this()));

	runtimeInfo_.reset(new RunTimeRoomInfo(shared_from_this()));
	
	mapRect_ = MapRectPtr(new MapRect(roomInfo_, mapInfo_, shared_from_this()));

	MineCreateParam mineParam;
	mineParam.maxPotNum_ = mineParam.minPotNum_ = mineParam.num_ = mapInfo_.minenum();
	mineParam.genType_ = mapInfo_.minegeneratetype();
	mineParam.freshTime_ = mapInfo_.minetimedelta();

	m_gameWorld = GameWorldEnginePtr(new GameWorldEngine());
	m_gameWorld->init(mapInfo_.hlength()*kGameWorldScale, mapInfo_.vlength()*kGameWorldScale);
	m_gameWorld->setCollisionCallback(bind(&Room::gameWorldCollisionCallback, this, _1, _2, _3, _4)) ;
	m_gameWorld->setPlayerMoCallback(bind(&Room::gameWorldPlayerMoCollisionCallback, this, _1, _2, _3));

	mapRect_->initMineRes(mineParam);

	initAI();

}

void Room::initAI(){

	aiMgr_->mapRect_ = mapRect_;

	refreshBornAI();


}

void Room::refreshBornAI(){
	int32_t numByCamp = campMaxCount_* kAIPercent;
	for(int32_t ii = 0; ii < numByCamp - aiMgr_->size(); ii++){
		AIInfoPtr ai = aiMgr_->addAI();
		players_[ai->id] = ai;

		if(!playerBorn(ai->id, BATTLE_OBJECT_TYPE_AI)){
			aiMgr_->removeAI(ai->id);
			continue;
		}	
		roomManager_->playerRooms_[ai->id] = shared_from_this();

		newPlayerOnline_.push_back(ai->id);
	}
}



void Room::start(){
	//frameTimer_ = 
	EventLoop* loop = roomManager_->getEventLoop();
	frameTimer_ = loop->runEvery(kFrameDelta, bind(&Room::keyFrame, this));
	frameCounter_ = 0;	
	lastFrameTime_ = Timestamp::now().getMsEpoch();
	//frameManager_->addNewFrame(lastFrameTime_);
}

void Room::stop(){
	EventLoop* loop = roomManager_->getEventLoop();
	loop->cancel(frameTimer_);
}


void Room::processAllCombine(int32_t uid, MoveRuntimeInfoPtr moveInfo, int64_t now){

	if(moveInfo.get() == NULL){
		LOG_ERROR <<"get move by id:"<<uid;
		return;
	}

	set<int32_t> waitRemoved;
	vector<set<int32_t>> vanishSets;
	vector<set<int32_t>> blackHoleSets;

	for(auto itt = moveInfo->units_.begin(); itt!= moveInfo->units_.end(); ++itt){
		MoveUnitPtr mUnit = *itt;

		if(!mUnit->needSyncSpeed  && now > mUnit->combineTime && mUnit->besideUnits.size() > 0){
			int32_t unitId = mUnit->id;
			bool inserted = false;
			waitRemoved.insert(unitId);
			
			for(size_t ii = 0; ii < vanishSets.size(); ii++){
				if(vanishSets[ii].find(unitId) != vanishSets[ii].end() 
					|| blackHoleSets[ii].find(unitId) != blackHoleSets[ii].end()){
					vanishSets[ii].insert(unitId);
					blackHoleSets[ii].erase(unitId);
					inserted = true;
					break;
				}
			}

			if(!inserted){
				vanishSets.push_back(set<int32_t>({unitId}));
				blackHoleSets.push_back(set<int32_t>(mUnit->besideUnits.begin(), mUnit->besideUnits.end()));
			}
		}
	}


	MoveUnitPtr mainUnit = moveInfo->getMainUnit();
	if(mainUnit.get() == NULL){
		LOG_ERROR<<"main unit null, uid:"<<uid;
		return;
	}

	for(size_t ii = 0; ii < vanishSets.size(); ii++){

		if(blackHoleSets[ii].size() <= 0){
			combinePlayer(uid, mainUnit->id, vanishSets[ii]);
		}else{
			combinePlayer(uid, *(blackHoleSets[ii].begin()), vanishSets[ii]);
		}
	}

	for(auto itt = waitRemoved.begin(); itt!= waitRemoved.end(); ++itt){
		moveInfo->removeUnit(*itt);
	}

}


void Room::processAllOutOfMap(int32_t uid, MoveRuntimeInfoPtr moveInfo){

	//set<int32_t> waitRemoved;

	for(auto itt = moveInfo->units_.begin(); itt!= moveInfo->units_.end(); ++itt){
		MoveUnitPtr mUnit = *itt;
		if(!mapRect_->isUnitInMap(mUnit)){
			mUnit->revPos();
			m_gameWorld->setUnitPos(uid, mUnit->id, btVector3(mUnit->pos.x * kGameWorldScale, 0, mUnit->pos.y* kGameWorldScale));

			//m_gameWorld->removePlayer(uid, mUnit->id);
			//waitRemoved.insert(mUnit->id);
		}
	}

	/*for(auto itt = waitRemoved.begin(); itt!= waitRemoved.end(); ++itt){
		moveInfo->removeUnit(*itt);
	}

	if(moveInfo->units_.size() <= 0){
		playerRoundOver(uid, GameOverStatus::GAMEOVER_FAILURE);
	}*/
}


void Room::keyFrame(){

	//mapRect_->reset();
	int64_t now = Timestamp::now().getMsEpoch();
	frameManager_->addNewFrame(now);	

	int64_t frameDelta = now - lastFrameTime_;
	if(frameDelta <= 0){
		LOG_ERROR <<"time error";
		return;
	}

	stopWatcher_->handleAllEvents(now);


//	cout << "key frame:" <<frameDelta<<" now:"<<now<<endl;

	if(frameCounter_ % kAIThinkCounter == 0){
	
		aiMgr_->updateAIs();
	}

	if(frameCounter_ % kMineRefreshCounter == 0){
		mapRect_->updateMineRes();

	}

	if(frameCounter_ % kAIGenerateCounter == 0){
		refreshBornAI();
	}

	if(frameCounter_ % kShrinkCounter == 0){
		shrinkPlayers();
	}

	for(auto it = players_.begin(); it != players_.end();){
		if(it->second.get() == NULL){
			LOG_ERROR <<"a null player, uid:" << it->first;
			players_.erase(it++);
			continue;
		}else{
			++it;
		}
	}


	for(auto it = players_.begin(); it != players_.end(); ++it){	
		if(it->second->battleType_ == BATTLE_OBJECT_TYPE_PLAYER && getPlayerState(it->first)!= PlayerState::PLAYING){
			continue;
		}

		int32_t uid = it->first;

		MoveRuntimeInfoPtr moveInfo = getPlayerMoveInfoById(uid);
		if(moveInfo.get() == NULL){
			LOG_ERROR <<"get move by id:"<<uid;
			continue;
		}

		processAllCombine(uid, moveInfo, now);
		processAllOutOfMap(uid, moveInfo);

		MoveUnitPtr mainUnit = moveInfo->getMainUnit();
    	if(mainUnit.get() == NULL){
    			LOG_ERROR<<"main unit null";
    			continue;
    	}

    	for(auto itt = moveInfo->units_.begin(); itt!= moveInfo->units_.end(); ++itt){
    		MoveUnitPtr mUnit = *itt;    
    		mUnit->revSpeed(kFrameDelta);	
    		mUnit->besideUnits.clear();
    		if(mUnit->needSyncSpeed){
    			//std::cout <<"unit "<<mUnit->id<< " set speed:"<<mUnit->currV.x<<", "<<mUnit->currV.y<<std::endl;
				m_gameWorld->setUnitSpeed(uid, mUnit->id, btVector3(mUnit->currV.x*kGameWorldScale, 0., mUnit->currV.y*kGameWorldScale));
				mUnit->oldV = mUnit->currV;

				//OG_ERROR <<"unit " <<mUnit->id <<" is sync";
    		}else{
    			if(now > mUnit->flyExistTime){

    				if(mainUnit->id == mUnit->id){
    					mainUnit->needSyncSpeed = true;
    					mainUnit->m_state = MOVE_UNIT_STATE_NORMAL;
    					return;
    				}else{
    					mUnit->m_state = MOVE_UNIT_STATE_COMBINING;
    					//LOG_ERROR <<"timeline unitid:" << mUnit->id <<"timestamp:"<< now<<" posx:"<<mUnit->pos.x <<" posy:"<<mUnit->pos.y <<" vx"<<mUnit->currV.x<<" vy"<<mUnit->currV.y;
    				}

    				mUnit->oldV = mainUnit->currV;
    				mUnit->revSpeed(kFrameDelta);
    				btVector3 compSpeed;
    				m_gameWorld->setUnitFollow(uid, mainUnit->id, uid, mUnit->id, btVector3(mUnit->oldV.x*kGameWorldScale, 0, mUnit->oldV.y*kGameWorldScale), compSpeed);
    				mUnit->oldV.x = compSpeed[0]/kGameWorldScale;
    				mUnit->oldV.y = compSpeed[2]/kGameWorldScale;

    				//LOG_ERROR <<"unit " <<mUnit->id <<" is follow";
    			}else{
    				//mUnit->revSpeed(kFrameDelta);                    
    				m_gameWorld->setUnitSpeed(uid, mUnit->id, btVector3(mUnit->currV.x*kGameWorldScale, 0., mUnit->currV.y*kGameWorldScale));		
    				
    				//LOG_ERROR <<" vx:"<<mUnit->currV.x<<" vy:"<<mUnit->currV.y << " ovx:"<<mUnit->oldV.x << " ovy:"<<mUnit->oldV.y;

    				mUnit->oldV = mUnit->currV;
    				mUnit->currV = mUnit->oldV + mUnit->a * kFrameDelta;



    				/*
		    		btVector3 uPos;
					if(!m_gameWorld->getUnitPos(uid, mUnit->id, uPos)){
						LOG_ERROR <<"get unit failed, uid:"<<uid<<"unitId:"<<mUnit->id;
					}*/
					//LOG_ERROR <<"unitid:" << mUnit->id <<"timestamp:"<< now<<" posx:"<<uPos[0] <<" posy:"<<uPos[2] <<" vx"<<mUnit->currV.x<<" vy"<<mUnit->currV.y;
					//LOG_ERROR <<"unit " <<mUnit->id <<" is flying";
    			}
    		}
    	}

	}

	m_gameWorld->stepSimulation(btScalar(kFrameDelta), 0, kFrameDelta);

	mapRect_->reset();

	for(auto it = players_.begin(); it != players_.end();){
		if(it->second.get() == NULL){
			LOG_ERROR <<"a null player, uid:" << it->first;
			players_.erase(it++);
			continue;
		}else{
			++it;
		}
	}

	for(auto it = players_.begin(); it != players_.end(); ++it){

		if(it->second->battleType_ == BATTLE_OBJECT_TYPE_PLAYER && getPlayerState(it->first)!= PlayerState::PLAYING){
			continue;
		}

		int32_t uid = it->first;

		MoveRuntimeInfoPtr moveInfo = getPlayerMoveInfoById(uid);
		if(moveInfo.get() == NULL){
			LOG_ERROR <<"get move by id:"<<uid;
			continue;
		}

		int32_t ret = frameManager_->updateMoveRuntimeInfo(uid, moveInfo);	
		if(ret != 0){
			LOG_ERROR << "move null";
			continue;
		}

    	for(auto itt = moveInfo->units_.begin(); itt!= moveInfo->units_.end(); ++itt){
    		MoveUnitPtr mUnit = *itt;
    		btVector3 uPos;
			if(!m_gameWorld->getUnitPos(uid, mUnit->id, uPos)){
				LOG_ERROR <<"get unit failed, uid:"<<uid<<"unitId:"<<mUnit->id;
			}

			btVector3 speed;
			m_gameWorld->getUnitSpeed(uid, mUnit->id, speed);
			//std::cout <<"uid:"<<uid<<" unitId:"<<mUnit->id<<" speed:"<<speed[0]<<", "<<speed[1]<<", "<<speed[2] <<" pos:"<< uPos[0]<<" ," <<uPos[1]<<" , "<<uPos[2]<<std::endl;

			mUnit->oldV.x = speed[0]/kGameWorldScale;
			mUnit->oldV.y = speed[2]/kGameWorldScale;

			mUnit->pos.x = uPos[0]/kGameWorldScale;
			mUnit->pos.y = uPos[2]/kGameWorldScale;
    	}

		mapRect_->addUser(uid);
	}

	lastFrameTime_ = now;

	int32_t ret = mapRect_->updateNeighbor(true);
	if(ret != 0){
		LOG_ERROR <<"update neighbor failed";
		return ;
	}
	
	if(frameCounter_ % kRadiusSortCounter == 0){
		updateScoreSort();
	}

	for(auto it = players_.begin(); it!= players_.end();++it){
		if(it->second->battleType_ != BATTLE_OBJECT_TYPE_PLAYER || getPlayerState(it->first)!= PlayerState::PLAYING){
			continue;
		}

		int32_t uid = it->first;
		set<int32_t> idset;
		int32_t ret = mapRect_->getNeighbor(uid, idset);
		if(ret != 0){
			LOG_ERROR <<"get neighbor failed, id:"<<uid;
			return;
		}

		ret = sendSync(uid, idset, now);
		if(ret != 0){
			LOG_ERROR<<"send sync failed:"<<uid;
			return;
		}
	}

	for(auto it = players_.begin(); it!= players_.end();++it){
		int32_t uid = it->first;

    	MoveRuntimeInfoPtr moveInfo = getPlayerMoveInfoById(uid);
    	if(moveInfo.get() == NULL){
    		LOG_ERROR <<"move info null";
    		return;
    	}
		moveInfo->urgenEvents_.clear();
	}

	mapRect_->clearWaitDels();

    if(newPlayerOnline_.size() > 0){
    	newPlayerOnline_.clear();
	}

	if(UINT32_MAX == frameCounter_){
		frameCounter_ = 0;
	}else{
		frameCounter_++;
	}

}


static bool rankSortFunction(RankSortUnit x, RankSortUnit y){
	return x.score > y.score;

}

void Room::updateScoreSort(){

	scoreRank_.clear();

	for(auto it = players_.begin(); it != players_.end(); ++it){		
		BattleObjectInfoPtr bo = it->second;

		if(bo->battleType_ == BATTLE_OBJECT_TYPE_PLAYER){

			PlayerBattleInfoPtr player = static_pointer_cast<PlayerBattleInfo>(bo);
			if(player.get()== NULL){
				LOG_ERROR<<"player data empty,uid:" << it->first;
				continue;
			}

			if(player->state_ != PlayerState::PLAYING){
				continue;
			}
		}

		scoreRank_.push_back(RankSortUnit(it->first, bo->score_));

	}

	std::sort(scoreRank_.begin(), scoreRank_.end(), rankSortFunction);
}



int32_t Room::sendSync(int32_t uid, const set<int32_t>& idset, int64_t timestamp){
    shared_ptr<RoomMessage> msg(new RoomMessage());
   	msg->set_msgtype(CSDirectMove_SyncDown);
    SyncDown *sync = msg->mutable_syncdown();
    CSDirectMoveSycnDown *syncDown = sync->mutable_move();
    syncDown->set_timestamp(timestamp);

    if(frameCounter_ % kRadiusSortCounter == 0){
    	for(size_t ii = 0; ii < scoreRank_.size() && ii < 5; ii++){
    		CSDirectMoveSycnDown_RankUnit* rankUnit = syncDown->add_rank();
    		rankUnit->set_uid(scoreRank_[ii].uid);
    		rankUnit->set_score(scoreRank_[ii].score);
    		rankUnit->set_utype(0);
    	}
    }

    if(newPlayerOnline_.size() > 0){
    	for_each(newPlayerOnline_.begin(), newPlayerOnline_.end(), 
    		[&](int32_t& uid){
                PlayerBattleInfoPtr playerInfo = getPlayerBattleInfoBy(uid);
                if(playerInfo.get() == NULL){
                    LOG_ERROR <<"get player info empty, id:"<<uid;
                    return;
                }
                CSDirectMoveSycnDown_NewPlayerOnlineUnit* newPlayer = syncDown->add_newplayer();
                newPlayer->set_uid(uid);
                newPlayer->set_photoid(playerInfo->photoId);
                newPlayer->set_name(playerInfo->name);
            }
            );
    }

    for(int32_t ii = 0; ii < mapRect_->mineBytes_.size(); ii++){
    	syncDown->add_minebyte(mapRect_->mineBytes_[ii]);
    }


    for(auto it = idset.begin(); it!=idset.end(); ++it){
    	CSBasicMovement* move = syncDown->add_move();

    	MoveRuntimeInfoPtr moveInfo = getPlayerMoveInfoById(*it);
    	if(moveInfo.get() == NULL){
    		LOG_ERROR <<"move info null";
    		return false;
    	}

		BattleObjectInfoPtr playerInfo = getBattleObjectInfoById(*it);
    	if(playerInfo.get() == NULL){
    		LOG_ERROR <<"get player info empty, id:"<<*it;
    		return -1;
    	}
    	move->set_uid(*it);
    	move->set_photoid(playerInfo->photoId);
    	move->set_score(playerInfo->score_);

    	for(auto itt = moveInfo->units_.begin(); itt!= moveInfo->units_.end(); ++itt){
    		MoveUnitPtr mUnit = *itt;
	    	CSBasicMovementUnit* unit = move->add_units();
	    	Vector* vec = unit->mutable_pos();
	    	vec->set_x(mUnit->pos.x);
	    	vec->set_y(mUnit->pos.y);
	    	Vector* vel = unit->mutable_v();
	 		vel->set_x(mUnit->oldV.x);
	 		vel->set_y(mUnit->oldV.y);

	 		//LOG_INFO <<"sync down speed uid:" <<*it <<" unitId:" <<mUnit->id <<" speed: ("<<mUnit->currV.x <<", "<<mUnit->currV.y <<")" <<" pos:"<<mUnit->pos.x <<", " <<mUnit->pos.y<<")";
	    	unit->set_r(mUnit->r);
	    	unit->set_id(mUnit->id);
    	}

    	if(moveInfo->urgenEvents_.size() >0){
    		CSMoveUrgent* urgent = syncDown->add_urgent();

	    	for(auto itt = moveInfo->urgenEvents_.begin(); itt!= moveInfo->urgenEvents_.end(); ++itt){

	    		switch(itt->type){
	    			case MOVE_URGENT_EVENT_TYPE_SPLIT:
	    			{
                        if(itt->event.split.isStart){
                            CSMoveUrgentSplit* split = urgent->add_split();
                            split->set_uid(itt->event.split.uid);
                            split->set_unitid(itt->event.split.unitId);
                            split->set_newunitid(itt->event.split.newUnitId);
                            split->set_timeline(itt->event.split.timeline);
                            break;
                        }else{
                            CSMoveUrgentSplitEnd* splitEnd = urgent->add_splitend();
                            splitEnd->set_uid(itt->event.split.uid);
                            splitEnd->set_unitid(itt->event.split.unitId);
                            break;
                        }
	    			}

	    		}

	    	}
            
            //moveInfo->urgenEvents_.clear();
    	}

    }
    return sendMessageById(uid, std::static_pointer_cast<google::protobuf::Message>(msg));
}


int32_t Room::sendMessageById(int32_t uid, MessagePtr msg){
	if(roomManager_ == NULL){
		LOG_ERROR << "room manager empty:"<<roomId_;
		return -1;
	}

	RoomServer* server = roomManager_->getRoomServer();
	if(server == NULL){
		LOG_ERROR <<"server null from room manager";
		return -1;
	}

	server->sendMessageById(uid, msg);
	//cout <<"sent"<<endl;
	return 0;
}

bool Room::broadCastMessageById(set<int32_t> ids, MessagePtr msg){
	vector<int32_t> ais;

	for_each(ids.begin(), ids.end(), [&](const int32_t & id){
		if(tmp::isAIId(id)){
			ais.push_back(id);
		}
	});

	for_each(ais.begin(), ais.end(), [&](const int32_t & id){
		ids.erase(id);
	});

	if(roomManager_ == NULL){
		LOG_ERROR << "room manager empty:"<<roomId_;
		return false;
	}

	RoomServer* server = roomManager_->getRoomServer();
	if(server == NULL){
		LOG_ERROR <<"server null from room manager";
		return false;
	}

	server->sendMessageByIdSet(ids, msg);
	//cout <<"sent"<<endl;
	return true;
}


int32_t Room::getRoomId(){
	return roomId_;
}

int32_t Room::getCampNum(){
	return campNum_;
}

int32_t Room::getPlayerCountByCamp(int32_t camp){
	if(camp < 0 || camp >= campNum_){
		LOG_ERROR << "camp no error:"<<camp;
		return 0;
	}	
	return playerCountByCamp_[camp];
}

int32_t Room::getPlayerWaitByCamp(int32_t camp){
	if(camp < 0 || camp >= campNum_){
		LOG_ERROR << "wait no error:"<<camp;
		return 0;
	}
	return waitPlayerCountByCamp_[camp];
}

int32_t Room::availableEnterNum(){
	int32_t restNum = 0;
	restNum = campMaxCount_*campNum_ - playingPlayersCount_ - waitingPlayersCount_;

	if(restNum < 0){
		LOG_ERROR<<"rest num less than 0"<<restNum;
		return 0;
	}
	return restNum;
}


int32_t Room::getAvailByCamp(int32_t camp){
	int32_t avail = campMaxCount_ - getPlayerCountByCamp(camp) - getPlayerWaitByCamp(camp);
	return avail;
}

int32_t Room::isRoomFull(){

	int32_t avail = availableEnterNum();

	return avail<=0;
}

int32_t Room::chooseCamp(int32_t uid){
	if(isSingleCamp_){
		return 0;
	}	
	
	auto it = std::min_element(playerCountByCamp_.begin(), playerCountByCamp_.end());
	if(it == playerCountByCamp_.end()){
		LOG_ERROR << "min select failed";
	}

	int32_t campNo = it - playerCountByCamp_.begin();
	return campNo;
}


bool Room::tryEnterRoom(vector<int32_t>&& uids, int32_t camp, TcpConnectionPtr conn){

	if(camp <0 || camp >= campMaxCount_){
		LOG_ERROR << " camp error:"<<camp;
		return false;
	}
	
	int32_t avail = getAvailByCamp(camp);
	if(avail < static_cast<int>(uids.size())){
		LOG_ERROR<< " avail less than wanted, avail:"<<avail<<" wanted:"<<uids.size();
		return false;
	}

	for(auto it = uids.begin(); it!= uids.end(); ++it){
		int32_t id = *it;
		waitUsers_[id] = camp;
		PlayerBattleInfoPtr player =  getPlayerBattleInfoBy(id);

		if(player.get() == NULL){
			return false;
		}

		player->move_ = MoveRuntimeInfoPtr(new MoveRuntimeInfo(id, mapRect_));

		player->score_ = 0;
		player->state_ = PlayerState::WAITFOR_ROOM;
	}


	waitPlayerCountByCamp_[camp] += uids.size();
	waitingPlayersCount_+= uids.size();


	return true;
}



bool Room::tryEnterRoom(vector<pbmsg::common::UserBasicInfo>&& uids, int32_t camp, TcpConnectionPtr conn){
	if(camp <0 || camp >= campMaxCount_){
		LOG_ERROR << " camp error:"<<camp;
		return false;
	}
	
	int32_t avail = getAvailByCamp(camp);
	if(avail < static_cast<int>(uids.size())){
		LOG_ERROR<< " avail less than wanted, avail:"<<avail<<" wanted:"<<uids.size();
		return false;
	}

	waitPlayerCountByCamp_[camp] += uids.size();
	waitingPlayersCount_+= uids.size();
	for(auto it = uids.begin(); it!= uids.end(); ++it){
		int32_t id = it->uid();

		waitUsers_[id] = camp;
		PlayerBattleInfoPtr player = PlayerBattleInfoPtr(new PlayerBattleInfo(id, mapRect_));

		//player->id = id;
		player->name = it->name();
		player->camp = camp;
		player->photoId = it->photo();
		player->lv_ = it->lv();
		player->exp_ = it->exp();
		player->token_ = it->token();
		player->state_ = PlayerState::WAITFOR_ROOM;
		player->loginConn_ = conn;
		player->score_ = 0;

		players_[id] = player;	
	}

	return true;
}


bool Room::checkWaitUserValid(int32_t uid){
    if(waitUsers_.find(uid) == waitUsers_.end()){
		LOG_ERROR << "no such uid in wait: "<<uid;
	    return false;
	}		
    return true;
}


bool Room::enterRoom(int32_t uid){
	if(waitUsers_.find(uid) == waitUsers_.end()){
	    LOG_ERROR << "no such uid in wait: "<<uid;
        return false;
    }		
	int32_t camp = waitUsers_[uid];
	
	if(camp <0 || camp >= campMaxCount_){
		LOG_ERROR << "camp error:"<<camp;		
		return false;
	}
	waitPlayerCountByCamp_[camp] -= 1;

	waitUsers_.erase(uid);
	waitingPlayersCount_ -= 1;

	playingPlayersCount_ += 1;
	playerCountByCamp_[camp] +=1;
	playUsers_[uid] = camp;	
	
	PlayerBattleInfoPtr player = getPlayerBattleInfoBy(uid);
	if(player.get() == NULL){
		LOG_ERROR <<"player not created";
		return false;
	}
	player->state_ = PlayerState::WAITFOR_BORN;
	
	return true;
}


int32_t Room::LoadRoomResource(){
	//localSave::Map map;
	

	return 0;	
}


MoveUnitPtr Room::getUnitMoveInfoById(int32_t uid, int32_t unitId){
	PlayerBattleInfoPtr battle = getPlayerBattleInfoBy(uid);
	if(battle == NULL){
		LOG_ERROR << "battle info null, id:"<<uid;
		return NULL;
	}

	if(battle->move_ == NULL){
		LOG_ERROR << "move module null, id:"<<uid;
		return NULL;
	}

	MoveRuntimeInfoPtr move = battle->move_;

	return move->getUnitById(unitId);


}

MoveRuntimeInfoPtr Room::getPlayerMoveInfoById(int32_t id){
	BattleObjectInfoPtr battle = getBattleObjectInfoById(id);
	if(battle.get() == NULL){
		LOG_ERROR << "battle info null, id:"<<id;
		return NULL;
	}

	if(battle->move_ == NULL){
		LOG_ERROR << "move module null, id:"<<id;
		return NULL;
	}

	return battle->move_;

}

PlayerState Room::getPlayerState(int32_t uid){

	do{

		if(players_.find(uid) == players_.end()){
			LOG_ERROR<<"no such player";
			break;
		}

		BattleObjectInfoPtr bo = players_[uid];

		if(bo->battleType_ == BATTLE_OBJECT_TYPE_PLAYER){

			PlayerBattleInfoPtr player = static_pointer_cast<PlayerBattleInfo>(bo);
			if(player.get()== NULL){
				LOG_ERROR<<"player data empty,uid:" << uid;
				break;
			}

			return player->state_;
		}
	}while(0);

	return  PlayerState::PlayerState_INVALID;

}


bool Room::setMoveRuntimeInfoById(int32_t id, MoveRuntimeInfoPtr& info){
	PlayerBattleInfoPtr battle = getPlayerBattleInfoBy(id);
	if(battle == NULL){
		LOG_ERROR << "battle info null, id:"<<id;
		return false;
	}

	battle->move_ = info;
	return true;
}




int32_t Room::insertToFrameManager(int32_t uid, MoveRuntimeInfoPtr& info){
	if(frameManager_ == NULL){
		LOG_ERROR <<"frame manager null";
		return -1;
	}
    return 0;
}


int32_t Room::getRoomType(){
	return roomInfo_->roomtype();
}

bool Room::isPlayerInRoom(int32_t uid){
	if(players_.find(uid) == players_.end()){
		LOG_ERROR <<"player is not in player list:"<<uid;
		return false;
	}

	PlayerState state = getPlayerState(uid);
	if(state == PlayerState::WAITFOR_BORN || state == PlayerState::PLAYING){
		return true;
	}
	return false;

}

bool Room::playerBorn(int32_t uid, BattleObjectType battleType){
	BattleObjectInfoPtr bo = getBattleObjectInfoById(uid);
	if(bo.get() == NULL){
		LOG_ERROR <<"player not created:"<<uid;
		return false;
	}
	
	bo->battleType_ = battleType;

	PlayerBattleInfoPtr player = NULL;
	if(battleType == BATTLE_OBJECT_TYPE_PLAYER){
		player = static_pointer_cast<PlayerBattleInfo>(bo);	
	
		if(player.get() != NULL && player->state_ != PlayerState::WAITFOR_BORN){
			LOG_ERROR <<"player not in wait born state:"<<uid;
			return false;
		}
	}

	Point bornPos;

	int32_t ret = mapRect_->selectBornPosition(bornPos);
	if(ret != 0){
		LOG_ERROR<<"select born failed:"<<uid;
		return false;
	}

/*
	if(battleType == BATTLE_OBJECT_TYPE_PLAYER){
		bornPos.x = 100.f;
	}else{
		bornPos.x = 1000.f;
	}*/

	//bornPos = Point(300.0, 300.0);


	MoveRuntimeInfoPtr move = bo->move_;

	if(move.get() == NULL){
		LOG_ERROR<<"NO move module:"<<uid;
		return false;
	}

	MoveUnitPtr unit(new MoveUnit(move->getPtr()));
	unit->pos = bornPos;
	if(battleType == BATTLE_OBJECT_TYPE_PLAYER){
		unit->r = kInitR*1;
	}else{
		unit->r = kInitR*1;
	}

	unit->w = kInitW;

	move->addUnit(unit);

	if(player.get()!= NULL && !isStart_){
		isStart_ = true;
		start();
	}

	if(isAIId(uid)){
		unit->r += 20;
	}

	m_gameWorld->playerBorn(uid, unit->id, unit->r*kGameWorldScale, btVector3(unit->pos.x*kGameWorldScale, 0, unit->pos.y*kGameWorldScale));

	newPlayerOnline_.push_back(uid);

	if(player.get() != NULL){
		player->state_ = PlayerState::PLAYING;
	}

	mapRect_->addUser(uid);

	mapRect_->updateNeighbor();

	return true;

}


bool Room::removePlayerFromGameWorld(int32_t uid){
	BattleObjectInfoPtr player = getBattleObjectInfoById(uid);
	if(player.get() == NULL){
		LOG_ERROR <<"player not created:"<<uid;
		return false;
	}

	MoveRuntimeInfoPtr move = player->move_;

	if(move.get() == NULL){
		LOG_ERROR<<"NO move module:"<<uid;
		return false;
	}

	for(auto it = move->units_.begin(); it != move->units_.end(); ++it){
		MoveUnitPtr unit = *it;
		m_gameWorld->removePlayer(uid, unit->id);
	}

	player->move_->removeAllUnits();

	mapRect_->removeUser(uid);
	
	return true;
}


int32_t Room::ssSendPlayerGameRound(int32_t uid){

	RoomServer* server = roomManager_->getRoomServer();
	if(server == NULL){
		LOG_ERROR <<"server null from room manager";
		return -1;
	}

	PlayerBattleInfoPtr player = getPlayerBattleInfoBy(uid);
	if(player.get() == NULL){
		LOG_ERROR <<"player not created:"<<uid;
		return -1;
	}

    shared_ptr<LoginRoomMessage> msg(new LoginRoomMessage());
   	msg->set_msgtype(SSPlayerGameStatus_Request);
	msg->set_seq(0);
    loginroom::Request *request = msg->mutable_request();
    SSPlayerGameStatusRequest *req = request->mutable_playerroomstatus();
    req->set_uid(uid);
    req->set_roomid(roomId_);
    req->set_status(PlayerGameStatus::GAME_STATUS_ROUND_OVER);
    req->set_score(player->score_);

    if(playUsers_.find(uid) != playUsers_.end()){
    	req->set_campid(playUsers_[uid]);
    }else if(waitUsers_.find(uid) != waitUsers_.end()){
    	req->set_campid(waitUsers_[uid]);
    }

	server->sendServerMessageByConnection(player->loginConn_, std::static_pointer_cast<google::protobuf::Message>(msg));

	return 0;
}

int32_t Room::ssSendPlayerOffline(int32_t uid){

	RoomServer* server = roomManager_->getRoomServer();
	if(server == NULL){
		LOG_ERROR <<"server null from room manager";
		return -1;
	}

    shared_ptr<LoginRoomMessage> msg(new LoginRoomMessage());
   	msg->set_msgtype(SSPlayerGameStatus_Request);
	msg->set_seq(0);
    loginroom::Request *request = msg->mutable_request();
    SSPlayerGameStatusRequest *req = request->mutable_playerroomstatus();
    req->set_uid(uid);
    req->set_roomid(roomId_);
    req->set_status(PlayerGameStatus::GAME_STATUS_OFFLINE);

    if(playUsers_.find(uid) != playUsers_.end()){
    	req->set_campid(playUsers_[uid]);
    }else if(waitUsers_.find(uid) != waitUsers_.end()){
    	req->set_campid(waitUsers_[uid]);
    }

    for(int32_t i = 0; i < playerCountByCamp_.size(); i++){
    	int32_t count = playerCountByCamp_[i] + waitPlayerCountByCamp_[i];
    	req->add_numbycamp(count);
    }

	server->sendServerMessageByUID(uid, std::static_pointer_cast<google::protobuf::Message>(msg));

	return 0;
}

void Room::csSendPlayerRoundOverNotif(int32_t uid){

	PlayerBattleInfoPtr player = getPlayerBattleInfoBy(uid);
	if(player.get() == NULL){
		LOG_ERROR <<"player not created:"<<uid;
		return;
	}

    shared_ptr<RoomMessage> msg(new RoomMessage());
   	msg->set_msgtype(CSRoundOver_SyncDown);

    SyncDown *sync = msg->mutable_syncdown();
    CSRoundOverSyncDown *syncDown = sync->mutable_battleover();
   	syncDown->set_score(player->score_);

   	sendMessageById(uid, std::static_pointer_cast<google::protobuf::Message>(msg));

}

int32_t Room::playerLogout(int32_t uid){

	if(!removePlayerFromGameWorld(uid)){
		LOG_ERROR <<"remove player from world failed";
	}

	mapRect_->removeUser(uid);

	if(playUsers_.find(uid) != playUsers_.end()){
		int32_t camp = playUsers_[uid];
		if(camp >= 0 && camp < playerCountByCamp_.size()){
			playerCountByCamp_[camp] -= 1;
		}
	}
	


	return 0;

}

int32_t Room::playerExit(int32_t uid){

	if(playerLogout(uid) != 0){
		LOG_ERROR <<"player logout failed";
	}

	if(waitUsers_.find(uid) != waitUsers_.end()){
		int32_t camp = waitUsers_[uid];
		if(camp >= 0 && camp < waitPlayerCountByCamp_.size()){
			waitPlayerCountByCamp_[camp] -=1;
		}
		waitUsers_.erase(uid);
	}

	RoomServer* server = roomManager_->getRoomServer();
	if(server == NULL){
		LOG_ERROR <<"server null from room manager";
		return -1;
	}

	server->kickOffConnectionById(uid, false);

	ssSendPlayerOffline(uid);

	if(players_.find(uid) != players_.end()){
		players_.erase(uid);
	}

	return 0;
}



int32_t Room::playerRoundOver(int32_t uid, GameOverStatus status){

	BattleObjectInfoPtr bo = getBattleObjectInfoById(uid);
	if(bo.get() == NULL){
		LOG_ERROR <<"bo not exits, uid:"<<uid;
		return -1;
	}

	if(bo->battleType_ == BATTLE_OBJECT_TYPE_AI){
		removePlayerFromGameWorld(uid);
		aiMgr_->removeAI(uid);
		players_.erase(uid);
		roomManager_->playerRooms_.erase(uid);
		return 0;
	}

	PlayerBattleInfoPtr player = getPlayerBattleInfoBy(uid);
	if(player.get() == NULL){
		LOG_ERROR <<"player not created:"<<uid;
		return -1;
	}

	if(player->move_.get() == NULL){
		LOG_ERROR << "player move is null, uid"<<uid;
		return -1;
	}

	player->state_ = PlayerState::WAITFOR_BORN;

	if(playUsers_.find(uid) == playUsers_.end()){
		LOG_ERROR <<"player users no this one";
	}

	int32_t camp = playUsers_[uid];

	if(playerLogout(uid) != 0){
		LOG_ERROR <<"player logout failed";
	}

	if(!tryEnterRoom(vector<int32_t>{uid}, camp, player->loginConn_)){
		LOG_ERROR<< "round over try to wait failed";
	}
	if(!enterRoom(uid)){
		LOG_ERROR << "round over try enter failed";
	}

	csSendPlayerRoundOverNotif(uid);
	ssSendPlayerOffline(uid);

	return 0;

}


int32_t Room::kickOffPlayer(int32_t uid){
	playerExit(uid);	

	return 0;
}

bool Room::isPlayerId(int32_t id){
	return id > tmp::USER_ID_MIN && id < tmp::USER_ID_MAX;
}

bool Room::isMoId(int32_t id){
	return id > tmp::MO_ID_MIN && id < tmp::MO_ID_MAX;
}


bool Room::tryEatMine(int32_t uid, int32_t unitId, int32_t mineId, int64_t timestamp){
	
	MinePtr mine;

	//LOG_INFO << "try eat mine:"<<mineId;

	if(isAIId(uid)){
		if(!mapRect_->aiEatMine(uid, unitId, mineId, mine) ){
			return false;
		}
	}else if(!mapRect_->tryEatMine(uid, unitId, mineId, timestamp, mine)){
		LOG_ERROR <<"try eat mine failed";
		return false;
	}

	MoveUnitPtr unit = getUnitMoveInfoById(uid, unitId);
	if(unit.get() == NULL){
		LOG_ERROR << "get unit move failed";
		return false;
	}

	unit->r = generateNewRadius(unit->r, mine->radius_);
	changeUnitRadiusInGameWorld(uid, unitId, unit->r);


	DROPPtr drop = DropData::getInstance().getById(mine->kind_);

	if(drop.get() == NULL){
		LOG_ERROR << " drop type has no record:"<<mine->kind_;
		return false;
	}

	BattleObjectInfoPtr player = getBattleObjectInfoById(uid);
	if(player.get() == NULL){
		LOG_ERROR <<"player is null:"<<uid;
		return false;
	}

	player->score_ += drop->add_gold();

	//LOG_INFO << "eat mine:"<<mineId;

	return true;	
}

bool Room::tryEatEnemy(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1, int64_t timestamp){

	if(isAIId(uid0)){
		if(mapRect_->aiEatPlayer(uid0, unitId0, uid1, unitId1) != 0){
			return false;
		}
	}else if(mapRect_->tryEatEnemy(uid0, unitId0, uid1, unitId1, timestamp) != 0){
		LOG_ERROR <<"try eat enemy failed";
		return false;
	}

	MoveUnitPtr unit0 = getUnitMoveInfoById(uid0, unitId0);
	if(unit0.get() == NULL){
		LOG_ERROR << "get unit move failed";
		return false;
	}

	MoveUnitPtr unit1 = getUnitMoveInfoById(uid1, unitId1);
	if(unit1.get() == NULL){
		LOG_ERROR << "get unit move failed";
		return false;
	}

	unit0->r = generateNewRadius(unit0->r, unit1->r);

	eatUnitInGameWorld(uid0, unitId0, uid1, unitId1, unit0->r);

	MoveRuntimeInfoPtr move = getPlayerMoveInfoById(uid1);

	if(move.get() == NULL){
		LOG_ERROR <<"move is null, uid:"<<uid1;
		return false;
	}

	move->removeUnit(unitId1);

	if(move->units_.size() <= 0){
		playerRoundOver(uid1, GameOverStatus::GAMEOVER_FAILURE);
	}

	BattleObjectInfoPtr player = getBattleObjectInfoById(uid0);
	if(player.get() == NULL){
		LOG_ERROR <<"player is null:"<<uid0;
		return false;
	}

	player->score_ += unit1->r*unit1->r/100;


	return true;

}



bool Room::splitPlayer(int32_t uid, Vec dir, int64_t timestamp, int32_t seq){
	SplitRes splitRes;
	splitRes.uid_ = uid;

	MoveRuntimeInfoPtr move = getPlayerMoveInfoById(uid);
	if(move.get() == NULL){
		LOG_ERROR<<"player move info failed, uid:"<<uid;
		return false;
	}
	if(!splitPlayerInGameWorld(uid, move, dir, timestamp, splitRes)){
		return false;
	}

    /*
	MoveUrgentEvent ev;
    ev.type = MOVE_URGENT_EVENT_TYPE_SPLIT;
	ev.event.split.isStart = true;
	ev.event.split.uid = uid;
	ev.event.split.unitId = splitRes.unitId_;
	ev.event.split.newUnitId = splitRes.newUnitId_;
	ev.event.split.timeline = splitRes.timeline_;

	move->urgenEvents_.push_back(ev);*/
	
	MoveFramePtr currF = frameManager_->getCurrFrame();

	//LOG_ERROR <<"curr frame start time:"<<currF->startTime_<<", end:"<<currF->endTime_;

    if(!isAIId(uid)){
        if(sendSplit(uid, splitRes, seq) != 0){
            LOG_ERROR <<"send split failed";
            return false;
        }
    }

	return true;

}

bool Room::combinePlayer(int32_t uid, int32_t unitId, set<int32_t> subUnits){

	if(subUnits.find(unitId) != subUnits.end()){
		LOG_ERROR << "combine with combine, uid:" << uid << " unitId:" << unitId;
	}

	MoveRuntimeInfoPtr move = getPlayerMoveInfoById(uid);
	if(move.get() == NULL){
		LOG_ERROR<<"player move info failed, uid:"<<uid;
		return false;
	}

	MoveUnitPtr unit = move->getUnitById(unitId);

	if(unit.get() == NULL){
		LOG_ERROR <<"unit null";
		return false;
	}

	float area = unit->r*unit->r;

	set<pair<int32_t, int32_t>> subSet;

	for(auto it = subUnits.begin(); it != subUnits.end(); it++){
		subSet.insert(make_pair(uid, *it));
		MoveUnitPtr sub = move->getUnitById(*it);
		if(sub.get() == NULL){
			LOG_ERROR <<"acc unit null";
			return false;
		}
		area += sub->r * sub->r;
	}

	float newR = sqrt(area);

	unit->r = newR;

	
	m_gameWorld->combinePlayer(make_pair(uid, unitId), subSet, newR*kGameWorldScale);
    
    return true;

}




MoveUnitPtr Room::createSplitPartFromUnit(MoveUnitPtr unit, Vec dir, int64_t timestamp){

	float dirMag = dir.magnitude();

	if(dirMag < EPS){
		dir = Vec(1, 0);
	}

	float oldR = unit->r;

	float newR = oldR/1.415f;

	if(newR < 0){
		LOG_ERROR <<"new R less than 0";
		return NULL;
	}

	MoveUnitPtr newUnit(new MoveUnit(unit, true));

	newUnit->r = newR;

	newUnit->pos = unit->pos + dir * (newR * 2);

    newUnit->currV = dir * Move::kSplitSpeed;
	newUnit->oldV = newUnit->currV;
	newUnit->a = dir*Move::kSplitDamping;

	//newUnit->currV = dir * GameWorldEngine::SPLIT_FLY_SPEED;

    int64_t flyDeltaTime = static_cast<int64_t>((newUnit->oldV.magnitude()/std::fabs(Move::kSplitDamping)) * 1000);
    
	newUnit->needSyncSpeed = false;
	newUnit->m_state = MOVE_UNIT_STATE_FLYING;
    newUnit->flyExistTime = timestamp + flyDeltaTime;

    Rect border = this->mapRect_->getBorderRect();
    Point borderPos;
    int64_t borderDeltaTime;
    
    if(!border.isContainPoint(newUnit->pos)){
        newUnit->flyExistTime = 0;
        newUnit->revSpeed(Room::kFrameDelta/1000.f);
        
        if(!mapRect_->isUnitInMap(newUnit)){
            newUnit->revPos();
            auto userMoveInfo = unit->root_.lock();
            if(userMoveInfo.get()!= NULL){
            	m_gameWorld->setUnitPos(userMoveInfo->uid, newUnit->id, btVector3(newUnit->pos.x * kGameWorldScale, 0, newUnit->pos.y* kGameWorldScale));
            }else{
            	LOG_ERROR <<"no root , unitId:" << newUnit->id;
            }
            
        }
        
    }else if(border.cutMove(newUnit->pos, newUnit->oldV, Move::kSplitDamping, flyDeltaTime, borderPos, borderDeltaTime)){
        newUnit->flyExistTime = timestamp + borderDeltaTime;
    }
    
    newUnit->combineTime = newUnit->flyExistTime + kSplitCombineTimeMs;
    
	return newUnit;

}

bool Room::changeUnitRadiusInGameWorld(int32_t uid, int32_t unitId, float newR){

	m_gameWorld->changePlayerRadius(uid, unitId, newR*kGameWorldScale);
    return true;
}

void Room::twoPlayerUnitCollision(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1){
	MoveUnitPtr unit0 = getUnitMoveInfoById(uid0, unitId0);

	MoveUnitPtr unit1 = getUnitMoveInfoById(uid1, unitId1);

	if(unit0.get() == NULL || unit1.get() == NULL){

		LOG_ERROR <<"one unit null, uid:" << uid0<<" unit0:"<<unitId0<< " uid1"<< uid1<<" unit1:"<<unitId1;

		return;
	}
	if(unit0->r > unit1->r && tmp::isAIId(uid0)){
		tryEatEnemy(uid0, unitId0, uid1, unitId1, 0);
	}else if(tmp::isAIId(uid1)){
		tryEatEnemy(uid1, unitId1, uid0, unitId0, 0);
	}


}


void Room::gameWorldCollisionCallback(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1){
	if(uid0 != uid1){
		twoPlayerUnitCollision(uid0, unitId0, uid1, unitId1);

		return ;
	}
    
    int64_t now = Timestamp::now().getMsEpoch();

	MoveUnitPtr unit0 = getUnitMoveInfoById(uid0, unitId0);

	MoveUnitPtr unit1 = getUnitMoveInfoById(uid1, unitId1);
    
    if(unit0.get() != NULL && unit0->flyExistTime < now && unit0->m_state == MOVE_UNIT_STATE_FLYING){
        
        unit0->m_state = MOVE_UNIT_STATE_NORMAL;
        MoveUrgentEvent ev;
        ev.type = MOVE_URGENT_EVENT_TYPE_SPLIT;
        ev.event.split.isStart = false;
        ev.event.split.uid = uid0;
        ev.event.split.unitId = unitId0;
        MoveRuntimeInfoPtr move = getPlayerMoveInfoById(uid0);
        if(move.get() == NULL){
            LOG_ERROR <<"get move failed";
        }
        
        move->urgenEvents_.push_back(ev);
        
    }
    
    if(unit1.get() != NULL && unit1->flyExistTime < now && unit0->m_state == MOVE_UNIT_STATE_FLYING){
        
        unit1->m_state = MOVE_UNIT_STATE_NORMAL;
        MoveUrgentEvent ev;
        ev.type = MOVE_URGENT_EVENT_TYPE_SPLIT;
        ev.event.split.isStart = false;
        ev.event.split.uid = uid1;
        ev.event.split.unitId = unitId1;
        MoveRuntimeInfoPtr move = getPlayerMoveInfoById(uid1);
        if(move.get() == NULL){
            LOG_ERROR <<"get move failed";
        }
        
        move->urgenEvents_.push_back(ev);
    }

	if(unit0.get() == NULL || unit1.get() == NULL){
		LOG_ERROR <<"one unit null, uid:" << uid0<<" unit0:"<<unitId0<<" unit1:"<<unitId1;
		return;
	}

	unit0->besideUnits.insert(unitId1);
	unit1->besideUnits.insert(unitId0);

}

void Room::gameWorldPlayerMoCollisionCallback(int32_t uid, int32_t unitId, int32_t moId){
	if(isMineId(moId) && isAIId(uid)){
		if(!tryEatMine(uid, unitId, moId, 0)){
			LOG_INFO <<"eat mine failed, uid:"<<uid <<" unitId:"<<unitId <<" moId:"<<moId;
			return;
		}

	}
}


bool Room::splitPlayerInGameWorld(int32_t uid, MoveRuntimeInfoPtr ptr, Vec dir, int64_t timestamp, SplitRes& splitRes){

	if(ptr->units_.size() < 1){
		LOG_ERROR <<"split no main unit";
		return false;
	}

	MoveUnitPtr unit = ptr->units_[0];

	if(unit->r < kCanSplitMinR){
		return false;
	}

	MoveUnitPtr newUnit = createSplitPartFromUnit(unit, dir, timestamp);

	if(newUnit.get() == NULL){
		LOG_ERROR <<"create new u failed";
		return false;
	}

	ptr->addUnit(newUnit);

	float oldR = unit->r;

	unit->a = Vec(0, 0);
	unit->r = newUnit->r;
    unit->currV = Vec(0, 0);
	unit->oldV = Vec(0, 0);
	unit->flyExistTime = newUnit->flyExistTime;
	unit->combineTime = newUnit->combineTime;
	unit->m_state = newUnit->m_state;
	unit->needSyncSpeed = false;

	newUnit->m_state = MOVE_UNIT_STATE_FLYING;
	unit->m_state = MOVE_UNIT_STATE_FLYING;

	splitRes.unitId_ = unit->id;
	splitRes.newUnitId_ = newUnit->id;
    splitRes.newR_ = newUnit->r;
    splitRes.timeline_ = newUnit->flyExistTime;
    
    bool success = false;

    do{

	    if(!isAIId(uid)){
	        int32_t ret = frameManager_->updateAccMoveRuntimeForward(uid, newUnit, timestamp, newUnit->pos, newUnit->currV, newUnit->a, splitRes.timeline_);
	        if(ret != 0){
	            LOG_ERROR << "move null";
	            return false;
	        }
	    }

	    /*
		MoveRuntimeInfoPtr moveCap = frameManager_->getCurrMoveRuntime(uid);
		if(moveCap.get() == NULL){
			LOG_ERROR <<"curr move null, uid:"<<uid;
			return false;
		}

		MoveUnitPtr unitCap = moveCap->getUnitById(newUnit->id);
		if(unitCap.get() == NULL){
			LOG_ERROR <<"curr unit null, unitId:"<<newUnit->id;
			return false;
		}*/

		if(!m_gameWorld->splitPlayer(uid, unit->id, newUnit->id, newUnit->r*kGameWorldScale,
			btVector3(newUnit->pos.x*kGameWorldScale, 0, newUnit->pos.y*kGameWorldScale),
			btVector3(dir.x, 0, dir.y), Move::kSplitSpeed*kGameWorldScale)){
			return false;
		}

		success = true;

	//*newUnit = *unitCap;
	}while(0);

	if(!success){
		ptr->removeUnit(newUnit->id);
		unit->r = oldR;
		unit->needSyncSpeed = true;
		unit->m_state = MOVE_UNIT_STATE_NORMAL;
	}


	return true;

}

bool Room::eatUnitInGameWorld(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1, float newR){
	m_gameWorld->combinePlayer(make_pair(uid0, unitId0), set<pair<int32_t, int32_t>>({make_pair(uid1, unitId1)}),newR*kGameWorldScale);
    return true;
}



bool Room::sendSplit(int32_t uid, SplitRes& splitRes, int32_t seq){

/*	set<int32_t> idSet;

	int32_t ret = mapRect_->getNeighborPlayer(uid, idSet);

	if(ret != 0){
		LOG_ERROR <<"get player neighbor failed";
		return false;
	}

	idSet.insert(uid);*/

    shared_ptr<RoomMessage> msg(new RoomMessage());
   	msg->set_msgtype(CSSplit_Response);
	msg->set_seq(seq);
    pbmsg::room::Response *rsp = msg->mutable_response();
    rsp->set_err(0);
    rsp->set_ret(Ret::Success);
    CSSplitResponse *split = rsp->mutable_split();
    CSSplitUnit* unit = split->add_moves();
    unit->set_uid(splitRes.uid_);
    unit->set_unitid(splitRes.unitId_);
    unit->set_newunitid(splitRes.newUnitId_);
    unit->set_timeline(splitRes.timeline_);
    
    split->set_servertime(Timestamp::now().getMsEpoch());

    if(sendMessageById(uid, msg)!= 0){
    	LOG_ERROR <<"send split failed";
    	return false;
    }


    return true;



}


bool Room::shrinkPlayers(){
	for(auto it = players_.begin(); it != players_.end(); ++it){	
		if(it->second->battleType_ == BATTLE_OBJECT_TYPE_PLAYER && getPlayerState(it->first)!= PlayerState::PLAYING){
			continue;
		}

		int32_t uid = it->first;

		MoveRuntimeInfoPtr moveInfo = getPlayerMoveInfoById(uid);
		if(moveInfo.get() == NULL){
			LOG_ERROR <<"get move by id:"<<uid;
			continue;
		}

    	for(auto itt = moveInfo->units_.begin(); itt!= moveInfo->units_.end(); ++itt){
    		MoveUnitPtr mUnit = *itt;
			float r = mUnit->r;

			if(r < kInitR){
				continue;
			}

			float lost = (r - kInitR)*kShrinkPercent * (1.0f * kShrinkCounter*kFrameDelta);

			mUnit->r = generateNewRadius(mUnit->r, -lost);
			changeUnitRadiusInGameWorld(uid, mUnit->id, mUnit->r);

    	}

	}
	return true;
}












