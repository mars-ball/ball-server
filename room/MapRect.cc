#include"MapRect.h"
#include "HMath.h"
#include "common/Util.h"
#include"RoomManager.h"
#include"common/Logging.h"
#include "database/tnt_deploy_room_info.pb.h"
#include "database/MineData.h"
#include "database/Map.pb.h"
#include "database/MoType.pb.h"
#include "common/TmpDefine.h"
#include "room/Move.h"
#include <algorithm>
#include <limits>

using namespace tmp;
using namespace localSave;

//const MapRect::kBorderGap = 5;
const float MapRect::kMineRadius = 33.0f;
const float MapRect::kEatCheckGap = 50.0f;
const int32_t MapRect::kMineRefreshCount = 40;

MapRect::MapRect(ROOM_INFOPtr roomInfo, MapData& mapData, RoomPtr room)
{
	std::srand(std::time(0));
	roomInfo_ = roomInfo;
	mapData_ = mapData;
	room_ = room;
	hCellSize_ = mapData.hcellsize();
	vCellSize_ = mapData.vcellsize();
	
	hCellNum_ = mapData.hcellnum();
	vCellNum_ = mapData.vcellnum();

	cellNum_ = hCellNum_* vCellNum_;

	mapSize_.x_ = 0;
	mapSize_.y_ = 0;

	mapSize_.width_ = mapData.vlength();
	mapSize_.height_ = mapData.hlength();

	borderSize_.x_ = mapSize_.x_ + kBorderGap;
	borderSize_.y_ = mapSize_.y_ + kBorderGap;

	borderSize_.width_ = mapSize_.width_ - kBorderGap*2;
	borderSize_.height_ = mapSize_.height_ - kBorderGap*2;


	if(fabs(hCellSize_ *hCellNum_ - mapSize_.width_) > 10){
		LOG_ERROR << "hoz length no matched";
	}
	if(fabs(vCellSize_*vCellNum_ - mapSize_.height_) > 10){
		LOG_ERROR << "ver length not matched";
	}

	for(int32_t ii = 0; ii < cellNum_; ii++){
		mapMesh_.push_back(IdSet());
		mineMesh_.push_back(IdSet());
	}
	
	for(int32_t ii = 0; ii < cellNum_; ii++){
		int32_t row = getRowIndex(ii);
		int32_t col = getColIndex(ii);
		
		float offsetH = col*hCellSize_;
		float offsetV = row*vCellSize_;
		cellBound_.push_back(Rect(offsetH, offsetV, hCellSize_, vCellSize_));
	}

	mineId_ = tmp::MINE_ID_MIN;

}	

void MapRect::reset(){
	idSet_.clear();
	resetNeighborAndMesh();
}

int32_t MapRect::addUser(int32_t uid){
	if(idSet_.find(uid) != idSet_.end()){
		LOG_ERROR<< "user already exists, uid:"<<uid;
		return -1;
	}
	idSet_.insert(uid);	
	neighbor_[uid] = IdSet();
	return 0;
}

void MapRect::removeUser(int32_t uid){
	if(idSet_.find(uid) == idSet_.end()){
		return;
	}
	idSet_.erase(uid);	

	if(neighbor_.find(uid) == neighbor_.end()){
		return;
	}
	neighbor_.erase(uid);

	for(auto it = neighbor_.begin(); it != neighbor_.end(); ++it){
		auto ids = it->second;
		if(ids.find(uid) != ids.end()){
			it->second.erase(uid);
			continue;
		}
	}

	return;
}


int32_t MapRect::getRowIndex(int32_t idx){
	return static_cast<int32_t>(floor(idx/hCellNum_));
}

int32_t MapRect::getColIndex(int32_t idx){
	return static_cast<int32_t>(floor(idx%hCellNum_));
}

int32_t MapRect::getIndex(int32_t row, int32_t col){
	return col + row*hCellNum_;
}


int32_t MapRect::getIndexByPos(const Point& pos){
	int32_t col = getColIndexByX(pos.x);
	int32_t row = getRowIndexByY(pos.y);
	return getIndex(row, col);
	
}

void MapRect::resetNeighborAndMesh(){
	for(auto it = mapMesh_.begin(); it != mapMesh_.end(); ++it){
		it->clear();
	}

	for(auto it = neighbor_.begin(); it != neighbor_.end(); ++it){
		it->second.clear();
	}


}

int32_t MapRect::getNeighbor(int32_t uid, IdSet& idset){
	if(neighbor_.find(uid) == neighbor_.end()){
		LOG_ERROR << "no such user: " <<uid;
		return -1;
	}
	
	idset = neighbor_[uid];	
	return 0;
}


int32_t MapRect::getNeighborPlayer(int32_t uid, IdSet& idset){

	if(neighbor_.find(uid) == neighbor_.end()){
		LOG_ERROR <<"no such user: "<<uid;
		return -1;
	}

	IdSet all = neighbor_[uid];

	std::for_each(all.begin(), all.end(), [&idset](int32_t const& id){
		if(isPlayerId(id)){
			idset.insert(id);
		}
	});
	return 0;
}

int32_t MapRect::getPlayerBound(shared_ptr<BattleObjectInfo> player, Rect& rect){
	float hmin = std::numeric_limits<float>::max();
	float hmax = std::numeric_limits<float>::min();
	float vmax = std::numeric_limits<float>::min();
	float vmin = std::numeric_limits<float>::max();

	if(player->move_ == NULL){
		LOG_ERROR << "move module null";
		return -1;
	}

	for(auto it = player->move_->units_.begin(); it != player->move_->units_.end(); it++){
		MoveUnitPtr unit = *it;
		hmin = std::min(hmin, unit->pos.x - unit->r);
		hmax = std::max(hmax, unit->pos.x + unit->r);
		vmin = std::min(vmin, unit->pos.y - unit->r);
		vmax = std::max(vmax, unit->pos.y + unit->r);	
	}		
	
	rect =  Rect(hmin, vmin, hmax - hmin, vmax - vmin);
	return 0;

}


int32_t MapRect::updateNeighbor(bool needMine){
	resetNeighborAndMesh();

	for(auto it = idSet_.begin(); it != idSet_.end(); ++it){	
		BattleObjectInfoPtr player = room_->getBattleObjectInfoById(*it);

		if(player.get() == NULL){
			LOG_ERROR <<"battle obj is null, id:"<<*it;
			continue;
		}

		Rect bound;
		int32_t ret = getPlayerBound(player, bound);
		if(ret != 0){
			LOG_ERROR <<"get bound failed";
			return -1;
		}

		float hmin = bound.x_;
		float vmin = bound.y_;
		float hmax = bound.x_ + bound.width_;
		float vmax = bound.y_ + bound.height_;

		int rowMin = static_cast<int32_t>(floor(vmin/vCellSize_));
		int colMin = static_cast<int32_t>(floor(hmin/hCellSize_));
		int rowMax = static_cast<int32_t>(ceil(vmax/vCellSize_));
		int colMax = static_cast<int32_t>(ceil(hmax/hCellSize_));

		if(rowMin < 0) rowMin = 0;
		if(colMin < 0) colMin = 0;
		if(rowMax > vCellNum_-1) rowMax = vCellNum_-1;
		if(colMax > hCellNum_-1) colMax = hCellNum_-1;

		for(int32_t ii = rowMin; ii <= rowMax; ii++){
			for(int32_t jj = colMin; jj<= colMax; jj++){
				int32_t idx = getIndex(ii, jj);
				mapMesh_[idx].insert(*it);
			}
		}	

	}		

	for(auto it = idSet_.begin(); it != idSet_.end(); ++it){
		BattleObjectInfoPtr player = room_->getBattleObjectInfoById(*it);
		Rect bound;
	    int ret = getPlayerBound(player, bound);
		if(ret != 0){
			LOG_ERROR <<"get bound failed";
			return -1;
		}
		
		int32_t row = getRowIndexByY(bound.center().y);
		int32_t col = getColIndexByX(bound.center().x);
		int32_t hCellCoverNum = static_cast<int32_t>(round(bound.width_/hCellSize_));
		int32_t vCellCoverNum = static_cast<int32_t>(round(bound.height_/vCellSize_));

		if(hCellCoverNum <= 0){
			hCellCoverNum = 1;
		}
		if(vCellCoverNum <= 0){
			vCellCoverNum = 1;
		}

		int32_t rowMin = row - vCellCoverNum*vExtendTime_;
		int32_t rowMax = row + vCellCoverNum*vExtendTime_;
		int32_t colMin = col - hCellCoverNum*hExtendTime_;
		int32_t colMax = col + hCellCoverNum*hExtendTime_;

		if(rowMin < 0) rowMin = 0;
		if(colMin < 0) colMin = 0;
		if(rowMax > vCellNum_-1) rowMax = vCellNum_-1;
		if(colMax > hCellNum_-1) colMax = hCellNum_-1;

		for(int32_t ii = rowMin; ii <= rowMax; ii++){
			for(int32_t jj = colMin; jj<= colMax; jj++){
				int32_t idx = getIndex(ii, jj);
				neighbor_[*it].insert(mapMesh_[idx].begin(), mapMesh_[idx].end());
				/*
				if(needMine){
					neighbor_[*it].insert(mineMesh_[idx].begin(), mineMesh_[idx].end());
				}*/
			}
		}
	}
	return 0;
}

int32_t MapRect::getColIndexByX(float X){
	return static_cast<int>(floor(X/hCellSize_));
}

int32_t MapRect::getRowIndexByY(float Y){
	return static_cast<int>(float(Y/vCellSize_));
}

int32_t MapRect::selectBornPosition(Point& pos){

	int cnt = mapMesh_.size();

	int checkStart = randRange(0, cnt); //0
	bool found = false;
	int index = -1;

	for(int ii = checkStart; ii < cnt; ii++){
		if(mapMesh_[ii].size() == 0){
			found = true;
			index = ii;
			break;
		}
	}

	if(!found){
		for(int ii = 0; ii < checkStart; ii++){
			if(mapMesh_[ii].size() == 0){
				found = true;
				index = ii;
				break;
			}
		}
	}

	if(!found){
		return -1;
	}

	pos = cellBound_[index].center();
	return 0;		
}


void MapRect::findNewMinePos(Point& pos){
	float xMin = mapSize_.x_;
	float yMin = mapSize_.y_;

	float width = mapSize_.width_;
	float height = mapSize_.height_;
	
	float x = randRange(xMin, xMin + width);
	float y = randRange(yMin, yMin + height);
	
	pos.x = x;
	pos.y = y;
}

void MapRect::bornANewMine(int32_t byteIndex){
	Point pos;
	findNewMinePos(pos);
	MinePtr mine = MinePtr(new Mine());
	mine->radius_ = kMineRadius;
	mine->pos_ = pos;
	mine->color_ = randMineColorId();
	mine->kind_ = MO_TYPE_MINE;
	mines_[mineId_] = mine;
	int32_t index = getIndexByPos(pos);
	mine->meshIndex_ = index;
	mineMesh_[index].insert(mineId_);
	mine->byteIndex_ = byteIndex;
	mine->id = mineId_;
	mine->alive_ = true;
	room_->m_gameWorld->mineBorn(mineId_, kMineRadius*Room::kGameWorldScale, btVector3(pos.x*Room::kGameWorldScale, 0, pos.y*Room::kGameWorldScale));

	mineId_++;
}

bool MapRect::initMineRes(MineCreateParam param){
	
	int num = param.num_;
	mineNum_ = num;
	mineCreateInfo_ = param;
/*	
	vector<int32_t> pots;

	int32_t tot = num;

	while(tot > 0){
		int32_t tmp = randRange(param.minPotNum_, param.maxPotNum_);
		if(tmp > tot){
			pots.push_back(tot);
			tot = 0;
			break;
		}

		pots.push_back(tmp);
		tot -= tmp;
	}
	Point pos;	
	for(auto it = pots.begin(); it != pots.end(); ++it){
				
	}
*/
	Point pos;
	for(int32_t ii = 0; ii < num; ii++){
		bornANewMine(ii);
	}

	if(mineNum_ != mines_.size()){
		LOG_ERROR <<"room " <<room_->roomId_ << " born mine num failed";
		mineNum_ = mines_.size();
	}
	
	initMineByte();

	return true;

}

void MapRect::initMineByte(){
	int32_t mineByteNum = static_cast<int32_t>(ceil(1.0f*mineNum_/8));
	for(int32_t ii = 0; ii < mineByteNum; ii++){
		mineBytes_.push_back(numeric_limits<uint32_t>::max());
	}
}





void MapRect::removeMines(std::vector<int32_t> mineIds, bool async){
	for(auto it = mineIds.begin(); it != mineIds.end(); ++it){
		if(mines_.find(*it) == mines_.end()){
			LOG_ERROR <<"delete a point not in set:id:"<<*it;
			continue;
		}

		MinePtr mine = mines_[*it];
		if(mine.get() == NULL){
			LOG_ERROR <<"mine is null";
			return;
		}

		int32_t byteIndex = mine->byteIndex_;
		int32_t bigIndex = static_cast<int32_t>(floor(byteIndex/8));
		int32_t smallIndex = byteIndex%8;

		if(byteIndex < 0 || byteIndex >= mineNum_ || bigIndex >= mineBytes_.size()){
			LOG_ERROR <<"mine index error, mineId:"<<*it;
			return;
		}

		uint32_t byteOne = 1<<smallIndex;
		uint32_t byteFlip = ~byteOne;

		mineBytes_[bigIndex] = mineBytes_[bigIndex] & byteFlip;

		mineDieQueue_.push(*it);

		mine->alive_ = false;

		if(!async){
			room_->m_gameWorld->removeMine(*it);	
		}else{
			worldWaitDelMines_.insert(*it);
		}


		mineMesh_[mine->meshIndex_].erase(*it);
		/*
		do{
			
			MinePtr mine = mines_[*it];
			if(mine.get() == NULL){
				LOG_ERROR <<"mine is null";
				break;
			}
			int32_t index = mine->meshIndex_;
			if(index < 0 || index >= mineMesh_.size()){
				LOG_ERROR <<"index not in mine mesh, index:" << index;
				break;
			}

			mineMesh_[index].erase(*it);

		}while(0);
		mines_.erase(*it);*/


	}
}

void MapRect::clearWaitDels(){

	for_each(worldWaitDelMines_.begin(), worldWaitDelMines_.end(), 
		[&](const int32_t & id){
			room_->m_gameWorld->removeMine(id);
		}
	);

	worldWaitDelMines_.clear();

}


bool MapRect::checkEatMine(MoveUnitPtr unit, MinePtr mine, int64_t deltaTime){
	
	if(unit.get() == NULL){
		LOG_ERROR <<"move cap null";
		return false;
	}	
	
	int64_t borderDelta;
	Point pos;
	if(!borderSize_.cutMove(unit->pos, unit->oldV, 0, deltaTime, pos, borderDelta)){
		LOG_ERROR <<"cut move failed pos:"<<unit->pos.x<<", "<<unit->pos.y<<" deltams:"<<deltaTime;
		return false;
	}

	Vec distVec = pos - mine->pos_;
	float dist = distVec.magnitude();
	if(dist < fabs(unit->r + kEatCheckGap/*- mine->radius_*/)){
		return true;
	}	

	return false;
}


void MapRect::updateMineRes(){

	int addNum = mineDieQueue_.size();

	if(addNum > 0){
		/*
		for(int32_t ii = 0; ii < num; ii++){
			bornANewMine(ii);
		}*/
		if(addNum > kMineRefreshCount){
			addNum = kMineRefreshCount;
		}
		for(int32_t ii = 0; ii < addNum; ii++){

			if(mineDieQueue_.size() <= 0){
				break;
			}

			int32_t mid = mineDieQueue_.front();
			mineDieQueue_.pop();

			if(mines_.find(mid) == mines_.end()){
				LOG_ERROR << "mines not found mid:"<<mid;
				continue;
			}

			MinePtr mine = mines_[mid];

			if(mine.get() == NULL){
				LOG_ERROR << "mine null:"<<mid;
				continue;
			}

			mine->alive_ = true;

			room_->m_gameWorld->mineBorn(mid, kMineRadius*Room::kGameWorldScale, btVector3(mine->pos_.x*Room::kGameWorldScale, 0, mine->pos_.y*Room::kGameWorldScale));

			int32_t byteIndex = mine->byteIndex_;

			int32_t bigIndex = static_cast<int32_t>(floor(byteIndex/8));
			int32_t smallIndex = byteIndex%8;			
			uint32_t byteOne = 1<<smallIndex;
			if(bigIndex < 0 || bigIndex >= mineBytes_.size() || smallIndex < 0 || smallIndex >= 32){
				LOG_ERROR << "index failed bigIndex:" << bigIndex << " smallIndex:"<<smallIndex;
				continue;
			}

			mineBytes_[bigIndex] = mineBytes_[bigIndex] | byteOne;

			mineMesh_[mine->meshIndex_].insert(mine->id);
		}


	}

}

int32_t MapRect::randMineColorId(){

	int32_t color_size = MineData::getInstance().size();

	int32_t idx = randRange(0, color_size);

	MINEPtr mine = MineData::getInstance().getByIndex(idx);

	if(mine.get() == NULL){
		LOG_ERROR << "get mine failed, mine idx:" << idx;
		return 0;
	}

	return mine->id();

}


Rect MapRect::getBorderRect(){
	return borderSize_;
}

bool MapRect::isUnitInMap(MoveUnitPtr unit){

	return mapSize_.isContainPoint(unit->pos);

}

bool MapRect::aiEatMine(int32_t uid, int32_t unitId, int32_t mineId, MinePtr& food){
	if(mines_.find(mineId) == mines_.end()){
		LOG_ERROR <<"mine not exist:"<<mineId;
		return false;
	}
	MinePtr mine = mines_[mineId];
	food = mine;
	removeMines(vector<int32_t>{mineId}, true);
	return true;
}


bool MapRect::tryEatMine(int32_t uid, int32_t unitId, int32_t mineId, int64_t timestamp, MinePtr& food){

	if(mines_.find(mineId) == mines_.end()){
		LOG_ERROR <<"mine not exist:"<<mineId;
		return false;
	}

	MinePtr mine = mines_[mineId];

	if(!mine->alive_){
		LOG_ERROR <<"mine not alive"<<mineId;
		return false;
	}

	if(room_.get() == NULL || room_->frameManager_.get() == NULL){
		LOG_ERROR <<"frame or room null";
		return false;
	}

	MoveRuntimeInfoPtr cap;
	int64_t deltams;
	if(room_->frameManager_->getPlayerCaptureByTime(uid, timestamp, cap, deltams) != 0){
		LOG_ERROR <<"get cap by time failed, uid"<<uid <<" ,time:"<<timestamp;
		return false;
	}

	MoveUnitPtr unit = cap->getUnitById(unitId);
	if(unit.get() == NULL){
		LOG_ERROR <<"get cap unit null";
		return false;
	}


	if(checkEatMine(unit, mine, deltams)){
		food = mine;
		removeMines(vector<int32_t>{mineId});
		return true;
	}else{
		LOG_ERROR <<"eat mine condition not meet";
	}

	return false;
}


int32_t MapRect::tryEatEnemy(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1, int64_t timestamp){
	if(room_.get() == NULL || room_->frameManager_.get() == NULL){
		LOG_ERROR <<"frame or room null";
		return -1;
	}

	MoveFrameManagerPtr frameManager = room_->frameManager_;

	MoveRuntimeInfoPtr cap0;
	int64_t deltams;
	if(frameManager->getPlayerCaptureByTime(uid0, timestamp, cap0, deltams) != 0){
		LOG_ERROR <<"get cap by time failed, uid"<<uid0 <<" ,time:"<<timestamp;
		return -1;
	}

	MoveUnitPtr unit0 = cap0->getUnitById(unitId0);
	if(unit0.get() == NULL){
		LOG_ERROR <<"get unit failed, uid:"<<uid0 << " unitId:"<<unitId0;
		return -1;
	}

	MoveRuntimeInfoPtr cap1;
	if(frameManager->getPlayerCaptureByTime(uid1, timestamp, cap1, deltams) != 0){
		LOG_ERROR <<"get cap by time failed, uid"<<uid1 <<" ,time:"<<timestamp;
		return -1;
	}

	MoveUnitPtr unit1 = cap1->getUnitById(unitId1);
	if(unit1.get() == NULL){
		LOG_ERROR <<"get unit failed, uid:"<<uid1 << " unitId:"<<unitId1;
		return -1;
	}

	if(unit0->r < unit1->r){
		LOG_ERROR <<"not large enough eat r0:"<< unit0->r <<" r1:"<<unit1->r;
		return 1;
	}

	int64_t borderDelta;
	Point pos0;
	if(!borderSize_.cutMove(unit0->pos, unit0->oldV, 0, deltams, pos0, borderDelta)){
		LOG_ERROR <<"cut move failed, uid"<<uid0<<" pos:"<<unit0->pos.x<<", "<<unit0->pos.y<<" deltams:"<<deltams;
		return -1;
	}

	Point pos1;
	if(!borderSize_.cutMove(unit1->pos, unit1->oldV, 0, deltams, pos1, borderDelta)){
		LOG_ERROR <<"cut move failed, uid"<<uid1<<" pos:"<<unit1->pos.x<<", "<<unit1->pos.y<<" deltams:"<<deltams;
		return -1;
	}

	Vec dis = pos1 - pos0;
	float disMag = dis.magnitude();

	LOG_ERROR <<"eat enemy check, oldpos0:"<<unit0->pos.x <<", "<<unit0->pos.y <<" v:"<<unit0->oldV.x <<", "<<unit0->oldV.y<< " newPos"<<pos0.x <<" ,"<<pos0.y
			<<"oldpos1:"<<unit1->pos.x <<", "<<unit1->pos.y <<" v:"<<unit1->oldV.x <<", "<<unit1->oldV.y<< " newPos"<<pos1.x <<" ,"<<pos1.y <<" dist:"<<disMag;

	if(disMag < unit0->r + kEatCheckGap){
		LOG_INFO <<"eat from:"<<uid0<<", "<<unitId0<<" to:"<<uid1<<", "<<unitId1;
		return 0;
	}
	return 1;

}


int32_t MapRect::aiEatPlayer(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1){

	MoveUnitPtr unit0 = room_->getUnitMoveInfoById(uid0, unitId0);
	if(unit0.get() == NULL){
		LOG_ERROR << "get unit move failed";
		return 1;
	}

	MoveUnitPtr unit1 = room_->getUnitMoveInfoById(uid1, unitId1);
	if(unit1.get() == NULL){
		LOG_ERROR << "get unit move failed";
		return 1;
	}


	if(unit0->r < unit1->r){
		return 1;
	}

	Point pos0 = unit0->pos;
	Point pos1 = unit1->pos;

	Vec dis = pos1 - pos0;
	float disMag = dis.magnitude();

	if(disMag < (unit0->r - unit1->r)){
		LOG_INFO <<"ai eat player from:"<<unit0->id<<" to:"<<unit1->id;
		return 0;
	}

	return 1;

}



MinePtr MapRect::getMineById(int32_t mid){
	if(!isMineId(mid)){
		LOG_ERROR <<" not mine id:"<<mid;
		return NULL;
	}

	if(mines_.find(mid) == mines_.end()){
		LOG_ERROR <<"mid not in mines, mid:"<<mid;
		return NULL;
	}

	return mines_[mid];
}


int MapRect::getThreatValue(int32_t uid0, int32_t uid1){

	if(uid0 == uid1){
		return 0;
	}

	Vec offset;
	float speed;
	if(!getEatDirV(uid0, uid1, offset, speed)){
		return -1;
	}

	if(speed < numeric_limits<float>::min() ){
		LOG_ERROR <<"speed is too low";
		return false;
	}

	float dis = offset.magnitude();
	float timeDiff = dis/Move::kSplitSpeed;
	if(timeDiff < 4){
		return static_cast<int>(ceil(timeDiff*100));
	}
	return 0;

}


bool MapRect::getEatDirV(int32_t uid0, int32_t uid1, Vec& offset, float& speed){

	MoveRuntimeInfoPtr move0 = room_->getPlayerMoveInfoById(uid0);
	if(move0.get() == NULL){
		LOG_ERROR << "get move runtime null, uid:"<<uid0;
		return false;
	}
	MoveRuntimeInfoPtr move1 = room_->getPlayerMoveInfoById(uid1);
	if(move1.get() == NULL){
		LOG_ERROR << "get move runtime null, uid:"<<uid1;
		return false;
	}

	float r0 = move0->getBiggestRadius();
	float r1 = move1->getBiggestRadius();

	if(r0 < r1){
		return false;
	}

	MoveUnitPtr mainUnit0 = move0->getBiggestUnit();
	if(mainUnit0.get() == NULL){
		LOG_ERROR <<"main unit is null";
		return false;
	}

	float maxV0 = move0->getMaxV();
	if(maxV0 < numeric_limits<float>::min() ){
		LOG_ERROR <<"speed is too low";
		return false;
	}

	speed = maxV0;

	Vec minV;
	float min = numeric_limits<float>::max();
	bool found = false;


	for(int ii = 0; ii < move1->units_.size(); ii++){
		MoveUnitPtr unit1 = move1->units_[ii];
		if(unit1.get() == NULL){
			LOG_ERROR <<"unit1 is null";
			continue;
		}

		Vec diff = unit1->pos - mainUnit0->pos;
		float diffMag = diff.magnitude();
		if(diffMag < min){
			found = true;
			minV = diff;
			min = diffMag;
		}
	}

	if(!found){
		LOG_ERROR <<"search failed, uid0:"<<uid0<<" ,uid1"<<uid1;
		return false;
	}

	offset = minV;
	return true;

}











