#FILES = $(shell sh -c 'ls') 
#CPPFILES = $(filter %.cc,$(FILES))
CPPFILES = $(wildcard *.cc)
OBJ = $(CPPFILES:.cc=.o)

all: $(OBJ)
	#$(shell sh -c 'cp $(OBJ) ../Output/')
	echo "complete"

$(OBJ): %.o: %.cc
	$(CC) -o $@ -I ../ -I ../database -I ../bullet -c $(CPPFLAGS) $(DEBUG) $(WARNINGS) $< $(LDFLAGS)



common:
	@echo $(OBJ)


.PHONY: clean


clean:
	-rm *.o
