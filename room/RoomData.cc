#include "RoomData.h"

int32_t RoomDataInfo::loadRoomData() {
	string dataFileName = tmp::ROOM_FILE_NAME;
	fstream fin(dataFileName, ios::in | ios::binary); 
    ROOM_INFO_ARRAY infoArr;
    infoArr.ParseFromIstream(&fin);
    for (int32_t ii = 0; ii < infoArr.items_size(); ii++) {
    	ROOM_INFOPtr info = ROOM_INFOPtr(new ROOM_INFO());
    	*info = infoArr.items(ii);
    	int32_t roomTypeId = info->roomid();
    	if (rooms_.find(roomTypeId) != rooms_.end()) {
    		LOG_ERROR << "have two same room id:" << info->roomid();
    		return -1;
		}
		
		MapData mapData;
		int ret = loadMapData(info->mapname() + ".data", mapData);	
		if(ret != 0){
			LOG_ERROR << "map data failed, mapname:" <<info->mapname();
			return -1;
		}

	    rooms_[roomTypeId] = make_pair(info, mapData);
    }
	fin.close();
	return 0;
}

int32_t RoomDataInfo::loadMapData(string fileName, MapData& mapData){
	fstream fin(fileName, ios::in | ios::binary);	
	mapData.ParseFromIstream(&fin);
	fin.close();
	return 0;
}

int32_t RoomDataInfo::getRoomInfo(int32_t roomType, ROOM_INFOPtr& roomInfo){
	if(rooms_.find(roomType) == rooms_.end()){
		LOG_ERROR << "no room type: " <<roomType;
		return -1;
	}
	roomInfo = rooms_[roomType].first;
	return 0;
}
int32_t RoomDataInfo::getMapInfo(int32_t roomType, MapData& mapData){
	if(rooms_.find(roomType) == rooms_.end()){
		LOG_ERROR << "no room type: " <<roomType;
		return -1;
	}
	mapData = rooms_[roomType].second;
	return 0;
}

int32_t RoomDataInfo::getAllType(vector<int32_t>& types){
	for(auto it = rooms_.begin(); it != rooms_.end(); ++it){
		types.push_back(it->first);
	}
	return 0;
}