#include "Move.h"
#include <stdint.h>
#include <limits>
#include "common/Logging.h"
#include "MapRect.h"
#include <iostream>
#include <algorithm>
#include "Room.h"

using namespace std;
using namespace fsk;


const float Move::kMaxSpeed = 600;
const float Move::kJoyStickSpeed = 600;
const float Move::kLowestSpeedPercent = 0.2f;
const float Move::kSplitSpeed = 2000;
const float Move::kSplitDamping = -1200;


int32_t MoveUnit::unitId = 500;

MoveUnit::~MoveUnit(){
	//LOG_ERROR <<"~~unit destroyed, unitid:"<<id;
}

void MoveUnit::revSpeed(float timeDelta){
	MapRectPtr mapRect = getMapRect();
	if(mapRect.get() == NULL){
		LOG_ERROR <<"map rect is null";
		return;
	}

	Rect border = mapRect->getBorderRect();

	Vec speed = currV;

	if(!needSyncSpeed && m_state == MOVE_UNIT_STATE_COMBINING){
		speed = oldV;
	}

	Point oldPos = pos;
	Point newPos = pos + speed*timeDelta;	

	Vec revV;
	int32_t cutMask;
	border.cutSpeed(newPos, speed, revV, cutMask);

	//LOG_INFO <<"rev id:"<<id<<" speed "

	currV = revV;

	if(!needSyncSpeed && m_state == MOVE_UNIT_STATE_COMBINING){
		oldV = revV;
	}

}

void MoveUnit::revPos(){
	MapRectPtr mapRect = getMapRect();
	if(mapRect.get() == NULL){
		LOG_ERROR <<"map rect is null";
		return;
	}

	Rect border = mapRect->getBorderRect();
	
	if(pos.x < 0){
		pos.x = Rect::borderGap;
	}

	if(pos.y < 0){
		pos.y = Rect::borderGap;
	}

	if(pos.x > border.x_ + border.width_){
		pos.x = border.x_ + border.width_ - Rect::borderGap;
	}

	if(pos.y > border.y_ + border.height_){
		pos.y = border.y_ + border.height_ - Rect::borderGap;
	}

	LOG_ERROR <<"unit id:"<<id <<" out of map";

}


void MoveUnit::step(int64_t deltaTime){

	MapRectPtr mapRect = getMapRect();
	if(mapRect.get() == NULL){
		LOG_ERROR <<"map rect is null";
		return;
	}

	Rect border = mapRect->getBorderRect();

	Point oldPos = pos;
	Point newPos = pos + currV*(deltaTime/1000.0);	

	Vec revV;
	int32_t cutMask;
	border.cutSpeed(newPos, currV, revV, cutMask);
	currV = revV;

	bool isPosInBorder = border.isContainPoint(pos, cutMask);
	newPos = pos + currV*(deltaTime/1000.0);	


	std::cout <<" new pos: (" <<newPos.x << " ,"<<newPos.y <<") currV x:"<<currV.x <<" y: "<<currV.y <<" mask:" << cutMask<<std::endl;
	if(border.isContainPoint(newPos, cutMask)){
		pos = newPos;
	}else if(isPosInBorder){
		cout <<"status 3"<<endl;
		int64_t borderDelta;
		Point borderPos;
		if(!border.cutMove(pos, currV, 0, deltaTime, borderPos, borderDelta)){
			LOG_ERROR <<"cut move failed";
			return;
		}
		pos = borderPos;
	}	

	std::cout <<"id: " << id << " from pos: ("<< oldPos.x << ", "<<oldPos.y<<")" << " to pos: (" <<pos.x << " ,"<<pos.y <<") oldV x:"<<oldV.x << " y:"<<oldV.y <<" currV x:"<<currV.x <<" y: "<<currV.y<<std::endl;

	oldV = currV;

}

MapRectPtr MoveUnit::getMapRect(){
	auto root = root_.lock();
	if(root.get() == NULL){
		LOG_ERROR <<"root is null";
		return NULL;
	}

	return root->mapRect_;
}

int32_t Move::getSpeedFromJoy(JoyStickInput& joy, Vec& speed){
	float w = joy.dis;
	if(w < 0 || w > 1){
		LOG_ERROR << "joy input wrong";
		return -1;
	}	

	if(w < numeric_limits<float>::epsilon()){
		w = 0;
	}

	speed = joy.dir.norm()* (kJoyStickSpeed * w);
	return 0;

}


int32_t Move::getSpeedFromDir(Vec dir, Vec& speed){

	speed = dir.norm()* kJoyStickSpeed;
	return 0;

}

MoveUnitPtr MoveRuntimeInfo::getBiggestUnit(){
	float radius = -1;
	MoveUnitPtr res;

	for(auto it = units_.begin(); it != units_.end(); ++it){
		MoveUnitPtr unit = *it;
		if(unit->r > radius){
			radius = unit->r;
			res = unit;
		}
	}

	return res;

}

float MoveRuntimeInfo::getBiggestRadius(){
	float radius = -1;

	for(auto it = units_.begin(); it != units_.end(); ++it){
		MoveUnitPtr unit = *it;
		if(unit->r > radius){
			radius = unit->r;
		}
	}
	return radius;
}

float MoveRuntimeInfo::getMaxV(){
	float maxR = getBiggestRadius();

	float maxV = Move::kMaxSpeed * Move::velocityPercent(maxR);

	return maxV;
}

bool MoveRuntimeInfo::setMainV(Vec v){
	MoveUnitPtr unit = getMainUnit();
	if(unit.get() == NULL){
		LOG_ERROR <<"set main v null";
		return false;
	}

	if(!unit->needSyncSpeed){
		return false;
	}

	float maxR = getMaxUnitR();

	Vec vv = v * Move::velocityPercent(maxR);

	unit->currV = vv;
    
    return true;
}

MoveUnitPtr MoveRuntimeInfo::getMainUnit(){
	//std::cout <<"units size:" <<units_.size()<<std::endl;
	if(units_.size() < 1){
		LOG_ERROR <<"no main units";
		/*
		if(mapRect_.get() == NULL){
			LOG_ERROR<< "map rect is null";
			return NULL;
		}


		RoomPtr room = mapRect_->room_;

		if(room.get() == NULL){
			LOG_ERROR<<"room is null";
		}

		room->playerRoundOver(uid1, GameOverStatus::GAMEOVER_FAILURE);*/
		if(noUnitCallback_ != NULL){
			//noUnitCallback_();
		}

		return NULL;
	}
	return units_[0];

}

void MoveRuntimeInfo::step(int64_t nowTime, int64_t deltaTime){
	std::cout <<"step"<<std::endl;
/*
	for(auto it = units_.begin(); it != units_.end(); ++it){

		MoveUnitPtr unit = *it;
		
		if(unit->needSync && nowTime> unit->existTime){

			MoveUnitPtr mainUnit = units_[0];

			if(mainUnit.get() == NULL){
				LOG_ERROR << "main unit null";
				return;
			}

			float eatR = unit->r;
			float mainR = mainUnit->r;

			float newR = eatR + mainR;

			mainUnit->r = newR;

			it = units_.erase(it);
			continue;

		}

		unit->step(deltaTime);

	}
*/
}



MoveUnitPtr MoveRuntimeInfo::getUnitById(int32_t unitId){
/*
	auto it = find_if(units_.begin(), units_.end(), [&](MoveUnitPtr& unit)->bool{
														if(unit->id == unitId){
															return true;
														}else{
															return false;
														}
													});

	if(it == units_.end()){
		return NULL;
	}	
*/
	bool found = false;
	for(auto it = units_.begin(); it != units_.end(); ++it){
		MoveUnitPtr unit = *it;
		if(unit->id == unitId){
			found = true;
			return unit;
		}
	}
	return NULL;
	

}


void MoveRuntimeInfo::removeUnit(int32_t unitId){

	/*auto it = find_if(units_.begin(), units_.end(), [&](MoveUnitPtr& unit)->bool{
														if(unit->id == unitId){
															return true;
														}else{
															return false;
														}
													});

	if(it == units_.end()){
		return;
	}	

	units_.erase(it);*/

	vector<MoveUnitPtr>::iterator del;

	for(auto it = units_.begin(); it != units_.end(); ++it){
		MoveUnitPtr unit = *it;
		if(unit->id == unitId){
			del = it;
		}
	}

	if(del == units_.begin()){
		(*(units_.end() - 1))->needSyncSpeed = true;
	}

	iter_swap(del, units_.end() - 1);
	units_.erase(units_.end() - 1);

	if(units_.size() <= 0){
		return;
	}

	MoveUnitPtr unit = getMainUnit();
	if(unit.get() != NULL){
		unit->needSyncSpeed = true;
	}

	LOG_INFO<<"remove unit:"<<unitId;
}


void MoveRuntimeInfo::removeAllUnits(){

	/*auto it = find_if(units_.begin(), units_.end(), [&](MoveUnitPtr& unit)->bool{
														if(unit->id == unitId){
															return true;
														}else{
															return false;
														}
													});

	if(it == units_.end()){
		return;
	}	

	units_.erase(it);*/


	units_.clear();
	LOG_INFO<<"remove all unit:";
}


float MoveRuntimeInfo::getMaxUnitR(){
	float maxR = 0;
	for(auto it = units_.begin(); it != units_.end(); ++it){
		MoveUnitPtr unit = *it;
		if(unit->r > maxR){
			maxR = unit->r;
		}
	}

	return maxR;

}



float Move::velocityPercent(float r){

	float takeUp = 0;
	if(r < Room::kInitR){
		takeUp = 0;
	}else if(r > Room::kMaxR){
		takeUp = 1;
	}else{
		takeUp = (r-Room::kInitR)/(Room::kMaxR - Room::kInitR);
	}

	float per = kLowestSpeedPercent + (1- kLowestSpeedPercent)*(1-takeUp);

	return per;
}










