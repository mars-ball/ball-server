#include "CSExitRoomProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "RoomServer.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "user/UserManager.h"
#include <vector>
#include "Room.h"

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::room;

void CSExitRoomProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){
	LOG_INFO<<"EXIXT ENEMY PROCESS";
	shared_ptr<RoomMessage> message =
			std::static_pointer_cast<RoomMessage>(_message);
	const CSExitRoomRequest req = message->request().exitroom();
	pbmsg::Ret csRet = Ret::Failed;

	RoomServer * roomServer = NULL;

	do{
		roomServer = dynamic_cast<RoomServer*>(server);
		if(roomServer == NULL){
			LOG_ERROR << "cast null";
			csRet = Ret::Error;
			break;
		}

		int32_t uid;

		int32_t ret = roomServer->getUserIdByConnection(conn, uid);
		if(ret != 0){
			LOG_ERROR<<"no id conn record:";
			break;
		}

		RoomManagerPtr roomManager = roomServer->getRoomManager();

		if(roomManager == NULL){
			LOG_ERROR <<"room server manager null";
			csRet = Ret::Error;
			break;
		}

		RoomPtr room;
		ret = roomManager->getPlayerRoom(uid, room);
		if(ret != 0){
			LOG_ERROR << "get player room failed, uid:"<<uid;
			return;
		}

		room->playerRoundOver(uid, GameOverStatus::GAMEOVER_FAILURE);

		csRet = Ret::Success;
	}while(0);
	
    shared_ptr<RoomMessage> msg(new RoomMessage());
   	msg->set_msgtype(CSExitRoom_Response);
	msg->set_seq(message->seq());
    Response *response = msg->mutable_response();
    response->set_ret(csRet);
    CSExitRoomResponse *rsp = response->mutable_exitroom();
    rsp->set_ret(csRet);

	roomServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	


}
