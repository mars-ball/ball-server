#pragma once
#include<algorithm>
#include<fstream>
#include"google/protobuf/message.h"
#include "database/tnt_deploy_buff_info.pb.h"
#include "common/TmpDefine.h"
#include "common/Logging.h"
#include "common/Singleton.h"

using namespace google::protobuf;
using namespace tnt_deploy;
using namespace std;
using namespace fsk;

typedef shared_ptr<BUFF_INFO> BUFF_INFOPtr;


class BuffDataInfo: public Singleton<BuffDataInfo>{

public:
    int32_t loadBuffData();
	BUFF_INFOPtr getById(int32_t buffId);

private:
	map<int32_t, BUFF_INFOPtr > buffs_;


};
