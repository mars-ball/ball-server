#include "CSDirectMoveSyncUpProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "RoomServer.h"
#include "user/UserManager.h"
#include "RoomManager.h"
#include "Room.h"
#include "protobuf/RoomMessage.pb.h"
#include "Move.h"
#include <iostream>

using namespace std;

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::room;


void CSDirectMoveSyncUpProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){
		shared_ptr<RoomMessage> message =
				std::static_pointer_cast<RoomMessage>(_message);
		const CSDirectMoveSyncUp sync = message->syncup().move();
		
		RoomServer* roomServer = dynamic_cast<RoomServer*>(server);
		if(roomServer == NULL){
			LOG_ERROR << "cast null";
			return ;
		}

		int32_t uid_rsp = sync.uid();

		CSJoyStickOperation oper = sync.joy();
		JoyStickInput joy;
		joy.dir.x = oper.dir().x();		
		joy.dir.y = oper.dir().y();
		joy.dis = oper.dis();
		
		Vec speed;
		int32_t ret = Move::getSpeedFromJoy(joy, speed);
		if(ret != 0){
			LOG_ERROR <<"get speedFromJoy failed";
			return;
		}

		//cout <<"recv speed x:"<<speed.x <<" y:"<<speed.y<<endl;

		/*
		UserInfoPtr user = UserManager::getInstance().getById(uid);
		if(user == NULL){
			LOG_ERROR<<"no user:"<<uid;
			return;
		}

		if(!user->isInRoom_){
			LOG_ERROR <<"user is not in room, id:"<<uid;
			return;
		}*/

		int32_t uid;

		ret = roomServer->getUserIdByConnection(conn, uid);
		if(ret != 0){
			LOG_ERROR<<"no id conn record:"<<uid_rsp;
			return;
		}

		RoomPtr room;
		ret = roomServer->getRoomManager()->getPlayerRoom(uid, room);
		if(ret != 0){
			LOG_ERROR << "get player room failed, uid:"<<uid;
			return;
		}


		if(room->getPlayerState(uid)!= PlayerState::PLAYING){
			LOG_ERROR <<"user is not playing";
			return;
		}


		MoveRuntimeInfoPtr move;
		move = room->getPlayerMoveInfoById(uid);
		if(move.get() == NULL){
			LOG_ERROR<<"player move info failed, uid:"<<uid;
			return;
		}


		move->setMainV(speed);

		
		//LOG_ERROR << "up speed uid:" <<uid<<":(" << speed.x<<", "<<speed.y<<")";


/*
        shared_ptr<LoginRoomMessage> msg(new LoginRoomMessage());
       	msg->set_msgtype(SSEnterRoom_Response);
		msg->set_seq(message->seq());
        Response *response = msg->mutable_response();
        SSEnterRoomResponse *rsp = response->mutable_enterroomresponse();

		if(ret){
			rsp->set_ret(Success);
			rsp->set_msg("entered");

			rsp->set_roomid(roomId);
			for(auto it = uids.begin(); it != uids.end(); ++it){
				rsp->add_uids(*it);
			}
		}else{
			rsp->set_ret(Error);
			rsp->set_msg("error");
		}


		roomServer->sendServerMessageByConnection(conn, std::static_pointer_cast<google::protobuf::Message>(msg));
*/


}
