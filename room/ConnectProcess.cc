#include "ConnectProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "RoomServer.h"

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::loginroom;

void ConnectProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

		shared_ptr<LoginRoomMessage> message =
				std::static_pointer_cast<LoginRoomMessage>(_message);
		const ConnectRequest req = message->request().connectrequest();

		RoomServer* roomServer = dynamic_cast<RoomServer*>(server);
		if(roomServer == NULL){
			LOG_ERROR << "cast null";
			return ;
		}

        int32_t sid = req.sid();
		string token = req.token();

        shared_ptr<LoginRoomMessage> msg(new LoginRoomMessage());
       	msg->set_msgtype(Connect_Response);
		msg->set_seq(message->seq());
        Response *response = msg->mutable_response();
        ConnectResponse *rsp = response->mutable_connectresponse();
		rsp->set_ret(Success);
		rsp->set_msg("connected");

		if(!roomServer->serverList_->isLoginSidInList(sid)){
			LOG_ERROR<<"not a login sid:"<<sid;
			return;
		}
		roomServer->updateServerConnection(sid, conn);
		//roomServer->sendServerMessageBySid(sid, std::static_pointer_cast<google::protobuf::Message>(msg));



}
