#include "AI.h"
#include "Room.h"
#include "lua/LuaManager.h"
#include "database/PhotoData.h"
#include "database/AINameData.h"
#include "common/Util.h"

AIInfo::AIInfo(int32_t id, MapRectPtr mapRect)
:BattleObjectInfo(id),
 isStart_(false),
 luaParam_(new LuaAIParam())
{
	//this->id = id;

	int photoNum = PhotoData::getInstance().imagePhotos_.size();
	int photoRand = randRange(0, photoNum);

	this->photoId = PhotoData::getInstance().imagePhotos_[photoRand]->id();
	this->score_ = 0;

	int aiNameSize = AINameData::getInstance().size();

	int aiNameRand = randRange(0, aiNameSize);
	this->name = AINameData::getInstance().getByIndex(aiNameRand)->name();


	move_ = MoveRuntimeInfoPtr(new MoveRuntimeInfo(id, mapRect));
	luaMgr_ = getLuaManager();
	luaFileName_ = "ai";
}

void AIInfo::luaStart(){
	luaMgr_->call_lua_main(id, luaFileName_, "start");

}

void AIInfo::luaUpdate(){

	luaMgr_->call_lua_main(id, luaFileName_, "update");
}

void AIInfo::luaTimer(LuaAITimerParam param){
	
	luaParam_->timer = param;
	luaMgr_->call_lua_main(id, luaFileName_, "timer");

}

void AIInfo::resetParam(){
	


}



AIInfoPtr AIManager::addAI(){

	int32_t id = aiId_;
	do{
		id += 1;
		if(id >= tmp::AI_ID_MAX){
			id = tmp::AI_ID_MIN;
		}
	}while(ais_.find(id) != ais_.end());


	AIInfoPtr newAI(new AIInfo(id, mapRect_));

	ais_[id] = newAI;
	return newAI;

}

void AIManager::removeAI(int32_t uid){
	if(ais_.find(uid) != ais_.end()){
		ais_.erase(uid);
	}
}


AIManager::AIManager(Room* room){
	
	mapRect_ = room->mapRect_;
}


void AIManager::updateAIs(){
	
	for(auto it = ais_.begin(); it != ais_.end(); it++){
		AIInfoPtr ai = it->second;
		if(!ai->isStart_){
			ai->luaStart();
			ai->isStart_ = true;
		}else{
			ai->luaUpdate();
		}
	}

}

AIInfoPtr AIManager::getAIInfoById(int32_t id){
	
	if(ais_.find(id) == ais_.end()){
		LOG_ERROR << "no such id:"<<id;
		return NULL;
	}
	
	return ais_[id];

}

int32_t AIManager::size(){
	return ais_.size();
}










