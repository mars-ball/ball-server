#pragma once

#include <vector>
#include <algorithm>
#include <memory>
#include "lua/LuaAIParam.h"
#include "common/Logging.h"

class Room;

using namespace std;
using namespace fsk;


enum TIMER_EVENT_TYPE{
TIMER_EVENT_TYPE_INVALID,
TIMER_EVENT_TYPE_AI,
TIMER_EVENT_TYPE_MAX
};



struct TimerEvent{
	int32_t id; 
	int64_t timeStamp_;
	TIMER_EVENT_TYPE type_;


	LuaAITimerParam aiPlayer_;

};




class StopWatcher{

public:
StopWatcher(Room* room);

void AddEvent(TimerEvent event);

vector<TimerEvent> PopEvents(int64_t nowtime);

void handleEvent(TimerEvent event);

void handleAllEvents(int64_t now);

void handleAIEvent(int32_t id, LuaAITimerParam param);

private:
vector<TimerEvent> timers_;
Room* room_;


};

typedef shared_ptr<StopWatcher> StopWatcherPtr;
