#ifndef KUN_SERVER_MESSAGEREGISTER_H
#define KUN_SERVER_MESSAGEREGISTER_H
#include "net/ProtobufServer.h"
#include <functional>
#include <vector>
#include <memory>
#include <functional>
#include "net/ProcessBase.h"

using namespace fsk::net;
using namespace std;
using namespace std::placeholders;
using namespace pbmsg;


class MessageRegister{
public:
	MessageRegister(ProtobufServer* server): server_(server){
	}
	
	void registerMessages();

	template <typename T>
	void registerOneMessage(int32_t topType, int msgType);
	void registerMessageDefines();

private:
	ProtobufServer *server_;
	vector<shared_ptr<ProcessBase>> handlers_;

};




















#endif
