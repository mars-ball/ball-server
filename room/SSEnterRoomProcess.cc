#include "SSEnterRoomProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "RoomServer.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "user/UserManager.h"
#include <vector>
#include "Room.h"

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::loginroom;

void SSEnterRoomProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<LoginRoomMessage> message =
			std::static_pointer_cast<LoginRoomMessage>(_message);
	const SSEnterRoomRequest req = message->request().enterroomrequest();

	RoomServer* roomServer = dynamic_cast<RoomServer*>(server);
	if(roomServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
/*
	int32_t uid;
	int32_t ret = roomServer->getUserIdByConnection(conn, uid);
	if(ret != 0){
		LOG_ERROR <<"room server ";
		return;
	}*/

	int32_t roomId = req.roomid();

	int32_t camp = req.camp();

	vector<pbmsg::common::UserBasicInfo> userInfos;

	vector<int32_t> idset;

	for(int i = 0; i < req.users_size(); i++){
		userInfos.push_back(req.users(i));
		idset.push_back(req.users(i).uid());
	}
	
	RoomManagerPtr roomManager = roomServer->getRoomManager();

	if(roomManager == NULL){
		LOG_ERROR <<"room server manager null";
		return;
	}

	RoomPtr room;

	int32_t ret = roomManager->getRoomById(roomId, room);
	if(ret != 0){
		LOG_ERROR <<"get room failed:, roomid:"<<roomId;
		return;
	}

	if(!room->tryEnterRoom(std::move(userInfos), camp, conn)){
		LOG_ERROR <<"enter room failed, id:"<<roomId;
		return;
	}
	
    shared_ptr<LoginRoomMessage> msg(new LoginRoomMessage());
   	msg->set_msgtype(SSEnterRoom_Response);
	msg->set_seq(message->seq());
    Response *response = msg->mutable_response();
    response->set_ret(0);
    SSEnterRoomResponse *rsp = response->mutable_enterroomresponse();
    rsp->set_roomid(roomId);
    rsp->set_camp(camp);
	rsp->set_ret(Success);

	for(auto it = idset.begin(); it != idset.end(); ++it){
		rsp->add_uids(*it);
	}


	roomServer->sendServerMessageByConnection(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	


}
