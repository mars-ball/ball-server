#ifndef KUN_SERVER_CONNECTION_PROCESS
#define KUN_SERVER_CONNECTION_PROCESS

#include "net/CommonInc.h"
#include "net/ProcessBase.h"
#include "net/ProtobufServer.h"
#include "user/UserManager.h"
#include "common/TmpDefine.h"
#include "net/TcpConnection.h"
#include "hredis/RedConnection.h"
#include "protobuf/LoginRoomMessage.pb.h"

using namespace fsk;
using namespace fsk::net;

class ConnectProcess : public ProcessBase{

public:
	typedef shared_ptr<google::protobuf::Message> MessagePtr;

	ConnectProcess():ProcessBase(pbmsg::Login_Request){
	}


	void callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& message, Timestamp);
};


#endif
