#include "CSEatEnemyProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "RoomServer.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "user/UserManager.h"
#include <vector>
#include "Room.h"

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::room;

void CSEatEnemyProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){
	LOG_INFO<<"EAT ENEMY PROCESS";
	shared_ptr<RoomMessage> message =
			std::static_pointer_cast<RoomMessage>(_message);
	const CSEatEnemyRequest req = message->request().eatenemy();
	pbmsg::Ret csRet = Ret::Success;

	RoomServer * roomServer = NULL;

	do{
		roomServer = dynamic_cast<RoomServer*>(server);
		if(roomServer == NULL){
			LOG_ERROR << "cast null";
			csRet = Ret::Error;
			break;
		}

		int32_t uid = req.uid();

		RoomManagerPtr roomManager = roomServer->getRoomManager();

		if(roomManager == NULL){
			LOG_ERROR <<"room server manager null";
			csRet = Ret::Error;
			break;
		}

		RoomPtr room;
		int32_t ret = roomManager->getPlayerRoom(uid, room);
		if(ret != 0){
			LOG_ERROR << "get player room failed, uid:"<<uid;
			return;
		}

		int uid0 = req.uid();
		int uid1 = req.oppuid();
		int unitId0 = req.unitid();
		int unitId1 = req.oppunitid();
		int64_t eatTime = req.servertime();

		bool eatRet = room->tryEatEnemy(uid0, unitId0, uid1, unitId1, eatTime);
		if(eatRet){
			csRet = Ret::Success;
		}else{
			csRet = Ret::Failed;
		}

		//roomServer->updateUserConnection(uid, conn);
	}while(0);
	
    shared_ptr<RoomMessage> msg(new RoomMessage());
   	msg->set_msgtype(CSEatEnemy_Response);
	msg->set_seq(message->seq());
    Response *response = msg->mutable_response();
    response->set_ret(Ret::Success);
    CSEatEnemyResponse *rsp = response->mutable_eatenemy();
    rsp->set_ret(csRet);

	roomServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	


}
