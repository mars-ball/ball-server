#include "RoomEnterRoomProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "RoomServer.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "user/UserManager.h"
#include <vector>
#include "Room.h"

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::room;

void RoomEnterRoomProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){

	shared_ptr<RoomMessage> message =
			std::static_pointer_cast<RoomMessage>(_message);
	const RoomEnterRoomRequest req = message->request().enterroomrequest();
	pbmsg::Ret csRet = Ret::Success;

	RoomServer * roomServer = NULL;

	do{
		roomServer = dynamic_cast<RoomServer*>(server);
		if(roomServer == NULL){
			LOG_ERROR << "cast null";
			csRet = Ret::Error;
			break;
		}
	/*
		int32_t uid;
		int32_t ret = roomServer->getUserIdByConnection(conn, uid);
		if(ret != 0){
			LOG_ERROR <<"room server ";
			return;
		}*/

		int32_t roomId = req.roomid();
		int32_t uid = req.uid();

		RoomManagerPtr roomManager = roomServer->getRoomManager();

		if(roomManager == NULL){
			LOG_ERROR <<"room server manager null";
			csRet = Ret::Error;
			break;
		}

		int32_t ret = roomManager->playerEnterRoom(uid, roomId);
		if(ret != 0){
			LOG_ERROR <<"room enter room failed, id:"<<uid <<" roomid:"<<roomId;
			csRet = Ret::Error;
			break;
		}
		roomServer->updateUserConnection(uid, conn);
	}while(0);
	
    shared_ptr<RoomMessage> msg(new RoomMessage());
   	msg->set_msgtype(RoomEnterRoom_Response);
	msg->set_seq(message->seq());
    Response *response = msg->mutable_response();
    response->set_ret(csRet);
    RoomEnterRoomResponse *rsp = response->mutable_enterroomresponse();
    rsp->set_ret(csRet);


	roomServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	


}
