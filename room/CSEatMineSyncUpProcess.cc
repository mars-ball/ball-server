#include "CSEatMineSyncUpProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "RoomServer.h"
#include "user/UserManager.h"
#include "RoomManager.h"
#include "Room.h"
#include "protobuf/RoomMessage.pb.h"
#include "Move.h"

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::room;


void CSEatMineSyncUpProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){
	shared_ptr<RoomMessage> message =
			std::static_pointer_cast<RoomMessage>(_message);
	if(!message->has_syncup()){
		LOG_ERROR <<"no sync up";
		return;
	}

	room::SyncUp sync = message->syncup();
	if(!sync.has_eatmine()){
		LOG_ERROR <<"no eat message";
		return;
	}

	const CSEatMineSyncUp eat = sync.eatmine();
	
	RoomServer* roomServer = dynamic_cast<RoomServer*>(server);
	if(roomServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}

	vector<CSEatMineUnit> mines;
	for(int32_t ii = 0; ii < eat.mines_size(); ii++){
		mines.push_back(eat.mines(ii));
	}   

	int64_t serverTime = eat.servertime();

	int32_t uid;

	int32_t ret = roomServer->getUserIdByConnection(conn, uid);
	if(ret != 0){
		LOG_ERROR<<"no id conn record:";
		return;
	}

	RoomPtr room;
	ret = roomServer->getRoomManager()->getPlayerRoom(uid, room);
	if(ret != 0){
		LOG_ERROR << "get player room failed, uid:"<<uid;
		return;
	}

	for(auto it = mines.begin(); it != mines.end(); ++it){
		CSEatMineUnit mine = *it;
		bool isEat = room->tryEatMine(uid, mine.unitid(), mine.mineid(), serverTime);
		if(!isEat){
			LOG_ERROR <<"cannot eat, uid:"<<uid<<" unitid:"<<mine.unitid()<<" mineId:"<<mine.mineid();;
		}
	}



}
