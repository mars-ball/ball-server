#include "RoomServer.h"
#include "common/CfgReader.h"
#include "RoomManager.h"
#include "protobuf/RoomMessage.pb.h"


int32_t RoomManager::roomGlobalId = 1000;
const int32_t RoomManager::PREPARE_ROOM_COUNT = 1;

RoomServer::RoomServer(string name, EventLoop* loop, const InetAddress& listenAddr)
		:ProtobufServer(name, loop, listenAddr),
		msgRegister_(this),
		serverList_(new ServerList()),
		roomManager_(new RoomManager(this))
{

	dispatchers_[pbmsg::LoginRoom_Message] = ProtobufDispatcherBase::ptr_t(new
				ProtobufDispatcher<pbmsg::loginroom::LoginRoomMessage>(this,
					bind(&ProtobufServer::onUnkownMessage, this,
					placeholders::_1, placeholders::_2, placeholders::_3)));
	dispatchers_[pbmsg::Room_Message] = ProtobufDispatcherBase::ptr_t(new
				ProtobufDispatcher<pbmsg::room::RoomMessage>(this,
					bind(&ProtobufServer::onUnkownMessage, this,
					placeholders::_1, placeholders::_2, placeholders::_3)));
	//server_.runEvery(kHeartBeatGap, bind(&RoomServer::SendHeartBeat, this));
	msgRegister_.registerMessages();
}


int32_t RoomServer::initAll(){
	using namespace std::placeholders;
	roomManager_->init();
	serverList_->loadAllList();
	registerKickOffCallback(bind(&RoomManager::killOffPlayer, roomManager_.get(), _1));
    return 0;
}

bool RoomServer::isInWhiteList(pbmsg::ServerType type, std::string ip){
	return std::find(serverWhiteList_[type].begin(), serverWhiteList_[type].end(), ip) != serverWhiteList_[type].end();
}



void RoomServer::sendServerMessageByConnection(const TcpConnectionPtr& conn, MessagePtr msg){
	if(conn == NULL){
		fsk::LOG_ERROR << "conn null";
	}
	codec_->send(conn, msg);
}

void RoomServer::sendServerMessageByUID(int32_t uid, MessagePtr msg){
	int32_t sid;
	if(!serverList_->getLoginSidByUid(uid, sid)){
		LOG_ERROR <<"get sid from uid failed, uid:"<<uid;
		return;
	}

	sendServerMessageBySid(sid, msg);
}


void RoomServer::SendHeartBeat(){
}

void RoomServer::setServerId(int32_t serverId){
	serverId_ = serverId;
}

int32_t RoomServer::getServerId(){
	return serverId_;
}	


int32_t RoomServer::SetWhiteList(std::map<pbmsg::ServerType, std::vector<std::string>>&& list){
    serverWhiteList_ = list;
    return 0;
}

RoomManagerPtr RoomServer::getRoomManager(){
	return roomManager_;
}
