#include "HMath.h"


const float Rect::borderGap = 2;

Point calcAccDistance(Point initPos, Vec initSpeed, Vec accSpeed, int64_t timeMs){

	float timeS = static_cast<float>(1.0f*timeMs/1000.0f);
	float initSpeedMag = initSpeed.magnitude();
	float accSpeedMag = accSpeed.magnitude();
	Vec initSpeedDir(0, 0);
	if(initSpeedMag > EPS){
		initSpeedDir = initSpeed.norm();
	}
	Vec accSpeedDir(0, 0);
	if(accSpeedMag > EPS){
		accSpeedDir = accSpeed.norm();
	}


	Point finalPos = initPos + initSpeedDir * (initSpeedMag*timeS) + accSpeedDir*(1.0f/2.0f*accSpeedMag*timeS*timeS);

	return finalPos;

}


Vec calcAccSpeed(Vec initSpeed, Vec accSpeed, int64_t timeMs){
	float timeS = static_cast<float>(1.0f*timeMs/1000.0f);
	Vec speed = initSpeed + accSpeed*timeS;
	return speed;

}


