#pragma once
#include "Buff.h"
#include "database/tnt_deploy_room_info.pb.h"
#include "database/Map.pb.h"
#include "HMath.h"
#include "RoomManager.h"
#include "Move.h"
#include "MoveFrame.h"
#include "common/TimerId.h"
#include "MoveFrame.h"
#include <set>
#include "GameWorld.h"
#include "protobuf/common.pb.h"

#include "StopWatcher.h"

class AIInfo;
typedef shared_ptr<AIInfo> AIInfoPtr;
class AIManager;
typedef shared_ptr<AIManager> AIManagerPtr;

using namespace localSave;
typedef shared_ptr<google::protobuf::Message> MessagePtr;

class MapRect;
typedef shared_ptr<MapRect> MapRectPtr;
class Room;
typedef shared_ptr<Room> RoomPtr;
namespace tnt_deploy{
	class ROOM_INFO;	
	typedef shared_ptr<ROOM_INFO> ROOM_INFOPtr;
}

class RoomManager;
typedef shared_ptr<RoomManager> RoomManagerPtr;

class MoveFrameManager;
typedef shared_ptr<MoveFrameManager> MoveFrameManagerPtr;

using namespace tnt_deploy;


enum RoundResult{
	ROUND_RESULT_INVALID = 0,
	ROUND_RESULT_SUCCESS = 1,
	ROUND_RESULT_FAILED = 2
};

enum GameOverStatus{
	GAMEOVER_SUCCESS,
	GAMEOVER_FAILURE
};

class StateMachine{
};


enum PlayerState{
	PlayerState_INVALID,
	INIT,
	WAITFOR_ROOM,
	WAITFOR_BORN,
	WAITFOR_INIT,
	PLAYING,
	OFFLINE,
	MAX
};

enum BattleObjectType{
	BATTLE_OBJECT_TYPE_INVALID,
	BATTLE_OBJECT_TYPE_PLAYER,
	BATTLE_OBJECT_TYPE_AI,
	BATTLE_OBJECT_TYPE_MO
};

//template<class T>
class BattleObjectInfo{
public:
	//typedef shared_ptr<T> ptr_t;

	int32_t id;
	BattleObjectType battleType_;
	
	MoveRuntimeInfoPtr move_;

	//int64_t lastMoveTime;
	int32_t photoId;
	string name;
	int32_t camp;

	int32_t score_;

	BuffManager buffManager;
	shared_ptr<StateMachine> stateMachine;

    BattleObjectInfo(const int32_t& uid)
    :id(uid){
        
    }
    
};

typedef shared_ptr<BattleObjectInfo> BattleObjectInfoPtr;

class PlayerBattleInfo:public BattleObjectInfo{
public:
	int32_t lv_;
	int32_t exp_;
	string token_;
	PlayerState state_;
	TcpConnectionPtr loginConn_;

	PlayerBattleInfo(const int32_t& uid, MapRectPtr mapRect)
	:BattleObjectInfo(uid),
    state_(PlayerState::INIT){
		move_ = MoveRuntimeInfoPtr(new MoveRuntimeInfo(uid, mapRect));
		move_->noUnitCallback_ = bind(&PlayerBattleInfo::noUnitCallback, this);
		battleType_ = BATTLE_OBJECT_TYPE_PLAYER;
	}

	void initPlayerBattleData();

	void noUnitCallback();

};
class MoBattleInfo:public BattleObjectInfo{


};

typedef shared_ptr<PlayerBattleInfo> PlayerBattleInfoPtr;


class RunTimeRoomInfo{
public:
	RunTimeRoomInfo(RoomPtr room);
	
	int32_t tryRegisterRuntimeRoomInfo(PlayerBattleInfoPtr player);

private:
	RoomPtr room_;

};

struct SplitRes;

struct RankSortUnit{
	RankSortUnit(int32_t _uid, float _score):uid(_uid), score(_score){}
	int32_t uid;
	float score;
};


class Room: public std::enable_shared_from_this<Room>{
public:

	Room(RoomManagerPtr roomManager, int32_t roomId, ROOM_INFOPtr roomInfo, MapData& mapData);

	int32_t availableEnterNum();

	int32_t chooseCamp(int32_t uid);

	int32_t TryEnterRoom(int32_t uid);
	bool tryEnterRoom(vector<pbmsg::common::UserBasicInfo>&& uids, int32_t camp, TcpConnectionPtr conn);
	bool tryEnterRoom(vector<int32_t>&& uids, int32_t camp, TcpConnectionPtr conn);
	bool enterRoom(int32_t uid);

	PlayerBattleInfoPtr getPlayerBattleInfoBy(int32_t id);
	BattleObjectInfoPtr getBattleObjectInfoById(int32_t id);
	AIInfoPtr getAIInfoById(int32_t id);
	MoveRuntimeInfoPtr getPlayerMoveInfoById(int32_t id);
	MoveUnitPtr getUnitMoveInfoById(int32_t uid, int32_t unitId);

	PlayerState getPlayerState(int32_t uid);

	bool setMoveRuntimeInfoById(int32_t id, MoveRuntimeInfoPtr& info);
	bool playerBorn(int32_t uid,BattleObjectType battleType);
	bool isPlayerInRoom(int32_t uid);

	int32_t LoadRoomResource();

	int32_t isRoomFull();
	int32_t getRoomId();
	int32_t getCampNum();
	int32_t getPlayerCountByCamp(int32_t camp);
	int32_t getPlayerWaitByCamp(int32_t camp);
	int32_t getAvailByCamp(int32_t camp);
	
	bool checkWaitUserValid(int32_t uid);

	void init();
	void initAI();

	void start();
	void stop();

	void keyFrame();
	void updateScoreSort();
	void processAllCombine(int32_t uid, MoveRuntimeInfoPtr userMove, int64_t now);
	void processAllOutOfMap(int32_t uid, MoveRuntimeInfoPtr moveInfo);

	int32_t insertToFrameManager(int32_t uid, MoveRuntimeInfoPtr& info);
	
	int32_t sendSync(int32_t uid, const set<int32_t>& idset, int64_t timestamp);
	
	int32_t sendMessageById(int32_t uid, MessagePtr msg);

	int32_t getRoomType();

	int32_t kickOffPlayer(int32_t uid);
	int32_t playerRoundOver(int32_t uid, GameOverStatus status);
	int32_t playerLogout(int32_t uid);
	int32_t playerExit(int32_t uid);
	int32_t ssSendPlayerGameRound(int32_t uid);
	int32_t ssSendPlayerOffline(int32_t uid);
	void csSendPlayerRoundOverNotif(int32_t uid);

	bool removePlayerFromGameWorld(int32_t uid);

	static bool isPlayerId(int32_t id);
	static bool isMoId(int32_t id);
	bool tryEatMine(int32_t uid, int32_t unitId, int32_t mineId, int64_t timestamp);
	bool tryEatEnemy(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1, int64_t timestamp);
	void refreshBornAI();

	bool splitPlayer(int32_t uid, Vec dir, int64_t timestamp, int32_t seq = 0);
	bool combinePlayer(int32_t uid, int32_t unitId, set<int32_t> subUnits);

	bool shrinkPlayers();

private:
	bool splitPlayerByMoveInfo(int32_t uid, MoveRuntimeInfoPtr ptr, Vec dir, int64_t timestamp, SplitRes& splitRes);
	bool splitPlayerInGameWorld(int32_t uid, MoveRuntimeInfoPtr ptr, Vec dir, int64_t timestamp, SplitRes& splitRes);
	bool changeUnitRadiusInGameWorld(int32_t uid, int32_t unitId, float newR);
	bool eatUnitInGameWorld(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1, float newR);
	void gameWorldCollisionCallback(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1);
	void twoPlayerUnitCollision(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1);

	void gameWorldPlayerMoCollisionCallback(int32_t uid, int32_t unitId, int32_t moId);


	bool splitPlayerByMoveCapture(int32_t uid, Vec dir, int64_t timestamp, SplitRes& splitRes);
	MoveUnitPtr createSplitPartFromUnit(MoveUnitPtr unit, Vec dir, int64_t timestamp);
	bool sendSplit(int32_t uid, SplitRes& splitRes, int32_t seq);
	bool broadCastMessageById(set<int32_t> ids, MessagePtr msg);

private:
	RoomManagerPtr roomManager_;
	MoveFrameManagerPtr frameManager_;
	GameWorldEnginePtr m_gameWorld;
	
		
	ROOM_INFOPtr roomInfo_;
	MapData mapInfo_;
	shared_ptr<RunTimeRoomInfo> runtimeInfo_;

	int32_t playingPlayersCount_;
	int32_t waitingPlayersCount_;
	int32_t campNum_;
	std::vector<int32_t> playerCountByCamp_;
	std::vector<int32_t> waitPlayerCountByCamp_;
	std::map<int32_t, int32_t> playUsers_;
	std::map<int32_t, int32_t> waitUsers_;

	bool isSingleCamp_;
	int32_t campMaxCount_;

	int64_t lastFrameTime_;
 	TimerId frameTimer_;
 	uint32_t frameCounter_;

 	vector<RankSortUnit> scoreRank_;

 	vector<int32_t> newPlayerOnline_;

 	bool isStart_;

public:
	int32_t roomId_;
	
	MapRectPtr mapRect_;
	AIManagerPtr aiMgr_;
	StopWatcherPtr stopWatcher_;
	std::map<int32_t, BattleObjectInfoPtr> players_;

	static const float kAiPercent;
	static const float kFrameDelta;
	static int64_t kFrameDeltaMs;
	static const float kInitR;
	static const float kMaxR;
	static const float kInitW;

	static const float kCanSplitMinR;
	static const float kSplitLoseR;
	static const int64_t kSplitCombineTimeMs;

	static const int32_t kFreshMineDelta;

	static const float kGameWorldScale;

	static const int32_t kRadiusSortCounter;
	static const int32_t kAIThinkCounter;
	static const int32_t kAIGenerateCounter;
	static const int32_t kMineRefreshCounter;
	static const int32_t kShrinkCounter;


	static const float kAIPercent;

	static const float kShrinkPercent;

	friend class MapRect;
};




struct SplitRes{
	//Point fromPos_;
	//Point targetPos_;
	//Vec speed_;
	int32_t uid_;
	int32_t unitId_;
	int32_t newUnitId_;
	int64_t timeline_;
	float newR_;
};


typedef shared_ptr<Room> RoomPtr;
