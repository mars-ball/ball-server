#pragma once

#include<set>
#include<map>
#include<vector>
#include<queue>
#include<stdint.h>
#include "Room.h"
#include "database/Map.pb.h"
#include "HMath.h"
#include "database/tnt_deploy_room_info.pb.h"
#include "database/MoType.pb.h"
#include "Move.h"

class Room;
typedef shared_ptr<Room> RoomPtr;
class PlayerBattleInfo;
typedef shared_ptr<ROOM_INFO> ROOM_INFOPtr;

typedef std::set<int32_t> IdSet;

using namespace std;
using namespace tnt_deploy; 
using namespace localSave;

struct Mine{
	int32_t id;
	MineGenerateType genType_;
	MoType kind_;
	Point pos_;
	Vec speed_;
	float radius_;
	float weight_;
	uint8_t color_;
	int32_t meshIndex_;
	int32_t byteIndex_;
	bool alive_;
};

typedef std::shared_ptr<Mine> MinePtr;

struct MineCreateParam{
	int32_t num_;
	MineGenerateType genType_;
	int32_t freshTime_;
	int32_t maxPotNum_;
	int32_t minPotNum_;
	float createRadius_;
};


class MapRect{

public:
    static constexpr float lengthGap_ = 10;
    static const int32_t vExtendTime_ = 4;
    static const int32_t hExtendTime_ = 3;
    static constexpr float kBorderGap = 50;

    MapRect(){}
    MapRect(ROOM_INFOPtr roomInfo,MapData& mapData, RoomPtr room);

    int32_t addUser(int32_t uid);
    void removeUser(int32_t uid);
    int32_t getRowIndex(int32_t idx);
    int32_t getColIndex(int32_t idx);
    int32_t getIndex(int32_t row, int32_t col);
	int32_t getIndexByPos(const Point& pos);
    int32_t getNeighbor(int32_t uid, IdSet& idset);
    int32_t getNeighborPlayer(int32_t uid, IdSet& idset);
	int32_t getPlayerBound(shared_ptr<BattleObjectInfo> player, Rect& rect);
    int32_t updateNeighbor(bool needMine = false);
	int32_t selectBornPosition(Point& pos);
    int32_t getPlayerBound(shared_ptr<PlayerBattleInfo> player, Rect& rect);
    void resetNeighborAndMesh();
    void reset();
    int32_t getColIndexByX(float X);
    int32_t getRowIndexByY(float Y);

	bool initMineRes(MineCreateParam param);
	void bornANewMine(int32_t index);

	bool isUnitInMap(MoveUnitPtr unit);


	void updateMineRes();
	void removeMines(std::vector<int32_t> mineIds, bool async = false);
	bool checkEatMine(MoveUnitPtr cap, MinePtr mine, int64_t deltaTime);
	void findNewMinePos(Point& pos);
	bool tryEatMine(int32_t uid, int32_t unitId, int32_t mineId, int64_t timestamp, MinePtr& mine);
	bool aiEatMine(int32_t uid, int32_t unitId, int32_t mineId, MinePtr& food);

	int getThreatValue(int32_t uid0, int32_t uid1);
	bool getEatDirV(int32_t uid0, int32_t uid1, Vec& offset, float& speed);

	MinePtr getMineById(int32_t mid);
	int32_t randMineColorId();

	int32_t aiEatPlayer(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1);
	int32_t tryEatEnemy(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1, int64_t timestamp);
	Rect getBorderRect();

	void clearWaitDels();

private:
	void initMineByte();
	void setMineByteDie(int32_t id);

public:
	RoomPtr room_;
	vector<uint32_t> mineBytes_;
	int32_t mineNum_;
	std::map<int32_t, MinePtr> mines_;

private:
    int32_t hCellNum_;
    int32_t vCellNum_;
    int32_t cellNum_;
    float hCellSize_;
    float vCellSize_;
	ROOM_INFOPtr roomInfo_;
	MapData mapData_;


    Rect mapSize_;
    Rect borderSize_;

    std::vector<Rect> cellBound_;
    std::vector<IdSet> mapMesh_;
	std::vector<IdSet> mineMesh_;
    std::map<int32_t, IdSet> neighbor_;
    IdSet idSet_;
	MineCreateParam mineCreateInfo_;
	queue<int32_t> mineDieQueue_;

	IdSet worldWaitDelMines_;

	int32_t mineId_;

	const static float kMineRadius;
	const static float kEatCheckGap;
	const static int32_t kMineRefreshCount;

};

