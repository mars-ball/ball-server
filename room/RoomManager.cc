#include "RoomManager.h"
#include "RoomServer.h"
#include "Room.h"
#include "RoomData.h"
#include "lua/LuaManager.h"
#include "lua/LuaInterface.h"

using namespace localSave;

RoomManager::RoomManager(RoomServer *roomServer)	
:roomServer_(roomServer),
 roomCreateInfo_(new RoomDataInfo())
{

}

int32_t RoomManager::init(){
	roomCreateInfo_->loadRoomData();
	vector<int32_t> types;
	int32_t ret = roomCreateInfo_->getAllType(types);
	if(ret != 0){
		LOG_ERROR << "get types failed";
		return -1;
	}		

	for(auto it = types.begin(); it != types.end(); ++it){

		for(int32_t ii = 0; ii < PREPARE_ROOM_COUNT; ii++){
			int32_t roomId;
			ret = createNewRoom(*it, roomId);
			if(ret != 0){
				LOG_ERROR << "create new room failed, type:"<<*it;
				return -1;
			}
		}
	}

	getLuaManager()->init();
	getLuaManager()->roomMgr_ = shared_from_this();
	getLuaInterface()->roomMgr_ = shared_from_this();
    return 0;
}

int32_t RoomManager::getPlayerRoom(int32_t uid, RoomPtr& room){
	if(playerRooms_.find(uid) == playerRooms_.end()){
		LOG_ERROR <<"uid not in playerrooms:"<<uid;
		return -1;
	}
	room = playerRooms_[uid];
	if(room.get() == NULL){
		LOG_ERROR<<"room is empty, uid:"<<uid;
		return -1;
	}
	return 0;
}

int32_t RoomManager::getPlayerRoomId(int32_t uid, int32_t roomId){
	if(playerRooms_.find(uid) == playerRooms_.end()){
		LOG_ERROR <<"uid not in playerrooms:"<<uid;
		return -1;
	}
	RoomPtr room = playerRooms_[uid];
	if(room.get() == NULL){
		LOG_ERROR<<"room is empty, uid:"<<uid;
		return -1;
	}
	roomId = room->getRoomId();
	return 0;

}


bool RoomManager::isPlayerExist(int32_t uid){
	if(playerRooms_.find(uid) == playerRooms_.end()){
		return false;
	}
	return true;
}


int32_t RoomManager::createNewRoom(int32_t typeId, int32_t& roomId){
	ROOM_INFOPtr roomInfo;
    int ret = roomCreateInfo_->getRoomInfo(typeId, roomInfo);
	if(ret != 0){
		LOG_ERROR << "GET ROOM info failed, type:" <<typeId;
		return -1;
	}

	MapData mapData;
	ret = roomCreateInfo_->getMapInfo(typeId, mapData);
	if(ret != 0){
		LOG_ERROR <<"get map info failed, typeid:" <<typeId;
		return -1;
	}

	roomGlobalId++;
	int32_t roomFullId = (roomServer_->getServerId() * tmp::ROOM_ID_MIN) + roomGlobalId;
	roomId = roomFullId;

	RoomPtr room = RoomPtr(new Room(shared_from_this(), roomId, roomInfo, mapData));
	room->init();

	rooms_[roomFullId] = room;
    return 0;
}
/*
bool RoomManager::playerTryEnterRoom(vector<int32_t>& uids, int32_t roomId, int32_t camp){
	
	for(auto it = uids.begin(); it != uids.end(); ++it){
		int32_t uid = *it;
		if(rooms_.find(uid) == rooms_.end()){
			LOG_ERROR << "no such room:"<<roomId;
			return false;
		}
	}


	Room::ptr_t room = rooms_[roomId];
	if(room == NULL){
		LOG_ERROR <<"room empty:"<<roomId;
		return false;
	}	

	bool ret = room->tryEnterRoom(std::move(uids), camp);
	if(!ret){
		LOG_ERROR << "enter room failed"<<(uids.size() > 0? std::to_string(uids[0]): string("empty")) <<" camp:"<<camp;
		return false;
	}
	return true;

}*/

int32_t RoomManager::playerEnterRoom(int32_t uid, int32_t roomId){
	if(rooms_.find(roomId) == rooms_.end()){
		LOG_ERROR << "no such room:"<<roomId;
		return -1;
	}
	RoomPtr room = rooms_[roomId];
	if(room == NULL){
		LOG_ERROR <<"room empty:"<<roomId;
		return -1;
	}	
	
	bool ret = room->enterRoom(uid);
	if(!ret ){
		LOG_ERROR <<"enter room failed:"<<uid;
		return -1;
	}

	playerRooms_[uid] = room;

	return 0;


}

int32_t RoomManager::getAllRoomStatByType(int32_t type, vector<RoomPtr>& rooms){
	
	for(auto it = rooms_.begin(); it != rooms_.end(); ++it){
		if(!it->second->isRoomFull() && it->second->getRoomType() == type){
			rooms.push_back(it->second);	
		}
	}
	return 0;

}

RoomServer* RoomManager::getRoomServer(){
	return roomServer_;
}

int32_t RoomManager::getRoomById(int32_t roomId, RoomPtr& room){
	if(rooms_.find(roomId) == rooms_.end()){
		LOG_ERROR <<"no such roomid:"<<roomId;
		return -1;
	}

	if(rooms_[roomId] == NULL){
		LOG_ERROR << "this room null, roomid:"<<roomId;
		return -1;
	}

	room = rooms_[roomId];
	return 0;
}


EventLoop* RoomManager::getEventLoop(){
	if(roomServer_ == NULL){
		LOG_ERROR << "room server null";
		return NULL;
	}
	EventLoop* loop = roomServer_->getEventLoop();

	if(loop == NULL){
		LOG_ERROR << "loop null";
		return NULL;
	}

	return loop;

}

bool RoomManager::playerBorn(int32_t uid){
	if(playerRooms_.find(uid) == playerRooms_.end()){
		LOG_ERROR <<"player not logged in room:"<<uid;
		return false;
	}

	RoomPtr room = playerRooms_[uid];

	if(room.get() == NULL){
		LOG_ERROR <<"room is empty, uid:"<<uid;
		return false;
	}

	if(!room->playerBorn(uid, BATTLE_OBJECT_TYPE_PLAYER)){
		LOG_ERROR << "player born failed:"<<uid;
		return false;
	}

	return true;

}


int32_t RoomManager::killOffPlayer(int32_t uid){
	RoomPtr room;
    int32_t ret = getPlayerRoom(uid, room);
	if(ret != 0){
		LOG_ERROR <<"player not in room";
		return -1;
	}
	if(room.get() == NULL){
		LOG_ERROR <<"empty room:"<<uid;
		return -1;
	}
	
	playerRooms_.erase(uid);	

	if(!room->kickOffPlayer(uid)){
		LOG_ERROR <<"room kick off player, uid:"<<uid;
		return -1;
	}

	return 0;
}





















