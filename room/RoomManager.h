#ifndef KUN_SERVER_ROOM_MANAGER
#define KUN_SERVER_ROOM_MANAGER

#include "common/noncopyable.h"
#include "common/Logging.h"
#include "database/tnt_deploy_room_info.pb.h"

#include <memory>
#include <map>
#include <string>
#include <set>
#include <vector>
#include <cmath>
#include "common/Logging.h"


#include "HMath.h"
#include "common/TmpDefine.h"

#include "net/EventLoop.h"


using namespace std;
using namespace fsk;
using namespace fsk::net;


namespace fsk{
	namespace net{
		class RoomServer;
	}
}
using namespace fsk::net;

class RoomDataInfo;
class Room;
typedef shared_ptr<RoomDataInfo> RoomDataInfoPtr;
typedef shared_ptr<Room> RoomPtr;


class RoomManager:public std::enable_shared_from_this<RoomManager>{
public:
	RoomManager(fsk::net::RoomServer *roomServer);

	int32_t init();

	int32_t createNewRoom(int32_t typeId, int32_t& roomId);

	int32_t playerEnterRoom(int32_t uid, int32_t roomId);
	
	int32_t getAllRoomStatByType(int32_t type, vector<RoomPtr>& rooms);

//	bool playerTryEnterRoom(vector<int32_t>& uid, int32_t roomId, int32_t camp);

	bool playerBorn(int32_t uid);

	int32_t getPlayerRoom(int32_t uid, RoomPtr& room);

	int32_t getPlayerRoomId(int32_t uid, int32_t roomId);

	RoomServer* getRoomServer();

	int32_t getRoomById(int32_t roomId, RoomPtr& room);

	int32_t killOffPlayer(int32_t playerId);

	bool isPlayerExist(int32_t uid);

	EventLoop* getEventLoop();


public:
	std::map<int32_t, RoomPtr> playerRooms_;

private:
	RoomServer *roomServer_;	
	RoomDataInfoPtr roomCreateInfo_;
	std::map<int32_t, RoomPtr> rooms_;

	static int32_t roomGlobalId;
	const static int32_t PREPARE_ROOM_COUNT;

};






#endif
