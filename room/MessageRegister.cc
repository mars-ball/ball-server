#include "MessageRegister.h"
#include "net/ProtobufServer.h"
#include "protobuf/HMessage.pb.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "protobuf/RoomMessage.pb.h"

#include "CSDirectMoveSyncUpProcess.h"
#include "CSSplitProcess.h"
#include "BornProcess.h"
#include "SSEnterRoomProcess.h"
#include "SSRoomInfoProcess.h"
#include "RoomEnterRoomProcess.h"
#include "ConnectProcess.h"
#include "SSRoomInfoProcess.h"
#include "CSEatEnemyProcess.h"
#include "CSEatMineSyncUpProcess.h"
#include "CSGetInitSyncProcess.h"
#include "CSExitRoomProcess.h"


using namespace fsk::net;
using namespace std;
using namespace std::placeholders;
using namespace pbmsg::loginroom;
using namespace pbmsg::room;

template <typename T>
void MessageRegister::registerOneMessage(int32_t topType, int32_t msgType){
	shared_ptr<T> tmp(new T);
	handlers_.push_back(tmp);
	server_->registerMessageCallback(topType, msgType, bind(&T::callback, tmp.get(), _1, _2, _3, _4 ));

}

void MessageRegister::registerMessages(){
	registerOneMessage<ConnectProcess>(pbmsg::LoginRoom_Message, pbmsg::Connect_Request);
	registerOneMessage<SSRoomInfoProcess>(pbmsg::LoginRoom_Message, pbmsg::SSRoomInfo_Request);
	registerOneMessage<SSEnterRoomProcess>(pbmsg::LoginRoom_Message, pbmsg::SSEnterRoom_Request);
	registerOneMessage<CSDirectMoveSyncUpProcess>(pbmsg::Room_Message, pbmsg::CSDirectMove_SyncUp);
	registerOneMessage<RoomEnterRoomProcess>(pbmsg::Room_Message, pbmsg::RoomEnterRoom_Request);
	registerOneMessage<BornProcess>(pbmsg::Room_Message, pbmsg::Born_Request);
    registerOneMessage<CSSplitProcess>(pbmsg::Room_Message, pbmsg::CSSplit_Request);
	registerOneMessage<CSEatEnemyProcess>(pbmsg::Room_Message, pbmsg::CSEatEnemy_Request);
	registerOneMessage<CSGetInitSyncProcess>(pbmsg::Room_Message, pbmsg::CSGetInitSync_Request);
	registerOneMessage<CSExitRoomProcess>(pbmsg::Room_Message, pbmsg::CSExitRoom_Request);	

	registerOneMessage<CSEatMineSyncUpProcess>(pbmsg::Room_Message, pbmsg::CSEatMine_SyncUp);
	registerMessageDefines();
}


void MessageRegister::registerMessageDefines(){
	MessageDefine::getInstance().AddIdDes(pbmsg::LoginRoom_Message, pbmsg::LoginRoom_Message, LoginRoomMessage::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::LoginRoom_Message, Connect_Request, ConnectRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::LoginRoom_Message, SSEnterRoom_Request, SSEnterRoomRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::LoginRoom_Message, SSEnterRoom_Response, SSEnterRoomResponse::descriptor());
	
	MessageDefine::getInstance().AddIdDes(pbmsg::Room_Message, pbmsg::Room_Message, RoomMessage::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::Room_Message, CSDirectMove_SyncUp, CSDirectMoveSyncUp::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::Room_Message, CSDirectMove_SyncDown, CSDirectMoveSycnDown::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::Room_Message, RoomEnterRoom_Request, RoomEnterRoomRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::Room_Message, Born_Request, BornRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::Room_Message, CSEatEnemy_Request, CSEatEnemyRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::Room_Message, CSGetInitSync_Request, CSGetInitSyncRequest::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::Room_Message, CSExitRoom_Request, CSExitRoomRequest::descriptor());
    MessageDefine::getInstance().AddIdDes(pbmsg::Room_Message, CSSplit_Request, CSSplitRequest::descriptor());

	
	MessageDefine::getInstance().AddIdDes(pbmsg::Room_Message, CSEatMine_SyncUp, CSEatMineSyncUp::descriptor());
}

