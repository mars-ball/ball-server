#include "MoveCalculate.h"
#include "common/Logging.h"

using namespace fsk;

float generateNewRadius(float R1, float R2){

	if(R2 < 0){
		if(R1 - R2 < 0){
			LOG_ERROR << "change r, R1:"<< R1 << " less than R2:"<<R2;
		}
		return sqrt(R1*R1 - R2*R2);
	}

	return sqrt(R1*R1 + R2*R2);


}