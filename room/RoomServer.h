#pragma once
#include "net/ProtobufServer.h"
#include "user/UserManager.h"
#include "MessageRegister.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "net/MessageDefine.h"
#include "net/Dispatcher.h"
#include <vector>
#include <string>
#include <set>
#include <map>
#include "hredis/RedConnection.h"
#include "common/ServerList.h"

#include <algorithm>

using namespace std;


class RoomManager;
typedef shared_ptr<RoomManager> RoomManagerPtr;
typedef shared_ptr<RedConnection> RedConnectionPtr;


namespace fsk{
namespace net{

class RoomServer:public ProtobufServer{
public:

    RoomServer(string name, EventLoop* loop, const InetAddress& listenAddr);

	int32_t initAll();

	bool isInWhiteList(pbmsg::ServerType type, std::string ip);

	void sendServerMessageByConnection(const TcpConnectionPtr& conn, MessagePtr msg);

	void sendServerMessageByUID(int32_t uid, MessagePtr msg);

	TcpConnectionPtr getServerConnectionBySid(int32_t sid);

	void UpdateServerConnection(int32_t sid, TcpConnectionPtr conn);

	void SendHeartBeat();

	void setServerId(int32_t serverId);
	int32_t getServerId();


	int32_t SetWhiteList(std::map<pbmsg::ServerType, std::vector<std::string>>&& list);

	RoomManagerPtr getRoomManager();

private:

    MessageRegister msgRegister_;
	
	std::map<pbmsg::ServerType, std::vector<std::string>>  serverWhiteList_;

	int32_t serverId_;
		
	RoomManagerPtr roomManager_;
	RedConnectionPtr red_;
	


	const int kHeartBeatGap = 3000;

public:
	ServerListPtr serverList_;

};



}
}
