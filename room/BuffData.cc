#include "BuffData.h"

int32_t BuffDataInfo::loadBuffData() {
	string dataFileName = tmp::BUFF_FILE_NAME;
	fstream fin(dataFileName, ios::in | ios::binary); 
    BUFF_INFO_ARRAY infoArr;
    infoArr.ParseFromIstream(&fin);
    for (int32_t ii = 0; ii < infoArr.items_size(); ii++) {
    	BUFF_INFOPtr info = BUFF_INFOPtr(new BUFF_INFO());
    	*info = infoArr.items(ii);
    	int32_t buffId = info->buff_id();
    	if (buffs_.find(buffId) != buffs_.end()) {
    		LOG_ERROR << "have two same buff id:" << buffId;
    		return -1;
		}
		
	    buffs_[buffId] = info;
    }
	fin.close();
	return 0;
}

BUFF_INFOPtr BuffDataInfo::getById(int32_t buffId){
	if(buffs_.find(buffId) == buffs_.end()){
		LOG_ERROR << "no buff type: " <<buffId;
		return BUFF_INFOPtr(NULL);
	}
	
	return buffs_[buffId];
}
