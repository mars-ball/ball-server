#pragma once

#include "btBulletDynamicsCommon.h"
#include "LinearMath/btVector3.h"
#include "LinearMath/btAlignedObjectArray.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"

#include <map>
#include <set>
#include <memory>
#include <iostream>
#include <functional>

using namespace std;

typedef function<void(int32_t uid0, int32_t unitId0, int32_t uid1, int32_t unitId1)> GameWorldCollisionCallback;
typedef function<void(int32_t uid, int32_t unitId, int32_t moId)> PlayerMoCollisionCallback;

enum ColTag{
    COLTAG_NOTHING = 0,
    COLTAG_GROUND = 1,
    COLTAG_BORDER = 2,
    COLTAG_PLAYER = 4,
};

struct PlayerData{
    PlayerData(int uid, int unitId):
    m_id(uid),
    m_unitId(unitId){
    }
    int m_id;
    int m_unitId;
};

typedef std::pair<int, int> BattleID;

struct GameWorldEngine: public std::enable_shared_from_this<GameWorldEngine>{
    
    static map<btDynamicsWorld*, GameWorldEngine*> m_worlds;

    btBroadphaseInterface*  m_broadphase;
    btCollisionDispatcher*  m_dispatcher;
    btConstraintSolver* m_solver;
    btDefaultCollisionConfiguration* m_collisionConfiguration;
    btDiscreteDynamicsWorld* m_dynamicsWorld;
    btGhostPairCallback* m_ghostPairCallback;  
    GameWorldCollisionCallback m_collisionCallback;
    PlayerMoCollisionCallback m_playerMoCallback;

    btVector3 m_mapSize;
    
    btAlignedObjectArray<btRigidBody*> m_gameMap;

    std::map<int, btCollisionObject*> m_mines;
    std::set<const btCollisionObject*> m_mineSet;
    
    btAlignedObjectArray<btCollisionShape*> m_shapes;
    
    std::map<const void*, BattleID> m_rig2id;
    std::map<BattleID, void*> m_id2rig;
    
    btRigidBody* getPlayerRigidById(int uid, int unitId);
    
    void init(float width, float height);
    void initBoder();
    void initDynamicWorld();
    void setCollisionCallback(GameWorldCollisionCallback callback);
    void setPlayerMoCallback(PlayerMoCollisionCallback callback);
    
    btRigidBody* createRigidBody(float mass, const btTransform& startTransform, btCollisionShape* shape);
    void removePlayer(int uid, int unitId);
    bool playerBorn(int uid, int unitId, int R, btVector3 pos);
    bool splitPlayer(int uid, int unitId, int newUnitId, float newR, btVector3 pos, btVector3 dir, const float& newSpeed);
    bool combinePlayer(pair<int, int> unit, set<pair<int, int>> subs, float newR);
    bool changePlayerRadius(int uid, int unitId, int newR);
    bool changePlayerRadius(int uid, int unitId, float newR);

    bool setUnitFollow(int uid0, int unitId0, int uid1, int unitId1, btVector3 baseSpeed, btVector3& outSpeed);
    bool setUnitSpeed(int uid, int unitId, btVector3 speed);
    bool getUnitSpeed(int uid, int unitId, btVector3& speed);
    bool getUnitTransform(int uid, int unitId, btTransform& tran);
    bool setUnitPos(int uid, int unitId, btVector3 pos);
    bool getUnitPos(int uid, int unitId, btVector3& pos);
    bool getUnitDistance(int uid0, int unitId0, int uid1, int unitId1, float& dist);
    
    bool mineBorn(int mineId, int R, btVector3 pos);
    void removeMine(int mineId);

    bool stepSimulation(float timeStep, int steps, float fixedStep);


    static const float BODER_THICKNESS;
    static const float PLAYER_RIGID_HEIGHT;
    static const float SPLIT_FOLLOW_SPEED;
    
};

typedef std::shared_ptr<GameWorldEngine> GameWorldEnginePtr;

