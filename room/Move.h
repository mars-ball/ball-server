#pragma once
#include "HMath.h"
#include "common/Logging.h"
#include <vector>
#include <set>
#include <memory>
#include <algorithm>
#include <iostream>
#include "net/Callbacks.h"


using namespace fsk;
using namespace fsk::net;
using namespace std;

class MapRect;
typedef shared_ptr<MapRect> MapRectPtr;

class MoveUnit;
typedef shared_ptr<MoveUnit> MoveUnitPtr;

class MoveRuntimeInfo;
typedef shared_ptr<MoveRuntimeInfo> MoveRuntimeInfoPtr;

enum MoveUnitState{
	MOVE_UNIT_STATE_INVALID,
	MOVE_UNIT_STATE_NORMAL,
	MOVE_UNIT_STATE_FLYING,
	MOVE_UNIT_STATE_COMBINING,
	MOVE_UNIT_STATE_WAITFORCOMBINE
};

class MoveUnit{
public:
	int32_t id;
	Point pos;
	Vec oldV;
	Vec currV;
	Vec a;
	float r;
	float w;
	set<int32_t> besideUnits;

	bool needSyncSpeed;
	int64_t flyExistTime;
	int64_t combineTime;
	weak_ptr<MoveRuntimeInfo> root_;

	MoveUnitState m_state;

	//vector<Vec> compV;
	//vector<Vec> compA;

	static int32_t unitId;

	MoveUnit(MoveRuntimeInfoPtr root)
	:pos(0, 0),
	oldV(0, 0),
	currV(0, 0),
	a(0, 0),
	r(0),
	w(0),
	needSyncSpeed(true),
	id(unitId++),
	root_(root),
	m_state(MOVE_UNIT_STATE_NORMAL)
	{
	}

	MoveUnit(MoveUnitPtr p, bool isNew = false)
	:id(p->id),
	pos(p->pos.x, p->pos.y),
	oldV(p->oldV.x, p->oldV.y),
	currV(p->currV.x, p->currV.y),
	a(p->a.x, p->a.y),
	r(p->r),
	w(p->w),
	needSyncSpeed(p->needSyncSpeed),
	m_state(p->m_state),
	root_(p->root_)
	{
		if(isNew){
			id = unitId++;
		}
	}


	void copyFrome(const MoveUnitPtr& p){
		id = p->id;
		pos = Point(p->pos.x, p->pos.y);
		oldV = Vec(p->oldV.x, p->oldV.y);
		currV = Vec(p->currV.x, p->currV.y);
		a = Vec(p->a.x, p->a.y);
		r = p->r;
		w = p->w;
		needSyncSpeed = p->needSyncSpeed;
		m_state = p->m_state;
		root_ = p->root_;
	}


	~MoveUnit();

	MapRectPtr getMapRect();

	void step(int64_t deltaTime);

	void revSpeed(float timeDelta);

	void revPos();

	/*
	void setExistTime(int64_t time){
		existTime = time;
	}*/

	/*void setRoot(MoveRuntimeInfoPtr& ptr){
		root_ = ptr;
	}*/

	void clearCompSpeed();

};


enum MoveUrgentEventType{
	MOVE_URGENT_EVENT_TYPE_INVALID = 0,
	MOVE_URGENT_EVENT_TYPE_SPLIT = 1,
};

struct MoveSplitEvent{
    bool isStart; //true, false:end
    int32_t uid;
    int32_t unitId;
    int32_t newUnitId;
    int64_t timeline;
};


struct MoveUrgentEvent{
	MoveUrgentEventType type;
	union Event{
		MoveSplitEvent split;
	};
	Event event;
};





class MoveRuntimeInfo: public std::enable_shared_from_this<MoveRuntimeInfo>{
public:
    int32_t uid;
	bool isSingle;
	vector<MoveUnitPtr> units_;
	MapRectPtr mapRect_;
	CallBackWithNoParamter noUnitCallback_;
	vector<MoveUrgentEvent> urgenEvents_;

	MoveRuntimeInfo(){}
	
	MoveRuntimeInfo(const int32_t& _uid, const MapRectPtr& mapRect)
	:uid(_uid),
    mapRect_(mapRect),
	isSingle(true){
	}

	MoveRuntimeInfo(MoveRuntimeInfoPtr p)
	:uid(p->uid),
    isSingle(p->isSingle),
	mapRect_(p->mapRect_)
	{
		for(auto it = p->units_.begin(); it != p->units_.end(); ++it){
			MoveUnitPtr newU(new MoveUnit(*it));
			units_.push_back(newU);
		}
	}

	void resetUnitRoot(){
		for_each(units_.begin(), units_.end(), [&](MoveUnitPtr &unit)
			{
				unit->root_ = shared_from_this();
			}
		);
	}

	MoveRuntimeInfoPtr getPtr(){
		return shared_from_this();
	}

	MoveUnitPtr getMainUnit();

	void addUnit(MoveUnitPtr unit){
		unit->root_ = shared_from_this();
		units_.push_back(unit);
	}

	void updateUnit(MoveUnitPtr unit){
		bool found = false;
		for(auto it = units_.begin(); it != units_.end(); ++it){
			if((*it)->id == unit->id){
				*it = unit;
				found = true;
				break;
			}
		}
		if(!found){
			units_.push_back(unit);
		}

	}

	void setAllAV(Vec v, Vec a){
		for(auto it = units_.begin(); it != units_.end(); ++it){
			MoveUnitPtr unit = *it;
			unit->currV = v;
			unit->a = a;
		}
	}

	bool setMainV(Vec v);

	float getBiggestRadius();

	MoveUnitPtr getBiggestUnit();


	void step(int64_t nowTime, int64_t deltaTime);

	MoveUnitPtr getUnitById(int32_t unitId);

	void removeUnit(int32_t unitId);
	void removeAllUnits();

	float getMaxUnitR();

	float getMaxV();

};

typedef shared_ptr<MoveRuntimeInfo> MoveRuntimeInfoPtr;



struct JoyStickInput{
	Vec dir;
	float dis;
};



class Move{
public:
	static int32_t getSpeedFromJoy(JoyStickInput& joy, Vec& speed);
	static float velocityPercent(float r);
	static int32_t getSpeedFromDir(Vec dir, Vec& speed);



    static const float kSplitSpeed;
    static const float kSplitDamping;
	static const float kMaxSpeed;
	static const float kJoyStickSpeed;
	static const float kLowestSpeedPercent;



};
