#include "CSGetInitSyncProcess.h"
#include "net/ProtobufServer.h"
#include "net/Dispatcher.h"
#include "RoomServer.h"
#include "protobuf/LoginRoomMessage.pb.h"
#include "user/UserManager.h"
#include <vector>
#include "Room.h"
#include "MapRect.h"

using namespace fsk;
using namespace fsk::net;
using namespace pbmsg;
using namespace pbmsg::room;

void CSGetInitSyncProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){
	shared_ptr<RoomMessage> message =
			std::static_pointer_cast<RoomMessage>(_message);
	const CSGetInitSyncRequest req = message->request().initsync();
	pbmsg::Ret csRet = Ret::Failed;

	RoomServer * roomServer = NULL;
	RoomPtr room;
	MapRectPtr mapRect = NULL;

	do{
		int32_t ret;

		roomServer = dynamic_cast<RoomServer*>(server);
		if(roomServer == NULL){
			LOG_ERROR << "cast null";			
			break;
		}

		int32_t uid;

		ret = roomServer->getUserIdByConnection(conn, uid);
		if(ret != 0){
			LOG_ERROR<<"no id conn record:";
			return;
		}

		RoomManagerPtr roomManager = roomServer->getRoomManager();

		if(roomManager == NULL){
			LOG_ERROR <<"room server manager null";
			break;
		}

		ret = roomManager->getPlayerRoom(uid, room);
		if(ret != 0){
			LOG_ERROR << "get player room failed, uid:"<<uid;
			break;
		}

		mapRect = room->mapRect_;
		if(mapRect.get() == NULL){
			LOG_ERROR <<"map rect module is null, room id"<<room->roomId_;
			break;
		}

		csRet = Ret::Success;
	}while(0);
	
    shared_ptr<RoomMessage> msg(new RoomMessage());
   	msg->set_msgtype(CSGetInitSync_Response);
	msg->set_seq(message->seq());
    Response *response = msg->mutable_response();
    response->set_ret(csRet);
    CSGetInitSyncResponse *rsp = response->mutable_initsync();
    if(csRet == Ret::Success){
	    for(auto it = room->players_.begin(); it != room->players_.end(); ++it){	
	    	if(it->second->battleType_ == BATTLE_OBJECT_TYPE_PLAYER 
	    		&& room->getPlayerState(it->first)!= PlayerState::PLAYING){
				continue;
			}

			BattleObjectInfoPtr player = room->getBattleObjectInfoById(it->first);

    		CSGetInitSyncResponse_User *user = rsp->add_users();
    		user->set_uid(player->id);
    		user->set_photoid(player->photoId);
    		user->set_name(player->name);
	    				
		}

		rsp->set_minenum(mapRect->mineNum_);

		for (auto it = mapRect->mines_.begin(); it != mapRect->mines_.end(); ++it)
		{
                CSBasicMinePos* mineMove = rsp->add_mines();
                MinePtr mine = it->second;
				mineMove->set_mid(it->first);
				mineMove->set_type(MINE_KIND_NORMAL);
				mineMove->set_color(mine->color_);
				Vector* pos = mineMove->mutable_pos();
				pos->set_x(mine->pos_.x);
				pos->set_y(mine->pos_.y);
		}

/*
		std::for_each(mapRect->mines_.begin(), mapRect->mines_.end(), 
                [](std::pair<int32_t, MinePtr>& item){

           });*/


    }    

	roomServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	


}
