#include "GameWorld.h"
#include "common/Logging.h"
#include <algorithm>

using namespace std;
using namespace fsk;


extern ContactAddedCallback     gContactAddedCallback;

int groundCollideWith = COLTAG_NOTHING;
int playerCollideWith = COLTAG_BORDER | COLTAG_PLAYER | btBroadphaseProxy::SensorTrigger;
int borderCollideWith = COLTAG_PLAYER;


const float GameWorldEngine::BODER_THICKNESS = 5.0;
const float GameWorldEngine::PLAYER_RIGID_HEIGHT = 1.0;
//const float GameWorldEngine::SPLIT_FLY_SPEED = 1.6;
//const float GameWorldEngine::SPLIT_DAMPING = 0.4;
const float GameWorldEngine::SPLIT_FOLLOW_SPEED = 10;

map<btDynamicsWorld*, GameWorldEngine*> GameWorldEngine::m_worlds;

struct BroadFilterCallback : public btOverlapFilterCallback {
    
    std::shared_ptr<GameWorldEngine> m_gameWorld;
    
    BroadFilterCallback(GameWorldEnginePtr gameWorld)
    :m_gameWorld(gameWorld){
    }
    
    // return true when pairs need collision
    virtual bool needBroadphaseCollision(btBroadphaseProxy* proxy0,btBroadphaseProxy* proxy1) const {
        return true;
        bool collides = (proxy0->m_collisionFilterGroup & proxy1->m_collisionFilterMask) != 0;
        collides = collides && (proxy1->m_collisionFilterGroup & proxy0->m_collisionFilterMask);
        
        if(!collides){
            return false;
        }
        
        void* collider0 = proxy0->m_clientObject;
        void* collider1 = proxy1->m_clientObject;
        
        if(m_gameWorld->m_rig2id.find(collider0) == m_gameWorld->m_rig2id.end()||
           m_gameWorld->m_rig2id.find(collider1) == m_gameWorld->m_rig2id.end()){
            return true;
        }
        
        if(m_gameWorld->m_rig2id[collider0].first == m_gameWorld->m_rig2id[collider1].first){
            return true;
        }
        std::cout <<"not collide"<<std::endl;
        return false;
    }
};

class FCollisionDispatcher : public btCollisionDispatcher
{
public:
    FCollisionDispatcher(shared_ptr<GameWorldEngine> gameWorld, btCollisionConfiguration* config ) 
    : btCollisionDispatcher( config ),
        m_gameWorld(gameWorld)
    {
    }

    virtual ~FCollisionDispatcher()
    {
    }

    weak_ptr<GameWorldEngine> m_gameWorld;

    bool   needsResponse(const btCollisionObject* collider0, const btCollisionObject* collider1){
        auto gameWorld = m_gameWorld.lock();
        if(gameWorld->m_rig2id.find(collider0) == gameWorld->m_rig2id.end()||
           gameWorld->m_rig2id.find(collider1) == gameWorld->m_rig2id.end()){
            return false;
        }
        if(gameWorld->m_rig2id[collider0].first != gameWorld->m_rig2id[collider1].first){
            /*LOG_INFO << "no rsp, u0:" <<m_gameWorld->m_rig2id[collider0].first
                    << ", " << m_gameWorld->m_rig2id[collider0].second
                    <<" u1:" <<m_gameWorld->m_rig2id[collider1].first
                    <<", " << m_gameWorld->m_rig2id[collider1].second;*/
            return false;
        }

        return true;
    }



    /*
    virtual void dispatchAllCollisionPairs( btOverlappingPairCache* pairCache, const btDispatcherInfo& info, btDispatcher* dispatcher ) BT_OVERRIDE
    {
        int grainSize = 40;  // iterations per task
        int pairCount = pairCache->getNumOverlappingPairs();
        Updater updater;
        updater.mCallback = getNearCallback();
        updater.mPairArray = pairCount > 0 ? pairCache->getOverlappingPairArrayPtr() : NULL;
        updater.mDispatcher = this;
        updater.mInfo = &info;

        btPushThreadsAreRunning();
        parallelFor( 0, pairCount, grainSize, updater );
        btPopThreadsAreRunning();
    }*/
};



/*
static bool btRigidBodySoftContactCallback(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0Wrap,int partId0,int index0,const btCollisionObjectWrapper* colObj1Wrap,int partId1,int index1)
{
    const btCollisionObject* co0 =  colObj0Wrap->getCollisionObject();
    //btVector3 speed0 = co0->getInterpolationLinearVelocity();
    const btCollisionObject* co1 =  colObj0Wrap->getCollisionObject();

    GameWorldEngine* engine0 = static_cast<GameWorldEngine*>(co0->getUserPointer());
    GameWorldEngine* engine1 = static_cast<GameWorldEngine*>(co1->getUserPointer());

    if(engine0 == NULL || engine1 == NULL){
        return false;
    }

    if(engine1 != engine0){
        LOG_ERROR<<"two collision from different";
        return false;
    }


    const btRigidBody* rb0 = btRigidBody::upcast(co0);
    const btRigidBody* rb1 = btRigidBody::upcast(co1);

    if(engine0->m_rig2id.find(rb0) == engine0->m_rig2id.end()
        || engine0->m_rig2id.find(rb1) == engine0->m_rig2id.end()){
        LOG_ERROR <<"rb0 or rb1 not in world";
        return false;
    }

    BattleID u0 = engine0->m_rig2id[rb0];
    BattleID u1 = engine0->m_rig2id[rb1];

    if(u0.first == u1.first){
        return true;
    }

   // if(engine0->m_collisionCallback != NULL){
     //   engine0->m_collisionCallback(u0.first, u0.second, u1.first, u1.second);
    //}
    return false; 

}*/






static void worldTickCallback(btDynamicsWorld* dynamicWorld, btScalar timeStep){

    if(GameWorldEngine::m_worlds.find(dynamicWorld) == GameWorldEngine::m_worlds.end()){
        LOG_ERROR << "dynamic world not in worlds map";
        return;
    }

    GameWorldEngine* engine = GameWorldEngine::m_worlds[dynamicWorld];

    int numManifolds = dynamicWorld->getDispatcher()->getNumManifolds();
    for(int i = 0; i < numManifolds; i++){
        btPersistentManifold* contactManifold = dynamicWorld->getDispatcher()->getManifoldByIndexInternal(i);
        const btCollisionObject* co0 = contactManifold->getBody0();
        const btCollisionObject* co1 = contactManifold->getBody1();

        if(engine->m_mineSet.find(co0) != engine->m_mineSet.end()
            || engine->m_mineSet.find(co1) != engine->m_mineSet.end()){
            continue;
        }

        const btRigidBody* rb0 = btRigidBody::upcast(co0);
        const btRigidBody* rb1 = btRigidBody::upcast(co1);

        if(engine->m_rig2id.find(rb0) == engine->m_rig2id.end()
        || engine->m_rig2id.find(rb1) == engine->m_rig2id.end()){
            continue;
        }

        BattleID u0 = engine->m_rig2id[rb0];
        BattleID u1 = engine->m_rig2id[rb1];

        if(engine->m_collisionCallback != NULL){
            engine->m_collisionCallback(u0.first, u0.second, u1.first, u1.second);
        }
    }

    for(auto it = engine->m_mines.begin(); it != engine->m_mines.end(); it++){
        int mid = it->first;
        btGhostObject* mine = btGhostObject::upcast(it->second);

        for(int ii = 0; ii < mine->getNumOverlappingObjects(); ii++)
        {
            btRigidBody *rb = btRigidBody::upcast(mine->getOverlappingObject(ii));
            if(engine->m_rig2id.find(rb) != engine->m_rig2id.end()){
                engine->m_playerMoCallback(engine->m_rig2id[rb].first, engine->m_rig2id[rb].second, mid);
                

                break;
            }
        }
    }
}



void GameWorldEngine::setCollisionCallback(GameWorldCollisionCallback callback){
    m_collisionCallback = callback;
}

void GameWorldEngine::setPlayerMoCallback(PlayerMoCollisionCallback callback){
    m_playerMoCallback = callback;
}

void GameWorldEngine::init(float width, float height){
    m_mapSize = btVector3(width/2., 0, height/2.);
    initDynamicWorld();
    initBoder();
    GameWorldEngine::m_worlds[m_dynamicsWorld] = this;
}

void GameWorldEngine::initBoder(){
    btBoxShape* borderXShape = new btBoxShape(btVector3(btScalar(BODER_THICKNESS), btScalar(BODER_THICKNESS), m_mapSize.z() + BODER_THICKNESS*2));
    m_shapes.push_back(borderXShape);
    
    btTransform borderXTransform;
    borderXTransform.setIdentity();
    borderXTransform.setOrigin(btVector3(m_mapSize.x()*2 + BODER_THICKNESS, 0, m_mapSize.z()));
    
    {
        btScalar mass(0.);
        btRigidBody* body = createRigidBody(mass, borderXTransform, borderXShape);
        body->setRestitution(btScalar(0.));
        body->setFriction(btScalar(0.));
        body->setRollingFriction((0.));
        int flags = body->getCollisionFlags();
        m_dynamicsWorld->addRigidBody(body, COLTAG_BORDER, borderCollideWith);
        //body->setCollisionFlags(flags | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
    }
    
    borderXTransform.setOrigin(btVector3(-BODER_THICKNESS, 0, m_mapSize.z()));
    {
        btScalar mass(0.0);
        btRigidBody* body = createRigidBody(mass, borderXTransform, borderXShape);
        body->setRestitution(btScalar(0.));
        body->setFriction(btScalar(0.));
        body->setRollingFriction((0.));
        m_dynamicsWorld->addRigidBody(body, COLTAG_BORDER, borderCollideWith);
    }
    
    btBoxShape* borderYShape = new btBoxShape(btVector3(m_mapSize.x(), btScalar(BODER_THICKNESS), btScalar(BODER_THICKNESS)));
    
    m_shapes.push_back(borderYShape);
    
    btTransform borderYTransform;
    borderYTransform.setIdentity();
    borderYTransform.setOrigin(btVector3(m_mapSize.x(), 0, m_mapSize.z()*2.0 + BODER_THICKNESS));
    
    {
        btScalar mass(0.);
        btRigidBody* body = createRigidBody(mass, borderYTransform, borderYShape);
        body->setRestitution(btScalar(0.));
        body->setFriction(btScalar(0.));
        body->setRollingFriction((0.));
        m_dynamicsWorld->addRigidBody(body, COLTAG_BORDER, borderCollideWith);
    }
    
    borderYTransform.setOrigin(btVector3(m_mapSize.x(), 0, -BODER_THICKNESS));
    {
        btScalar mass(0.0);
        btRigidBody* body = createRigidBody(mass, borderYTransform, borderYShape);
        body->setRestitution(btScalar(0.));
        body->setFriction(btScalar(0.));
        body->setRollingFriction((0.));
        m_dynamicsWorld->addRigidBody(body, COLTAG_BORDER, borderCollideWith);
    }

    

    btBoxShape* borderXYShape = new btBoxShape(btVector3(m_mapSize.x(), btScalar(BODER_THICKNESS), m_mapSize.z()));
    
    m_shapes.push_back(borderXYShape);
    
    btTransform borderXYTransform;
    borderXYTransform.setIdentity();
    borderXYTransform.setOrigin(btVector3(m_mapSize.x(), -15, m_mapSize.z()));
    
    {
        btScalar mass(0.);
        btRigidBody* body = createRigidBody(mass, borderXYTransform, borderXYShape);
        body->setRestitution(btScalar(0.));
        body->setFriction(btScalar(0.));
        body->setRollingFriction((0.));
        m_dynamicsWorld->addRigidBody(body);
    }
    
    
}

btRigidBody* GameWorldEngine::createRigidBody(float mass, const btTransform& startTransform, btCollisionShape* shape)
{
    btAssert((!shape || shape->getShapeType() != INVALID_SHAPE_PROXYTYPE));
    
    m_shapes.push_back(shape);
    
    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);
    
    btVector3 localInertia(0, 0, 0);
    if (isDynamic)
        shape->calculateLocalInertia(mass, localInertia);
    
    //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
    
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    
    btRigidBody::btRigidBodyConstructionInfo cInfo(mass, myMotionState, shape, localInertia);
    
    btRigidBody* body = new btRigidBody(cInfo);
    body->setAngularFactor(btVector3(0, 0, 0));
    body->setLinearFactor(btVector3(1., 0., 1.));
    
    //body->setContactProcessingThreshold(m_defaultContactProcessingThreshold);
    
    body->setUserIndex(-1);
    
    return body;
}


void GameWorldEngine::initDynamicWorld(){
    ///collision configuration contains default setup for memory, collision setup
    m_collisionConfiguration = new btDefaultCollisionConfiguration();

    //gContactAddedCallback = btRigidBodySoftContactCallback;

    //m_collisionConfiguration->setConvexConvexMultipointIterations();
    
    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    m_dispatcher = new  FCollisionDispatcher(shared_from_this(), m_collisionConfiguration);
    //m_dispatcher->setNearCallback(MyNearCallback);
    
    m_broadphase = new btDbvtBroadphase();
    
    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    btSequentialImpulseConstraintSolver* sol = new btSequentialImpulseConstraintSolver;
    m_solver = sol;
    
    m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher, m_broadphase, m_solver, m_collisionConfiguration);
    
    m_dynamicsWorld->getSolverInfo().m_erp2 = 0.f;
    m_dynamicsWorld->getSolverInfo().m_globalCfm = 0.f;
    m_dynamicsWorld->getSolverInfo().m_numIterations = 3;
    m_dynamicsWorld->getSolverInfo().m_solverMode = SOLVER_SIMD;// | SOLVER_RANDMIZE_ORDER;
    m_dynamicsWorld->getSolverInfo().m_splitImpulse = false;
    
    m_dynamicsWorld->setGravity(btVector3(0, 0, 0));

    m_ghostPairCallback = new btGhostPairCallback(); 
    m_dynamicsWorld->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(m_ghostPairCallback);


    btOverlapFilterCallback* broadFilterCallback = new BroadFilterCallback(shared_from_this());
    //m_dynamicsWorld->getPairCache()->setOverlapFilterCallback(broadFilterCallback);
    m_dynamicsWorld->setInternalTickCallback(worldTickCallback);


}


bool GameWorldEngine::playerBorn(int uid, int unitId, int R, btVector3 pos){
    
    btCylinderShape* colShape = new btCylinderShape(btVector3(R, PLAYER_RIGID_HEIGHT, R));
    colShape->setMargin(btScalar(0.5));
    m_shapes.push_back(colShape);

    btTransform startTransform;
    startTransform.setIdentity();
    
    btScalar mass(1.f);
    
    //pos[1] = PLAYER_RIGID_HEIGHT;
    
    startTransform.setOrigin(pos);
    
    btRigidBody* body = createRigidBody(mass, startTransform, colShape);
    
    body->setActivationState(DISABLE_DEACTIVATION);

    //body->setUserPointer(this);
    
    int flags = body->getCollisionFlags();
    body->setCollisionFlags(flags | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
    
    m_dynamicsWorld->addRigidBody(body, COLTAG_PLAYER, playerCollideWith);
    
    m_rig2id[body] = std::make_pair(uid, unitId);
    m_id2rig[std::make_pair(uid, unitId)] = body;
    //PlayerData* playerData = new PlayerData(uid, unitId);
    
    return true;
    
}


void GameWorldEngine::removePlayer(int uid, int unitId){
    
    btRigidBody* player = getPlayerRigidById(uid, unitId);
    
    if(player == NULL){
        std::cout <<"player is null, uid:"<<uid <<"unitId:"<<unitId<<std::endl;
        return;
    }
    
    BattleID id = std::make_pair(uid, unitId);
    if(m_rig2id.find(player) != m_rig2id.end()){
        m_rig2id.erase(player);
    }
    
    if(m_id2rig.find(id) != m_id2rig.end()){
        m_id2rig.erase(id);
    }
    
    btCollisionShape* shape = player->getCollisionShape();
    
    m_shapes.remove(shape);
    
    if (player && player->getMotionState())
    {
        delete player->getMotionState();
    }
    
    m_dynamicsWorld->removeRigidBody(player);
    
    /*if(player->getUserPointer() != NULL){
        delete (PlayerData*)(player->getUserPointer());
    }*/
    
    
    delete player;
    delete shape;
    
}

void GameWorldEngine::removeMine(int mineId){
    
    if(m_mines.find(mineId) == m_mines.end()){
        LOG_ERROR <<"remove a nonexist mine, mineId:"<<mineId;
        return;
    }

    btCollisionObject* mine = m_mines[mineId];

    if(mine == NULL){
        LOG_ERROR <<"mine is null, mid:"<<mineId;
        return;
    }
        
    btCollisionShape* shape = mine->getCollisionShape();
    
    m_shapes.remove(shape);
    
    m_dynamicsWorld->removeCollisionObject(mine);
    
    m_mines.erase(mineId);
    m_mineSet.erase(mine);

    delete mine;
    delete shape;
    
}




btRigidBody* GameWorldEngine::getPlayerRigidById(int uid, int unitId){
    return (btRigidBody*)m_id2rig[std::make_pair(uid, unitId)];
}

bool GameWorldEngine::splitPlayer(int uid, int unitId, int newUnitId, float newR, btVector3 pos, btVector3 dir, const float& newSpeed){
    
    dir = dir.safeNormalize();

    btRigidBody* player = getPlayerRigidById(uid, unitId);
    
    if(player == NULL){
        return false;
    }

    //btTransform trans = player->getInterpolationWorldTransform();
    
    //btVector3 oldPos = trans.getOrigin();
    
    //btCylinderShape* shape = dynamic_cast<btCylinderShape*>(player->getCollisionShape());
    
    //btScalar radius = shape->getRadius();
    
    
    btScalar newMass(1.0);
    
    //btCylinderShape* new0Shape = new btCylinderShape(btVector3(newR, PLAYER_RIGID_HEIGHT, newR));
    btCylinderShape* new1Shape = new btCylinderShape(btVector3(newR, PLAYER_RIGID_HEIGHT, newR));
    
    //m_shapes.push_back(new0Shape);
    m_shapes.push_back(new1Shape);
    
    //btVector3 oldSpeed = player->getLinearVelocity();
    //newSpeed = dir.safeNormalize()*oldSpeed.length() * SPLIT_FLY_SPEED;
    
    //btVector3 newPos = oldPos + dir.safeNormalize()*(newR*2 + 0.1);
    
   // printf("cylinder r: %lf", shape->getRadius());
    
    //removePlayer(uid, unitId);
    
    //btRigidBody* newUnit0 = createRigidBody(newMass, trans, new0Shape);
    
    //newUnit0->setActivationState(DISABLE_DEACTIVATION);
    
    //m_dynamicsWorld->addRigidBody(newUnit0, COLTAG_PLAYER, playerCollideWith);
    
    //newUnit0->setLinearVelocity(oldSpeed);
    
    //int flags = newUnit0->getCollisionFlags();
    //newUnit0->setCollisionFlags(flags | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);


    //m_rig2id[newUnit0] = std::make_pair(uid, unitId);
    //m_id2rig[std::make_pair(uid, unitId)] = newUnit0;
    
    changePlayerRadius(uid, unitId, newR);

    btTransform newTrans;
    newTrans.setIdentity();
    newTrans.setOrigin(pos);
    
    btRigidBody* newUnit1 = createRigidBody(newMass, newTrans, new1Shape);
    
    m_dynamicsWorld->addRigidBody(newUnit1, COLTAG_PLAYER, playerCollideWith);
    
    newUnit1->setActivationState(DISABLE_DEACTIVATION);
    newUnit1->setLinearVelocity(dir*newSpeed);
    //newUnit1->setDamping(btScalar(SPLIT_DAMPING), btScalar(SPLIT_DAMPING));



    int flags = newUnit1->getCollisionFlags();
    newUnit1->setCollisionFlags(flags | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);

    m_rig2id[newUnit1] = std::make_pair(uid, newUnitId);
    m_id2rig[std::make_pair(uid, newUnitId)] = newUnit1;
    
    //m_guiHelper->autogenerateGraphicsObjects(m_dynamicsWorld);
    return true;
}

bool GameWorldEngine::getUnitTransform(int uid, int unitId, btTransform& tran){
    
    btRigidBody* player = getPlayerRigidById(uid, unitId);
    
    if(player == NULL){
        return false;
    }
    
    tran = player->getInterpolationWorldTransform();
    
    return true;
    
}

bool GameWorldEngine::getUnitPos(int uid, int unitId, btVector3& pos){
    
    btTransform trans;
    if(!getUnitTransform(uid, unitId, trans)){
        std::cout << "unit transform failed";
        return false;
    }
    
    pos = trans.getOrigin() ;

    return true;
    
}

bool GameWorldEngine::setUnitPos(int uid, int unitId, btVector3 pos){
    
    btTransform trans;
    if(!getUnitTransform(uid, unitId, trans)){
        std::cout << "unit transform failed";
        return false;
    }
    
    trans.setOrigin(pos) ;

    return true;
    
}


bool GameWorldEngine::getUnitSpeed(int uid, int unitId, btVector3& speed){
    btRigidBody* player = getPlayerRigidById(uid, unitId);
    
    if(player == NULL){
        std::cout <<"get player failed"<<std::endl;
        return false;
    }

    speed = player->getLinearVelocity();
    return true;
}

bool GameWorldEngine::setUnitSpeed(int uid, int unitId, btVector3 speed){
    
    btRigidBody* player = getPlayerRigidById(uid, unitId);
    
    if(player == NULL){
        std::cout <<"get player failed"<<std::endl;
        return false;
    }
    
    player->setLinearVelocity(speed);
    
    return true;
}

bool GameWorldEngine::setUnitFollow(int uid0, int unitId0, int uid1, int unitId1, btVector3 baseSpeed, btVector3& outSpeed){
    
    btVector3 pos0;
    
    if(!getUnitPos(uid0, unitId0, pos0)){
        std::cout <<"get unit pos failed, uid:"<<uid0<<" unitId:"<<unitId0<<std::endl;
    }
    
    btRigidBody* player1 = getPlayerRigidById(uid1, unitId1);
    
    if(player1 == NULL){
        std::cout <<"get player null, uid0:"<< uid0 << "unitId0:" << unitId0 << "uid1:"<<uid1<<"unitId1:"<<unitId1<<std::endl;
        return false;
    }
    
    player1->setDamping(0., 0.);
    btVector3 pos1 = player1->getInterpolationWorldTransform().getOrigin();
    
    btVector3 speed = (pos0 - pos1).safeNormalize() * SPLIT_FOLLOW_SPEED;
    
    outSpeed = speed + baseSpeed;

    player1->setLinearVelocity(outSpeed);
    
    return true;
    
}



bool GameWorldEngine::combinePlayer(pair<int, int> unit, set<pair<int, int>> subs, float newR){
    
    if(subs.find(unit) != subs.end()){
        LOG_ERROR <<"combine self, uid:"<<unit.first << " unitId:"<<unit.second;
        return false;
    }

    btVector3 pos;
    if(!getUnitPos(unit.first, unit.second, pos)){
        std::cout<<"get unit pos failed, uid:"<<unit.first<<" unitID:"<<unit.second<<std::endl;
        return false;
    }
    
    for_each(subs.begin(), subs.end(), [&](const pair<int, int>& subId){
        removePlayer(subId.first, subId.second);
    });
    
    //playerBorn(unit.first, unit.second, newR, pos);
    changePlayerRadius(unit.first, unit.second, newR);

    return true;
}


bool GameWorldEngine::getUnitDistance(int uid0, int unitId0, int uid1, int unitId1, float& dist){
    
    btVector3 pos0;
    if(!getUnitPos(uid0, unitId0, pos0)){
        std::cout <<"get unit failed, uid:"<<uid0<<" unitId:"<<unitId0<<std::endl;
        return false;
    }
    
    btVector3 pos1;
    if(!getUnitPos(uid1, unitId1, pos1)){
        std::cout <<"get unit failed, uid:"<<uid1<<" unitId:"<<unitId1<<std::endl;
        return false;
    }
    
    dist = (pos1 - pos0).length();
    return true;
}

bool GameWorldEngine::stepSimulation(float timeStep, int steps, float fixedStep){
    //std::cout << "world objs:" << m_dynamicsWorld->getNumCollisionObjects()<<std::endl;
    m_dynamicsWorld->stepSimulation(timeStep, steps, fixedStep);
    return true;
}

bool GameWorldEngine::changePlayerRadius(int uid, int unitId, float newR){
    btRigidBody* player = getPlayerRigidById(uid, unitId);

    if(player == NULL){
        LOG_ERROR <<"change radius failed, uid:"<<uid<<" unitId:"<<unitId;
        return false;
    }
    
    btTransform trans = player->getInterpolationWorldTransform();
    
    btVector3 oldPos = trans.getOrigin();
    
    btCylinderShape* shape = dynamic_cast<btCylinderShape*>(player->getCollisionShape());
    
    const btVector3 oldScale = shape->getLocalScaling ();

    btScalar radius = shape->getRadius();

    btScalar oriRadius = radius / oldScale[0];
	
	float scale = newR/oriRadius;

   	shape->setLocalScaling(btVector3(scale, 1.f, scale)); 
    
    return true;
    
}

bool GameWorldEngine::mineBorn(int mineId, int R, btVector3 pos){
    
    btCylinderShape* colShape = new btCylinderShape(btVector3(R, PLAYER_RIGID_HEIGHT, R));
    m_shapes.push_back(colShape);

    btTransform startTransform;
    startTransform.setIdentity();    
    startTransform.setOrigin(pos);
    
    btGhostObject* ghostObject = new btGhostObject();
    ghostObject->setCollisionShape(colShape);
    ghostObject->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);        // We can choose to make it "solid" if we want...
    ghostObject->setWorldTransform(startTransform);

    //body->setActivationState(DISABLE_DEACTIVATION);

    m_mines[mineId] = ghostObject;
    m_mineSet.insert(ghostObject);

    m_dynamicsWorld->addCollisionObject(ghostObject,btBroadphaseProxy::SensorTrigger,btBroadphaseProxy::AllFilter & ~btBroadphaseProxy::SensorTrigger);
    
    return true;


}
















