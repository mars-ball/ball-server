SUB_MOD = MakeRouter
SUB_MAK = $(SUB_MOD:=.mk)

all:init
	$(foreach mk, $(SUB_MAK), $(MAKE) -f $(mk)) 		


init:
	echo "starting..."

.PHOY: clean

clean:
	$(foreach mk, $(SUB_MAK), $(MAKE) clean -f $(mk))
