//
//  main.cpp
//  KunServer
//
//  Created by meryn on 15/12/6.
//  Copyright (c) 2015年 meryn. All rights reserved.
//


#include "common/Logging.h"
#include "net/InetAddress.h"
#include "router/RouterServer.h" 

using namespace fsk;
using namespace fsk::net;

void foo(){
	LOG_INFO << "HAHA";
}

int main(int argc, char* argv[]){

	LOG_ERROR << "AAAAAA";
	LOG_INFO << "BBBB";
	//cout << "pid = " <<getpid();

	//MessageDefine::getInstance().Add(0, "LoginRequestMessage");
	//MessageDefine::getInstance().Add(1, "LoginResponseMessage");

	if(argc){
        
		EventLoop loop;
		uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
		InetAddress serverAddr(port);
		RouterServer server("routerServer", &loop, serverAddr);
		server.start();
				//loop.runEvery(1, bind(&foo));
		loop.loop();

	}else{
		printf("usage: %s port\n", argv[0]);

	}
}
