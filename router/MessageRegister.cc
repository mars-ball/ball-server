#include "MessageRegister.h"
#include "net/ProtobufServer.h"
#include "protobuf/HMessage.pb.h"
#include <functional>
#include "net/Dispatcher.h"
#include "CSRouteRequestProcess.h"

using namespace fsk::net;
using namespace std;
using namespace std::placeholders;
using namespace pbmsg::login;

template <typename T>
void MessageRegister::registerOneProcessMessage(int32_t topType, int32_t msgType){
	shared_ptr<T> tmp(new T);
	handlers_.push_back(tmp);
	server_->registerMessageCallback(topType, msgType, bind(&T::callback, tmp.get(), _1, _2, _3, _4 ));

}

void MessageRegister::registerSpMessage(int32_t topType, int msgType, const CallbackT::ProtobufMessageTCallback& callback){
	server_->registerMessageCallback(topType, msgType, callback);
}



void MessageRegister::registerMessages(){
	registerOneProcessMessage<CSRouteRequestProcess>(pbmsg::H_Message, pbmsg::CSRoute_Request);
	registerMessageDefines();
}


void MessageRegister::registerMessageDefines(){
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::H_Message, HMessage::descriptor());
	MessageDefine::getInstance().AddIdDes(pbmsg::H_Message, pbmsg::CSRoute_Request, CSRouteRequest::descriptor());
}






