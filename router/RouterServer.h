#pragma once
#include "net/ProtobufServer.h"
#include "MessageRegister.h"
#include "protobuf/HMessage.pb.h"
#include "net/MessageDefine.h"
#include "net/Dispatcher.h"
#include "common/bimap.h"
#include "common/Logging.h"
#include "RouterServerPoolManager.h"

using namespace std;
using namespace codeproject::bimap_detail;
	

namespace fsk{
namespace net{


class RouterServer:public ProtobufServer{
public:
    
    RouterServer(string name, EventLoop* loop, const InetAddress& listenAddr)
			:ProtobufServer(name, loop, listenAddr),
			msgRegister_(this),
			routerPool_(new RouterServerPoolManager(this))

			
	{
		dispatchers_[pbmsg::H_Message] = ProtobufDispatcherBase::ptr_t(new 
					ProtobufDispatcher<pbmsg::login::HMessage>(this, 
						bind(&ProtobufServer::onUnkownMessage, this, 
						placeholders::_1, placeholders::_2, placeholders::_3)));	
		msgRegister_.registerMessages();

	}

	void start(){
		ProtobufServer::start();
		routerPool_->StartFromCfg();
	}


public:
	RouterServerPoolManagerPtr routerPool_;
private:
    MessageRegister msgRegister_;

};
}
}
