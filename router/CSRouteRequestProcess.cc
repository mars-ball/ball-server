#include"CSRouteRequestProcess.h"
#include"database/UserBasicData.pb.h"

void CSRouteRequestProcess::callback(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& _message, Timestamp){
	shared_ptr<pbmsg::login::HMessage> message = 
			std::static_pointer_cast<pbmsg::login::HMessage>(_message);
	pbmsg::login::CSRouteRequest req = message->request().route();
	RouterServer* routerServer = static_cast<RouterServer*>(server);
	if(routerServer == NULL){
		LOG_ERROR << "cast null";
		return ;
	}
	pbmsg::Ret csRet = Ret::Success;

    shared_ptr<pbmsg::login::HMessage> msg(new pbmsg::login::HMessage());
    msg->set_msgtype(pbmsg::CSRoute_Response); 
    msg->set_seq(message->seq());        
    pbmsg::login::Response *response = msg->mutable_response();
	pbmsg::login::CSRouteResponse* routeRsp = response->mutable_route();

	do{
		if(!req.hasuid() || !req.has_uid() ){
			const AddrData& addr = routerServer->routerPool_->generateRandomAddr();
			routeRsp->set_ip(addr.ip);
			routeRsp->set_port(addr.port);
		}else{
			const int32_t& id = req.uid();
			const AddrData& addr = routerServer->routerPool_->generateAddrById(id);
			routeRsp->set_ip(addr.ip);
			routeRsp->set_port(addr.port);
		}
		
	}while(0);


	routerServer->send(conn, std::static_pointer_cast<google::protobuf::Message>(msg));

	LOG_INFO << "route process end";
}
