#pragma once
#include<fstream>
#include "stdint.h"
#include<memory>
#include"common/Logging.h"
#include"net/ProtobufClient.h"
#include"net/ProtobufServer.h"
#include"net/ServerPoolManager.h"

#include <map>
#include "common/CfgReader.h"
#include <vector>
#include <string>
#include "net/TcpClient.h"

using namespace std;

namespace fsk{
namespace net{

struct AddrData{
	string ip;
	int32_t port;
};


class RouterServerPoolManager:public ServerPoolManager{
public:
	static const string cfgFileName_;

	RouterServerPoolManager(ProtobufServer *server):ServerPoolManager(server){}
	int32_t StartFromCfg();
	const AddrData& generateRandomAddr();
	const AddrData& generateAddrById(int32_t id);

private:
	vector<AddrData> addrs_;

};


typedef shared_ptr<RouterServerPoolManager> RouterServerPoolManagerPtr;

}
}

