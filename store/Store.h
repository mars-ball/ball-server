#pragma once
#include <string>
#include <string.h>
#include <memory>
#include "protobuf/HMessage.pb.h"

using namespace std;
using namespace pbmsg::login;

enum StoreItemType{
  STORE_ITEM_TYPE_INVALID,
  STORE_ITEM_TYPE_GOLD = 110001,
  STORE_ITEM_TYPE_PHOTO = 110002,
};

enum StoreItemPriceType{
  STORE_ITEM_PRICE_TYPE_INVALID,
  STORE_ITEM_PRICE_TYPE_MONEY = 101,
  STORE_ITEM_PRICE_TYPE_GOLD = 102
};


struct Receipt{
  PLATFORM_TYPE platformType;
  string receipt;
  string payLoad;
  string signature;

};


class Store{
  static const std::string base64_chars;

  static const char* public_key;

public:  
	static std::string Base64Encode(const unsigned char* buffer, size_t length);
	static size_t calcDecodeLength(const char* b64input);
	static bool Base64Decode(const char* b64message, string& ret);
	static std::string base64_decode(std::string const& encoded_string);
  static int IAPVerify(const char* data, const char* signature, const char* pub_key_id);
	static bool IAPVerifyByReceipt(int32_t uid, Receipt& receipt);	
	static bool buy(int32_t uid, int32_t itemId, Receipt& reciept);




};


typedef shared_ptr<Store> StorePtr;