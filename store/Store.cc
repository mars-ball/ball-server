#include "Store.h"
#include <iostream>
#include <memory>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <string>
#include <string.h>
#include "common/Logging.h"
#include "common/json.hpp"
#include "user/UserManager.h"
#include "database/StoreData.h"


using namespace localSave;
using namespace std;
using namespace nlohmann;
using namespace fsk;

const string Store::base64_chars = 
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";

const char* Store::public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmarnWQgXwkG8kDEqVDiC2/gDFXDMkYTQ7MGW+pgzMf44GXQnDIllw0az6JQ89QxFUetRKJ918/b0S12LqEHcmx3nFI7nmJIUfhcGzx9ou8bUiLlpm+H8ugg1F86etJFkFio5Pp8G+IrwD5tzB5b0FavPjJ4lYvYh5Xo7YE42x6vwT7Mj712klzSF32pre2AP9GKJQzJZ1M0AGZv3h/CiahZIE8SOxe0H6fB2VpxQGzltCN3LUS1csThJlO0Fje2nKsOZMMXgR884mroUG8cHDN6uZ8i8WC9zDxTQNVibea1YAU+V0oxbZGNhMGOjYNCbz6vX2HnVb73y7CjoR/3elwIDAQAB";

string Store::Base64Encode(const unsigned char* buffer, size_t length) { //Encodes a binary safe base 64 string
  BIO *bio, *b64;
  BUF_MEM *bufferPtr;

  b64 = BIO_new(BIO_f_base64());
  bio = BIO_new(BIO_s_mem());
  bio = BIO_push(b64, bio);

  BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); //Ignore newlines - write everything in one line
  BIO_write(bio, buffer, length);
  BIO_flush(bio);
  BIO_get_mem_ptr(bio, &bufferPtr);

  string ret(bufferPtr->data);
  free(bufferPtr);

  BIO_set_close(bio, BIO_NOCLOSE);
  BIO_free_all(bio);

  return ret;
}

size_t Store::calcDecodeLength(const char* b64input) { //Calculates the length of a decoded string
  size_t len = strlen(b64input),
      padding = 0;

  if (b64input[len-1] == '=' && b64input[len-2] == '=') //last two chars are =
      padding = 2;
  else if (b64input[len-1] == '=') //last char is =
      padding = 1;

  return (len*3)/4 - padding;
}

bool Store::Base64Decode(const char* b64message, string& ret) { //Decodes a base64 encoded string
  BIO *bio, *b64;

  int decodeLen = calcDecodeLength(b64message);
  unsigned char *buffer = (unsigned char*)malloc(decodeLen + 1);
  buffer[decodeLen] = '\0';

  char *message = new char[strlen(b64message)+1];
  memcpy(message, b64message, strlen(b64message)+1);

  bio = BIO_new_mem_buf(message, -1);
  b64 = BIO_new(BIO_f_base64());
  bio = BIO_push(b64, bio);

  BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); //Do not use newlines to flush buffer
  size_t length = BIO_read(bio, buffer, strlen(b64message));
  //assert(*length == decodeLen); //length should equal decodeLen, else something went horribly wrong
  if(length != decodeLen){
      cout <<"bio64 horribly wrong not equal";
      return false;
  }

  ret.assign(reinterpret_cast<char*>(buffer), length);

  free(buffer);

  BIO_free_all(bio);

  delete message;

  return true; //success
}

static inline bool is_base64(unsigned char c) {
  return (isalnum(c) || (c == '+') || (c == '/'));
}

string Store::base64_decode(std::string const& encoded_string) {
  int in_len = encoded_string.size();
  int i = 0;
  int j = 0;
  int in_ = 0;
  unsigned char char_array_4[4], char_array_3[3];
  std::string ret;
  while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
    char_array_4[i++] = encoded_string[in_]; in_++;
    if (i ==4) {
      for (i = 0; i <4; i++)
        char_array_4[i] = base64_chars.find(char_array_4[i]);
      char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
      char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
      char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

      for (i = 0; (i < 3); i++)
        ret += char_array_3[i];
      i = 0;
    }
  }

  if (i) {
    for (j = i; j <4; j++)
      char_array_4[j] = 0;

    for (j = 0; j <4; j++)
      char_array_4[j] = base64_chars.find(char_array_4[j]);

    char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
    char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
    char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

    for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
  }

  return ret;
}

int Store::IAPVerify(const char* data, const char* signature, const char* pub_key_id)
{

  //data = "{\"packageName\":\"brave.bubble.jp\",\"productId\":\"brave.bubble.final.100\",\"purchaseTime\":1467032334745,\"purchaseState\":0,\"purchaseToken\":\"deflghgjmagmfpeihmmcimae.AO-J1Ox2zAX1cFmX_jbGRO_YIAH0W367R325XkK64cXaG0Y8ikuPLcYr5e6CBkLoBrclnaQVqz6npupxtsTpgebKDSdOkgNCInAcEziPeqyFL0Bx_28i-HUhvSgRv1XjN9HCy4XozPS0\"}";
  //signature = "AT8ntrhecR+7IGdCS66kvxdzzxideIwXCJ+bX/EAkgijpkE+sg6cEe19+oX8VGR04BZgogkCkPTTNLqFNrjdwnLoCNUsGfnNPQEQCiDUGW1EG/Sk2Xpz0EoQMqRhBLvb2KOt4lBZde+lfl7qOTfNy6GM8V4awEb3jk/H38LHlgnAEvDw/cZRHQXbhQgVdsoYkHotVIKUbv+OCV9HTYChybdvHMUqygtXD2seXuZs+Ygk2sjtegtNz/R+o5zEXzxIJsVVgSN6uR+dZjnykCwLnUbbL5VFinv34QjX4nXSV66OLXuB0iZrLUkNjcmk10nfYq1R+Ou1u7IbcvISGKZ6Mg==";
  //pub_key_id = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAniVuFj3+iYWn/4GTwip0uX02d9+C/F7VvNVfLGI4ySRG4Rcj3RF+u83iXF/MzgyZYU8fZbSIRPgtIDnwiF9a9WovxqzBcfvptyAEZYufRaHbNmW2CRtxDVWb7yjyfw1NTYK8tRsiokUOXgbUBhV/dKQzppuOSOu8xzYc8b686Ga9bK51YW7cQe/qTnsJBzaoGHZB4PGt6nntDi1CTIlX8+lUy780DuINnQ/TAyODCM1lbnt5kmmj2JajlJH/g02JIMWtsTXR7Px1sXps48eZXv2xPFceaSsvWqxpxY4ZjZaABFICNlliLjAOubTf3d3Z0ar7YeVbTS+AKZnOGQKTJwIDAQAB";

    OpenSSL_add_all_digests();
  std::shared_ptr<EVP_MD_CTX> mdctx =
  std::shared_ptr<EVP_MD_CTX>(EVP_MD_CTX_create(), EVP_MD_CTX_destroy);
  const EVP_MD* md = EVP_get_digestbyname("SHA1");

  EVP_VerifyInit_ex(mdctx.get(), md, NULL);

  EVP_VerifyUpdate(mdctx.get(), (void*)data, strlen(data));

  std::shared_ptr<BIO> b64 = std::shared_ptr<BIO>(BIO_new(BIO_f_base64()), BIO_free);
  BIO_set_flags(b64.get(),BIO_FLAGS_BASE64_NO_NL);

  std::shared_ptr<BIO> bPubKey = std::shared_ptr<BIO>(BIO_new(BIO_s_mem()), BIO_free);
  BIO_puts(bPubKey.get(),pub_key_id);
  BIO_push(b64.get(), bPubKey.get());

  std::shared_ptr<EVP_PKEY> pubkey = 
           std::shared_ptr<EVP_PKEY>(d2i_PUBKEY_bio(b64.get(), NULL), EVP_PKEY_free);
  std::string decoded_signature;// = base64_decode(signature);

  
  if(!Base64Decode(signature, decoded_signature)){
      cout <<"base64 failed";
      return -1;
  }
  cout<<decoded_signature<<endl;
  int ret = EVP_VerifyFinal(mdctx.get(), (unsigned char*)decoded_signature.c_str(), 
           decoded_signature.length(), pubkey.get());
  return ret;
}


bool Store::IAPVerifyByReceipt(int32_t uid, Receipt& receipt){
  /*
  json receiptAll = json::parse(receipt);
  json payload = receiptAll["Payload"];
  string s0 = payload.dump();


  if(payload.is_null()){
    LOG_ERROR << "payload string is null, uid:"<<uid;
    return false;
  }
  //return true;
  json payloadJson = payload["json"];
  if(payloadJson.is_null()){
    LOG_ERROR << "payload json is null, uid:"<<uid;
    return false;
  }

  string payloadSig = payload["signature"];
  if(payloadSig.size() <= 0){
    LOG_ERROR << "payload sig is null, uid:"<<uid;
    return false;
  }

  string payloadStr = payloadJson.dump();*/

  if(receipt.platformType == PLATFORM_TYPE_ANDROID){
    if(IAPVerify(receipt.payLoad.c_str(), receipt.signature.c_str(), public_key) == 1){
      return true;
    }else{
      LOG_ERROR << "verification is failed";
      return false;
    }
  }


  return false;
}


bool Store::buy(int32_t uid, int32_t itemId, Receipt& receipt){
  //IAPVerifyByReceipt(uid, receipt);
  UserPackDataPtr packData = UserManager::getInstance().getPackDataById(uid);
  if(packData.get() == NULL){
    LOG_ERROR <<"packData null:"<<uid;
    return false;
  }


  STOREPtr itemData = StoreData::getInstance().getById(itemId);

  if(itemData.get() == NULL){
    LOG_ERROR <<"store data null:"<<uid;
    return false;
  }

  UserBasicDataPtr basicData = UserManager::getInstance().getUserBasicDataById(uid);
  if(basicData.get() == NULL){
    LOG_ERROR <<"basicData null:"<<uid;
    return false;
  }

  bool canPurchase = false;

  if(itemData->price_type() == STORE_ITEM_PRICE_TYPE_MONEY){
    if(IAPVerifyByReceipt(uid, receipt)){
      canPurchase = true;
    }
  }else if(itemData->price_type() == STORE_ITEM_PRICE_TYPE_GOLD){
    int32_t price = itemData->price();
    if(basicData->gold() > price){
      canPurchase = true;
    }    
  }

  if(canPurchase){
    if(itemData->price_type() == STORE_ITEM_PRICE_TYPE_GOLD){
      basicData->set_gold(basicData->gold() - itemData->price());
    }

    switch(itemData->type()){
      case STORE_ITEM_TYPE_GOLD:
        basicData->set_gold(basicData->gold() + itemData->add_gold_num());
        break;
      case STORE_ITEM_TYPE_PHOTO:
      {
        UserItemData* newItem = packData->add_items();
        newItem->set_storeid(itemId);
        newItem->set_type(USER_ITEM_TYPE_PHOTO);
        UserItemPhoto* photo = newItem->mutable_photo();
        if(photo == NULL){
          LOG_ERROR<<"photo mutable null";
          return false;
        }
        photo->set_photoid(itemData->photo_id());
        break;
      }
      default:
        LOG_ERROR <<"error item type:"<<itemData->type();
        break;
    }

  }
  return true;


}


/*
int main()
{
    const char* purchase_data =
     "{"
     "\"orderId\":\"GPA.1355-2081-8150-25375\","
     "\"packageName\":\"com.zhanglei.conan.test.iap\","
     "\"productId\":\"test.conan.iap.consumable.100\","
     "\"purchaseTime\":1463124967310,"
     "\"purchaseState\":0,"
     "\"purchaseToken\":\"mlofjlikjigbgmfljhglnhgf.AO-J1Ow20UNjG2SZoXAmxelg2YcLZyNmYuX1Q8V-3w9jPMOdHs7jwwESsdM7VV0342iydZqDAnL7RaafJ6ABToQ5ODpDaJmiUj2USYFGbUxlEe44TeX61YfIPpSdLU0BG2wdFHi1RGk6JwA15Y9zQ0n-bZpDXT055w\""
     "}";


    const char* public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAj/C7jY/p1qXM/PyAgfdX4oNdDUNzMb5gGFPYAtM+N1ytygKh15y4Mp8+0GRu82rIOgYjKXAfsZ6KhsQiPVOohDlhroUVmN/RY9TwvuRWB7V7ui2nfQa2gs6QZOAbiAvLUzjNkEYr/eqZNRyRuV/XOI4Y92RvFwYeGi1O7+o+PHoZVsxx3YV1H33o4vkE4di1NVj7zF7b1l2OVyV1g4wIkixwp6kIj8X2Uk4miBBZNLCpupFtgkJCZEMK95ncZKGZMdHoYAQ/s+1yVpnRTZSFwyFjfPRYs+vT4AahxlMaKP2FdhvKEjm4zBK0ERZBiYa1Ua8G7DNWdCg7NsWnC1IK6QIDAQAB";
    char* signature = "a9fnw2SmaiugpzlmuAPyZpKc2M6jlkMHwo68f3F9xiw7PnUwFZTzEIiJPRdNN4xVTokHhwXH8eFR2bOZSwUkZ5ZPtVpLVluqiDpeRUJvzUzaPvlyvX+pJsQ85mmOzq0OV5ERj1WW0moN9qPseSqWRgMt6Ekth9dFX1XSTfH7JuiygVOJLPfuYrj2TelOwLfrWQu4InQ9ggaZRCGo9A/qxJXLtYdmlO4j9dhUqneeoT2dgq55IuURxTSEn3WwHRhVK2U0txVwUjyVixVCarBYrubEZTH51Y7aRjlY8rQ3J8qO0ALSMvbejlDNw6oJ1yF+R19zFjGMBFAw0RMHA==";
    
    OpenSSL_add_all_digests();
    std::cout << InappBillingVerify(purchase_data, signature, public_key) << std::endl;
    EVP_cleanup();

    return 0;
}


int main() {
  char* base64DecodeOutput;
  size_t test;
  string rr;
  //char* ss = "SGVsbG8gV29ybGQ=";
  char* ss = "a9fnw2SmaiugpzlmuAPyZpKc2M6jlkMHwo68f\\/3F9xiw7PnUwFZTzEIiJPRdNN4xVTokHhwXH8eFR2bOZSwUkZ5ZPt\\/VpLVluqiDpeRUJvzUzaPvlyvX+pJsQ85mmOzq0OV\\/5ERj1WW0moN9qP\\/seSqWRgMt6Ekth9dFX1XSTfH7JuiygVOJLPfuYrj2TelOwLfrWQu4InQ9ggaZRCGo9A\\/qxJXLtYdmlO4j9dhUqneeoT2dgq55IuURxTSEn3WwHRhVK2U0txV\\/wUjyVixVCarBYrubEZTH51Y7aRjlY8rQ3J8qO0ALSMvbejlDNw6oJ1yF+R19zFjGMBFAw0RMHA==";
  rr = base64_decode(ss);
  //Base64Decode(ss, rr);
  cout <<rr;
  
  return(0);
}*/
