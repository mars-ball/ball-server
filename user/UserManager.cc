#include "UserManager.h"

#include "common/Util.h"

#include "database/UserBasicData.pb.h"
#include "database/UserNotifData.pb.h"
#include "database/PhotoData.h"
#include "database/tnt_deploy_photo.pb.h"
#include "common/TmpDefine.h"

using namespace fsk;
using namespace localSave;
using namespace tnt_deploy;
using namespace tmp;

UserInfo::UserInfo()
:isLogin_(false),
isInRoom_(false),
state_(UserState::DEFAULT),
basicData_(new UserBasicData())/*,
friendData_(new UserFriendData()),
scoreData_(new UserScoreData()),
packData_(new UserPackData()),
notifData_(new UserNotifData())*/

{
	lastUpdateTime_ = Timestamp::now();
}


void UserManager::setupRed(RedConnectionPtr red){
	red_ = red;
}


int32_t UserManager::simpleAddUser(UserInfoPtr& user){

	int32_t uid = user->uid_;
	if(users_.find(uid) == users_.end()){
		users_[uid] = user;	
	}
	
	UserInfoPtr oldUser = users_[uid];
	oldUser->updateTime();	
	return 0;
}



int32_t UserManager::createUser(int64_t uuid, string name, int32_t& uid, string& token, string deviceId){
	UserInfoPtr user(new UserInfo());
	user->basicData_.reset(new UserBasicData());

	shared_ptr<UserBasicData> basicData_ = user->basicData_;
	bool hasUser = red_->HasItem("devid", deviceId);
	if(hasUser){
		do{
			string res;
			int32_t ret = red_->GetItem("devid", deviceId, res);
			if(ret != 0){
				LOG_ERROR<< "red get from devid failed , deviceId:"<<deviceId;
				break;
			}

			int32_t id;
			if(!str2num(res, id)){
				LOG_ERROR <<" convert id str to num failed, id str:"<<res;
				break;
			}

			UserInfoPtr oldUser = UserManager::getById(id);
			if(oldUser.get() == NULL){
				LOG_ERROR <<"get old user failed, id:"<<id;
				break;
			}

			UserBasicDataPtr oldbasic = oldUser->basicData_;

			uid = id;
			token = oldbasic->token();

			/*oldbasic->set_uuid(uuid);
			if(!saveUserBasicData(oldUser)){
				LOG_ERROR <<"save user basic failed:"<<uid;
			}*/

			return 0;

		}while(0);
		LOG_ERROR <<"has device id process failed";
	}


	basicData_->set_uuid(uuid);
	basicData_->set_token(token);

	int photoNum = PhotoData::getInstance().colorPhotos_.size();
	int photoRand = randRange(0, photoNum);

	basicData_->set_rmads(0);
	basicData_->set_name("Warrior ");
	basicData_->set_uid(0);
	basicData_->set_photo(PhotoData::getInstance().colorPhotos_[photoRand]->id());
	basicData_->set_lv(0);
	basicData_->set_exp(0);
	basicData_->set_gold(50);
	basicData_->set_srctype(USER_SRC_TYPE_INVALID);
	basicData_->set_srcname("");
	basicData_->set_language(USER_LANGUAGE_ENGLISH);

	//int nbuf = basicData_->ByteSize();
	//shared_ptr<char> buf(new char[nbuf]);
	//basicData_->SerializeWithCachedSizesToArray(reinterpret_cast<uint8_t*>(buf.get()));
	string buf;
	basicData_->SerializeToString(&buf);

	int64_t id;
	int32_t ret = red_->AddItem("user_basic", buf, id);
	if(ret != 0){
		LOG_ERROR << "add user basic error , id:"<<id << " buf :" << buf;
		return -1;//fixme
	}

	uid = static_cast<int32_t>(id);

	red_->SetItem("devid", deviceId, std::to_string(id));
	if(ret != 0){
		LOG_ERROR << "set devid failed:"<<deviceId<<" id:"<<id;
		return -1;//fixme
	}

	user->uid_ = uid;
	basicData_->set_uid(uid);
	user->uuid_ = uuid;
	basicData_->set_name("Warrior " + to_string(uid));

	string buf2;
	basicData_->SerializeToString(&buf2);	

	ret = red_->SetItem("user_basic", uid, buf2);
	if(ret != 0){
		LOG_ERROR << "add user basic error , id:"<<id << " buf :" << buf2;
		return -1;//fixme
	}


	if(createUserFriendData(user)!= 0){
		LOG_ERROR <<"create user friend data failed";
		return -1;
	}

	if(createUserScoreData(user) != 0){
		LOG_ERROR <<"create user score data failed";
		return -1;
	}

	if(createUserNotifData(user) != 0){
		LOG_ERROR <<" create user notif data failed";
		return -1;
	}

	if(createUserPackData(user) != 0){
		LOG_ERROR <<" create user pack data failed: id"<<uid;
		return -1;
	}

	//nbuf = basicData_->ByteSize();
	//buf.reset(new char[nbuf]);
	//basicData_->SerializeWithCachedSizesToArray(reinterpret_cast<uint8_t*>(buf.get()));


	if(cacheByIdNorm_ != NULL && cacheByIdNorm_(uid)){
		addUser(uid, user);	
	}

	return 0;

}

int32_t UserManager::saveAllData(int32_t &uid){
	UserInfoPtr user = getById(uid);
	if(user.get() == NULL){
		LOG_ERROR << "user info cannot be found";
		return -1;
	}

	if(!saveUserBasicData(user)){
		LOG_ERROR <<"save user basic failed:"<<uid;
	}

	if(!saveUserFriendData(user)){
		LOG_ERROR<<"save user friend failed:"<<uid;
	}

	if(!saveUserScoreData(user)){
		LOG_ERROR<<"save user score failed:"<<uid;
	}

	if(!saveUserNotifData(user)){
		LOG_ERROR<<"save user notif failed:"<<uid;
	}

	if(!saveUserPackData(user)){
		LOG_ERROR<<"save user pack failed:"<<uid;
	}
	return 0;
}

int32_t UserManager::saveUserBasicData(UserInfoPtr& user){
	if(user->basicData_.get() == NULL){
		LOG_ERROR <<"user basic data null";
		return -1;
	}
	string buf;
	user->basicData_->SerializeToString(&buf);

	int32_t uid = user->uid_;
	int32_t ret = red_->SetItem("user_basic", uid, buf);
	if(ret != 0){
		LOG_ERROR << "save user basic error , id:"<<uid << " buf :" << buf;
		return -1;
	}

	if(user->basicData_->has_mail() && user->basicData_->mail().length() > 0){

		int32_t ret = red_->SetItem("user_mail", user->basicData_->mail(), to_string(uid));
		if(ret != 0){
			LOG_ERROR << "save user mail error , id:"<<uid << " mail :" << user->basicData_->mail();
			return -1;
		}
	}


	return 0;
}

UserInfoPtr UserManager::getByMail(string& mail){

	string res;
	int32_t ret = red_->GetItem("user_mail", mail, res);
	if(ret != 0){
		LOG_ERROR<< "red get from user_mail failed , mail:"<<mail;
		return NULL;
	}

	int32_t uid;
	if(!str2num(res, uid)){
		return NULL;
	}

	return getById(uid);

}


int32_t UserManager::createUserFriendData(UserInfoPtr& user){
	user->friendData_.reset(new UserFriendData());
	UserFriendDataPtr friendData = user->friendData_;
	string buf;
	friendData->SerializeToString(&buf);

	int32_t uid = user->uid_;
	int32_t ret = red_->AddItemById("user_friend", buf, uid);
	if(ret != 0){
		LOG_ERROR << "add user friend error , id:"<<uid << " buf :" << buf;
		return -1;
	}
	return 0;
}

int32_t UserManager::saveUserFriendData(UserInfoPtr& user){
	if(user->friendData_.get() == NULL){
		LOG_ERROR << "user friend data null";
		return -1;
	}
	string buf;
	user->friendData_->SerializeToString(&buf);

	int32_t uid = user->uid_;
	int32_t ret = red_->SetItem("user_friend", uid, buf);
	if(ret != 0){
		LOG_ERROR << "save user friend error , id:"<<uid << " buf :" << buf;
		return -1;
	}
	return 0;
}

int32_t UserManager::createUserScoreData(UserInfoPtr& user){
	int32_t uid = user->uid_;

	user->scoreData_.reset(new UserScoreData());
	UserScoreDataPtr scoreData = user->scoreData_;
	scoreData->set_id(uid);
	scoreData->set_maxscore(0);
	string buf;
	scoreData->SerializeToString(&buf);

	int32_t ret = red_->AddItemById("user_score", buf, uid);
	if(ret != 0){
		LOG_ERROR << "add user score error , id:"<<uid << " buf :" << buf;
		return -1;
	}
	return 0;
}

int32_t UserManager::saveUserScoreData(UserInfoPtr& user){
	if(user->scoreData_.get() == NULL){
		LOG_ERROR << "user user score data null";
		return -1;
	}
	string buf;
	user->scoreData_->SerializeToString(&buf);

	int32_t uid = user->uid_;
	int32_t ret = red_->SetItem("user_score", uid, buf);
	if(ret != 0){
		LOG_ERROR << "save user_score error , id:"<<uid << " buf :" << buf;
		return -1;
	}
	return 0;
}


int32_t UserManager::createUserNotifData(UserInfoPtr& user){
	int32_t uid = user->uid_;

	user->notifData_.reset(new UserNotifData());
	UserNotifDataPtr notifData = user->notifData_;
	notifData->set_seqnomax(101);
	string buf;
	notifData->SerializeToString(&buf);

	int32_t ret = red_->AddItemById("user_notif", buf, uid);
	if(ret != 0){
		LOG_ERROR << "add user notif error , id:"<<uid << " buf :" << buf;
		return -1;
	}
	return 0;
}

int32_t UserManager::saveUserNotifData(UserInfoPtr& user){
	if(user->notifData_.get() == NULL){
		LOG_ERROR << "user notif data null";
		return -1;
	}
	string buf;
	user->notifData_->SerializeToString(&buf);

	int32_t uid = user->uid_;
	int32_t ret = red_->SetItem("user_notif", uid, buf);
	if(ret != 0){
		LOG_ERROR << "save user_notif error , id:"<<uid << " buf :" << buf;
		return -1;
	}
	return 0;
}


int32_t UserManager::createUserPackData(UserInfoPtr& user){
	int32_t uid = user->uid_;

	user->packData_.reset(new UserPackData());
	UserPackDataPtr packData = user->packData_;
	packData->set_capacity(USER_INIT_PACK_SIZE);
	packData->set_size(0);
	string buf;
	packData->SerializeToString(&buf);

	int32_t ret = red_->AddItemById("user_pack", buf, uid);
	if(ret != 0){
		LOG_ERROR << "add user pack error, id:" << uid<<" buf:" <<buf;
		return -1;
	}


	return 0;
}

int32_t UserManager::saveUserPackData(UserInfoPtr& user){
	if(user->packData_.get() == NULL){
		LOG_ERROR << "user notif data null";
		return -1;
	}
	string buf;
	user->packData_->SerializeToString(&buf);

	int32_t uid = user->uid_;
	int32_t ret = red_->SetItem("user_pack", uid, buf);
	if(ret != 0){
		LOG_ERROR << "save user_pack error , id:"<<uid << " buf :" << buf;
		return -1;
	}
	return 0;
}


int32_t UserManager::addUser(int id, shared_ptr<UserInfo> userInfo){
	if(users_.find(id) != users_.end()){
		//LOG_ERROR << "has user id:" << id;
		return 0;
	}
	users_[id] = userInfo;
	return 0;
}

UserInfoPtr UserManager::getById(int32_t id){
	if(users_.find(id) == users_.end()){

		UserInfoPtr userInfo;
		int32_t ret = getUserInfoFromRed(id, userInfo);
		if(ret != 0){
			LOG_ERROR << "get from red failed, id:"<<id;
			return UserInfoPtr(NULL);
		}
		return userInfo;
	}
	return users_[id];
}


UserScoreDataPtr UserManager::getScoreById(int32_t id){
	UserInfoPtr user = getById(id);
	if(user.get() == NULL){
		LOG_ERROR << " get user failed";
		return NULL;
	}	
	return user->scoreData_;
}



template<class T>
int32_t  UserManager::getFromRed(int32_t id, string name, shared_ptr<T>& data){
	string res;
	int32_t ret = red_->GetItem(name, id, res);
	if(ret != 0){
		LOG_ERROR<< "red get from " <<name<<" failed , id:"<<id;
		return -1;
	}

	data.reset(new T());

	if(!data->ParseFromString(res)){
		LOG_ERROR<< "parse "<<name<<" info failed, id:"<<id;
		return -1;
	}
	return 0;
}

int32_t  UserManager::getUserInfoFromRed(int32_t uid, UserInfoPtr& userInfo){

	UserInfoPtr user(new UserInfo());

	if(getFromRed<UserBasicData>(uid, "user_basic", user->basicData_) != 0){
		LOG_ERROR<< "get user basic from red failed, id:"<<uid;
		return -1;
	}

	if(getFromRed<UserFriendData>(uid, "user_friend", user->friendData_) != 0){
		LOG_ERROR <<"get friend from red is failed, id:"<<uid;
		return -1;
	}

	if(getFromRed<UserScoreData>(uid, "user_score", user->scoreData_) != 0){
		LOG_ERROR <<"get score from red is failed, id:"<<uid;
		return -1;
	}

	if(getFromRed<UserNotifData>(uid, "user_notif", user->notifData_) != 0){
		LOG_ERROR <<"get notif from red is failed, id:"<<uid;
		return -1;
	}

	if(getFromRed<UserPackData>(uid, "user_pack", user->packData_) != 0){
		LOG_ERROR <<"get pack from red is failed, id:"<<uid;
		return -1;
	}

	userInfo = user;
	userInfo->uid_ = uid;

	if(cacheByIdNorm_ != NULL && cacheByIdNorm_(uid)){
		addUser(uid, user);	
	}

	return 0;
}

UserBasicDataPtr UserManager::getUserBasicDataById(int32_t& uid){

	UserInfoPtr userInfo = getById(uid);
	if(userInfo.get() == NULL){
		LOG_ERROR <<"user info get failed, uid:"<<uid;
		return NULL;
	}

	if(userInfo->basicData_.get() == NULL){
		LOG_ERROR <<"user basic data null";
		return NULL;
	}

	return userInfo->basicData_;


}


UserFriendDataPtr UserManager::getFriendDataById(int32_t& uid){

	UserInfoPtr userInfo = getById(uid);
	if(userInfo.get() == NULL){
		LOG_ERROR <<"user info get failed, uid:"<<uid;
		return NULL;
	}

	if(userInfo->friendData_.get() == NULL){
		if(getFromRed<UserFriendData>(uid, "user_friend", userInfo->friendData_) != 0 || userInfo->friendData_.get()==NULL){
			LOG_ERROR <<"get friend from red is failed";
			return NULL;
		}
	}

	return userInfo->friendData_;
}

UserScoreDataPtr UserManager::getScoreDataById(int32_t& uid){

	UserInfoPtr userInfo = getById(uid);
	if(userInfo.get() == NULL){
		LOG_ERROR <<"user info get failed, uid:"<<uid;
		return NULL;
	}

	if(userInfo->scoreData_.get() == NULL){
		if(getFromRed<UserScoreData>(uid, "user_score", userInfo->scoreData_) != 0 || userInfo->scoreData_.get()==NULL){
			LOG_ERROR <<"get score from red is failed";
			return NULL;
		}
	}

	return userInfo->scoreData_;
}

UserNotifDataPtr UserManager::getNotifDataById(int32_t& uid){

	UserInfoPtr userInfo = getById(uid);
	if(userInfo.get() == NULL){
		LOG_ERROR <<"user info get failed, uid:"<<uid;
		return NULL;
	}

	if(userInfo->notifData_.get() == NULL){
		if(getFromRed<UserNotifData>(uid, "user_notif", userInfo->notifData_) != 0 || userInfo->notifData_.get()==NULL){
			LOG_ERROR <<"get notif from red is failed";
			return NULL;
		}
	}

	return userInfo->notifData_;
}

UserPackDataPtr UserManager::getPackDataById(int32_t& uid){
	UserInfoPtr userInfo = getById(uid);
	if(userInfo.get() == NULL){
		LOG_ERROR <<"user info get failed, uid:"<<uid;
		return NULL;
	}

	if(userInfo->packData_.get() == NULL){
		if(getFromRed<UserPackData>(uid, "user_pack", userInfo->packData_) != 0 || userInfo->packData_.get()==NULL){
			LOG_ERROR <<"get pack from red is failed";
			return NULL;
		}
	}

	return userInfo->packData_;
}


const UserNotifDataItem* UserManager::getUserNotifDataItemBySeqNo(int32_t uid, int32_t seqNo, int32_t &index){

	UserInfoPtr userInfo = getById(uid);
	if(userInfo.get() == NULL){
		LOG_ERROR <<"user info get failed, uid:"<<uid;
		return NULL;
	}

	if(userInfo->notifData_.get() == NULL){
		if(getFromRed<UserNotifData>(uid, "user_notif", userInfo->notifData_) != 0 || userInfo->notifData_.get()==NULL){
			LOG_ERROR <<"get notif from red is failed";
			return NULL;
		}
	}
	UserNotifDataPtr notifData = userInfo->notifData_;

	for(int32_t ii = 0; ii < notifData->items_size(); ii++){
		const UserNotifDataItem& item = notifData->items(ii);

		if(item.seqno() == seqNo){
			index = ii;
			return &item;
		} 
	}

	return NULL;

}






void UserManager::registerCacheByIdNorm(function<bool(int32_t uid)> norm){

	cacheByIdNorm_ = norm;

}












