#ifndef  FSK_SERVER_USER_MANANGER
#define FSK_SERVER_USER_MANANGER

#include "net/CommonInc.h"
#include <map>
#include "common/Timestamp.h"
#include "net/TcpConnection.h"
#include "common/Singleton.h"
#include "common/Logging.h"
#include "database/UserBasicData.pb.h"
#include "database/UserNotifData.pb.h"
#include <functional>

#include "hredis/RedConnection.h"
//#include "login/LoginServer.h"


using namespace std;
using namespace localSave;

namespace fsk{

enum UserState{
	INVALID,
	DEFAULT,
	WAIT_ENTER,
	IN_ROOM,
	WAIT_BORN,
	DEAD_,
	MAX
};


typedef shared_ptr<UserBasicData> UserBasicDataPtr;
typedef shared_ptr<UserFriendData> UserFriendDataPtr;
typedef shared_ptr<UserScoreData> UserScoreDataPtr;
typedef shared_ptr<UserNotifData> UserNotifDataPtr;
typedef shared_ptr<UserPackData> UserPackDataPtr;


class UserInfo{
public:
	int64_t uuid_;
	int32_t uid_;
	string ip_;
	string port_;

	bool isLogin_;
	bool isInRoom_;
	UserState state_;
	int32_t roomTypeId_;
	int32_t roomId_;
	int32_t campId_;

	UserBasicDataPtr basicData_;
	UserFriendDataPtr friendData_;
	UserScoreDataPtr scoreData_;
	UserNotifDataPtr notifData_;
	UserPackDataPtr packData_;

	vector<int32_t> friends_;

	Timestamp lastLoginTime_;
	Timestamp lastUpdateTime_;

	UserInfo();

	void reset(){
		isLogin_ = false;
		isInRoom_ = false;
		state_ = UserState::DEFAULT;
	}

	void updateTime(){
		lastUpdateTime_ = Timestamp::now();
	}

	void login(){
		reset();
		isLogin_ = true;
		lastUpdateTime_ = Timestamp::now();
		lastLoginTime_ = lastUpdateTime_;
	}

	void setInRoom(int32_t roomId, int32_t camp, bool isIn = true){
		roomId_ = roomId;
		campId_ = camp;
		
		if(!isIn){
			isInRoom_ = false;
		}else{
			isInRoom_ = true;
		}
	}

/*
	void SetServer(int32_t id, string ip){
		uid_  = uid;
		ip_ = ip;
	}
*/
};

typedef shared_ptr<UserInfo> UserInfoPtr;


class UserManager: public Singleton<UserManager>{

public:
	void setupRed(RedConnectionPtr red);

	int32_t simpleAddUser(UserInfoPtr& user);	

	int32_t createUser(int64_t uuid, string name, int32_t& uid, string& token, string deviceId);

	int32_t createUserFriendData(UserInfoPtr& user);

	int32_t createUserScoreData(UserInfoPtr& user);

	int32_t createUserNotifData(UserInfoPtr& user);

	int32_t createUserPackData(UserInfoPtr& user);

	int32_t saveUserBasicData(UserInfoPtr& user);

	int32_t saveUserFriendData(UserInfoPtr& user);

	int32_t saveUserScoreData(UserInfoPtr& user);

	int32_t saveUserNotifData(UserInfoPtr& user);

	int32_t saveUserPackData(UserInfoPtr& user);	

	int32_t saveAllData(int32_t &uid);

	int32_t addUser(int id, shared_ptr<UserInfo> userInfo);

	UserInfoPtr getById(int32_t id);

	UserInfoPtr getByMail(string& mail);

	UserBasicDataPtr getUserBasicDataById(int32_t& id);

	UserFriendDataPtr getFriendDataById(int32_t& id);

	UserNotifDataPtr getNotifDataById(int32_t& id);

	UserScoreDataPtr getScoreDataById(int32_t& id);

	UserPackDataPtr getPackDataById(int32_t& uid);

	int32_t  getUserInfoFromRed(int32_t id, UserInfoPtr& userInfo);

	template<class T>
	int32_t  getFromRed(int32_t id, string name, shared_ptr<T>& data);

	//int32_t updateConnectionById(int id, net::TcpConnectionPtr _conn);
	
	UserScoreDataPtr getScoreById(int32_t id);

	const UserNotifDataItem* getUserNotifDataItemBySeqNo(int32_t uid, int32_t seqNo, int32_t &index);

	void registerCacheByIdNorm(function<bool(int32_t uid)> norm);


private:
	map<int32_t,  UserInfoPtr> users_;

	RedConnectionPtr red_;

	function<bool(int32_t uid)> cacheByIdNorm_;

	//LoginServer* loginServer_;

};
}

#endif

