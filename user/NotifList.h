#pragma once
#include "common/Timestamp.h"

using namespace fsk;

enum NotifType{
	NOTIF_TYPE_INVALID,
	NOTIF_TYPE_ADDFRIEND,
	NOTIF_TYPE_MAX
};


class NotifInterface{
public:
	NotifInterface(int32_t type, Timestamp timestamp);
protected:
	int32_t notifType_;
	Timestamp timestamp_;
};


class AddFriendNotif : public NotifInterface{

public:
	AddFriendNotif(int32_t fromUid, Timestamp timestamp)
	:NotifInterface(NotifType::NOTIF_TYPE_ADDFRIEND, timestamp),
	fromUid_(fromUid)
	{

	}



public:
	int32_t fromUid_;
};


class NotifModule{
public:
	NotifModule(){

	}







};












