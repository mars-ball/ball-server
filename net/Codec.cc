#include "Codec.h"
#include "Endian.h"
#include "google-inl.h"
//#include "google/protobuf/descriptor.h"
#include "zlib.h"
#include "TcpConnection.h"
#include <string>
#include <iostream>
#include "MessageDefine.h"
#include "common/Logging.h"
#include "ProtobufServer.h"
#include "protobuf/LoginRoomMessage.pb.h"

using namespace fsk;
using namespace fsk::net;
using namespace google;
using namespace google::protobuf;
using namespace std;


namespace{
	const string kNoErrorStr = "NoError";
	const string kInvalidLengthStr = "InvalidLength";
	const string kCheckSumErrorStr = "CheckSumError";
	const string kInvalidNameLenStr = "InvalidNameLen";
	const string kUnkownMessagetTypeStr = "UnkownMessageLen";
	const string kParseErrorStr = "ParseError";
	const string kUnknownErrorStr = "UnknownError";
}

/*
void ProtobufCodec::fillEmptyBuffer(Buffer* buf, const protobuf::Message& message){
	//assert(buf->readableBytes() == 0);
	const string& msgname = message.GetTypeName();
	int32_t msgId = MessageDefine::getInstance().GetIdByName(msgname);
	//int32_t nameLen = static_cast<int32_t>(msgname.size() + 1);
	//buf->appendInt32(nameLen);
	buf->appendInt32(msgId);
	//buf->append(msgname.c_str(), nameLen);

	int byte_size = message.ByteSize();
	buf->ensureWritableBytes(byte_size);

	uint8_t* start = reinterpret_cast<uint8_t*>(buf->beginWrite());
	uint8_t* end = message.SerializeWithCachedSizesToArray(start);
	if(end - start != byte_size){
		ByteSizeConsistencyError(byte_size, message.ByteSize(), static_cast<int>(end - start));
	}

	buf->hasWritten(byte_size);

	int32_t checkSum = static_cast<int32_t>(
		::adler32(1, reinterpret_cast<const Bytef*>(buf->peek()),
					static_cast<int>(buf->readableBytes())));
	buf->appendInt32(checkSum);
	assert(buf->readableBytes() == (sizeof (int32_t)  + byte_size + sizeof(checkSum)));
	int32_t len = sockets::hostToNetwork32(static_cast<int32_t>(buf->readableBytes()));
	buf->prepend(&len, sizeof(len));


}*/
/*
void ProtobufCodec::fillResponse(pbmsg::HMessage& msg){
	pbmsg::Response* response = msg.mutable_response();
	response->set_lastresponse(true);
	response->set_ret(pbmsg::Success);
}

void ProtobufCodec::fillNotification(pbmsg::HMessage& msgd){
	(void)msgd;
}
*/


void ProtobufCodec::fillHEmptyBuffer(Buffer* buf, MessagePtr& msg){
/*	
	if(msg.has_response())
	{
		fillResponse(msg);
	}else if(msg.has_notification()){
		fillNotification(msg);
	}
	*/
	//const string& msgname = msg.GetTypeName();
	//(void)msgname;
	//int32_t msgId = MessageDefine::getInstance().GetIdByName(msgname);
	//int32_t nameLen = static_cast<int32_t>(msgname.size() + 1);
	//buf->appendInt32(nameLen);
	//buf->appendInt32(static_cast<int32_t>(msgid));
	//buf->append(msgname.c_str(), nameLen);

	int byte_size = msg->ByteSize();
	buf->ensureWritableBytes(byte_size);

	uint8_t* start = reinterpret_cast<uint8_t*>(buf->beginWrite());
	uint8_t* end = msg->SerializeWithCachedSizesToArray(start);
	if(end - start != byte_size){
		ByteSizeConsistencyError(byte_size, msg->ByteSize(), static_cast<int>(end - start));
	}

	buf->hasWritten(byte_size);

	int32_t topKind = MessageDefine::getInstance().GetMsgIdByDes(msg->GetDescriptor());
	buf->prependInt32(topKind);

	int32_t checkSum = static_cast<int32_t>(
		::adler32(1, reinterpret_cast<const Bytef*>(buf->peek()),
					static_cast<int>(buf->readableBytes())));
	buf->appendInt32(checkSum);
	assert(buf->readableBytes() ==  byte_size + sizeof(checkSum) + sizeof(int32_t));
	int32_t len = sockets::hostToNetwork32(static_cast<int32_t>(buf->readableBytes()));
	buf->prepend(&len, sizeof(len));

}


const string& ProtobufCodec::errorCodeToString(ErrorCode errorCode){
	switch(errorCode){
		case kNoError:
			return kNoErrorStr;
		case kInvalidLength:
			return kInvalidLengthStr;
		case kCheckSumError:
			return kCheckSumErrorStr;
		case kUnkownMessageType:
			return kUnkownMessagetTypeStr;
		case kParseError:
			return kParseErrorStr;
		default:
			return kUnknownErrorStr;
	}
}

void ProtobufCodec::defaultErrorCallback(const TcpConnectionPtr& conn, Buffer* buf, Timestamp timestamp, ErrorCode errorCode){
	LOG_ERROR <<" protobufcodec::defaultErrorCallback-" << errorCodeToString(errorCode);
	if(conn && conn->connected()){
		conn->shutdown();
	}
	(void)buf;
	(void)timestamp;
}

int32_t asInt32(const char* buf){
	int32_t be32 = 0;
	::memcpy(&be32, buf, sizeof(be32));
	return sockets::networkToHost32(be32);
}


void ProtobufCodec::onMessage(const TcpConnectionPtr& conn, Buffer* buf, Timestamp timestamp){
	while(buf->readableBytes() >= kMinMessageLen + kHeaderLen){
		const int32_t len = buf->peekInt32();
		if(len > kMaxMessageLen || len < kMinMessageLen){
			errorCallback_(conn, buf, timestamp, kInvalidLength);
			break;
		}else if(buf ->readableBytes() >= static_cast<size_t>(len + kHeaderLen)){
			ErrorCode errorCode = kNoError;
			int32_t topKind = -1;

			MessagePtr message = parse(buf->peek() + kHeaderLen, len, &topKind, &errorCode);
			ProtobufDispatcherBase::ptr_t dis = server_->getDispatcherByType(topKind);
			if(dis == NULL){
					LOG_ERROR<< "top kind failed kind:" <<topKind;
					break;
			}
			if(errorCode == kNoError && message){
				dis->onProtobufMessage(conn, message, timestamp);
				buf->retrieve(kHeaderLen + len);
			}else{
				errorCallback_(conn, buf, timestamp, errorCode);
				break;
			}
		}else{
			break;
		}
	}
}


Message* ProtobufCodec::createMessage(const string& typeName){
	Message* message = NULL;
	const Descriptor* descriptor = DescriptorPool::generated_pool()->FindMessageTypeByName(typeName);
	if(descriptor){
		const Message* prototype = MessageFactory::generated_factory()->GetPrototype(descriptor);
		if(prototype){
			message = prototype->New();
		}
	}

	return message;
}

MessagePtr ProtobufCodec::parse(const char* buf, int len , int32_t *topKind, ErrorCode* error){
	MessagePtr message;
	int32_t expectedCheckSum = asInt32(buf + len - kHeaderLen);
	int32_t checkSum = static_cast<int32_t>(::adler32(1,
						reinterpret_cast<const Bytef*>(buf),
						static_cast<int>(len - kHeaderLen)));

	if(checkSum == expectedCheckSum){
		*topKind = asInt32(buf);
		const Descriptor* des = MessageDefine::getInstance().GetDesByMsgId(*topKind);
		if(des == NULL){
			LOG_ERROR << "no such des with topKind:"<<topKind;
			return nullptr;
		}
		//pbmsg::loginroom::LoginRoomMessage msgtmp;


		message.reset(MessageFactory::generated_factory()->GetPrototype(des)->New());
		int32_t dataLen = static_cast<int32_t>(len - sizeof(int32_t)*2);//error pro 
		//bool tmp = msgtmp.ParseFromArray(buf + sizeof(int32_t), dataLen);

		if(message->ParseFromArray(buf + sizeof(int32_t), dataLen)){
			*error = kNoError;
		}else{
			*error = kParseError;
		}
	}else{
		*error = kCheckSumError;
	}



	//if(checkSum == expectedCheckSum){
		//int32_t nameLen = asInt32(buf);
		//int32_t msgId = asInt32(buf);
		//string msgName = MessageDefine::getInstance().GetNameById(msgId);
		/*LOG_TRACE << "receive a msg :" << msgName;
		message.reset(createMessage(msgName));
		if(message){
			const char* data = buf + sizeof(int32_t);
			int32_t dataLen = static_cast<int32_t>(len - sizeof(int32_t) - kHeaderLen);
			if(message->ParseFromArray(data, dataLen)){
				*error = kNoError;
			}else{
				*error = kParseError;
			}
		}else{
			*error = kUnkownMessageType;
		}*/

	/*}else{
	*error = kCheckSumError;
	}*/

	return message;
}



















































