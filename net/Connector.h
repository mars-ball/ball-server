#ifndef KUN_SERVER_CONNECTOR_H
#define KUN_SERVER_CONNECTOR_H

#include "net/InetAddress.h"
#include <memory>
#include <functional>
#include "common/noncopyable.h"

namespace fsk{
class Tunnel;
class EventLoop;

namespace net{

class Connector: noncopyable, public std::enable_shared_from_this<Connector>{
public:
	typedef function<void(int sockfd)> NewConnectionCallback;

	Connector(EventLoop* loop, const InetAddress& serverAddr);
	~Connector();

	void setNewConnectionCallback(const NewConnectionCallback& cb){
		newConnectionCallback_ = cb;
	}

	void start();
	void restart();
	void stop();

	const InetAddress& serverAddress() const { return serverAddr_;}

private:
	enum States{ kDisconnected, kConnecting, kConnected};
	static const int kMaxRetryDelayMs = 30*1000;
	static const int kInitRetryDelayMs = 500;

	void setState(States s) { state_ = s;}
	void startInLoop();
	void stopInLoop();
	void connect();
	void connecting(int sockfd);
	void handleWrite();
	void handleError();
	void retry(int sockfd);
	int removeAndResetTunnel();
	void resetTunnel();



	EventLoop* loop_;
	InetAddress serverAddr_;
	bool connect_;
	States state_;
	std::unique_ptr<Tunnel> tunnel_;
	NewConnectionCallback newConnectionCallback_;
	int retryDelayMs_;


};
}
}


#endif

