#include "ServerPoolManager.h"
#include "common/CfgReader.h"
#include "net/InetAddress.h"
#include "google/protobuf/message.h"

using namespace google::protobuf;
using namespace fsk;
using namespace fsk::net;

void ServerPoolManager::startNewConnection(int32_t sid, string name, string ip, int32_t port){
		SSConnectionInfoPtr basicInfo = 
			SSConnectionInfoPtr(new SSConnectionInfo(sid, name, ip, port));

		fsk::net::InetAddress addr(ip, port);
		
		ProtobufClientPtr client = 
			ProtobufClientPtr(new ProtobufClient(name, rootServer_->getEventLoop(), addr, rootServer_));

		client->setId(sid); 	
		client->registClientConnectCompleteCallback(clientConnectCompleteCallback_);

		client->start(); 

		connectors_[sid] = std::make_pair(basicInfo, client);
		serverIdBySort_.push_back(sid);

		if(name == rootServer_->getServerName()){
			setHostIndex(serverIdBySort_.size() -1);
		}

}


void ServerPoolManager::sendByServerId(int32_t sid, MessagePtr msg){
	if(connectors_.find(sid) == connectors_.end()) {
		LOG_ERROR << "no such serverid:"<<sid;
		return; 
	}
	
	ProtobufClientPtr client = connectors_[sid].second;

	client->send(msg);
	
}

void ServerPoolManager::sendByServerIndex(int32_t index, MessagePtr msg){
	if(index < 0 || index >= serverIdBySort_.size()){
		LOG_ERROR <<"index exceed sort array:"<<index;
		return;
	}

	int32_t sid = serverIdBySort_[index];

	sendByServerId(sid, msg);

}


int32_t ServerPoolManager::getServerIdByIndex(int32_t index){
	if(index < 0 || index >= serverIdBySort_.size()){
		LOG_ERROR <<"index exceed sort array:"<<index;
		return -1;
	}

	return serverIdBySort_[index];
}


void ServerPoolManager::sendBroadCast(MessagePtr msg){
	for(auto it = connectors_.begin(); it != connectors_.end(); ++it){
		ProtobufClientPtr client = connectors_[it->first].second;
		client->send(msg);
	}
}


bool ServerPoolManager::isServerAllConnected(){
	for(auto it = connectors_.begin(); it != connectors_.end(); ++it){
		ProtobufClientPtr client = it->second.second;
		if(!client->isConnected()){
			return false;
		}
	}
	return true;
}


int32_t ServerPoolManager::getClientById(int32_t id, ProtobufClientPtr& client){
	if(connectors_.find(id) == connectors_.end()){
		LOG_ERROR <<"no such client by id:"<<id;
		return -1;
	}

	client = connectors_[id].second;
	return 0;
}


SSConnectionInfoPtr ServerPoolManager::getConnectionInfoById(int32_t id){
	if(connectors_.find(id) == connectors_.end()){
		LOG_ERROR <<"no such client by id:"<<id;
		return NULL;
	}

	return connectors_[id].first;
}


void ServerPoolManager::registClientConnectCompleteCallback(const ClientConnectCompleteCallback& cb){
	clientConnectCompleteCallback_ = cb;
}









