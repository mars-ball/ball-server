#ifndef KUNSERVER_PROTOBUF_SERVER
#define KUNSERVER_PROTOBUF_SERVER

#include <iostream>
#include "common/noncopyable.h"
#include <pthread.h>
#include <thread>
#include "TcpServer.h"
#include "TcpConnection.h"
#include "EventLoop.h"
#include <functional>
#include "common/Timestamp.h"
#include "InetAddress.h"
#include "Acceptor.h"
#include "Dispatcher.h"
#include "Codec.h"
#include "MessageDefine.h"
#include "user/UserManager.h"
#include "Dispatchable.h"
#include "Callbacks.h"
#include "common/bimap.h"


using namespace codeproject;
using namespace std;
namespace fsk{
namespace net{

typedef shared_ptr<ProtobufCodec> ProtobufCodecPtr;

class ProtobufServer:public Dispatchable, noncopyable{
public:
	typedef shared_ptr<google::protobuf::Message> MessagePtr;

	ProtobufServer(string name, EventLoop* loop, const InetAddress& listenAddr)
        :name_(name),
        loop_(loop),
		server_(loop, listenAddr, "server"),
		codec_(new ProtobufCodec(this)),
		hostAddress_(listenAddr)
	{
		using namespace std::placeholders;
		server_.setConnectionCallback(
			bind(&ProtobufServer::onConnection, this, _1));
		server_.setMessageCallback(
			bind(&ProtobufCodec::onMessage, codec_.get(), _1, _2, _3));
		server_.setWriteCompleteCallback(bind(&ProtobufServer::onWriteComplete, this, _1));
	}

	void start(){
		server_.start();
	}
	
	void registerMessageCallback(int32_t topType, int32_t msg_type, const CallbackT::ProtobufMessageTCallback& callback){
		if(!dispatchers_.count(topType)){
				LOG_ERROR <<"no such dispatcher_:" << topType;
				return;
		}

		dispatchers_[topType]->registerMessageCallback(msg_type, callback);
	}

	void registerKickOffCallback(const KickOffCallback& callback){
		kickOffCallback_ = callback;
	}


	int32_t addUserWaitWrite(int32_t uid){
		if(userWaitWriteNum_.find(uid) == userWaitWriteNum_.end()){
			userWaitWriteNum_[uid] = 1;
		}else{
			userWaitWriteNum_[uid] += 1;
		}
		return userWaitWriteNum_[uid];
	}
	
	void subUserWaitWrite(int32_t uid){
		if(userWaitWriteNum_.find(uid) == userWaitWriteNum_.end()){
			LOG_ERROR <<"no user to sub:"<<uid;
			return ;
		}else{
			userWaitWriteNum_[uid] -= 1;
			if(userWaitWriteNum_[uid] < 0){
				LOG_ERROR <<"user wait less than 0:"<<uid;
				userWaitWriteNum_[uid] = 0;
			}
		}
		return;
	}

	bool handleWaitWrite(int32_t uid){
		int32_t waitNum = addUserWaitWrite(uid);
		if(waitNum >= MAX_WAIT_WRITE_NUM){
			//kickOffConnectionById(uid);
			return true;
		}
		return false;
	}

	void kickOffConnectionById(int32_t uid, bool needCallback = true){
		if(userConnections_.find(uid) != userConnections_.end()){
			//LOG_ERROR << "no user conn record, uid"<<uid;
			TcpConnectionPtr conn = userConnections_[uid];
			if(conn.get() != NULL){
				conn->shutdown();
			}
			userConnections_.erase(uid);
		}

		if(userWaitWriteNum_.find(uid) != userWaitWriteNum_.end()){
			userWaitWriteNum_.erase(uid);
		}
		
		if(kickOffCallback_ && needCallback){
			kickOffCallback_(uid);
		}

	}

	void sendMessageById(int32_t uid, MessagePtr msg){
		if(userConnections_.find(uid) == userConnections_.end()){
			LOG_ERROR <<"send message no user:"<<uid;
			return;
		}
		TcpConnectionPtr conn = userConnections_[uid];
		if(!conn->connected()){
			//LOG_ERROR << "conn offline:"<<uid;
			return;
		}
		send(conn, msg);
	}

	void sendMessageByIdSet(vector<int32_t> uids, MessagePtr msg){
		for(auto it = uids.begin(); it != uids.end(); ++it){
			sendMessageById(*it, msg);
		}
	}

	void sendMessageByIdSet(set<int32_t> uids, MessagePtr msg){
		for(auto it = uids.begin(); it != uids.end(); ++it){
			sendMessageById(*it, msg);
		}
	}

	void send(const TcpConnectionPtr& conn, const MessagePtr& msg){
		if(conn.get() != NULL && !conn->connected()){
			LOG_ERROR <<"conn is offline";
			return;
		}
		codec_->send(conn, msg);
		if(userConnections_.to.find(conn) == userConnections_.to.end()){
			return;
		}else{
			int32_t uid = userConnections_.to[conn];
			if(handleWaitWrite(uid)){
				conn->shutdown();
				return;
			}
		}
	}

	void onConnection(const TcpConnectionPtr& conn){
		cout << conn->localAddress().toIpPort() << "->"
			<<conn->peerAddress().toIpPort() << " is "
			<<(conn->connected() ? "UP": "DOWN");
		/*
		if(!conn->connected()){
			if(userConnections_.to.find(conn) != userConnections_.to.end()){
				int32_t uid = userConnections_.to[conn];
				userConnections_.erase(uid);
				if(userWaitWriteNum_.find(uid) != userWaitWriteNum_.end()){
					userWaitWriteNum_.erase(uid);
				}
			}
		}*/
		if(!conn->connected()){
			if(userConnections_.to.find(conn) != userConnections_.to.end()){
				int32_t uid = userConnections_.to[conn];
				kickOffConnectionById(uid);
			}
		}
	}

	void onWriteComplete(const TcpConnectionPtr& conn){
		if(userConnections_.to.find(conn) == userConnections_.to.end()){
			return;
		}
		int32_t uid = userConnections_.to[conn];
		if(userWaitWriteNum_.find(uid) == userWaitWriteNum_.end()){
			return;
		}
		
		subUserWaitWrite(uid);	
	}




	void registerConnectionCallback(const ConnectionCallback& cb){
		connectionCallback_ = cb;
	}

	void onUnkownMessage(const TcpConnectionPtr& conn,
						const MessagePtr& message,
						Timestamp){
		cout << "onUnkownMessage: " << message->GetTypeName();
		conn->shutdown();
	}

	int32_t getSid(){
		return sid_;
	}

	string getServerName(){
		return name_;
	}

	ProtobufCodecPtr getCodec(){
		return codec_;
	}






	void updateServerConnection(int32_t sid, TcpConnectionPtr conn){
		ssConnections_[sid] = conn;
		//if(clientConnectCompleteCallback_!= NULL){
		//	clientConnectCompleteCallback_(sid, conn);
		//}
	}

	int32_t getServerIdByConnection(TcpConnectionPtr conn, int32_t& serverId){
		if(ssConnections_.to.find(conn) == ssConnections_.to.end()){
			LOG_ERROR << "no such conn registered";
			return -1;
		}
		serverId =  ssConnections_.to[conn];
		return 0;
	}

	void sendServerMessageBySid(int32_t sid, MessagePtr msg){
		TcpConnectionPtr conn = getServerConnectionBySid(sid);
		if(conn == NULL){
			fsk::LOG_ERROR << "conn null, sid:" <<sid;
		}
		codec_->send(conn, msg);
	}


	TcpConnectionPtr getServerConnectionBySid(int32_t sid){
		if(ssConnections_.find(sid) == ssConnections_.end()){
			fsk::LOG_ERROR << "no such sid:" <<sid;
			return NULL;
		}
		return ssConnections_[sid];
	}







	EventLoop* getEventLoop(){
		return loop_;
	}

	void updateUserConnection(int32_t uid, TcpConnectionPtr conn){
		userConnections_[uid] = conn;
		userWaitWriteNum_[uid] = 0;
	}

	int32_t getUserConnectionById(int32_t uid, TcpConnectionPtr& conn){
		if(userConnections_.find(uid) == userConnections_.end()){
			LOG_ERROR <<"no user:"<<uid;
			return -1;
		}
		conn = userConnections_[uid];
        return 0;
	}

	int32_t getUserIdByConnection(const TcpConnectionPtr& conn, int32_t& id){
		if(userConnections_.to.find(conn) == userConnections_.to.end()){
			LOG_ERROR <<"no such conn:"<<conn->peerAddress().toIpPort();
			return -1;
		}
		id = userConnections_.to[conn];
		return 0;
	}

	bool isUserOnline(int32_t uid){
		if(userConnections_.find(uid) == userConnections_.end()){
			return false;
		}
		return true;
	}

	virtual bool isUserBelongToHost(int32_t){
        return false;
	}




protected:
	string name_;
	int32_t sid_;
	EventLoop* loop_;

	TcpServer server_;
	ProtobufCodecPtr codec_;
	ConnectionCallback connectionCallback_;    
	KickOffCallback kickOffCallback_;
	InetAddress hostAddress_;

	bimap<int32_t, TcpConnectionPtr> ssConnections_;
	bimap<int32_t, TcpConnectionPtr> userConnections_;
	map<int32_t, int32_t> userWaitWriteNum_;
	
	static const int32_t MAX_WAIT_WRITE_NUM = 100;

};

}
}











#endif

