#ifndef FSK_SERVER_H
#define FSK_SERVER_H

#include "common/StringPiece.h"
#include "Callbacks.h"
#include "common/Buffer.h"
#include "InetAddress.h"

#include "common/noncopyable.h"
#include <memory>

struct tcp_info;

namespace fsk{
class Tunnel;
class EventLoop;

namespace net{


class Socket;

class TcpConnection: noncopyable,
 					 public enable_shared_from_this<TcpConnection>
{
public:
	TcpConnection(EventLoop* loop, const string& name, int sockfd,
				const InetAddress& localAddr, const InetAddress& peerAddr);
	~TcpConnection();

	EventLoop* getLoop() const{ return loop_;}
	const string& name() const{ return name_;}
	const InetAddress& localAddress() const{ return localAddr_;}
	const InetAddress& peerAddress() const { return peerAddr_;}

	bool connected() const{ return state_ == kConnected; }
	bool disconnected() const{ return state_ == kDisconnected; }

	bool getTcpInfo(struct tcp_info*) const;
	string getTcpInfoString() const;


	void send(const void* message, int len);
	void send(const StringPiece& message);

	void send(Buffer* message);

	void shutdown();

	void forceClose();
	void forceCloseWithDelay(double seconds);

	void setTcpNoDelay(bool on);

	void setConnectionCallback(const ConnectionCallback& cb){
		connectionCallback_ = cb;
	}

	void setMessageCallback(const MessageCallback& cb){
		messageCallback_ = cb;
	}

	void setWriteCompleteCallback(const WriteCompleteCallback& cb){
		writeCompleteCallback_ = cb;
	}

	void setHighWaterMarkCallback(const HighWaterMarkCallback& cb, size_t highWaterMark){
		highWaterMarkCallback_ = cb;
		highWaterMark_ = highWaterMark;
	}

	Buffer* inputBuffer(){
		return &inputBuffer_;
	}

	Buffer* outputBuffer(){
		return &outputBuffer_;
	}

	void setCloseCallback(const CloseCallback& cb){
		closeCallback_ = cb;
	}

	void connectEstablished();
	void connectDestroyed();


private:
	enum StateE { kDisconnected, kConnecting, kConnected, kDisconnecting};

	void handleRead(Timestamp receiveTime);
	void handleWrite();
	void handleClose();
	void handleError();
	void sendInLoop(const StringPiece& message);
	void sendInLoop(const void* message, size_t len);

	void shutdownInLoop();
	void forceCloseInLoop();
	void setState(StateE s) { state_ = s;}
	const char* stateToString() const;


	EventLoop* loop_;
	const string name_;
	StateE state_;
	unique_ptr<Socket> socket_;
	unique_ptr<Tunnel> tunnel_;
	const InetAddress localAddr_;
	const InetAddress peerAddr_;

	ConnectionCallback connectionCallback_;
	MessageCallback messageCallback_;
	WriteCompleteCallback writeCompleteCallback_;

	HighWaterMarkCallback highWaterMarkCallback_;
	CloseCallback closeCallback_;
	size_t highWaterMark_;
	Buffer inputBuffer_;
	Buffer outputBuffer_;
  bool reading_;
};

typedef shared_ptr<TcpConnection> TcpConnectionPtr;

}
}






















#endif
