#ifndef FSK_SERVER_TCPSERVER_H
#define FSK_SERVER_TCPSERVER_H

#include "common/Atomic.h"
#include <functional>
#include "TcpConnection.h"
#include "common/noncopyable.h"
#include <map>
#include "common/TimerId.h"

namespace fsk{

class EventLoop;

namespace net{

class Acceptor;


class TcpServer: noncopyable{

public:
	typedef function<void(EventLoop*)> ThreadInitCallback;
	enum Option{
		kNoReusePort,
		kReusePort
	};

	TcpServer(EventLoop* loop, const InetAddress& listenAddr, const string& nameArg, Option option = kNoReusePort);

	~TcpServer();

	const string& hostport() const{ return hostport_;}
	const string& name() const{ return name_;}
	EventLoop* getLoop() const{ return loop_;}

	/*
	shared_ptr<EventLoopThreadPool> threadPool(){
		return threadPool_;
	}*/

	void start();

	void setConnectionCallback(const ConnectionCallback& cb){
		connectionCallback_ = cb;
	}

	void setMessageCallback(const MessageCallback& cb){
		messageCallback_ = cb;
	}

	void setWriteCompleteCallback(const WriteCompleteCallback& cb){
		writeCompleteCallback_ = cb;
	}

	TimerId runAfter(double delay, const TimerCallback& cb);
	TimerId runEvery(double interval, const TimerCallback& cb);
	void cancel(TimerId timerId);

private:
	void newConnection(int sockfd, const InetAddress& peerAddr);
	void removeConnection(const TcpConnectionPtr& conn);
	void removeConnectionInLoop(const TcpConnectionPtr& conn);

	typedef std::map<string, TcpConnectionPtr> ConnectionMap;

	EventLoop* loop_;
	const string hostport_;
	const string name_;
	unique_ptr<Acceptor> acceptor_;
	//unique_ptr<EventLoopThreadPool> threadPool_;
	ConnectionCallback connectionCallback_;
	MessageCallback messageCallback_;
	WriteCompleteCallback writeCompleteCallback_;
	AtomicInt32 started_;
	int nextConnId_;
	ConnectionMap connections_;


};

}

}




#endif
