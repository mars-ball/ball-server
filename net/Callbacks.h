#ifndef __KUNSERVER_CALLBACKS_H
#define __KUNSERVER_CALLBACKS_H

#include<functional>
#include<memory>

#include"../common/Timestamp.h"

namespace fsk{

typedef function<void()> Functor;
typedef function<void()> TimerCallback;


namespace net{

class Buffer;
class TcpConnection;
typedef shared_ptr<TcpConnection> TcpConnectionPtr;

typedef function<void(const TcpConnectionPtr&)> ConnectionCallback;
typedef function<void(const TcpConnectionPtr&)> CloseCallback;
typedef function<void(const TcpConnectionPtr&)> WriteCompleteCallback;
typedef function<void(const TcpConnectionPtr&, size_t)> HighWaterMarkCallback;


typedef function<void(const TcpConnectionPtr&,
						Buffer*,
						Timestamp)> MessageCallback;

void defaultConnectionCallback(const TcpConnectionPtr& conn);
void defaultMessageCallback(const TcpConnectionPtr& conn,
							Buffer* buffer,
							Timestamp receiveTime);

typedef function<void(int32_t uid)> KickOffCallback;

typedef function<void(int32_t sid, const TcpConnectionPtr&)> ClientConnectCompleteCallback;


typedef function<void()> CallBackWithNoParamter;




}

}













#endif
