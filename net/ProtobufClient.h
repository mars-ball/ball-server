#pragma once

#include "common/noncopyable.h"
#include <thread>
#include "TcpClient.h"
#include "EventLoop.h"
#include "common/Timestamp.h"
#include "InetAddress.h"
#include "Dispatcher.h"
#include "Codec.h"
#include "MessageDefine.h"
#include "TcpConnection.h"
#include <string>
#include "common/Logging.h"
#include "Dispatchable.h"
#include "ProtobufServer.h"

using namespace std;
namespace fsk{
namespace net{

typedef shared_ptr<ProtobufCodec> ProtobufCodecPtr;
typedef shared_ptr<ProtobufServer> ProtobufServerPtr;

class ProtobufClient: noncopyable, public Dispatchable{
public:
	typedef shared_ptr<google::protobuf::Message> MessagePtr;

	ProtobufClient(string name, EventLoop* loop, const InetAddress& toAddr, ProtobufServer* server)
	:connected_(false),
	name_(name),
	client_(loop, toAddr, name),
	codec_(server->getCodec()),
	server_(server)
	{
		using namespace std::placeholders;
		client_.setConnectionCallback(bind(&ProtobufClient::onConnection, this, _1));
		client_.setMessageCallback(
			bind(&ProtobufCodec::onMessage, codec_.get(), _1, _2, _3));
	}

	void start(){
		client_.connect();
	}
	
	void registerMessageCallback(int32_t topType, int32_t msg_type, const CallbackT::ProtobufMessageTCallback& callback){
		if(!dispatchers_.count(topType)){
				LOG_ERROR <<"no such dispatcher_:" << topType;
				return;
		}

		dispatchers_[topType]->registerMessageCallback(msg_type, callback);
	}

	void registClientConnectCompleteCallback(const ClientConnectCompleteCallback& cb){
		clientConnectCompleteCallback_ = cb;
	}

	void onConnection(const TcpConnectionPtr& conn){
		cout << conn->localAddress().toIpPort() << "->"
			<<conn->peerAddress().toIpPort() << " is "
			<<(conn->connected() ? "UP": "DOWN");
		
		if(conn->connected() ){
			connected_ = true;
			conn_ = conn;
			server_->updateServerConnection(id_, conn);
			if(clientConnectCompleteCallback_ != NULL){
				clientConnectCompleteCallback_(server_->getSid(), conn);
			}
		}else{
			connected_ = false;
			conn_ = nullptr;
		}
	}

	void onUnkownMessage(const TcpConnectionPtr& conn,
						const MessagePtr& message,
						Timestamp){
		LOG_INFO << "onUnkownMessage: " << message->GetTypeName();
		conn->shutdown();
	}

	string getServerName(){
		return name_;
	}

	void send(MessagePtr msg){
		codec_->send(conn_, msg);

	}

	bool isConnected(){
		return connected_;
	}

	void setId(int32_t id){
		id_ = id;
	}

	int32_t getId(){
		return id_;
	}

protected:
	bool connected_;
	string name_;
	TcpClient client_;
	ProtobufCodecPtr codec_;
	TcpConnectionPtr conn_;
	ProtobufServerPtr server_;
	int32_t id_;
	ClientConnectCompleteCallback clientConnectCompleteCallback_;

};

}
}
