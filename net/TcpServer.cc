#include "TcpServer.h"
#include "Acceptor.h"
#include "EventLoop.h"
#include "SocketsOps.h"

#include <stdio.h>
#include <memory>

using namespace fsk;
using namespace fsk::net;

TcpServer::TcpServer(EventLoop* loop,
					const InetAddress& listenAddr,
					const string& nameArg,
					Option option)
	:loop_(loop),
	hostport_(listenAddr.toIpPort()),
	name_(nameArg),
	acceptor_(new Acceptor(loop, listenAddr, option == kReusePort)),
	connectionCallback_(defaultConnectionCallback),
	messageCallback_(defaultMessageCallback),
	nextConnId_(1)
{
	acceptor_->setNewConnectionCallback(
		bind(&TcpServer::newConnection, this, placeholders::_1, placeholders::_2));
}

TcpServer::~TcpServer(){
	loop_->assertInLoopThread();
	for(ConnectionMap::iterator it(connections_.begin());
		it != connections_.end(); ++it){
		TcpConnectionPtr conn = it->second;
		it->second.reset();
		conn->getLoop()->runInLoop(
			bind(&TcpConnection::connectDestroyed, conn));
		conn.reset();

	}
}


void TcpServer::start(){
	if(started_.getAndSet(1) == 0){
		assert(!acceptor_->listenning());
		loop_->runInLoop(bind(&Acceptor::listen, acceptor_.get()));
	}
}

void TcpServer::newConnection(int sockfd, const InetAddress& peerAddr){

	loop_->assertInLoopThread();
	char buf[32];

	snprintf(buf, sizeof buf, ":%s#%d", hostport_.c_str(), nextConnId_);

	++nextConnId_;

	string connName = name_ + buf;

	InetAddress localAddr(sockets::getLocalAddr(sockfd));

	TcpConnectionPtr conn(new TcpConnection(loop_, connName, sockfd, localAddr, peerAddr));

	connections_[connName] = conn;
	conn->setConnectionCallback(connectionCallback_);
	conn->setMessageCallback(messageCallback_);
	conn->setWriteCompleteCallback(writeCompleteCallback_);
	conn->setCloseCallback(bind(&TcpServer::removeConnection, this, placeholders::_1));

	loop_->runInLoop(bind(&TcpConnection::connectEstablished, conn));

}


void TcpServer::removeConnection(const TcpConnectionPtr& conn){

	loop_->runInLoop(bind(&TcpServer::removeConnectionInLoop, this, conn));
}

void TcpServer::removeConnectionInLoop(const TcpConnectionPtr& conn){

	loop_->assertInLoopThread();
	size_t n = connections_.erase(conn->name());
	(void)n;
	assert(n == 1);
	
	loop_->queueInLoop(bind(&TcpConnection::connectDestroyed, conn));

}



































