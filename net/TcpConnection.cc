#include "TcpConnection.h"

#include "Tunnel.h"
#include "EventLoop.h"
#include "Socket.h"
#include "SocketsOps.h"
#include "string.h"

#include <functional>
#include <errno.h>

using namespace fsk;
using namespace fsk::net;


void fsk::net::defaultConnectionCallback(const TcpConnectionPtr& conn){

	cout << conn->localAddress().toIpPort() << " -> "
		<< conn->peerAddress().toIpPort() << " is "
		<< (conn->connected() ? "UP" : "DOWN");

}

void fsk::net::defaultMessageCallback(const TcpConnectionPtr&, Buffer* buf, Timestamp){
	buf->retrieveAll();
}

TcpConnection::TcpConnection(EventLoop* loop,
							const string& nameArg,
							int sockfd,
							const InetAddress& localAddr,
							const InetAddress& peerAddr)
	:loop_(loop),
	name_(nameArg),
	state_(kConnecting),
	socket_(new Socket(sockfd)),
	tunnel_(new Tunnel(loop, sockfd)),
	localAddr_(localAddr),
	peerAddr_(peerAddr),
	highWaterMark_(64*1024*1024)
{
	tunnel_->setReadCallback(bind(&TcpConnection::handleRead, this, placeholders::_1));
	tunnel_->setWriteCallback(bind(&TcpConnection::handleWrite, this ));
	tunnel_->setCloseCallback(bind(&TcpConnection::handleClose, this ));
	tunnel_->setErrorCallback(bind(&TcpConnection::handleError, this));

	socket_->setKeepAlive(true);
}

TcpConnection::~TcpConnection(){
	cout << "TcpConnection::dtor[" << name_ << "] at " <<this
		<< " fd= " << tunnel_->fd()
		<< " state= " <<stateToString();
}


bool TcpConnection::getTcpInfo(struct tcp_info* tcpi) const{
	return socket_->getTcpInfo(tcpi);
}

string TcpConnection::getTcpInfoString() const{
	char buf[1024];
	buf[0] = '\0';
	socket_->getTcpInfoString(buf, sizeof buf);
	return buf;
}

void TcpConnection::send(const void* data, int len){
	send(StringPiece(static_cast<const char*>(data), len));
}


void TcpConnection::send(const StringPiece& message){
	if(state_ == kConnected){
		if(loop_->isInLoopThread()){
			sendInLoop(message);
		}else{
			loop_->runInLoop(bind(static_cast<void(TcpConnection::*)(const StringPiece &)>(&TcpConnection::sendInLoop),
							this, message.as_string()));
		}
	}
}

void TcpConnection::send(Buffer* buf){
	if(state_ == kConnected){
		if(loop_->isInLoopThread()){
			sendInLoop(buf->peek(), buf->readableBytes());
			buf->retrieveAll();
		}else{
			loop_->runInLoop(bind(static_cast<void(TcpConnection::*)(const StringPiece &)>(&TcpConnection::sendInLoop)
							, this, buf->retrieveAllAsString()));
		}
	}
}


void TcpConnection::sendInLoop(const StringPiece& message){
	sendInLoop(message.data(), message.size());
}

void TcpConnection::sendInLoop(const void* data, size_t len){

	loop_->assertInLoopThread();
	ssize_t nwrote = 0;
	size_t remaining = len;
	bool faultError = false;

	if(state_ == kDisconnected){
		cout << "diconnected, give up";
		return;
	}
	if(!tunnel_->isWriting() && outputBuffer_.readableBytes() == 0){
		nwrote = sockets::write(tunnel_->fd(), data, len);
		if(nwrote >= 0){
			/*
			cout <<"write " << nwrote<<" :";

			for(int32_t ii = 0; ii < nwrote; ii++){
				cout << static_cast<int32_t>((static_cast<const char*>(data))[ii]) << " ";
			}
			cout <<endl;
			*/
			remaining = len - nwrote;
			if(remaining == 0 && writeCompleteCallback_){
				loop_->queueInLoop(bind(writeCompleteCallback_, shared_from_this()));
			}
		}else{
			nwrote = 0;
			if(errno != EWOULDBLOCK){
				if(errno == EPIPE || errno == ECONNRESET){
					faultError = true;
				}
			}
		}
	}

	assert(remaining <= len);
	if(!faultError && remaining > 0){
		size_t oldLen = outputBuffer_.readableBytes();
		if(oldLen + remaining >= highWaterMark_
			&& oldLen < highWaterMark_
			&& highWaterMarkCallback_){
			loop_->queueInLoop(bind(highWaterMarkCallback_, shared_from_this(), oldLen + remaining));
		}
		outputBuffer_.append(static_cast<const char*>(data)+nwrote, remaining);
		if(!tunnel_->isWriting()){
			tunnel_->enableWriting();
		}
	}
}



void TcpConnection::shutdown(){
	if(state_ == kConnected){
		setState(kDisconnecting);
		loop_->runInLoop(bind(&TcpConnection::shutdownInLoop, this));
	}
}

void TcpConnection::shutdownInLoop(){
	loop_->assertInLoopThread();
	if(!tunnel_->isWriting()){
		socket_->shutdownWrite();
	}
}

void TcpConnection::forceClose(){

	if(state_ == kConnected || state_ == kDisconnecting){
		setState(kDisconnecting);
		loop_->queueInLoop(bind(&TcpConnection::forceCloseInLoop, shared_from_this()));
	}
}


void TcpConnection::forceCloseInLoop(){
	loop_->assertInLoopThread();
	if(state_ == kConnected || state_ == kDisconnecting){
		handleClose();
	}
}


const char* TcpConnection::stateToString() const{
	switch(state_){
		case kDisconnected:
			return "disconnected";
		case kConnecting:
			return "connecting";
		case kConnected:
			return "connected";
		case kDisconnecting:
			return "disconnecting";
		default:
			return "unkown state";
	}
}


void TcpConnection::setTcpNoDelay(bool on){
	socket_->setTcpNoDelay(on);
}

void TcpConnection::connectEstablished(){
	loop_->assertInLoopThread();
	assert(state_ == kConnecting);
	setState(kConnected);
	//tunnel_->tie(shared_from_this());
	tunnel_->enableReading();
	connectionCallback_(shared_from_this());

}

void TcpConnection::connectDestroyed(){
	loop_->assertInLoopThread();
	if(state_ == kConnected){
		setState(kDisconnected);
		tunnel_->disableAll();

		connectionCallback_(shared_from_this());
	}
	tunnel_->remove();
}

void TcpConnection::handleRead(Timestamp receiveTime){

	loop_->assertInLoopThread();
	int savedErrno = 0;
	ssize_t n = inputBuffer_.readFd(tunnel_->fd(), &savedErrno);
	if(n > 0){
		messageCallback_(shared_from_this(), &inputBuffer_, receiveTime);
	}else if(n == 0){
		handleClose();

	}else{
		errno = savedErrno;
		handleError();
	}

}


void TcpConnection::handleWrite(){

	loop_->assertInLoopThread();
	if(tunnel_->isWriting()){
		ssize_t n = sockets::write(tunnel_->fd(), outputBuffer_.peek(), outputBuffer_.readableBytes());
		if(n > 0){
			/*
			cout <<"handle output " << n <<" :";
			for(int ii = 0; ii < n ; ii++){
				cout << static_cast<int32_t>(outputBuffer_.peek()[ii]) <<" ";
			}
			cout <<endl;
			*/
			outputBuffer_.retrieve(n);
			if(outputBuffer_.readableBytes() == 0){
				tunnel_->disableWriting();
				if(writeCompleteCallback_){
					loop_->queueInLoop(bind(writeCompleteCallback_, shared_from_this()));
				}
				if(state_ == kDisconnecting){
					shutdownInLoop();
				}
			}
		}else{
			cout << " handleWriter" <<endl;
		}

	}else{
		cout << " connection fd = " << tunnel_->fd()
			<< "is down" <<endl;
	}
}

void TcpConnection::handleClose(){

	loop_->assertInLoopThread();
	cout <<" fd = " << tunnel_->fd() << " state= "<<stateToString();
	assert(state_ == kConnected || state_ == kDisconnecting);
	setState(kDisconnected);
	tunnel_->disableAll();

	TcpConnectionPtr guardThis(shared_from_this());
	connectionCallback_(guardThis);
	closeCallback_(guardThis);

}


void TcpConnection::handleError(){
	int err = sockets::getSocketError(tunnel_->fd());
	cout << "handle error [" <<name_ << "] - SO_ERROR =" << err << " " << strerror(err) <<endl;
}
























