#ifndef __KUNSERVER_ACCEPTOR
#define __KUNSERVER_ACCEPTOR

#include <functional>
#include "common/noncopyable.h"

#include "Tunnel.h"
#include "Socket.h"


namespace fsk{

class EventLoop;


namespace net{


class InetAddress;


class Acceptor: noncopyable{
public:
	typedef function<void(int sockfd, const InetAddress&)> NewConnectionCallback;

	Acceptor(EventLoop* loop, const InetAddress& listenAddr, bool reuseport);
	~Acceptor();

	void setNewConnectionCallback(const NewConnectionCallback& cb){
		newConnectionCallback_ = cb;}

	bool listenning() const { return listenning_;}

	void listen();

private:
	void handleRead();

	EventLoop* loop_;
	Socket acceptSocket_;
	Tunnel acceptTunnel_;
	NewConnectionCallback newConnectionCallback_;
	bool listenning_;
	int idleFd_;


};
}

}
















#endif
