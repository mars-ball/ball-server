#include "Poller.h"
#include "Tunnel.h"
#include <errno.h>
#include <poll.h>
#include <assert.h>

using namespace fsk;


namespace{
const int kNew = -1;
const int kAdded = 1;
const int kDel = 2;

}

Poller::Poller(EventLoop* loop)
: ownerLoop_(loop)
{
}

Poller::~Poller(){
}

bool Poller::hasTunnel(Tunnel* tunnel)const{
	assertInLoopThread();
	TunnelMap::const_iterator it = tunnels_.find(tunnel->fd());
	return it != tunnels_.end() && it->second == tunnel;

}

Timestamp Poller::poll(int timeoutMs, TunnelList* activeTunnels){

	int numEvents = ::poll(&*pollfds_.begin(), pollfds_.size(), timeoutMs);
	int savedErrno = errno;
	Timestamp now(Timestamp::now());
	if(numEvents > 0){
		//cout << numEvents << " events happended" <<endl;
		fillActiveTunnels(numEvents, activeTunnels);
	}else if(numEvents == 0){
		cout <<"nothing happended" <<endl;
	}else{
		if(savedErrno != EINTR){
			errno = savedErrno;
			cout << "poll failed"<<endl;
		}
	}
	return now;
}


void Poller::fillActiveTunnels(int numEvents, TunnelList* activeTunnels) const{
	for(PollfdList::const_iterator pfd = pollfds_.begin();
		pfd != pollfds_.end() && numEvents > 0; ++pfd){
		if(pfd->revents > 0){
			--numEvents;
			TunnelMap::const_iterator ch = tunnels_.find(pfd->fd);
			Tunnel* tunnel = ch->second;
			tunnel->set_revents(pfd->revents);
			activeTunnels->push_back(tunnel);
		}
	

	}
}


void Poller::updateTunnel(Tunnel* tunnel){
	Poller::assertInLoopThread();
	if(tunnel->index() < 0){
		assert(tunnels_.find(tunnel->fd()) == tunnels_.end());
		struct pollfd pfd;
		pfd.fd = tunnel->fd();
		pfd.events = static_cast<short>(tunnel->events());
		pfd.revents = 0;
		pollfds_.push_back(pfd);
		int idx = static_cast<int>(pollfds_.size()) -1;
		tunnel->set_index(idx);
		tunnels_[pfd.fd] = tunnel;	
	}else{
		assert(tunnels_.find(tunnel->fd()) != tunnels_.end());
		assert(tunnels_[tunnel->fd()] == tunnel);
		int idx = tunnel->index();
		assert(0 <= idx && idx < static_cast<int>(pollfds_.size()));
		struct pollfd& pfd = pollfds_[idx];
		pfd.events = static_cast<short>(tunnel->events());
		pfd.revents = 0;
		if(tunnel->isNoneEvent()){
			pfd.fd = -tunnel->fd() - 1;
		}
	}

}

void Poller::removeTunnel(Tunnel* tunnel){
	Poller::assertInLoopThread();
	int fd = tunnel->fd();
	assert(tunnels_.find(fd) != tunnels_.end());
	assert(tunnels_[fd] == tunnel);
//	assert(tunnel->isNoneEvent());

	int idx = tunnel->index();

	assert(0 <= idx && idx < static_cast<int>(pollfds_.size()));
	const struct pollfd& pfd = pollfds_[idx]; (void)pfd;
	assert(pfd.fd == -tunnel->fd()-1 && pfd.events == tunnel->events());
	size_t n = tunnels_.erase(tunnel->fd());
	assert(n == 1); 
	if(static_cast<size_t>(idx) == pollfds_.size() - 1){
		pollfds_.pop_back();
	}else{
		int tunnelAtEnd = pollfds_.back().fd;
		iter_swap(pollfds_.begin() + idx, pollfds_.end() - 1);
		if(tunnelAtEnd < 0){
			tunnelAtEnd = -tunnelAtEnd -1;
		}
		tunnels_[tunnelAtEnd]->set_index(idx);
		pollfds_.pop_back();	
	}

}



