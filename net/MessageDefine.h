#ifndef KUN_SERVER_MESSAGE_DEFINE_H
#define KUN_SERVER_MESSAGE_DEFINE_H

#include "CommonInc.h"
#include "google/protobuf/message.h"
#include "common/Logging.h"

using namespace fsk;
using namespace google;
using namespace google::protobuf;
using namespace std;

namespace fsk{
namespace net{

class MessageDefine{
private:
	MessageDefine():isInit(false){};
	MessageDefine(MessageDefine const&) = delete;
	void operator=(MessageDefine const&) = delete;
	
public:
	bool isInit;
	static MessageDefine& getInstance(){
		static MessageDefine instance;
		return instance;
	}

   void AddIdDes(int32_t topKind, int32_t id, const Descriptor* des){
        idDesDic_[id] = des;
        desIdDic_[des] = id;
        topKindSet_[id] = topKind;
    }

    int32_t GetTopKindByDes(const Descriptor* des){
		int32_t id =  desIdDic_[des];
		int32_t top = GetTopKindById(id);
		return top;
	}

	int32_t GetTopKindById(int32_t id){
		if(topKindSet_.find(id) == topKindSet_.end()){
			LOG_ERROR <<"no top kind saved, id:" <<id;
			return 0;
		}
		int32_t top = topKindSet_[id];
		return top;

	}


	int32_t GetMsgIdByDes(const Descriptor* des){
		return desIdDic_[des];
	}

	const Descriptor* GetDesByMsgId(int32_t msgId){
		return idDesDic_[msgId];
	}

	void AddIdName(int32_t id, string name){
		nameIdDic_[name] = id;
		idNameDic_[id] = name;
	}

	int32_t GetIdByName(string name){
		return nameIdDic_[name];
	}

	string GetNameById(int32_t id){
		return idNameDic_[id];
	}

private:
	typedef map<string, int32_t> NameIdDic;
	typedef map<int32_t, string> IdNameDic;
	typedef map<int32_t, int32_t> TopKindSet;

	NameIdDic nameIdDic_;
	IdNameDic idNameDic_; 
	
	typedef map<const Descriptor*, int32_t> DesIdDic;
	typedef map<int32_t, const Descriptor*> IdDesDic;
	DesIdDic desIdDic_;
	IdDesDic idDesDic_;
	TopKindSet topKindSet_;

};


}
}





#endif
