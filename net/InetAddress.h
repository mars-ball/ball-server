#ifndef FSK_SERVER_INETADDR_H
#define FSK_SERVER_INETADDR_H

#include <netinet/in.h>
#include "common/StringPiece.h"

namespace fsk{
namespace net{

class InetAddress {

public:
	explicit InetAddress(uint16_t port = 0, bool loopbackOnly = false);

	InetAddress(StringArg ip, uint16_t port);

	InetAddress(const struct sockaddr_in& addr)
		:addr_(addr){
	}

	string toIp() const;
	string toIpPort() const;
	uint16_t toPort() const;

	const struct sockaddr_in& getSockAddrInet() const{return addr_;}

	void setSockAddrInet(const struct sockaddr_in& addr){ addr_ = addr;}

	uint32_t ipNetEndian() const { return addr_.sin_addr.s_addr;}
	uint16_t portNetEndian() const { return addr_.sin_port;}

	static bool resolve(StringArg hostname, InetAddress* result);

private:
	struct sockaddr_in addr_;
};

}
}



#endif

