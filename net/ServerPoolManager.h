#pragma once
#include<fstream>
#include "stdint.h"
#include<memory>
#include"common/Logging.h"
#include"net/ProtobufClient.h"

#include <map>
#include "common/CfgReader.h"
#include <string>
#include "net/TcpClient.h"

using namespace std;

namespace fsk{
namespace net{

typedef shared_ptr<ProtobufClient> ProtobufClientPtr;

struct SSConnectionInfo{
	int32_t sid_;
	string name_;
	string ip_;
	int32_t port_;

	SSConnectionInfo(int32_t sid, string name, string ip, int32_t port)
	: sid_(sid),
	  name_(name),
	  ip_(ip),
	  port_(port)	
	{};

};

typedef shared_ptr<SSConnectionInfo> SSConnectionInfoPtr;

typedef std::pair<SSConnectionInfoPtr, ProtobufClientPtr> ConnectionInfoPair;

class ServerPoolManager{
public:
	static const string cfgFileName_;

	ServerPoolManager(ProtobufServer* rootServer)	
	:rootServer_(rootServer){
	}

	virtual int32_t StartFromCfg()=0;

	void startNewConnection(int32_t sid, string name, string ip, int32_t port);
	bool isServerAllConnected();
	void sendByServerId(int32_t sid, MessagePtr msg);
	void sendByServerIndex(int32_t index, MessagePtr msg);
	int32_t getServerIdByIndex(int32_t index);

	int32_t getHostIndex(){return hostIndex_;}
	void setHostIndex(int32_t index){hostIndex_ = index;}


	void sendBroadCast(MessagePtr msg);
	int32_t getClientById(int32_t id, ProtobufClientPtr& client);	
	SSConnectionInfoPtr getConnectionInfoById(int32_t id);

	void registClientConnectCompleteCallback(const ClientConnectCompleteCallback& cb);

	virtual ~ServerPoolManager(){};

public:
	int32_t connectionNum_;
	int32_t hostIndex_;

protected:
	ProtobufServer* rootServer_;
	map<int32_t, ConnectionInfoPair> connectors_;
	vector<int32_t> serverIdBySort_;
	ClientConnectCompleteCallback clientConnectCompleteCallback_;

};


}
}

