#include "SocketsOps.h"
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <strings.h>
#include <sys/socket.h>
#include "Endian.h"
#include <iostream>
#include <assert.h>
#include <string.h>

using namespace std;

namespace fsk{

namespace sockets{


void setNonBlockAndCloseOnExec(int sockfd){
	int flags = ::fcntl(sockfd, F_GETFL, 0);
	flags |= O_NONBLOCK;
	int ret = ::fcntl(sockfd, F_SETFL, flags);

	flags = ::fcntl(sockfd, F_GETFD, 0);
	flags |= FD_CLOEXEC;
	ret = ::fcntl(sockfd, F_SETFD, flags);
	(void)ret;
}

const struct sockaddr* sockaddr_cast(const struct sockaddr_in* addr){
	return static_cast<const struct sockaddr*>(static_cast<const void*>(addr));
}

struct sockaddr* sockaddr_cast(struct sockaddr_in* addr){
	return static_cast<struct sockaddr*>(static_cast<void*>(addr));
}

const struct sockaddr_in* sockaddr_in_cast(const struct sockaddr* addr){
	return static_cast<const struct sockaddr_in*>(static_cast<const void*>(addr));
}

struct sockaddr_in* sockaddr_in_cast(struct sockaddr* addr){
	return static_cast<struct sockaddr_in*>(static_cast<void*>(addr));
}


int createNonblockingOrDie(){
    
#if VALGRIND || defined(__APPLE__)
	int sockfd = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(sockfd < 0){
		cout << "create nonblock or die"<<endl;
	}

	setNonBlockAndCloseOnExec(sockfd);
#else
	int sockfd = ::socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK | SOCK_CLOEXEC, IPPROTO_TCP);
	if(sockfd < 0){
		cout << "create non blocking or die"<<endl;
	}

#endif
	return sockfd;

}

void bindOrDie(int sockfd, const struct sockaddr_in& addr){
	int ret = ::bind(sockfd, sockaddr_cast(&addr), static_cast<socklen_t>(sizeof addr));
	if(ret < 0){
		cout << "bind or die" <<endl;
	}
}

void listenOrDie(int sockfd){
	int ret = ::listen(sockfd, SOMAXCONN);
	if(ret < 0){
		cout << "listen or die" << endl;
	}

}

int accept(int sockfd, struct sockaddr_in *addr){
	socklen_t addrlen = static_cast<socklen_t>(sizeof *addr);
#if VALGRIND || defined(NO_ACCEPT4) || defined(__APPLE__)
	int connfd = ::accept(sockfd, sockaddr_cast(addr), &addrlen);
	setNonBlockAndCloseOnExec(connfd);
#else
	int connfd = ::accept4(sockfd, sockaddr_cast(addr), &addrlen, SOCK_NONBLOCK|SOCK_CLOEXEC);

#endif
	if(connfd < 0){
		int savedErrno = errno;
		cout << "accept"<<endl;
		switch(savedErrno){
		case EAGAIN:
		case ECONNABORTED:
		case EINTR:
		case EPROTO:
		case EPERM:
			errno = savedErrno;
			break;
		case EBADF:
		case EFAULT:
		case EINVAL:
		case ENOBUFS:
		case ENOMEM:
		case ENOTSOCK:
		case EOPNOTSUPP:
			cout << "unexpected error of ::accept" <<savedErrno;
			break;
		default:
			cout << " unkown error of ::accept" <<savedErrno;
		}
	}
	return connfd;

}

int connect(int sockfd, const struct sockaddr_in& addr){
	return ::connect(sockfd, sockaddr_cast(&addr), static_cast<socklen_t>(sizeof addr));

}

ssize_t read(int sockfd, void *buf, size_t count){
	return ::read(sockfd, buf, count);
}

ssize_t readv(int sockfd, const struct iovec* iov, int iovcnt){
#ifndef __APPLE__
	return ::readv(sockfd, iov, iovcnt);
#else
    return readv(sockfd, iov, iovcnt);
#endif
}

ssize_t write(int sockfd, const void* buf, size_t count){
	return ::write(sockfd, buf, count);
}

void close(int sockfd){
	if(::close(sockfd)< 0){
		cout << "sockets close failed"<<endl;
	}
}

void shutdownWrite(int sockfd){
	if(::shutdown(sockfd, SHUT_WR)< 0){
		cout << "shutdown failed"<<endl;
	}
}

void toIpPort(char *buf, size_t size, const struct sockaddr_in& addr){
	assert(size > INET_ADDRSTRLEN);
	::inet_ntop(AF_INET, &addr.sin_addr, buf, static_cast<socklen_t>(size));
	size_t end = ::strlen(buf);
	uint16_t port = networkToHost16(addr.sin_port);
	assert(size > end);
	snprintf(buf + end, size-end, ":%u", port);
}

void toIp(char* buf, size_t size, const struct sockaddr_in& addr){
	assert(size >= INET_ADDRSTRLEN);
	::inet_ntop(AF_INET, &addr.sin_addr, buf, static_cast<socklen_t>(size));
}

void fromIpPort(const char* ip, uint16_t port, struct sockaddr_in* addr){
	addr->sin_family = AF_INET;
	addr->sin_port = hostToNetwork16(port);
	if(::inet_pton(AF_INET, ip, &addr->sin_addr) <= 0){
		cout << "inet pton failed"<<endl;
	}
}

int getSocketError(int sockfd){
	int optval;
	socklen_t optlen = static_cast<socklen_t>(sizeof optval);

	if(::getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &optval, &optlen) < 0){
		return errno;
	}else{
		return optval;
	}
}


struct sockaddr_in getLocalAddr(int sockfd){
	struct sockaddr_in localaddr;
	bzero(&localaddr, sizeof localaddr);
	socklen_t addrlen = static_cast<socklen_t>(sizeof localaddr);
	if(::getsockname(sockfd, sockaddr_cast(&localaddr), &addrlen) < 0){
		cout << "get local addr"<<endl;
	}
	return localaddr;
}

struct sockaddr_in getPeerAddr(int sockfd){
	struct sockaddr_in peeraddr;
	bzero(&peeraddr, sizeof peeraddr);
	socklen_t addrlen = static_cast<socklen_t>(sizeof peeraddr);
	if(::getpeername(sockfd, sockaddr_cast(&peeraddr), &addrlen) < 0){
		cout << "get peer addr"<<endl;
	}
	return peeraddr;
}

bool isSelfConnect(int sockfd){
	struct sockaddr_in localaddr = getLocalAddr(sockfd);
	struct sockaddr_in peeraddr = getPeerAddr(sockfd);
	return localaddr.sin_port == peeraddr.sin_port
		&& localaddr.sin_addr.s_addr == peeraddr.sin_addr.s_addr;
}

}

}


