//
//  EventLoop.h
//  KunServer
//
//  Created by meryn on 15/12/6.
//  Copyright (c) 2015年 meryn. All rights reserved.
//

#ifndef __KunServer__EventLoop__
#define __KunServer__EventLoop__

#include <iostream>
#include <vector>

#include <sys/types.h>
#include <thread>
#include "common/CurrThread.h"
#include "common/Timestamp.h"
#include "common/TimerId.h"
#include "Callbacks.h"
#include "common/Mutex.h"

namespace fsk{
    
class Poller;
class Tunnel;
class TimerQueue;

class EventLoop{
typedef std::vector<Tunnel*> TunnelList;
public:
	//typedef function<void()> Functor;

    EventLoop();
    ~EventLoop();
    void loop();
	void quit();
    bool isInLoopThread() const{
        return CurrThread::tHasher(threadId_) == CurrThread::tHasher(CurrThread::tid());
    }
    void assertInLoopThread() const{
        if(!isInLoopThread()){
            std::cout << "error not in loop thread";
        }
    }
	void updateTunnel(Tunnel* tunnel);    
    void removeTunnel(Tunnel* tunnel);
	EventLoop* getLoopFromCurrThread();

	void runInLoop(const Functor& cb);
	void queueInLoop(const Functor& cb);
	TimerId runAt(const Timestamp& time, const TimerCallback& cb);
	TimerId runAfter(double delay, const TimerCallback& cb);
	TimerId runEvery(double interval, const TimerCallback& cb);

	void cancel(TimerId timerId);
	void wakeup();

    
private:
	void handleRead();
	void doPendingFunctors();

    bool isLooping;
    const thread::id threadId_;
	bool quit_;
	
	unique_ptr<Poller> poller_;
	unique_ptr<TimerQueue> timerQueue_;
	int wakeupFd_;
	unique_ptr<Tunnel> wakeupTunnel_;
	Timestamp pollReturnTime_;
	TunnelList activeTunnels_;
	Tunnel* currentActiveTunnel_;

	MutexLock mutex_;
	bool callingPendingFunctors_;
	std::vector<Functor> pendingFunctors_;



};

}
#endif /* defined(__KunServer__EventLoop__) */
