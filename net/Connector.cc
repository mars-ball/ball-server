#include "Connector.h"
#include "common/Logging.h"
#include "Tunnel.h"
#include "EventLoop.h"
#include "SocketsOps.h"

#include <functional>
#include <errno.h>

using namespace fsk;
using namespace fsk::net;

const int Connector::kMaxRetryDelayMs;

Connector::Connector(EventLoop* loop, const InetAddress& serverAddr)
	: loop_(loop),
	  serverAddr_(serverAddr),
	  connect_(false),
	  state_(kDisconnected),
	  retryDelayMs_(kInitRetryDelayMs)
{
	LOG_INFO << "ctor[" << this << "]";
}

Connector::~Connector(){
	LOG_INFO <<"dtor[" << this << "]";
	assert(!tunnel_);
}

void Connector::start(){
	connect_ = true;
	loop_->runInLoop(std::bind(&Connector::startInLoop, this));
}

void Connector::startInLoop(){
	loop_->assertInLoopThread();
	assert(state_ == kDisconnected);
	if(connect_){
		connect();
	}else{
		LOG_INFO << " do not connect";
	}
}


void Connector::stop(){

	connect_ = false;
	loop_->queueInLoop(std::bind(&Connector::stopInLoop, this));
}


void Connector::stopInLoop(){
	loop_->assertInLoopThread();
	if(state_ == kConnecting){
		setState(kDisconnected);
		int sockfd = removeAndResetTunnel();
		retry(sockfd);
	}
}

void Connector::connect(){
	int sockfd = sockets::createNonblockingOrDie();
	int ret = sockets::connect(sockfd, serverAddr_.getSockAddrInet());
	int savedErrno = (ret == 0) ? 0: errno;
	switch(savedErrno){
		case 0:
		case EINPROGRESS:
		case EINTR:
		case EISCONN:
			connecting(sockfd);
			break;

		case EAGAIN:
		case EADDRINUSE:
		case EADDRNOTAVAIL:
		case ECONNREFUSED:
		case ENETUNREACH:
			retry(sockfd);
		case EACCES:
		case EPERM:
		case EAFNOSUPPORT:
		case EALREADY:
		case EBADF:
		case EFAULT:
		case ENOTSOCK:
			LOG_SYSERR << "connect error in startInLoop " << savedErrno;
			sockets::close(sockfd);
			break;
		default:
			LOG_SYSERR << "Unexpected error in startInLoop " << savedErrno;
			sockets::close(sockfd);
			break;
	}


}

void Connector::restart(){

	loop_->assertInLoopThread();
	setState(kDisconnected);
	retryDelayMs_ = kInitRetryDelayMs;
	connect_ = true;
	startInLoop();
}

void Connector::connecting(int sockfd){
	setState(kConnecting);
	assert(!tunnel_);
	tunnel_.reset(new Tunnel(loop_, sockfd));
	tunnel_->setWriteCallback(std::bind(&Connector::handleWrite, this));
	tunnel_->setErrorCallback(std::bind(&Connector::handleError, this));
	tunnel_->enableWriting();
}

int Connector::removeAndResetTunnel(){
	tunnel_->disableAll();
	tunnel_->remove();
	int sockfd = tunnel_->fd();
	loop_->queueInLoop(bind(&Connector::resetTunnel, this));
	return sockfd;
}

void Connector::resetTunnel(){
	tunnel_.reset();
}


void Connector::handleWrite(){
	LOG_TRACE << "Connector:: handleWrite " << state_;
	if(state_ == kConnecting){
		int sockfd = removeAndResetTunnel();
		int err = sockets::getSocketError(sockfd);
		if(err){
			LOG_WARN << "handle write - so error=" << err << " " << strerror_tl(err);
			retry(sockfd);
		}else if(sockets::isSelfConnect(sockfd)){
			LOG_WARN << "Connector handle write - self connect";
			retry(sockfd);
		}else{
			setState(kConnected);
			if(connect_){
				newConnectionCallback_(sockfd);
			}else{
				sockets::close(sockfd);
			}
		}
	}else{
		assert(state_ == kDisconnected);
	}
}

void Connector::handleError(){
	LOG_ERROR << " Connector handleError state=" << state_;
	if(state_ == kConnecting){
		int sockfd = removeAndResetTunnel();
		int err = sockets::getSocketError(sockfd);
		LOG_TRACE << "so error=" << err << " " << strerror_tl(err);
		retry(sockfd);
	}
}

void Connector::retry(int sockfd){
	sockets::close(sockfd);
	setState(kDisconnected);
	if(connect_){
		LOG_INFO << "retry connecting to " << serverAddr_.toIpPort()
			<< " in " << retryDelayMs_ << " milliseconds. ";
		loop_->runAfter(retryDelayMs_/1000.0,
						std::bind(&Connector::startInLoop, shared_from_this()));
	}else{
		LOG_INFO << " do not connect";
	}
}























