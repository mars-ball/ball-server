#include "Acceptor.h"
#include "EventLoop.h"
#include "InetAddress.h"
#include "SocketsOps.h"
#include <fcntl.h>
#include <unistd.h>


#include <errno.h>


using namespace fsk;
using namespace fsk::net;

Acceptor::Acceptor(EventLoop* loop, const InetAddress& listenAddr, bool reuseport)
: loop_(loop),
  acceptSocket_(sockets::createNonblockingOrDie()),
  acceptTunnel_(loop, acceptSocket_.fd()),
  listenning_(false),
  idleFd_(::open("/dev/null", O_RDONLY| O_CLOEXEC))
{
	acceptSocket_.setReuseAddr(true);
	acceptSocket_.setReusePort(reuseport);
	acceptSocket_.bindAddress(listenAddr);
	acceptSocket_.setTcpNoDelay(true);
	acceptTunnel_.setReadCallback(
		bind(&Acceptor::handleRead, this));

}

Acceptor::~Acceptor(){
	acceptTunnel_.disableAll();
	acceptTunnel_.remove();
	::close(idleFd_);
}


void Acceptor::listen(){
	loop_->assertInLoopThread();
	listenning_ = true;
	acceptSocket_.listen();
	acceptTunnel_.enableReading();
}



void Acceptor::handleRead(){
	loop_->assertInLoopThread();
	InetAddress peerAddr;

	int connfd = acceptSocket_.accept(&peerAddr);

	if(connfd >= 0){
		if(newConnectionCallback_){
			newConnectionCallback_(connfd, peerAddr);
		}else{
			sockets::close(connfd);
		}
	}else{
		cout << "in acceptor:: handleread";
		if(errno == EMFILE){
			::close(idleFd_);
			idleFd_ = ::accept(acceptSocket_.fd(), NULL, NULL);
			::close(idleFd_);
			idleFd_ = ::open("/dev/null", O_RDONLY | O_CLOEXEC);
		}
	}
}








































