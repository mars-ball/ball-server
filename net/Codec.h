#ifndef FSK_CODEC_H
#define FSK_CODEC_H

#include "common/Buffer.h"
#include "TcpConnection.h"
#include "Dispatchable.h"
#include "common/Timestamp.h"
#include <functional>
#include "common/noncopyable.h"
#include <memory>
#include "google/protobuf/message.h"
#include "Dispatcher.h"
#include <map>

using namespace google;
using namespace std;

typedef shared_ptr<protobuf::Message> MessagePtr;
namespace fsk{
namespace net{

class ProtobufCodec : noncopyable{
public:
	enum ErrorCode{
		kNoError = 0,
		kInvalidLength,
		kCheckSumError,
		kInvalidNameLen,
		kUnkownMessageType,
		kParseError
	};

	typedef function<void (const fsk::net::TcpConnectionPtr&, const MessagePtr&, Timestamp)> ProtobufMessageCallback;
	typedef function<void (const TcpConnectionPtr&, net::Buffer*, Timestamp, ErrorCode)> ErrorCallback;

	explicit ProtobufCodec(Dispatchable* server)
		:server_(server), 
		errorCallback_(defaultErrorCallback){
	}

	void onMessage(const net::TcpConnectionPtr& conn, net::Buffer* buf, Timestamp receiveTime);
	void send(const net::TcpConnectionPtr& conn, MessagePtr message){
		if(conn == nullptr){
			LOG_ERROR << "conn null";
			return;
		}
		net::Buffer buf;
		fillHEmptyBuffer(&buf, message);
		conn->send(&buf);
	}

	static const string& errorCodeToString(ErrorCode errorCode);
	static void fillHEmptyBuffer(net::Buffer* buf, MessagePtr& message);
	static protobuf::Message* createMessage(const string& type_name);
	static MessagePtr parse(const char* buf, int len, int32_t* topKind, ErrorCode* errorCode);
	//static void fillResponse(pbmsg::HMessage& msg);
	//static void fillNotification(pbmsg::HMessage& msg);

private:
	Dispatchable* server_;
	static void defaultErrorCallback(const TcpConnectionPtr&,
									Buffer*, Timestamp, ErrorCode);
	ErrorCallback errorCallback_;
	const static int kHeaderLen = sizeof(int32_t);
	const static int kMinMessageLen = 2*kHeaderLen + 2;
	const static int kMaxMessageLen = 64*1024*1024;




};

}
}


#endif





