#ifndef FSK_SERVER_DISPATCHER_H
#define FSK_SERVER_DISPATCHER_H

#include "Callbacks.h"
#include "google/protobuf/message.h"
#include <map>
#include <functional>
#include <memory>
#include  "common/noncopyable.h"
#include "EventLoop.h"
#include "MessageDefine.h"

using namespace google;
using namespace google::protobuf;

namespace fsk{
namespace net{

typedef shared_ptr<protobuf::Message> MessagePtr;
class ProtobufServer;

class Callback : noncopyable{

public:
	virtual ~Callback(){};
	virtual void onMessage(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& message, Timestamp time) const = 0;
};

class CallbackT : public Callback{
public:
	typedef function<void (ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& message, Timestamp)> ProtobufMessageTCallback;

	CallbackT(const ProtobufMessageTCallback callback)
		:callback_ (callback)
	{
	}

	virtual void onMessage(ProtobufServer* server, const TcpConnectionPtr& conn, const MessagePtr& message, Timestamp time) const{
//		const shared_ptr<pbmsg::HMessage> concrete = std::static_pointer_cast<pbmsg::HMessage>(message);
		callback_(server, conn, message, time);
	}

private:
	ProtobufMessageTCallback callback_;

};


class ProtobufDispatcherBase{
public:
	typedef shared_ptr<ProtobufDispatcherBase> ptr_t;
	virtual void onProtobufMessage(const TcpConnectionPtr& conn, const MessagePtr& message, Timestamp time)const=0;
	virtual void registerMessageCallback(int32_t msg_type, const CallbackT::ProtobufMessageTCallback& callback)=0;

};

template<class H>
class ProtobufDispatcher:public ProtobufDispatcherBase{

public:
	typedef function<void (const TcpConnectionPtr&, const MessagePtr&, Timestamp)>ProtobufMessageCallback;

	explicit ProtobufDispatcher(ProtobufServer* server, const ProtobufMessageCallback& defaultCb)
		:defaultCallback_(defaultCb), server_(server){
	}

	void onProtobufMessage(const TcpConnectionPtr& conn, const MessagePtr& message, Timestamp time) const
	{
		shared_ptr<H> msg = static_pointer_cast<H>(message);
		int32_t msgid = static_cast<int32_t>(msg->msgtype());
		//const Descriptor* des = MessageDefine::getInstance().GetDesByMsgId(msgid);
		
		CallbackMap::const_iterator it = callbacks_.find(msgid);
		if(it != callbacks_.end()){
			it->second->onMessage(server_, conn, message, time);
		}else{
			defaultCallback_(conn, message, time);
		}
	}


	void registerMessageCallback(int32_t msg_type, const CallbackT::ProtobufMessageTCallback& callback){
		shared_ptr<CallbackT> pd(new CallbackT(callback));
		callbacks_[msg_type] = pd;
		
	}



	private:
		typedef map<int32_t, shared_ptr<Callback>> CallbackMap;
		CallbackMap callbacks_;
		ProtobufMessageCallback defaultCallback_;
		ProtobufServer* server_;
};

}
}



#endif
