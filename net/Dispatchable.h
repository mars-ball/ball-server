#pragma once
#include<map>
#include"net/Dispatcher.h"
#include "common/Logging.h"
#include <memory>

using namespace std;
namespace fsk{
namespace net{

class Dispatchable{
public:
    void registerDispatcher(int32_t topType, ProtobufDispatcherBase::ptr_t& dispatcher){
        if(dispatchers_.count(topType)){
               LOG_ERROR << "already have dispatcher: " << topType;
               return;
        }
        dispatchers_[topType] = dispatcher;
    }

    ProtobufDispatcherBase::ptr_t getDispatcherByType(int32_t topType){
        if(!dispatchers_.count(topType)){
               LOG_ERROR << "no such dispatcher:" << topType;
               return ProtobufDispatcherBase::ptr_t(nullptr);
        }
        return dispatchers_[topType];
    }
	

protected:
	std::map<int, ProtobufDispatcherBase::ptr_t> dispatchers_;
};








}
}
