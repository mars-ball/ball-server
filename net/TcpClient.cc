#include "TcpClient.h"
#include "common/Logging.h"
#include "EventLoop.h"
#include "Connector.h"
#include "SocketsOps.h"
#include <stdio.h>

using namespace fsk;
using namespace fsk::net;

namespace fsk{
namespace net{
namespace detail{

void removeConnection(EventLoop* loop, const TcpConnectionPtr& conn){
	loop->queueInLoop(bind(&TcpConnection::connectDestroyed, conn));
}

void removeConnector(const ConnectorPtr& connector){
	(void)connector;
}
}
}
}


TcpClient::TcpClient(EventLoop* loop,
					const InetAddress& serverAddr,
					const string& nameArg)
	:loop_(CHECK_NOTNULL(loop)),
	connector_(new Connector(loop, serverAddr)),
	name_(nameArg),
	connectionCallback_(defaultConnectionCallback),
	messageCallback_(defaultMessageCallback),
	retry_(false),
	connect_(true),
	nextConnId_(1)
{
	using namespace std::placeholders;
	connector_->setNewConnectionCallback(bind(&TcpClient::newConnection, this, _1));
	LOG_INFO << "TcpClient::TcpClient[" << name_ <<"] - connector " << connector_.get();
}

TcpClient::~TcpClient(){
	LOG_INFO << "TcpClient::~TcpClient["<<name_ << "] - connector " << connector_.get();
	TcpConnectionPtr conn;
	bool unique = false;
	{
		MutexLockGuard lock(mutex_);
		unique = connection_.unique();
		conn = connection_;
	}

	if(conn){
		using namespace std::placeholders;

		assert(loop_ == conn->getLoop());
		CloseCallback cb = bind(&detail::removeConnection, loop_, _1);
		loop_->runInLoop(bind(&TcpConnection::setCloseCallback, conn, cb));
		if(unique){
			conn->forceClose();
		}
	}else{
		connector_->stop();
		loop_->runAfter(1, bind(&detail::removeConnector, connector_));
	}

}

void TcpClient::connect(){
	LOG_INFO << " connect[" <<name_ << "] - connecting to "
		<< connector_->serverAddress().toIpPort();

	connect_ = true;
	connector_->start();
}

void TcpClient::disconnect(){
	connect_ = false;
	{
		MutexLockGuard lock(mutex_);
		if(connection_){
			connection_->shutdown();
		}
	}
}

void TcpClient::stop(){
	connect_ = false;
	connector_->stop();
}


void TcpClient::newConnection(int sockfd){
	using namespace std::placeholders;

	loop_->assertInLoopThread();
	InetAddress peerAddr(sockets::getPeerAddr(sockfd));
	char buf[32];
	snprintf(buf, sizeof(buf), ":%s#%d", peerAddr.toIpPort().c_str(), nextConnId_);
	++nextConnId_;

	string connName = name_ + buf;

	InetAddress localAddr(sockets::getLocalAddr(sockfd));

	TcpConnectionPtr  conn(new TcpConnection(loop_, connName, sockfd, localAddr, peerAddr));

	conn->setConnectionCallback(connectionCallback_);
	conn->setMessageCallback(messageCallback_);
	conn->setWriteCompleteCallback(writeCompleteCallback_);
	conn->setCloseCallback(bind(&TcpClient::removeConnection, this, _1));
	{
		MutexLockGuard lock(mutex_);
		connection_ = conn;
	}

	conn->connectEstablished();
}

void TcpClient::removeConnection(const TcpConnectionPtr& conn){

	loop_->assertInLoopThread();
	assert(loop_ == conn->getLoop());
	{
		MutexLockGuard lock(mutex_);
		assert(connection_ == conn);
		connection_.reset();
	}

	loop_->queueInLoop(bind(&TcpConnection::connectDestroyed, conn));
	if(retry_ && connect_){
		LOG_INFO << "TcpClient::connect[" << name_ << "] - Reconnecting to "
			<< connector_->serverAddress().toIpPort();
		connector_->restart();
	}

}









