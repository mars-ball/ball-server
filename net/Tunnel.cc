#include "Tunnel.h"
#include "EventLoop.h"
#include <poll.h>

using namespace fsk;
using namespace std;

const int Tunnel::kNoneEvent = 0;
const int Tunnel::kReadEvent = POLLIN | POLLPRI;
const int Tunnel::kWriteEvent = POLLOUT;

Tunnel::Tunnel(EventLoop* loop, int fd)
:loop_(loop),
fd_(fd),
events_(0),
revents_(0),
index_(-1),
addedToLoop_(false),
eventProcessing_(false),
tied_(false)

{

}

Tunnel::~Tunnel(){

}

void Tunnel::update(){
	addedToLoop_ = true;
	loop_->updateTunnel(this);

}

void Tunnel::remove(){

	addedToLoop_ = false;
	loop_->removeTunnel(this);
}

void Tunnel::processEvent(Timestamp receiveTime){
	shared_ptr<void> guard;

	if(tied_){
		guard = tie_.lock();
		if(guard){
			processEventSafely(receiveTime);
		}
	}else{
		processEventSafely(receiveTime);
	}

}

void Tunnel::processEventSafely(Timestamp receiveTime){
	eventProcessing_ = true;

	if((revents_ & POLLHUP) && !(revents_ & POLLIN)){
		if(closeCallback_) closeCallback_();
	}
	if((revents_ & POLLNVAL)){
		cout << "fd= " <<fd_ << " POLLHUP"<<endl;
	}

	if(revents_ & (POLLERR | POLLNVAL)){
		if(errorCallback_) errorCallback_();
	}
#ifdef __APPLE__
    if(revents_ & (POLLIN | POLLPRI )){
        if(readCallback_) readCallback_(receiveTime);
    }
#else
	if(revents_ & (POLLIN | POLLPRI | POLLRDHUP)){
		if(readCallback_) readCallback_(receiveTime);
	}
#endif
	if(revents_ & POLLOUT){
		if(writeCallback_) writeCallback_();
	}

	eventProcessing_ = false;

}

































