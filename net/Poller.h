#ifndef KUN_SERVER_POLLER
#define KUN_SERVER_POLLER

#include "common/noncopyable.h"
#include "EventLoop.h"
#include <vector>
#include <map>
#include "common/Timestamp.h"
struct pollfd;
using namespace std;

namespace fsk{

class Tunnel;

class Poller: noncopyable{
public:
	typedef vector<Tunnel*> TunnelList;

	Poller(EventLoop* loop);
	~Poller();

	Timestamp poll(int timeoutMs, TunnelList* activeTunnels);

	void updateTunnel(Tunnel* tunnel);

	void removeTunnel(Tunnel* tunnel);

	bool hasTunnel(Tunnel* tunnel)const;

	void assertInLoopThread() const
	{
		ownerLoop_->assertInLoopThread();
	}

protected:
	typedef std::map<int, Tunnel*> TunnelMap;
	TunnelMap tunnels_;
private:
	void fillActiveTunnels(int numEvents, TunnelList* activeTunnels) const;
	typedef vector<struct pollfd> PollfdList;

	PollfdList pollfds_;
	EventLoop* ownerLoop_;



};

}

#endif










