#ifndef FSK_SERVER_TUNNEL
#define FSK_SERVER_TUNNEL
#include<functional>
#include<memory>
#include"common/noncopyable.h"
#include"common/Timestamp.h"

namespace fsk{
using namespace std;

class EventLoop;

class Tunnel: noncopyable{
public:

	typedef function<void()> EventCallback;
	typedef function<void(Timestamp)> ReadEventCallback;
	Tunnel(EventLoop* loop, int fd);
	~Tunnel();

	int fd() const { return fd_;}
	int events() const { return events_;};
	void set_revents(int revt) { revents_ = revt;}
	bool isNoneEvent() const { return events_ == kNoneEvent;}

	void processEvent(Timestamp receiveTime);
	
	void setReadCallback(const ReadEventCallback& cb){
		readCallback_ = cb;
	}
	void setWriteCallback(const EventCallback&& cb){
		writeCallback_ = move(cb);
	}
	void setCloseCallback(const EventCallback&& cb){
		closeCallback_ = move(cb);
	}
	void setErrorCallback(const EventCallback&& cb){
		errorCallback_ = move(cb);
	}

	void enableReading(){ events_ |= kReadEvent; update();}
	void disableReading(){ events_ &= ~kReadEvent; update();}
	void enableWriting(){ events_ |= kWriteEvent; update();}
	void disableWriting(){ events_ &= ~kWriteEvent; update();}
	void disableAll(){ events_ = kNoneEvent; update();}
	bool isWriting() const{ return events_ & kWriteEvent;}


	int index() { return index_;}
	void set_index(int idx) { index_ = idx;}
	EventLoop* ownerLoop(){ return loop_;}
	void remove();

private:
	void update();

	void processEventSafely(Timestamp);

	static const int kNoneEvent;
	static const int kReadEvent;
	static const int kWriteEvent;

	EventLoop* loop_;
	const int fd_;
	int events_;
	int revents_;
	int index_;
	
	bool addedToLoop_;
	bool eventProcessing_;
	weak_ptr<void> tie_;
	bool tied_;
	

	ReadEventCallback readCallback_;
	EventCallback writeCallback_;
	EventCallback closeCallback_;
	EventCallback errorCallback_;

};


}




















#endif
