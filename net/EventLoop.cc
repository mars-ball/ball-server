//
//  EventLoop.cpp
//  KunServer
//
//  Created by meryn on 15/12/6.
//  Copyright (c) 2015年 meryn. All rights reserved.
//

#include "EventLoop.h"
//#include <iostream>
#include <signal.h>
#include <assert.h>
#include "Poller.h"
#include "Tunnel.h"
#include "common/TimerQueue.h"
#include "common/Mutex.h"
#include <signal.h>
#ifndef __APPLE__
#include <sys/eventfd.h>
#endif
#include "SocketsOps.h"
#include "common/Logging.h"
#include <stdio.h>
#include <fcntl.h>


using namespace std;
using namespace fsk;

namespace fsk{
#ifdef __APPLE__
    EventLoop* t_currThreadLoop = NULL;
#else
    thread_local EventLoop* t_currThreadLoop = NULL;
#endif

    const int POLL_TIME_MS = 10000;

	int createEventfd(){
#ifdef __APPLE__
        int evfd = open("wakeup.tmp", O_RDWR);
        int flags = fcntl(evfd, F_GETFL, 0);
        fcntl(evfd, F_SETFL, flags | O_NONBLOCK);
        
#else
		int evfd = ::eventfd(0, EFD_NONBLOCK | EFD_CLOEXEC);
#endif
		if(evfd < 0){
			LOG_SYSERR<< "Failed in eventfd";
			abort();
		}
		return evfd;
	}

	class IgnoreSigPipe{
	public:
		IgnoreSigPipe(){
			::signal(SIGPIPE, SIG_IGN);
		}
	};
	IgnoreSigPipe initObj;

}


    EventLoop::EventLoop():
		isLooping(false),
		threadId_(CurrThread::tid()),
		quit_(false),
		poller_(new Poller(this)),
		timerQueue_(new TimerQueue(this)),
		wakeupFd_(createEventfd()),
		wakeupTunnel_(new Tunnel(this, wakeupFd_)),
		currentActiveTunnel_(NULL),
		callingPendingFunctors_(false)
		{
        if(t_currThreadLoop){
            cout << "Another EventLoop";
        }else{
            t_currThreadLoop = this;
        }
		wakeupTunnel_->setReadCallback(bind(&EventLoop::handleRead, this));
		wakeupTunnel_->enableReading();
    }

	void EventLoop::cancel(TimerId timerId){
		return timerQueue_->cancel(timerId);
	}


    EventLoop* EventLoop::getLoopFromCurrThread(){
        return t_currThreadLoop;
    }

    EventLoop::~EventLoop(){
        t_currThreadLoop = NULL;
    }

    void EventLoop::loop(){
        assert(!isLooping);
        assertInLoopThread();
        isLooping = true;
		quit_ = false;

		while(!quit_){
			activeTunnels_.clear();
			pollReturnTime_ = poller_->poll(POLL_TIME_MS, &activeTunnels_);
			for(TunnelList::iterator it = activeTunnels_.begin();
				it != activeTunnels_.end(); ++it){
				(*it)->processEvent(pollReturnTime_);
			}
			doPendingFunctors();
		}

		isLooping = false;
    }

	void EventLoop::quit(){
		quit_ = true;
		if(!isInLoopThread()){
			wakeup();
		}
	}

	void EventLoop::updateTunnel(Tunnel* tunnel){
		assert(tunnel->ownerLoop() == this);
		assertInLoopThread();
		poller_->updateTunnel(tunnel);
	}
	void EventLoop::removeTunnel(Tunnel* tunnel){
		assert(tunnel->ownerLoop() == this);
		assertInLoopThread();
		poller_->removeTunnel(tunnel);


	}

	void EventLoop::runInLoop(const Functor& cb){
		if(isInLoopThread()){
			cb();
		}else{
			queueInLoop(cb);
		}
	}

	void EventLoop::queueInLoop(const Functor& cb){
		{
			MutexLockGuard lock(mutex_);
			pendingFunctors_.push_back(cb);
		}
		if(!isInLoopThread() || callingPendingFunctors_){
			wakeup();
		}


	}

	TimerId EventLoop::runAt(const Timestamp& time, const TimerCallback& cb){
		return timerQueue_->addTimer(cb, time, 0.0);
	}

	TimerId EventLoop::runAfter(double delay, const TimerCallback& cb){
		Timestamp time(addTime(Timestamp::now(), delay));
		return runAt(time, cb);
	}

	TimerId EventLoop::runEvery(double interval, const TimerCallback& cb){
		Timestamp time(addTime(Timestamp::now(), interval));
		return timerQueue_->addTimer(cb, time, interval);
	}


	void EventLoop::wakeup(){
		uint64_t one = 1;
		ssize_t n = sockets::write(wakeupFd_, &one, sizeof one);
		if(n != sizeof one){
			LOG_ERROR << "wakeup writes " << n << " bytes instead of 8";
		}
	}

	void EventLoop::handleRead(){
		uint64_t one = 1;
		ssize_t n = sockets::read(wakeupFd_, &one, sizeof one);
		if(n != sizeof one){
			LOG_ERROR << "handleRead reads "<< n << " bytes of 8";
		}
	}

	void EventLoop::doPendingFunctors(){
		std::vector<Functor> functors;
		callingPendingFunctors_ = true;
		{
			MutexLockGuard lock(mutex_);
			functors.swap(pendingFunctors_);
		}

		for(size_t i = 0; i < functors.size(); ++i){
			functors[i]();
		}
		callingPendingFunctors_ = false;
	}


























