#ifndef FSK_SERVER_ENDIAN_H
#define FSK_SERVER_ENDIAN_H


#include <stdint.h>
#ifdef __APPLE__
#include "/usr/include/machine/endian.h"
#else
#include <endian.h>
#endif

#ifdef __APPLE__

#include <libkern/OSByteOrder.h>

#define htobe16(x) OSSwapHostToBigInt16(x)
#define htole16(x) OSSwapHostToLittleInt16(x)
#define be16toh(x) OSSwapBigToHostInt16(x)
#define le16toh(x) OSSwapLittleToHostInt16(x)

#define htobe32(x) OSSwapHostToBigInt32(x)
#define htole32(x) OSSwapHostToLittleInt32(x)
#define be32toh(x) OSSwapBigToHostInt32(x)
#define le32toh(x) OSSwapLittleToHostInt32(x)

#define htobe64(x) OSSwapHostToBigInt64(x)
#define htole64(x) OSSwapHostToLittleInt64(x)
#define be64toh(x) OSSwapBigToHostInt64(x)
#define le64toh(x) OSSwapLittleToHostInt64(x)



#endif



namespace fsk{
namespace sockets{
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wold-style-cast"

inline uint64_t hostToNetwork64(uint64_t host64){
	return htobe64(host64);

}

inline uint32_t hostToNetwork32(uint32_t host32){
	return htobe32(host32);
}

inline uint16_t hostToNetwork16(uint16_t host16){
	return htobe16(host16);
}

inline uint64_t networkToHost64(uint64_t net64){
	return be64toh(net64);
}

inline uint32_t networkToHost32(uint32_t net32){
	return be32toh(net32);
}


inline uint16_t networkToHost16(uint16_t net16){
	return be16toh(net16);
}



#pragma GCC diagnostic warning "-Wconversion"
#pragma GCC diagnostic warning "-Wold-style-cast"




}

}











#endif
