%module luaInterface
%{
#include "LuaInterface.h"
%}
%include <std_string.i>
%include <typemaps.i>
%include "std_vector.i"

namespace std{
	%template(IntSet) vector<int>;
}

using namespace std;

typedef int          int32_t;
typedef long long    int64_t;
typedef unsigned int uint32_t;
%include "LuaInterface.h"

