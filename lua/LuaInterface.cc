#include "LuaInterface.h"
#include "room/Room.h"
#include "room/MapRect.h"
#include <set>
#include <cmath>

using namespace std;


LuaInterface lua_interface;

LuaInterface* getLuaInterface()
{
	return &lua_interface;
}


bool LuaInterface::isPlayerExist(int32_t id){
	return roomMgr_->isPlayerExist(id);
}


void LuaInterface::setSpeed(int32_t id, LuaVector2 dir){

	RoomPtr room;
	int32_t ret = roomMgr_->getPlayerRoom(id, room);
	if(ret != 0 || room.get() == NULL){
		LOG_ERROR << "get player room failed, uid:"<<id;
		return;
	}

	MoveRuntimeInfoPtr move = room->getPlayerMoveInfoById(id);
	if(move.get() == NULL){
		LOG_ERROR << "lua get battle object null";
		return;
	}

	Vec speed;
	if(Move::getSpeedFromDir(Vec(static_cast<float>(dir.x), static_cast<float>(dir.y)), speed)!=0){
		LOG_ERROR <<"speed failed, id:"<<id;
	}

	move->setMainV(speed);

}

LuaVector2 LuaInterface::getMainSpeed(int32_t id){
	LuaVector2 res;
	RoomPtr room;
	int32_t ret = roomMgr_->getPlayerRoom(id, room);
	if(ret != 0 || room.get() == NULL){
		LOG_ERROR << "get player room failed, uid:"<<id;
		return res;
	}

	MoveRuntimeInfoPtr move = room->getPlayerMoveInfoById(id);
	if(move.get() == NULL){
		LOG_ERROR << "lua get battle object null";
		return res;
	}

	MoveUnitPtr mainUnit = move->getMainUnit();
	if(mainUnit.get() == NULL){
		LOG_ERROR <<"main unit is null";
		return res;
	}

	Vec speed = mainUnit->oldV;
	res.x = speed.x;
	res.y = speed.y;

	return res;


}


vector<int> LuaInterface::getAroundEnemy(int32_t id){
	vector<int> res;

	RoomPtr room;
	int32_t ret = roomMgr_->getPlayerRoom(id, room);
	if(ret != 0 || room.get() == NULL){
		LOG_ERROR << "get player room failed, uid:"<<id;
		return res;
	}

	MapRectPtr mapRect = room->mapRect_;
	if(mapRect.get() == NULL){
		LOG_ERROR <<"map rect is null, id:"<<id<<" roomid"<<room->roomId_;
		return res;
	}

	set<int32_t> ids;
	if(mapRect->getNeighbor(id, ids) != 0){
		LOG_ERROR <<"get neighbor is failed";
		return res;
	}

	return vector<int>(ids.begin(), ids.end());
}

int LuaInterface::getThreatValue(int32_t id0, int32_t id1){

	RoomPtr room;
	int32_t ret = roomMgr_->getPlayerRoom(id0, room);
	if(ret != 0 || room.get() == NULL){
		LOG_ERROR << "get player room failed, uid:"<<id0;
		return -1;
	}

	MapRectPtr mapRect = room->mapRect_;
	if(mapRect.get() == NULL){
		LOG_ERROR <<"map rect is null, id:"<<id0<<" roomid"<<room->roomId_;
		return -1;
	}

	return mapRect->getThreatValue(id0, id1);
}


void LuaInterface::fleeOrChase(int32_t id0, int32_t id1, bool flee){

	RoomPtr room;
	int32_t ret = roomMgr_->getPlayerRoom(id0, room);
	if(ret != 0 || room.get() == NULL){
		//LOG_ERROR << "get player room failed, uid:"<<id0;
		return;
	}

	MapRectPtr mapRect = room->mapRect_;
	if(mapRect.get() == NULL){
		//LOG_ERROR <<"map rect is null, id:"<<id0<<" roomid"<<room->roomId_;
		return;
	}

	Vec offset;
	float speed;
	if(!mapRect->getEatDirV(id0, id1, offset, speed)){
		//LOG_ERROR <<"get eat dir failed";
		return;
	}

	Vec offNorm = offset.norm();
	float angle = 0;

	if(!flee){
		angle = sin(30 * 3.1415926 / 180);
	}else{
		angle = sin(135 * 3.1415926 /180);
	}


	float sinAng = sin(angle);
	float cosAng = cos(angle);

	float xx = offNorm.x * cosAng - offNorm.y * sinAng;
	float yy = offNorm.x * sinAng + offNorm.y * cosAng;

	LuaVector2 dirVec;
	dirVec.x = xx;
	dirVec.y = yy;

	if(flee){
		setSpeed(id1, dirVec);
	}else{
		setSpeed(id0, dirVec);
	}



}

int64_t LuaInterface::getNowTime(){

	return Timestamp::now().getMsEpoch();


}


bool LuaInterface::splitIfCanEat(int32_t uid0, int32_t uid1){

	RoomPtr room;
	int32_t ret = roomMgr_->getPlayerRoom(uid0, room);
	if(ret != 0 || room.get() == NULL){
		LOG_ERROR << "get player room failed, uid:"<<uid0;
		return false;
	}


	MapRectPtr mapRect = room->mapRect_;
	if(mapRect.get() == NULL){
		LOG_ERROR <<"map rect is null, id:"<<uid0<<" roomid"<<room->roomId_;
		return false;
	}

	MoveRuntimeInfoPtr move0 = room->getPlayerMoveInfoById(uid0);
	if(move0.get() == NULL){
		LOG_ERROR << "get move runtime null, uid:"<<uid0;
		return false;
	}
	MoveRuntimeInfoPtr move1 = room->getPlayerMoveInfoById(uid1);
	if(move1.get() == NULL){
		LOG_ERROR << "get move runtime null, uid:"<<uid1;
		return false;
	}

	if(move0->units_.size() > 1){
		return false;
	}

	float r0 = move0->getBiggestRadius();
	float r1 = move1->getBiggestRadius();

	if(r0/1.414 < r1){
		return false;
	}


	Vec offset;
	float speed;
	if(!mapRect->getEatDirV(uid0, uid1, offset, speed)){
		LOG_ERROR <<"get eat dir failed";
		return false;
	}

	LuaVector2 dirVec;
	dirVec.x = offset.x;
	dirVec.y = offset.y;

    float flyTime = Move::kSplitSpeed/fabs(Move::kSplitDamping);

    float flyDist = Move::kSplitSpeed * flyTime + 0.5f * Move::kSplitDamping * flyTime * flyTime;
    
	if(offset.magnitude()*0.5f < flyDist){
		room->splitPlayer(uid0, offset.norm(), Timestamp::now().getMsEpoch());
		//setSpeed(uid0, dirVec);
		return true;
	}

	return false;


}




