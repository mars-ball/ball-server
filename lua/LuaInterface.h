#pragma once
//#include "LuaManager.h"

//LuaManager* getLuaManager();

#include <memory>
#include <vector>

using namespace std;

class RoomManager;

typedef shared_ptr<RoomManager> RoomManagerPtr;


struct LuaVector2{
	LuaVector2()
	:x(0), y(0){}
	double x;
	double y;
};


class LuaInterface{

public:

	void setSpeed(int32_t id, LuaVector2 speed);

	LuaVector2 getMainSpeed(int32_t id);

	vector<int>  getAroundEnemy(int32_t id);

	int getThreatValue(int32_t id0, int32_t id1);

	void fleeOrChase(int32_t id0, int32_t id1, bool flee);

	bool splitIfCanEat(int32_t id0, int32_t id1);

	bool isPlayerExist(int32_t id);

	int64_t getNowTime();

	RoomManagerPtr roomMgr_;

};

LuaInterface* getLuaInterface();