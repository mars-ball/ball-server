%module luaParam
%{
#include "LuaAIParam.h"
%}
%include <std_string.i>
%include <typemaps.i>
 
typedef int          int32_t;
typedef unsigned int uint32_t;
%include "LuaAIParam.h"

