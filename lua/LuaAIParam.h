#pragma once

#include<cstring>
#include<string>


struct LuaAITimerParam{
  std::string callbackMsg;

};

struct LuaBasicParam{
	std::string status;
	int target;
};

struct LuaAIParam{
   LuaAITimerParam timer; 
   LuaBasicParam basic;
};
