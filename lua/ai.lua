
local M = {}

luaMgr = luaManager.getLuaManager()
luaInf = luaInterface.getLuaInterface()


local id
local param

local thinkTime = 500;
local moveDirTimeMin = 2000 
local moveDirTimeMax = 5000
local chaseTimeMin = 1000
local chaseTimeMax = 4000
local avoidTimeMin = 1000
local avoidTimeMax = 3000
local movePer = 0;
local avoidPer = 0.3;
local attackPer = 0.7;

local function main(_id, msg)
  --luaMgr:print("in ai main, msg:" .. msg)
  id = _id
  param = luaMgr:getAIParamById(id)

  luaMgr:print("param:" .. param.timer.callbackMsg)  

  if msg == "start" then
    --luaMgr:print("in ai start")
    M.start()
  elseif msg == "update" then
    --luaMgr:print("in ai update")
    M.update()
  elseif msg == "timer" then
    --luaMgr:print("in ai timer")
  	M.timer()
  end 
end

local function update()
  luaMgr:print(id .. "updated, status:" + param.status )

  if param.status == "avoid eaten"
    M.updateAvoidEaten()
  elseif param.status == "chase"
    M.updateChase()
  end

end

local function start()
  luaMgr:print("in ai start")
  luaMgr:registerAfterTimer(id, 0, "ai think")
end

local function timer()
  luaMgr:print("in ai timer param:" .. param.timer.callbackMsg)
  if param.timer.callbackMsg == "ai think" then
    luaMgr:print("ai think")
    M.think()
  end
end

local function think()
  local per = math.random()
  luaMgr:print("think per:" .. per)
  if per > attackPer then
    M.chase()
  elseif per > avoidPer then
    M.avoidEaten()
  else 
    M.moveDirChange()
  end
end

local function moveDirChange()
    luaMgr:print("in move dir change")
    local per = math.random()*2 -1
    local speed = luaInterface.LuaVector2()
    speed.x = math.cos(per*math.pi)
    speed.y = math.sin(per*math.pi)

    luaMgr:print("in move dir id:" .. id .. "speed:" .. speed.x .. ", " .. speed.y .. " rand time:" .. (math.random()*(moveDirTimeMax - moveDirTimeMin) + moveDirTimeMin))
    luaInf:setSpeed(id, speed)
    luaMgr:registerAfterTimer(id, math.random()*(moveDirTimeMax - moveDirTimeMin) + moveDirTimeMin, "ai think")
end


local function avoidEaten()
    luaMgr:print("in avoid eaten")
    IntSet idSet = luaInf:getAroundEnemy(id)

    minValue = 1000
    minId = -1
    foudn = false

    for idIndex = 1, idSet:size() do 
      threat = luaInf:getThreatValue(idSet[idIndex], id)
      if threat > 0 then
        if minValue > threat then
          minValue = threat
          minId = idSet[idIndex]
          found = true
        end
      end
    end

    if not found then
      return
    end

    param.status = "avoid eaten"
    param.target = minId
    luaInf:fleeOrChase(minId, id, ture)

end

local function updateAvoidEaten()

  luaMgr:print("in update avoid eaten, target:" .. param.target)

  luaInf:fleeOrChase(param.target, id, ture)

end


local function chase()

  luaMgr:print("in chase")
  IntSet idSet = luaInf:getAroundEnemy(id)

  minValue = -1
  minId = -1
  found = false


  for idIndex = 1, idSet:size() do 
    threat = luaInf:getThreatValue(id, idSet[idIndex])
    if threat > 0 then
      if minValue > threat then
        minValue = threat
        minId = idSet[idIndex]
        found = true
      end
    end
  end

  if not found then
    return
  end

  param.status = "chase"
  param.target = minId
  if luaInf:splitIfCanEat(id, minId) then
    return
  else
    luaInf:fleeOrChase(id, mindId)
  end
end

local function updateChase()

  luaMgr:print("in update chase, target:" + param.target)

  if luaInf:splitIfCanEat(id, param.target) then
    return
  else
    luaInf:fleeOrChase(id, param.target)
  end

end


M.main = main
M.start = start
M.update = update
M.think = think
M.timer = timer
M.moveDirChange = moveDirChange
M.avoidEaten = avoidEaten
M.updateAvoidEaten = updateAvoidEaten
M.chase = chase
M.updateChase = updateChase


return M
