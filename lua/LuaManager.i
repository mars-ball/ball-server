%module luaManager
%{
#include "LuaManager.h"
%}
%include <std_string.i>
%include <typemaps.i>
 
typedef int          int32_t;
typedef unsigned int uint32_t;
%include "LuaManager.h"

