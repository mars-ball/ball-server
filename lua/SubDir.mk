CPPFILES = $(wildcard *.cc)
OBJ = $(CPPFILES:.cc=.o)
SWIGI = $(wildcard *.i)
SWIGCPP = $(SWIGI:.i=_wrap.cxx)
SWIGOBJ = $(SWIGI:.i=_wrap.o)
#CPPFLAGS=-std=c++11 
#DEBUG?= -g -ggdb
#WARNINGS=-Wall -W 
#CC=g++

#all: $(SWIGOBJ) $(OBJ)
#	$(CC) -o all $(OBJ) $(SWIGOBJ) $(CPPFLAGS) $(DEBUG) $(WARNINGS) $(LDFLAGS) -llua -ldl

all: $(SWIGOBJ) $(OBJ)
	@echo $(SWIGOBJ) $(OBJ)

$(OBJ): %.o: %.cc
	$(CC) -o $@ -I ../ -I ../database -I ../bullet -c $(CPPFLAGS) $(DEBUG) $(WARNINGS) $< $(LDFLAGS)

$(SWIGOBJ): %.o: %.cxx
	$(CC) $(CPPFLAGS) -c $< -o $@ -I ../ -I /usr/local/include/ $(DEBUG)

$(SWIGCPP): %_wrap.cxx: %.i
	swig -c++ -lua -o $@ $<


common:
	@echo $(OBJ)


.PHONY: clean


clean:
	-rm *.o
	-rm *cxx
