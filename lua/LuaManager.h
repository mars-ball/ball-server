#pragma once

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
extern int luaopen_luaManager(lua_State* L);
extern int luaopen_luaInterface(lua_State* L);
extern int luaopen_luaParam(lua_State* L);
}
#include <iostream>
#include <string>
#include <cstring>
#include <memory>
#include "LuaAIParam.h"

using namespace std;

class RoomManager;

typedef shared_ptr<RoomManager> RoomManagerPtr;

// Forward declaration of class callback
class LuaFunction;

// Helper class to load a Lua script
class LuaScript
{
friend class LuaFunction;
public:
    bool init();
    bool load(const std::string& file_path);

    void stackDump (lua_State *L);

    void close();

    ~LuaScript();
protected:
    void report_errors(lua_State *L);

public:
    lua_State *L;
};

// Helper class to call a function declared in a Lua script
class LuaFunction
{
public:
    LuaFunction();

    void init(LuaScript* s, const std::string& func, int n_params=0, int n_ret=0);
    virtual ~LuaFunction();
    void call();
    void call_begin();

    void call_end();

    void set_parameter(const std::string& val);
    void set_parameter(const double& val);

    void report_errors();


protected:
    int r_obj;
    int r_func;
    int num_params;
    int num_return;
public:
    lua_State *L;

 
};


// Class that we are going to access from the Lua script
class LuaManager
{
public:
    void init();

    void print(const std::string& text);

    void call_lua_function(std::string function_name, int nPara, int nRet, const char* sig, ...);
    
    void call_lua_main(int id, string luaFile, string msg);

    void registerAfterTimer(int32_t id, double time, string callbackMsg);

    LuaAIParam* getAIParamById(int32_t id);

    void luaTest(int32_t id, LuaAIParam* test);

    ~LuaManager();

    LuaScript*    lua_script;
    LuaFunction   lua_function;
    RoomManagerPtr roomMgr_;
};
// Function that give us a pointer to the myclass instance. It's defined in main.cpp
LuaManager* getLuaManager();
LuaAIParam* getTestParamById(int32_t id);