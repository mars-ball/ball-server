#include "LuaManager.h"
 
LuaManager my_class_instance; // global instance of myclass
 
LuaManager* get_myclass()  // Definition of get_myclass declared in myclass.h
{
    return &my_class_instance;
}

int main()
{
    my_class_instance.init();
   
	my_class_instance.call_lua_function("show_text", 1, 0, "f", 12.8);
    //my_class_instance.lua_function.call_begin();
    //my_class_instance.lua_function.set_parameter("Calling Lua Function");
    //my_class_instance.lua_function.call_end();
 
    return 0;
}
