#include "LuaManager.h"
#include "LuaInterface.h"
#include "room/Room.h"
#include "room/StopWatcher.h"
#include "room/AI.h"


LuaManager lua_instance; // global instance of myclass

LuaManager* getLuaManager()  // Definition of get_myclass declared in myclass.h
{
    return &lua_instance;
}

bool LuaScript::init(){
    L = luaL_newstate();
    luaL_openlibs(L);
    luaopen_luaManager(L);   // load the wrappered module
    luaopen_luaInterface(L);  
    luaopen_luaParam(L);  
    return true;

}

bool LuaScript::load(const std::string& file_path){
    // Load script
    int s = luaL_loadfile(L, file_path.c_str());

    // execute Lua program
    if ( s==0 )
    {
        s=lua_pcall(L, 0, 0, 0);
        
        //stackDump(L);
        if (s!=0)
        {
            report_errors(L);
            return false;
        }
    }
    else
    {
        report_errors(L);
        return false;
    }

    return true;
}


void LuaScript::stackDump (lua_State *L) {
	int i;
	int top = lua_gettop(L);
	for (i = 1; i <= top; i++) {  /* repeat for each level */
	int t = lua_type(L, i);
	switch (t) {

	  case LUA_TSTRING:  /* strings */
	    printf("`%s'", lua_tostring(L, i));
	    break;

	  case LUA_TBOOLEAN:  /* booleans */
	    printf(lua_toboolean(L, i) ? "true" : "false");
	    break;

	  case LUA_TNUMBER:  /* numbers */
	    printf("%g", lua_tonumber(L, i));
	    break;

	  default:  /* other values */
	    printf("%s", lua_typename(L, t));
	    break;

	}
	printf("  ");  /* put a separator */
	}
	printf("\n");  /* end the listing */
}


void LuaScript::close(){

    std::cout << "Calling lua_close... ";
    lua_close(L);
    std::cout << "done." <<std::endl;

}

LuaScript::~LuaScript(){
    lua_close(L);
}


void LuaScript::report_errors(lua_State *L){
    std::cout << "-- " << lua_tostring(L, -1) << std::endl;
    lua_pop(L, 1); // remove error message
}


LuaFunction::LuaFunction():r_obj(0), r_func(0), num_params(0), num_return(0), L(NULL) {}

void LuaFunction::init(LuaScript* s, const std::string& func, int n_params, int n_ret){
    L = s->L;
    num_params = n_params;
    num_return = n_ret;
    lua_getglobal(L, func.c_str());    // Get the function
    r_func = luaL_ref(L, LUA_REGISTRYINDEX);
    if ( (LUA_REFNIL == r_func) || (LUA_NOREF == r_func) )
        r_func = 0;

    //lua_pop(L, 1);
} 

LuaFunction::~LuaFunction(){
    if (r_obj) luaL_unref(L, LUA_REGISTRYINDEX, r_obj);
    if (r_func) luaL_unref(L, LUA_REGISTRYINDEX, r_func);
}


void LuaFunction::call(){
    call_begin();
    call_end();
}

void LuaFunction::call_begin()
{
    if (!r_func) return;
    lua_rawgeti(L, LUA_REGISTRYINDEX, r_func);
}


void LuaFunction::call_end(){
    if (r_obj) luaL_unref(L, LUA_REGISTRYINDEX, r_obj);
    if (r_func) luaL_unref(L, LUA_REGISTRYINDEX, r_func);
    /*
	if( lua_pcall(L,num_params,num_return,0) != 0)
    {
        report_errors();
    }*/
}

void LuaFunction::set_parameter(const std::string& val){
	lua_pushstring(L, val.c_str());
}


void LuaFunction::set_parameter(const double& val) {
	lua_pushnumber(L, val);
}

void LuaFunction::report_errors()
{
    std::cout << "-- " << lua_tostring(L, -1) << std::endl;
    lua_pop(L, 1); // remove error message
}


void LuaManager::init(){
    //lua_script = new LuaScript();
    //lua_script->init();
    //lua_script->load("main.lua");
}

void LuaManager::print(const std::string& text)
{
    //std::cout << "luaMgr: " << text <<std::endl;
}

void LuaManager::call_lua_function(std::string function_name, int nPara, int nRet, const char* sig, ...)
{
    
    lua_script = new LuaScript();
    lua_script->init();
    lua_script->load("main.lua");


    lua_function.init(lua_script, function_name, nPara, nRet);
    lua_State *L = lua_function.L;
    lua_function.call_begin();
    

    va_list vl;
    va_start(vl, sig);

    int narg = 0, nres = 0;

    while(*sig){
        switch(*sig++){
            case 'f':
                lua_pushnumber(L, va_arg(vl, double));
                break;
            case 'i':
                lua_pushnumber(L, va_arg(vl, int));
                break;
            case 's':
                lua_pushstring(L, va_arg(vl, char*));
                break;
            case '>':
                goto endwhilesig;
            default:
                goto endwhilesig;
        }
        narg +=1;
        luaL_checkstack(L, 1, "to many args");
    }endwhilesig:        

    nres = std::strlen(sig);

    if(narg != nPara || nres != nRet){
        std::cout << "para ret num failed"<<"narg:"<<narg<<" nPara:"<<nPara<<" nres:"<<nres<<" nRet:"<<nRet<<std::endl;
        goto endwhileret;
    }
    if(lua_pcall(L, narg, nres, 0) != 0){
        lua_function.report_errors();
        goto endwhileret;
    }

    nres = -nres;
    while(*sig){
        switch(*sig){
            case 'f':
                if(!lua_isnumber(L, nres)){
                    std::cout <<"wrong res type, not number";
                    goto endwhileret;
                }
                *va_arg(vl, double*) = lua_tonumber(L, nres);
                break;
            case 'i':
                if(!lua_isnumber(L, nres)){
                    std::cout <<"wrong res type, not number";
                    goto endwhileret;
                }
                *va_arg(vl, int*) = (int)lua_tonumber(L, nres);
                break;
            case 's':
                if(!lua_isstring(L, nres)){
                    std::cout <<" wrong res type, not string";
                    goto endwhileret;
                }
                *va_arg(vl, const char**) = lua_tostring(L, nres);
                break;
            default:
                std::cout <<"invalid option:"<<*(sig-1);
                goto endwhileret;
        }
    }endwhileret:
    


    va_end(vl); 
    lua_function.call_end();

    delete lua_script;
    //lua_settop(L, 0);
}


void LuaManager::call_lua_main(int id, string luaFile, string msg){
	call_lua_function("main", 3, 0, "iss>", id, luaFile.c_str(), msg.c_str());



}

void LuaManager::registerAfterTimer(int32_t id, double time, string callbackMsg){

    RoomPtr room;
    int32_t ret = roomMgr_->getPlayerRoom(id, room);
    if(ret != 0 || room.get() == NULL){
        LOG_ERROR << "get player room failed, uid:"<<id;
        return;
    }
	StopWatcherPtr sw = room->stopWatcher_;

	if(sw.get() == NULL){
		LOG_ERROR << "sw null";
		return;
	}

	TimerEvent ev;
	ev.timeStamp_ = Timestamp::now().getMsEpoch() + time;
	ev.id = id;
	ev.type_ = TIMER_EVENT_TYPE_AI;
	ev.aiPlayer_.callbackMsg = callbackMsg;

	sw->AddEvent(ev);


}


LuaAIParam* LuaManager::getAIParamById(int32_t id){
    
    RoomPtr room;
    int32_t ret = roomMgr_->getPlayerRoom(id, room);
    if(ret != 0 || room.get() == NULL){
        LOG_ERROR << "get player room failed, uid:"<<id;
        return NULL;//LuaAIParam();
    }

    AIInfoPtr aiInfo = room->getAIInfoById(id);

    if(aiInfo.get() == NULL){
        LOG_ERROR << "get ai info failed, id:"<<id;
        return NULL;//LuaAIParam();
    }

    return aiInfo->luaParam_.get();
}

LuaAIParam* getTestParamById(int32_t id){
    return getLuaManager()->getAIParamById(id);
    
}

void LuaManager::luaTest(int32_t id, LuaAIParam* test){

    RoomPtr room;
    int32_t ret = roomMgr_->getPlayerRoom(id, room);
    if(ret != 0 || room.get() == NULL){
        LOG_ERROR << "get player room failed, uid:"<<id;
        return ;//LuaAIParam();
    }

    AIInfoPtr aiInfo = room->getAIInfoById(id);

    if(aiInfo.get() == NULL){
        LOG_ERROR << "get ai info failed, id:"<<id;
        return ;//LuaAIParam();
    }

    test = aiInfo->luaParam_.get();

}

LuaManager::~LuaManager(){

    delete lua_script;

}



