#ifndef __KUNSERVER_MUTEX_H
#define __KUNSERVER_MUTEX_H

#include "CurrThread.h"
#include "noncopyable.h"
#include <assert.h>
#include <pthread.h>
#include <thread>
#include "CurrThread.h"

namespace fsk{

#ifdef __APPLE__
    
    class MutexLock{
    public:
        MutexLock():holder_(0){
            pthread_mutex_init(&mutex_, NULL);
        }
        
        ~MutexLock(){
            pthread_mutex_destroy(&mutex_);
        }
        
        bool isLockedByThisThread() const{
            uint64_t tid;
            pthread_threadid_np(NULL, &tid);
            
            return holder_ == tid;
        }
        
        void assertLocked() const{
            assert(isLockedByThisThread());
        }
        
        void lock(){
            pthread_mutex_lock(&mutex_);
            assignHolder();
        }
        
        void unlock(){
            unassignHolder();
            pthread_mutex_unlock(&mutex_);
        }
        
        pthread_mutex_t * getPthreadMutex(){
            return &mutex_;
        }
        
    private:
        friend class Condition;
        
        void unassignHolder(){
            holder_ = 0;
        }
        
        void assignHolder(){
            uint64_t tid;
            pthread_threadid_np(NULL, &tid);
            holder_ = tid;
        }
        
        pthread_mutex_t mutex_;
        
        uint64_t holder_;
        
    };
    
    
#else
    
class MutexLock{
public:
	MutexLock():holder_(0){
		pthread_mutex_init(&mutex_, NULL);
	}

	~MutexLock(){
		pthread_mutex_destroy(&mutex_);
	}

	bool isLockedByThisThread() const{
		return holder_ == CurrThread::tid();
	}

	void assertLocked() const{
		assert(isLockedByThisThread());
	}

	void lock(){
		pthread_mutex_lock(&mutex_);
		assignHolder();
	}

	void unlock(){
		unassignHolder();
		pthread_mutex_unlock(&mutex_);
	}

	pthread_mutex_t * getPthreadMutex(){
		return &mutex_;
	}

private:
	friend class Condition;

	void unassignHolder(){
		holder_ = static_cast<thread::id>(0);
	}

	void assignHolder(){
		holder_ = CurrThread::tid();
	}

	pthread_mutex_t mutex_;

	thread::id holder_;

};
#endif

class MutexLockGuard: noncopyable{
public:
	explicit MutexLockGuard(MutexLock& mutex)
		:mutex_(mutex){
		mutex_.lock();
	}

	~MutexLockGuard(){
		mutex_.unlock();
	}

private:
	MutexLock& mutex_;
};
}

#endif
