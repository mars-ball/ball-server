#pragma once
#include<fstream>
#include "stdint.h"
#include<memory>
#include"common/Logging.h"
#include"net/ProtobufServer.h"

#include <map>
#include "common/CfgReader.h"
#include <vector>
#include <string>

using namespace std;

namespace fsk{
namespace net{

struct ServerItem{
	string ip;
	int32_t port;
	int32_t sid;
	string name;
};


class ServerList{

public:
	static const string cfgFileName_;
	static const string LOGIN_TYPE;
	static const string ROOM_TYPE;

	ServerList(){}
	bool loadAllList();
	bool loadList(string name, vector<ServerItem>& list, map<int32_t, ServerItem>& map);

	bool isSidInList(string type, int32_t sid);
	bool getSidByIp(string type, string ip, int32_t& sid);
	bool getSidByUid(string type, int32_t uid, int32_t& sid);
	bool getSidByName(string type, string name, int32_t& sid);

	bool isLoginSidInList(int32_t sid);
	bool getLoginSidByIp(string ip, int32_t& sid);
	bool getLoginSidByUid(int32_t uid, int32_t& sid);
	bool getLoginSidByName(string name, int32_t& sid);

private:
	map<string, vector<ServerItem>> fullList_;
	map<string, map<int32_t, ServerItem>> fullDic_;


};


typedef shared_ptr<ServerList> ServerListPtr;

}
}