#include "Util.h"




float randFloat(){
	float r = static_cast<float>(rand())/static_cast<float>(RAND_MAX);
	return r;
}

int32_t randRange(int32_t low, int32_t high){
	float r = low + static_cast<float>(rand())/(static_cast<float>(RAND_MAX)/static_cast<float>(high - low - 0.001));
	int32_t rr = static_cast<int32_t>(r);
	return rr;
}


bool str2num(string str, int& num){

	try{
		num = std::stoi(str);
	}catch(std::exception e){
		return false;
	}

	return true;
}