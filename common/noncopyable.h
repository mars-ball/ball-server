#ifndef FSK_SERVER_NONCOPYABLE
#define FSK_SERVER_NONCOPYABLE

namespace fsk{
class noncopyable
{
public:
	noncopyable(const noncopyable&) = delete;
	noncopyable& operator=(const noncopyable&) = delete;
protected:
	noncopyable() = default;
	virtual ~noncopyable() = default;

};
}
#endif
