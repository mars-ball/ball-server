#ifndef __KUNSERVER_TIMERQUEUE_H
#define __KUNSERVER_TIMERQUEUE_H

#include <set>
#include <vector>

#include "noncopyable.h"

#include "Timestamp.h"
#include "net/Tunnel.h"
#include "net/Callbacks.h"

using namespace std;


namespace fsk{

class EventLoop;
class Timer;
class TimerId;

#ifdef __APPLE__

    class TimerFd
    : public noncopyable
    {
    private:
        int pipeFd_[2];
        std::atomic<bool> isTiming_;
    public:
        TimerFd();
        ~TimerFd();
        
        int GetFd() const;
        void SetTime(const std::chrono::milliseconds & timeout);
    };
    
#endif
    

class TimerQueue: noncopyable{

public:
	TimerQueue(EventLoop* loop);
	~TimerQueue();

	TimerId addTimer(const TimerCallback& cb, Timestamp when, double interval);
	void cancel(TimerId timerId);


private:
	typedef pair<Timestamp, Timer*> Entry;
	typedef set<Entry> TimerList;
	typedef pair<Timer*, int64_t> ActiveTimer;
	typedef set<ActiveTimer> ActiveTimerSet;
	
	void addTimerInLoop(Timer* timer);
	void cancelInLoop(TimerId timerId);
	void handleRead();

	vector<Entry> getExpired(Timestamp now);
	void reset(const vector<Entry>& expired, Timestamp now);

	bool insert(Timer* timer);

	EventLoop* loop_;
	const int timerfd_;
	Tunnel timerfdTunnel_;
	TimerList timers_;
	
	ActiveTimerSet activeTimers_;
	bool callingExpiredTimers_;
	ActiveTimerSet cancelingTimers_;
    


};



















}






#endif
