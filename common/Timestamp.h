#ifndef FSK_SERVER_TIMESTAMP
#define FSK_SERVER_TIMESTAMP
#include<chrono>
#include<ctime>
#include<string>
#include<cmath>

using namespace std;
using namespace std::chrono;

namespace fsk{

class Timestamp{
public:
	Timestamp():microEpoch_(0){
	}

	explicit Timestamp(int64_t microEpoch){
		microEpoch_ = microEpoch;
	}

	int64_t getMicroEpoch()const { return microEpoch_;}
	int64_t getMsEpoch() const { return static_cast<int64_t>(microEpoch_/kMilliSecondsPerSecond); }

	static Timestamp now();
	static Timestamp invalid(){
		return Timestamp();
	}

	std::string toString() const;
//	std::string toFormatString() const;

	bool valid() const{ return microEpoch_ > 0; }

	static const int kMicroSecondsPerSecond = 1000*1000;

	static const int kMilliSecondsPerSecond = 1000;

private:
	int64_t microEpoch_;


};

inline bool operator<(Timestamp lhs, Timestamp rhs){
	return lhs.getMicroEpoch() < rhs.getMicroEpoch();

}

inline bool operator==(Timestamp lhs, Timestamp rhs){
	return lhs.getMicroEpoch() < rhs.getMicroEpoch();
}
inline Timestamp operator+(Timestamp lhs, int ms){
	return Timestamp(lhs.getMicroEpoch() + ms * 1000);
}

inline int64_t operator-(Timestamp lhs, Timestamp rhs){
	int64_t diff = lhs.getMicroEpoch() - rhs.getMicroEpoch();
	return static_cast<int64_t>(diff/Timestamp::kMilliSecondsPerSecond);
}

inline double timeDiff(Timestamp high, Timestamp low){
	int64_t diff = high.getMicroEpoch() - low.getMicroEpoch();
	return static_cast<double>(diff)/Timestamp::kMicroSecondsPerSecond;
}



inline Timestamp addTime(Timestamp timestamp, double seconds){
	int64_t delta = static_cast<int64_t>(seconds * Timestamp::kMicroSecondsPerSecond);
	return Timestamp(timestamp.getMicroEpoch() + delta);
}

}


























#endif
