#pragma once

#include "tinyxml2.h"
#include "noncopyable.h"
#include <string>
#include <vector>

using namespace tinyxml2;
using namespace std;

namespace fsk{

class CfgReader: noncopyable{

public:
	CfgReader(string fileName):fileName_(fileName){
	}

	int32_t readString(string key, string& value);
	int32_t readInt(string key, int& value);
	int32_t loadFile();
	int32_t readStrings(vector<string>& list, int32_t nargs, ...);
	int32_t locateNode(int32_t nargs, ...);
	bool nextSibling();
	bool nextSibling(string name);
	int32_t readSubString(string key, string& value);
	int32_t readSubInt32(string key, int32_t& value);

private:
	string fileName_;
	XMLDocument doc_;
	XMLElement *currNode_;

};

}

