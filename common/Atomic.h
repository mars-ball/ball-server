#ifndef __KUNSERVER_ATOMIC_H
#define __KUNSERVER_ATOMIC_H
#include<atomic>
#include"noncopyable.h"
using namespace std;

namespace fsk{

template<typename T>
class AtomicIntegerT: noncopyable{
public:
	AtomicIntegerT()
	: value_(0){
	}

	T get(){
		return value_.load(memory_order_seq_cst);
	}

	T getAndSet(T newValue){
		return value_.exchange(newValue, memory_order_seq_cst);
	}

	T getAndAdd(T x){
		return value_.fetch_add(x, memory_order_seq_cst);

	}

	T addAndGet(T x){
		return getAndAdd(x) + x;
	}

	T incrementAndGet(){
		return addAndGet(1);
	}

	T decrementAndGet(){
		return addAndGet(-1);
	}

	void add(T x){
		getAndAdd(x);
	}

	void increment(){
		incrementAndGet();
	}

	void decrement(){
		decrementAndGet();
	}

private:
	atomic<T> value_;

};

typedef AtomicIntegerT<int32_t> AtomicInt32;
typedef AtomicIntegerT<int64_t> AtomicInt64;


}

#endif
