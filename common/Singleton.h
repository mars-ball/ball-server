#ifndef KUNSERVER_SINGLETON
#define KUNSERVER_SINGLETON

template<class T>
class Singleton{
public:
	static T& getInstance(){
		//m_isSingletonCreating = true;
		static T single;
		//m_isSingletonCreating = false;

		return single;
	}

protected:
	Singleton(){}
	Singleton(const Singleton&);
	//static bool m_isSingletonCreating;
};






















#endif
