#ifndef __KUNSERVER_TIMERID
#define __KUNSERVER_TIMERID

class Timer;


namespace fsk{

class TimerId{
public:
	TimerId(Timer* timer, int64_t seq)
	:timer_(timer),
	sequence_(seq){
	}

	TimerId()
	:timer_(NULL),
	sequence_(0){

	}

	friend class TimerQueue;

private:
	Timer* timer_;
	int64_t sequence_;

};




}












#endif
