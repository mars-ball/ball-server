#ifndef KUNSERVER_TMP_DEFINE
#define KUNSERVER_TMP_DEFINE
#include <string>
using namespace std;
namespace tmp{

extern string BATTLE_IP;
extern string BATTLE_PORT;
extern string ROOM_FILE_NAME;
extern string BUFF_FILE_NAME;
extern string DROP_FILE_NAME;
extern string PHOTO_FILE_NAME;
extern string MINE_FILE_NAME;
extern string STORE_FILE_NAME;
extern string AINAME_FILE_NAME;
extern int32_t ROOM_ID_MIN;
extern int32_t ROOM_ID_MAX;
extern int32_t USER_ID_MIN;
extern int32_t USER_ID_MAX;
extern int32_t MINE_ID_MIN;
extern int32_t MINE_ID_MAX;
extern int32_t MO_ID_MIN;
extern int32_t MO_ID_MAX;
extern int32_t AI_ID_MIN;
extern int32_t AI_ID_MAX;
extern int32_t WATCH_AD_REWARD_GOLOD;




extern int32_t USER_CREATE_PRESENT_COINT;

extern int32_t USER_INIT_PACK_SIZE;



bool isMineId(int32_t id);
bool isPlayerId(int32_t id);
bool isAIId(int32_t id);
int64_t sToMs(float sec);


}



#endif
