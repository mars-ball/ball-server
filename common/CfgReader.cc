#include "CfgReader.h"
#include "Logging.h"
#include "stdarg.h"

using namespace fsk;
using namespace tinyxml2;


int32_t CfgReader::loadFile(){

	doc_.LoadFile(fileName_.c_str());
	return doc_.ErrorID();
}

int32_t CfgReader::readString(string key, string& value){

	XMLElement* ele  = doc_.FirstChildElement(key.c_str());
	const char* ele_text = ele->GetText();
	value = ele_text;

	return doc_.ErrorID();
}


int32_t CfgReader::readInt(string key, int& value){

	return doc_.FirstChildElement(key.c_str())->QueryIntText(&value);


}

int32_t CfgReader::locateNode(int32_t nargs, ...){
		va_list argptr;
		va_start(argptr, nargs);

		char* par = va_arg(argptr, char*);
		XMLElement* ele = doc_.FirstChildElement(par);

		for(int32_t i = 1; i < nargs; i++){
			if(ele == NULL){
                LOG_ERROR << "find element failed:"<<par;
				return -1;
			}
			par = va_arg(argptr, char*);
			ele = ele->FirstChildElement(par);
		}
		
		currNode_  = ele;	
		va_end(argptr);
		return 0;
}

bool CfgReader::nextSibling(){
	if(currNode_ == NULL){
		return false;
	}
	currNode_ = currNode_->NextSiblingElement();
	if(currNode_ == NULL){
		return false;
	}	
	return true;
}

bool CfgReader::nextSibling(string name){
	if(currNode_ == NULL){
		return false;
	}
	currNode_ = currNode_->NextSiblingElement();
	if(currNode_ == NULL || string(currNode_->Name())!= name){
		return false;
	}
	return true;
}


int32_t CfgReader::readSubString(string key, string& value){
	if(currNode_ == NULL){
		LOG_ERROR <<"curr node is null";
		return -1;
	}	
	
	value = currNode_->FirstChildElement(key.c_str())->GetText();
	return 0;	
}	

int32_t CfgReader::readSubInt32(string key, int32_t& value){
	if(currNode_ == NULL){
		LOG_ERROR <<"curr node is null";
		return -1;
	}	
	
	currNode_->FirstChildElement(key.c_str())->QueryIntText(&value);
	return 0;	

}

int32_t CfgReader::readStrings(vector<string>& list, int32_t nargs, ...){
	
		va_list argptr;
		va_start(argptr, nargs);

		char* par = va_arg(argptr, char*);
		XMLElement* ele = doc_.FirstChildElement(par);

		for(int32_t i = 1; i < nargs; i++){
				if(ele == NULL){
                    LOG_ERROR << "find element failed:"<<par;
					return -1;
				}
				par = va_arg(argptr, char*);
				ele = ele->FirstChildElement(par);
		}

		do{
			string text = ele->GetText();
			list.push_back(ele->GetText());
			ele = ele->NextSiblingElement();
		}while(ele != NULL);

		va_end(argptr);
		return 0;
}




