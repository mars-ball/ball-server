#include "ServerList.h"
#include "common/CfgReader.h"
#include "net/InetAddress.h"
#include "google/protobuf/message.h"
#include "common/Util.h"

using namespace google::protobuf;
using namespace fsk;
using namespace fsk::net;

const string ServerList::cfgFileName_ = "roomCfg.xml";
const string ServerList::LOGIN_TYPE = "login";
const string ServerList::ROOM_TYPE = "room";


bool ServerList::loadAllList(){
	loadList("login", fullList_[LOGIN_TYPE], fullDic_[LOGIN_TYPE]);
	loadList("room", fullList_[ROOM_TYPE], fullDic_[ROOM_TYPE]);
    return true;
}

bool ServerList::isSidInList(string type, int32_t sid){

	const map<int32_t, ServerItem>& dic = fullDic_[type];
	if(dic.find(sid) != dic.end()){
		return true;
	}
	return true;
}

bool ServerList::getSidByName(string type, string name, int32_t& sid){
	const vector<ServerItem>& list = fullList_[type];

	auto it = find_if(list.begin(), list.end(), [&](const ServerItem& item){
		return item.name == name;
	});

	if(it == list.end()){
		return false;
	}

	sid = it->sid;
	return true;
}

bool ServerList::getSidByIp(string type, string ip, int32_t& sid){

	const vector<ServerItem>& list = fullList_[type];

	auto it = find_if(list.begin(), list.end(), [&](const ServerItem& item){
		return item.ip == ip;
	});

	if(it == list.end()){
		return false;
	}

	sid = it->sid;
	return true;

}

bool ServerList::getLoginSidByIp(string ip, int32_t& sid){

	return getSidByIp(LOGIN_TYPE, ip, sid);

}



bool ServerList::isLoginSidInList(int32_t sid){

	return isSidInList(LOGIN_TYPE, sid);
}



bool ServerList::loadList(string name, vector<ServerItem>& list, map<int32_t, ServerItem>& map){
	CfgReader reader(cfgFileName_);
	reader.loadFile();
	reader.locateNode(2, "list", name.c_str());

	do{
		string name;
	    int32_t ret = reader.readSubString("name", name);
		if(ret != 0){
			LOG_ERROR << "readString error";
			return -1;
		}

		int32_t sid;
		ret = reader.readSubInt32("id", sid);
		if(ret != 0){
			LOG_ERROR << "read id failed ";
			return -1;
		}
		
		string ip;
		ret = reader.readSubString("ip", ip);
		if(ret != 0){
			LOG_ERROR <<"read string failed";
			return -1;
		}
		
		int32_t port;
		ret = reader.readSubInt32("port", port);
		if(ret != 0){
			LOG_ERROR <<"read port failed";
			return -1;
		}
		list.push_back({ip, port, sid, name});
		map[sid] = {ip, port, sid, name};

	}while(reader.nextSibling(name));	

	return 0;
}

bool ServerList::getSidByUid(string type, int32_t uid, int32_t& sid){
	const vector<ServerItem>& list = fullList_[type];
	int32_t nSid = list.size();
	int32_t idx = uid%nSid;
	sid = list[idx].sid;
	return true;
}


bool ServerList::getLoginSidByUid(int32_t uid, int32_t& sid){
	return getSidByUid(LOGIN_TYPE, uid, sid);
}


bool ServerList::getLoginSidByName(string name, int32_t& sid){
	return getSidByName(LOGIN_TYPE, name, sid);


}