#pragma once
#include <stdint.h>
#include <memory>
#include <cmath>
#include <cstdlib>
#include "common/Logging.h"
#include <limits.h>
#include <fstream>
#include <string>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>

using namespace fsk;
using namespace std;


template<class T>
int32_t LoadProtobuf(shared_ptr<T> data, string path){
	
	fstream input(path, ios::in | ios::binary);
	
	data->ParseFromIstream(&input);

	return 0;
		
}


float randFloat();

int32_t randRange(int32_t low, int32_t high);

template<typename Element>
int ProtoRemoveByIdx(::google::protobuf::RepeatedPtrField<Element> *repeated, int index)
{
	if((repeated != NULL) && (index >= 0) && (index < repeated->size()))
	{
		repeated->SwapElements(index, repeated->size()-1); //I dont care whether index == size()-1 or not
		repeated->RemoveLast();

		return 0;
	}

return -1;
}

template<typename Element>
int ProtoRemoveObj(::google::protobuf::RepeatedPtrField<Element> *repeated, Element* element)
{
	if(repeated != NULL)
	{
		const typename ::google::protobuf::RepeatedPtrField<Element>::pointer_iterator p = std::find(repeated->pointer_begin(), repeated->pointer_end(), element);

		if(p != repeated->pointer_end())
		{
			return RemoveByIdx(repeated, std::distance(repeated->pointer_begin(), p));
		}
	}

	return -1;
}


bool str2num(string str, int& num);