#include "TimerQueue.h"

#include "TimerQueue.h"
#include "../net/EventLoop.h"
#include "Timer.h"
#include "TimerId.h"
#include "Logging.h"
#include "../net/SocketsOps.h"
#include "string.h"
#include "unistd.h"
#include "poll.h"

#include <functional>

#ifndef __APPLE__

#include <sys/timerfd.h>

#endif

using namespace fsk;

#ifdef __APPLE__


static shared_ptr<TimerFd> macTfd;


//TimerFd.cpp
TimerFd::TimerFd()
: isTiming_(false)
{
    if(::pipe(pipeFd_) == -1)
    {
        LOG_ERROR << "TimerFd not available.";
    }
}

TimerFd::~TimerFd()
{
    ::close(pipeFd_[1]);
    ::close(pipeFd_[0]);
}

int TimerFd::GetFd() const
{
    return pipeFd_[0];
}

void TimerFd::SetTime(const std::chrono::milliseconds & timeout)
{
    
    auto timeoutMs = static_cast<int>(timeout.count());
    isTiming_ = true;
    std::thread timer([this, timeoutMs]()
                      {
                          ::poll(nullptr, 0, timeoutMs);
                          ::write(pipeFd_[1], " ", 1);
                      });
    timer.detach();
}

int createTimerfd(){
    
    return macTfd->GetFd();
}

void resetTimerfd(int timerfd, Timestamp expiration){
 
    macTfd->SetTime(std::chrono::milliseconds(expiration.getMsEpoch() - Timestamp::now().getMsEpoch()));
}




#else



int createTimerfd(){
	int timerfd = ::timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK | TFD_CLOEXEC);
	if(timerfd < 0){
		LOG_ERROR << " failed in create timerfd";
	}

	return timerfd;
}


struct timespec timeFromNow(Timestamp when){

	int64_t micros = when.getMicroEpoch() - Timestamp::now().getMicroEpoch();
	if(micros < 100){
		micros = 100;
	}

	struct timespec ts;
	ts.tv_sec = static_cast<time_t>(micros/ Timestamp::kMicroSecondsPerSecond);
	ts.tv_nsec = static_cast<long>((micros% Timestamp::kMicroSecondsPerSecond)*1000);
	return ts;

}



void resetTimerfd(int timerfd, Timestamp expiration){

	struct itimerspec newValue;
	struct itimerspec oldValue;

	memset(&newValue, 0, sizeof(itimerspec));
	memset(&oldValue, 0, sizeof(itimerspec));

	newValue.it_value = timeFromNow(expiration);

	int ret = ::timerfd_settime(timerfd, 0, &newValue, &oldValue);
	if(ret){
		LOG_TRACE << "timerfd_settime()";
	}
}

#endif

void readTimerfd(int timerfd, Timestamp now){
    uint64_t x;
    ssize_t n = sockets::read(timerfd, &x, sizeof x);
    LOG_TRACE << "handle read()" << x << "at " <<now.toString();
    if(n != sizeof x){
        cout << "handle reads "<< n << " bytes instead of 8";
    }
}


TimerQueue::TimerQueue(EventLoop* loop)
: loop_(loop),
  timerfd_(createTimerfd()),
  timerfdTunnel_(loop, timerfd_)

{
  timerfdTunnel_.setReadCallback(bind(&TimerQueue::handleRead, this));
  timerfdTunnel_.enableReading();
}

TimerQueue::~TimerQueue(){
	timerfdTunnel_.disableAll();
	timerfdTunnel_.remove();
	sockets::close(timerfd_);

	for(TimerList::iterator it = timers_.begin();
		it != timers_.end(); ++it)
	{
		delete it->second;
	}
}

TimerId TimerQueue::addTimer(const TimerCallback& cb,
					Timestamp when,
					double interval){
	Timer* timer = new Timer(cb, when, interval);
	loop_->runInLoop(bind(&TimerQueue::addTimerInLoop, this, timer));
	return TimerId(timer, timer->sequence());
}


void TimerQueue::addTimerInLoop(Timer* timer){
	loop_->assertInLoopThread();
	bool preChanged = insert(timer);
	if(preChanged){
		resetTimerfd(timerfd_, timer->expiration());
	}
}

void TimerQueue::cancel(TimerId timerId){
	loop_->runInLoop(bind(&TimerQueue::cancelInLoop, this, timerId));
}

void TimerQueue::cancelInLoop(TimerId timerId){
	loop_->assertInLoopThread();
	assert(timers_.size() == activeTimers_.size());
	ActiveTimer timer(timerId.timer_, timerId.sequence_);
	ActiveTimerSet::iterator it = activeTimers_.find(timer);

	if(it != activeTimers_.end()){
		size_t n = timers_.erase(Entry(it->first->expiration(), it->first));
		assert(n == 1); (void)n;
		delete it->first;
		activeTimers_.erase(it);
	}else if(callingExpiredTimers_){
		cancelingTimers_.insert(timer);
	}
	assert(timers_.size() == activeTimers_.size());
}

void TimerQueue::handleRead()
{
	loop_->assertInLoopThread();
	Timestamp now(Timestamp::now());
	readTimerfd(timerfd_, now);

	vector<Entry> expired = getExpired(now);

	callingExpiredTimers_ = true;

	for(vector<Entry>::iterator it = expired.begin();
		it!=expired.end(); ++it){
		it->second->run();
	}

	callingExpiredTimers_ = false;
	reset(expired, now);


}


vector<TimerQueue::Entry> TimerQueue::getExpired(Timestamp now){

	assert(timers_.size() == activeTimers_.size());
	vector<Entry> expired;
	Entry sentry(now, reinterpret_cast<Timer*>(UINTPTR_MAX));
	TimerList::iterator end = timers_.lower_bound(sentry);
	assert(end == timers_.end() || now < end->first);

	copy(timers_.begin(), end, back_inserter(expired));

	timers_.erase(timers_.begin(), end);

	for(vector<Entry>::iterator it = expired.begin();
		it != expired.end(); ++it){
		ActiveTimer timer(it->second, it->second->sequence());
		size_t n = activeTimers_.erase(timer);
		assert(n == 1); (void)n;
	}
	assert(timers_.size() == activeTimers_.size());
	return expired;
}


void TimerQueue::reset(const vector<Entry>& expired, Timestamp now){

	Timestamp nextExpired;
	for(vector<Entry>::const_iterator it = expired.begin();
		it != expired.end(); it++){
		ActiveTimer timer(it->second, it->second->sequence());
		if(it->second->repeat()
			&& cancelingTimers_.find(timer) == cancelingTimers_.end()){
			it->second->restart(now);
			insert(it->second);
		}else{
			delete it->second;
		}
	}
	if(!timers_.empty()){
		nextExpired = timers_.begin()->second->expiration();
	}
	if(nextExpired.valid())
	{
		resetTimerfd(timerfd_, nextExpired);
	}
}




bool TimerQueue::insert(Timer* timer){
	loop_->assertInLoopThread();
	assert(timers_.size() == activeTimers_.size());
	bool prevChanged = false;
	Timestamp when = timer->expiration();
	TimerList::iterator it = timers_.begin();
	if(it == timers_.end() || when < it->first)
	{
		prevChanged = true;
	}

	{
		pair<TimerList::iterator, bool> result
			= timers_.insert(Entry(when, timer));
		assert(result.second); (void)result;
	}

	{
		pair<ActiveTimerSet::iterator, bool> result
			= activeTimers_.insert(ActiveTimer(timer, timer->sequence()));
		assert(result.second); (void)result;
	}

	assert(timers_.size() == activeTimers_.size());
	return prevChanged;
}











