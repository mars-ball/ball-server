#ifndef LIB_CURR_THREAD_H
#define LIB_CURR_THREAD_H

#include<thread>

using namespace std;


#ifndef __APPLE__


namespace fsk{
	namespace CurrThread{
		thread::id gettid();		
		extern thread_local thread::id t_tid;
		extern thread_local bool t_tid_is_init;
		inline thread::id tid(){
			if(__builtin_expect(t_tid_is_init == false, false)){
				t_tid = gettid();
				t_tid_is_init = true;
				return t_tid; 
			}
			
			return t_tid;	
		}
		extern thread_local hash<thread::id> tHasher;
	}


}





#else



namespace fsk{
    namespace CurrThread{
        thread::id gettid();
        extern thread::id t_tid;
        extern bool t_tid_is_init;
        inline thread::id tid(){
            if(__builtin_expect(t_tid_is_init == false, false)){
                t_tid = gettid();
                t_tid_is_init = true;
                return t_tid;
            }
            
            return t_tid;	
        }
        extern hash<thread::id> tHasher;
    }
    
    
}

#endif










#endif
