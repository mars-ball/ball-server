#include <unistd.h>
#include "CurrThread.h"
#include <sys/types.h>
#include <thread>

using namespace std;

#ifndef __APPLE__

namespace fsk{
	namespace CurrThread
	{
		thread_local thread::id t_tid;
		thread_local bool t_tid_is_init;
		thread::id gettid(){
			return this_thread::get_id(); 
		}
		thread_local hash<thread::id> tHasher;
	}


}
#else

namespace fsk{
    namespace CurrThread
    {
        thread::id t_tid;
        bool t_tid_is_init;
        thread::id gettid(){
            return this_thread::get_id();
        }
        hash<thread::id> tHasher;
    }
    
    
}



#endif