#include "TmpDefine.h"
#include <cmath>

using namespace std;
using namespace tmp;

namespace tmp{

 string BATTLE_IP = "192.168.56.101";
 string BATTLE_PORT = "9992";
 string ROOM_FILE_NAME = "tnt_deploy_room_info.data";
 string BUFF_FILE_NAME = "tnt_deploy_buff.data";
 string DROP_FILE_NAME = "tnt_deploy_drop.data";
 string PHOTO_FILE_NAME = "tnt_deploy_photo.data";
 string MINE_FILE_NAME = "tnt_deploy_mine.data";
 string STORE_FILE_NAME = "tnt_deploy_store.data";
 string AINAME_FILE_NAME = "tnt_deploy_ainame.data";
 int32_t ROOM_ID_MIN = 1000000;
 int32_t ROOM_ID_MAX = 1999999;
 int32_t USER_ID_MIN = 2000000;
 int32_t USER_ID_MAX =80000000;
 int32_t MINE_ID_MIN = 100000;
 int32_t MINE_ID_MAX = 199999;
 int32_t MO_ID_MIN = 200000;
 int32_t MO_ID_MAX = 299999;
 int32_t AI_ID_MIN = 300000;
 int32_t AI_ID_MAX = 399999;
 int32_t WATCH_AD_REWARD_GOLOD = 2;


 int32_t USER_CREATE_PRESENT_COINT = 5;

 int32_t USER_INIT_PACK_SIZE = 5;
}

bool tmp::isMineId(int32_t id){

	if(id >= MINE_ID_MIN && id <= MINE_ID_MAX){
		return true;
	}
	return false;
}

int64_t tmp::sToMs(float sec){
	return static_cast<int64_t>(round(sec*1000));
}

bool tmp::isPlayerId(int32_t id){

	if(id >= USER_ID_MIN && id <= USER_ID_MAX){
		return true;
	}
	return false;

}


bool tmp::isAIId(int32_t id){

	return id >= AI_ID_MIN && id <= AI_ID_MAX;

}