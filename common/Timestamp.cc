#include "Timestamp.h"
#include<string>
#include <inttypes.h>

using namespace std;
using namespace fsk;
using namespace std::chrono;

Timestamp Timestamp::now(){

	time_point<system_clock, microseconds> now =
		time_point_cast<microseconds>(system_clock::now());

	int64_t now_count = now.time_since_epoch().count();
	return Timestamp(now_count);
}
/*
string Timestamp::toFormatString() const{

//	time_t now_t = system_clock::to_time_t(microseconds(microEpoch_));
//	string res = put_time(std::localtime(&t), "%Y-%m-%d %H.%M.%S");
	return res;

}*/

string Timestamp::toString() const{
	char buf[32] = {0};
	int64_t seconds = microEpoch_ / kMicroSecondsPerSecond;
	int64_t micros = microEpoch_ % kMicroSecondsPerSecond;
	snprintf(buf, sizeof(buf)-1, "%" PRId64 ".%06" PRId64 "", seconds, micros);
	return buf;
}















