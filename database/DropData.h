#pragma once


#include "TntData.h"
#include "tnt_deploy_drop.pb.h"
#include <memory>

using namespace std;
using namespace google::protobuf;
using namespace tnt_deploy;

class DropData;
typedef shared_ptr<DROP> DROPPtr;



class DropData:public TntData<DROP, DropData>{
public:
    bool loadData();
};

typedef shared_ptr<DropData> DropDataPtr;