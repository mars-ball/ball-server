#pragma once

#include "TntData.h"
#include "tnt_deploy_store.pb.h"
#include <memory>

using namespace std;
using namespace google::protobuf;
using namespace tnt_deploy;

class StoreData;
typedef shared_ptr< tnt_deploy::STORE > STOREPtr;



class StoreData:public TntData< tnt_deploy::STORE, StoreData >{
public:
    bool loadData();
};

typedef shared_ptr<StoreData> StoreDataPtr;