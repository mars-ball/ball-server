#!/bin/bash 

if [ $# -eq 0 ]
then
    for file in *.proto
    do
        protoc --cpp_out=. $file
    done
elif [ $1 = "clean" ]
then
    for filename in *.proto
    do
        echo $filename
        rm "${filename%.*}.pb.cc"
        rm "${filename%.*}.pb.h"
    done
elif [ "${1##*.}" =  "proto" ]
then
    echo "${1##*.}"
fi

        
