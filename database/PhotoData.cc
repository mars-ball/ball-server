#include "PhotoData.h"
#include <string>
#include "common/TmpDefine.h"
#include "tnt_deploy_photo.pb.h"
#include <algorithm>

using namespace std;
using namespace tmp;
using namespace tnt_deploy;

bool PhotoData::loadData(){
	string path = PHOTO_FILE_NAME;
	if(TntData<PHOTO, PhotoData>::loadDataa<PHOTO_ARRAY>(path) != 0){
		LOG_ERROR << "load data failed";
		return false;
	}

	for_each(datas_.begin(), datas_.end(), 
		[&](pair<const int, PHOTOPtr>& item){
			if(item.second->photo_type() == 0){
				colorPhotos_.push_back(item.second);
			}else if(item.second->photo_type() == 1){
				imagePhotos_.push_back(item.second);
			}
		}
	);




	return true;
}







