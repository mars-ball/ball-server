#pragma once

#include "TntData.h"
#include "tnt_deploy_ainame.pb.h"
#include <memory>

using namespace std;
using namespace google::protobuf;
using namespace tnt_deploy;

class AINameData;
typedef shared_ptr< tnt_deploy::AINAME > AINAMEPtr;



class AINameData:public TntData< tnt_deploy::AINAME, AINameData >{
public:
    bool loadData();
    
    
};

typedef shared_ptr<AINameData> StoreDataPtr;