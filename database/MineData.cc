#include "MineData.h"
#include <string>
#include "common/TmpDefine.h"
#include "tnt_deploy_mine.pb.h"
#include "MoType.pb.h"
#include <algorithm>

using namespace std;
using namespace tmp;
using namespace tnt_deploy;
using namespace localSave;

bool MineData::loadData(){
	string path = MINE_FILE_NAME;
	if(TntData<MINE, MineData>::loadDataa<MINE_ARRAY>(path) != 0){
		LOG_ERROR << "load mine data failed";
		return false;
	}

	return true;
}







