#pragma once


#include "TntData.h"
#include "tnt_deploy_photo.pb.h"
#include <memory>

using namespace std;
using namespace google::protobuf;
using namespace tnt_deploy;

class PhotoData;
typedef shared_ptr<PHOTO> PHOTOPtr;



class PhotoData:public TntData<PHOTO, PhotoData>{
public:
    bool loadData();

    vector<PHOTOPtr> colorPhotos_;
    vector<PHOTOPtr> imagePhotos_;

};

typedef shared_ptr<PhotoData> PhotoDataPtr;