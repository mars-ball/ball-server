#include "DropData.h"
#include <string>
#include "common/TmpDefine.h"
#include "tnt_deploy_drop.pb.h"
#include "MoType.pb.h"
#include <algorithm>

using namespace std;
using namespace tmp;
using namespace tnt_deploy;
using namespace localSave;

bool DropData::loadData(){
	string path = tmp::DROP_FILE_NAME;
	if(TntData<DROP, DropData>::loadDataa<DROP_ARRAY>(path) != 0){
		LOG_ERROR << "load drop data failed";
		return false;
	}

	return true;
}







