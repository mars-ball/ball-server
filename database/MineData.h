#pragma once


#include "TntData.h"
#include "tnt_deploy_mine.pb.h"
#include <memory>

using namespace std;
using namespace google::protobuf;
using namespace tnt_deploy;

class MineData;
typedef shared_ptr<MINE> MINEPtr;



class MineData:public TntData<MINE, MineData>{
public:
    bool loadData();
};

typedef shared_ptr<MineData> MineDataPtr;