#pragma once
#include<algorithm>
#include<fstream>
#include"google/protobuf/message.h"
#include "common/TmpDefine.h"
#include "common/Logging.h"
#include "common/Singleton.h"
#include <map>

using namespace google::protobuf;
using namespace std;
using namespace fsk;


template<class T, class TT>
class TntData: public Singleton<TT>{

typedef shared_ptr<T> DataPtr;

public:
	template<class P>
    int32_t loadDataa(string path){
		fstream fin(path, ios::in | ios::binary); 
	    P dataArr;
	    dataArr.ParseFromIstream(&fin);
	    for (int32_t ii = 0; ii < dataArr.items_size(); ii++) {
	    	DataPtr info = DataPtr(new T());
	    	*info = dataArr.items(ii);
	    	int32_t id = info->id();
	    	if (datas_.find(id) != datas_.end()) {
	    		LOG_ERROR << "have two same data id:" << id;
	    		return -1;
			}
			
		    datas_[id] = info;
		    dataList_.push_back(info);
	    }
		fin.close();
		return 0;
    }

    virtual bool loadData(){return true;};

	DataPtr getById(int32_t id){
		if(datas_.find(id) == datas_.end()){
			LOG_ERROR << "no data type: " <<id;
			return DataPtr(NULL);
		}
		return datas_[id];;
	}

	DataPtr getByIndex(int32_t idx){
		if(idx < 0 || idx >= dataList_.size()){
			LOG_ERROR << "idx exceeds, idx:"<<idx;
			return DataPtr(NULL);
		}

		return dataList_[idx];
	}

	int32_t size(){
		return dataList_.size();
	}

protected:
	map<int32_t, DataPtr> datas_;
	vector<DataPtr> dataList_;


};
