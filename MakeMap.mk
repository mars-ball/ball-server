map.data:makemap
	./$<

makemap: makemap.o Map.pb.o type.pb.o
	g++ -o $@ $^ -lprotobuf -lz -pthread


makemap.o: MakeMapData.cc
	g++ -o $@ -c $< 
type.pb.o: database/MoType.pb.cc
	g++ -o $@ -c $<

Map.pb.o: database/Map.pb.cc
	g++ -o $@ -c $<
