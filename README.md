[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

## A C++ MOBA Game Server

This is the backend for a multiplayer online game similar to 'Agar.io', written in C++11, and composed of multiple sub-servers. It includes most features of a MOBA game. The framework is written from scratch and includes several low-level modules.
The client can be found at https://gitlab.com/mars-ball/ball-client

1. TCP communication module (net folder), based on Poll for asynchronous processing of connection requests, multithreading for handling requests from different clients, and adjustable thread pool. Communication uses the protobuf protocol.
2. Lobby server (login folder), includes the main game interface, login, registration, friends, mail, notifications, item purchasing, and team matchmaking features.
3. Battlefield server (login folder), includes game battlefield creation, adding players, physics engine logic, and calling NPC battle scripts, among other features.
4. Routing server (router folder), contains routing information for all lobby servers and battlefield servers, and returns available server addresses to the frontend.
5. 3D physics engine. Uses the Bullet physics engine, running on the server side to synchronize players' combat information and send the latest results back to the client. The communication protocol uses Protobuf.
6. Lua interpretation module (lua folder), with NPC and battlefield configuration logic written in Lua scripts. Also implements hot updates on the server side.
7. Redis module (hredis folder), used for sharing some data with other servers.


[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555


[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-url]: https://www.linkedin.com/in/shaokun-feng/