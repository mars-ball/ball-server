#include "RedConnection.h"
#include <hiredis/hiredis.h>
#include <iostream>
#include "common/Logging.h"

using namespace std;
using namespace fsk;

RedConnection::RedConnection(const std::string& host, const uint32_t port){
	c = redisConnect(host.c_str(), port);
	if(c->err != REDIS_OK){
		redisFree(c);
	}
}

RedConnection::~RedConnection(){
	redisFree(c);
}

void RedConnection::append(const std::vector<std::string> &commands){
	std::vector<const char*> argv;
	argv.reserve(commands.size());
	std::vector<size_t> argvlen;
	argvlen.reserve(commands.size());

	for(std::vector<std::string>::const_iterator it = commands.begin(); it != commands.end(); ++it){
		argv.push_back(it->c_str());
		argvlen.push_back(it->size());
	}

	int ret = redisAppendCommandArgv(c, static_cast<int>(commands.size()), argv.data(), argvlen.data());

	if(ret != REDIS_OK){
		cout << "redis failed "<<endl;
	}
}

RedReply RedConnection::get_reply(){

	redisReply *r;
	int error = redisGetReply(c, reinterpret_cast<void**>(&r));

	if(error != REDIS_OK){
		cout << "get reply failed"<<endl;
	}

	RedReply ret(r);
	freeReplyObject(r);

	if(ret.get_type() == RedReply::reply_type_t::ERROR &&
		(ret.get_str().find("READONLY") == 0)){

		cout << "read only "<<endl;
	}

	return ret;
}


vector<RedReply> RedConnection::get_replies(uint32_t count){
	std::vector<RedReply> ret;

	for(uint32_t i = 0; i < count; ++i){
		ret.push_back(get_reply());
	}
	return ret;

}


bool RedConnection::is_valid() const{
	return c->err == REDIS_OK;
}

int32_t RedConnection::SetItem(const std::string& name, const int64_t& id, const std::string& buf){
	string key = name + ":" + std::to_string(id); 
	RedReply ret = this->run(Command("SET") << key << buf);
	if(ret.get_str() != "OK"){
		LOG_ERROR << "set failed";
		return -1;
	}
	return 0;


}


int32_t RedConnection::SetItem(const std::string& name, const int64_t& id, const char* buf){
	string key = name + ":" + std::to_string(id); 
	RedReply ret = this->run(Command("SET") << key << buf);
	if(ret.get_str() != "OK"){
		LOG_ERROR << "set failed";
		return -1;
	}
	return 0;

}

int32_t RedConnection::SetItem(const std::string& name, const string& id, const std::string & buf){
	string key = name + ":" + id;
	RedReply ret = this->run(Command("SET") << key << buf);
	if(ret.get_str() != "OK"){
		LOG_ERROR << "set "<<name <<" failed";
		return -1;
	}
	return 0;
}


int32_t RedConnection::SetAdd(const std::string& name, const int64_t& id){
	RedReply ret = this->run(Command("SADD") <<name + ":all" << std::to_string(id));
	if(ret.get_integer() != 1){
			LOG_ERROR << "add set failed, name:" + name + "id:" + std::to_string(id);
			return -1;
	}
	return 0;

}	

int32_t RedConnection::AddItem(std::string name, const char* buf, int64_t& id){
		
	RedReply ret_id = this->run(Command("INCR") <<  name + ":id");
	int64_t res_id_t = ret_id.get_integer();	
	if(res_id_t <= 0 ){
			std::cout << "failed incr " <<std::endl; 	
			return -1;
	}		

	if(SetAdd(name, res_id_t)!= 0){
			LOG_ERROR << "failed to add name:" + name + "id:" + std::to_string(id);
			return -1;
	}

	string key = name + ":" + std::to_string(res_id_t);
	
	id = res_id_t;

	this->run(Command("DEL") << key);

	RedReply ret = this->run(Command("SET") <<key << buf);
	if(ret.get_str() != "OK" ){
		std::cout<<"insert into set failed" <<endl;
		return -1;
	}	
	return 0;
}


int32_t RedConnection::AddItem(std::string name, string& buf, int64_t& id){
		
	RedReply ret_id = this->run(Command("INCR") <<  name + ":id");
	int64_t res_id_t = ret_id.get_integer();	
	if(res_id_t <= 0 ){
			std::cout << "failed incr " <<std::endl; 	
			return -1;
	}		

	if(SetAdd(name, res_id_t)!= 0){
			LOG_ERROR << "failed to add name:" + name + "id:" + std::to_string(id);
			return -1;
	}

	string key = name + ":" + std::to_string(res_id_t);
	
	id = res_id_t;

	this->run(Command("DEL") << key);

	RedReply ret = this->run(Command("SET") <<key << buf);
	if(ret.get_str() != "OK" ){
		std::cout<<"insert into set failed" <<endl;
		return -1;
	}	
	return 0;
}



int32_t RedConnection::AddItemById(std::string name, const char* buf, int64_t id){

	string key = name + ":" + std::to_string(id);

	RedReply ret = this->run(Command("SET") << key << buf);
	if(ret.get_str() != "OK"){
		LOG_ERROR <<"add item failed, "<<name<<", id:"<<id;
		return -1;
	}
	return 0;
}

int32_t RedConnection::AddItemById(std::string name, string& buf, int64_t id){

	string key = name + ":" + std::to_string(id);

	RedReply ret = this->run(Command("SET") << key << buf);
	if(ret.get_str() != "OK"){
		LOG_ERROR <<"add item failed, "<<name<<", id:"<<id;
		return -1;
	}
	return 0;
}


int32_t RedConnection::GetItem(std::string name, int64_t id, string& res){
	
	string key = name + ":" + std::to_string(id);
	RedReply ret = this->run(Command("GET") << key);
	res = ret.get_str();
	return 0;
}


int32_t RedConnection::GetItem(std::string name, string& id, string& res){
	
	string key = name + ":" + id;
	RedReply ret = this->run(Command("GET") << key);
	res = ret.get_str();
	return 0;
}


bool RedConnection::HasItem(std::string name, int64_t id){
	string key = name + ":" + std::to_string(id);
	RedReply ret = this->run(Command("EXISTS") << key);
	if(ret.get_integer() == 1){
		return true;
	}else{
		return false;
	}
}


bool RedConnection::HasItem(std::string name, std::string id){
	string key = name + ":" + id;
	RedReply ret = this->run(Command("EXISTS") << key);
	if(ret.get_integer() == 1){
		return true;
	}else{
		return false;
	}
}





