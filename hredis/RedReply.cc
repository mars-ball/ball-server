#include <hiredis/hiredis.h>
#include "RedReply.h"


RedReply::RedReply(redisReply *c_reply):
type_(reply_type_t::ERROR),
integer_(0){
	type_ = static_cast<reply_type_t>(c_reply->type);
	switch(type_){
	case reply_type_t::ERROR:
	case reply_type_t::STRING:
	case reply_type_t::STATUS:
		str_ = std::string(c_reply->str, c_reply->len);
		break;
	case reply_type_t::INTEGER:
		integer_ = c_reply->integer;
		break;
	case reply_type_t::ARRAY:
		for(size_t i = 0; i< c_reply->elements; ++i){
			elements_.push_back(RedReply(c_reply->element[i]));
		}
		break;
	default:
		break;
	}
}


RedReply::RedReply():
	type_(reply_type_t::ERROR),
	integer_(0){
}










