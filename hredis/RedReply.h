#pragma once



#include<string>
#include<vector>


struct redisReply;

class RedReply{

public:
	enum class reply_type_t{
		STRING = 1,
		ARRAY = 2,
		INTEGER = 3,
		NIL = 4,
		STATUS = 5,
		ERROR = 6
	};

	inline reply_type_t get_type() const{ return type_;}

	inline const std::string& get_str() const{ return str_;}

	inline int64_t get_integer() const{ return integer_;}

	inline const std::vector<RedReply>& get_elements() const{ return elements_;}

	inline operator const std::string&() const { return str_;}

	inline operator int64_t () const { return integer_;}

	inline bool operator==(const std::string& rvalue) const{
		if(type_ == reply_type_t::STRING || type_ == reply_type_t::ERROR || type_ == reply_type_t::STATUS){
			return str_ == rvalue;
		}else{
			return false;
		}
	}


	inline bool operator==(const int64_t rvalue) const{
		if(type_ == reply_type_t::INTEGER){
			return integer_ == rvalue;
		}else{
			return false;
		}
	}




private:
	RedReply(redisReply *reply);
	RedReply();

	reply_type_t type_;
	std::string str_;
	int64_t integer_;
	std::vector<RedReply> elements_;

	friend class RedConnection;


};





