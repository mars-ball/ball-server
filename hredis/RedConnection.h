#pragma once


#include <string>
#include <iostream>
#include <vector>
#include <memory>

#include "common/noncopyable.h"
#include "RedReply.h"
#include "Command.h"


struct redisContext;

class RedConnection: fsk::noncopyable{

public:
	typedef std::shared_ptr<RedConnection> ptr_t;

	inline static ptr_t create(const std::string& host="localhost",
								const uint32_t port = 6379){
		return ptr_t(new RedConnection(host, port));
	}


	~RedConnection();

	bool is_valid() const;

	void append(const std::vector<std::string>& args);

	RedReply get_reply();
	std::vector<RedReply> get_replies(uint32_t count);

	inline RedReply run(const std::vector<std::string>& args){
		append(args);
		return get_reply();
	}

	inline redisContext* c_ptr(){ return c;}

	enum red_role_t{
		ANY = 0,
		MASTER = 1,
		SLAVE = 2
	};

	int32_t SetItem(const std::string& name, const int64_t& id, const std::string& buf);

	int32_t SetItem(const std::string& name, const int64_t& id, const char* buf);

	int32_t SetItem(const std::string& name, const string& id, const std::string & buf);

	int32_t SetAdd(const std::string& name, const int64_t& id);

	int32_t AddItem(std::string name, const char* buf, int64_t& id);

	int32_t AddItem(std::string name, string& buf, int64_t& id);

	int32_t AddItemById(std::string name, const char* buf, int64_t id);

	int32_t AddItemById(std::string name, string& buf, int64_t id);

	int32_t GetItem(std::string name, int64_t id, string& res);

	int32_t GetItem(std::string name, string& id, string& res);	

	bool HasItem(std::string name, int64_t id);

	bool HasItem(std::string name, std::string id);
private:
	RedConnection(const std::string& host, const uint32_t port);

	red_role_t role_;
	redisContext *c;


};

typedef shared_ptr<RedConnection> RedConnectionPtr;


