#pragma once


#include<string>
#include<vector>

using namespace std;


class Command{

public:
	inline Command(){}

	inline Command(string arg){
		args_.push_back(move(arg));
	}

	template<typename Type>
	inline Command& operator<<(const Type arg){
		args_.push_back(std::to_string(arg));
		return *this;
	}

	template<typename Type>
	inline Command& operator()(const Type arg){
		args_.push_back(std::to_string(arg));
		return *this;
	}

	inline operator const vector<string>& (){
		return args_;
	}

	inline string toDebugString(){
		string ret = "[redis args: ( ";
		bool first = true;
		for(vector<string>::iterator iterator = args_.begin(); iterator != args_.end(); iterator++){
			if(!first) ret += ", ";
			first = false;
			ret += "'" + *iterator + "'";
		}

		ret += " )]";
		return ret;
	}

private:
	vector<string> args_;

};


template<>
inline Command& Command::operator <<(const char* arg){
	args_.push_back(arg);
	return *this;
}

template<>
inline Command& Command::operator <<(std::string arg){
	args_.push_back(arg);
	return *this;
}


template<>
inline Command& Command::operator()(const char* arg)
{
    args_.push_back(arg);
    return *this;
}
















