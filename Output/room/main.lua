luaMgr = luaManager.getLuaManager()           -- Get the instance of luaManager

local scriptAI = require("ai")
local param

function show_text(text)                -- Declare a function to print stuff
    print("lua function:"..text)
end


function main(id, module, func)
  if module == "ai" then
    scriptAI.main(id, func)
  else
    luaMgr:print("error module, id" .. id .. " module:" .. module .. " func:" .. func)
  end 
end





--luaManager:call_lua_function("show_text")  -- Init c++ callback to a lua function
 
--luaManager:print("calling c++ function")   -- Call a luaManager methods declared in c++
