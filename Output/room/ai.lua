local M = {}

luaMgr = luaManager.getLuaManager()
luaInf = luaInterface.getLuaInterface()

local id
local param
local thinkTime = 500;
local moveDirTimeMin = 2000 
local moveDirTimeMax = 5000
local chaseTimeMin = 4000
local chaseTimeMax = 10000
local avoidTimeMin = 3000
local avoidTimeMax = 5000
local movePer = 0;
local avoidPer = 0.1;
local attackPer = 0.6;

local function main(_id, msg)
  --luaMgr:print("in ai main, msg:" .. msg)
  id = _id

  --luaManager.getLuaManager()
  param = luaManager.getTestParamById(id)
  --luaMgr:luaTest(id, param)
  --param:delete()
  --luaMgr:print("" .. _id .. " 4param:")
  luaMgr:print("" .. _id .. " param:" .. param.timer.callbackMsg)  
  if msg == "start" then
    --luaMgr:print("in ai start")
    M.start()
  elseif msg == "update" then
    --luaMgr:print("in ai update")
    M.update()
  elseif msg == "timer" then
    --luaMgr:print("in ai timer")
    M.timer()
  end 

end

local function update()
  param = luaManager.getTestParamById(id)
  if param.basic.status == nil then
    luaMgr:print(id .. "updated, status: nil")
    --return
  else
    luaMgr:print(id .. "updated, status:" .. param.basic.status .. " time:" .. luaInf:getNowTime())
    --return
  end

  if param.basic.status == "avoid eaten" then
    M.updateAvoidEaten()
  elseif param.basic.status == "chase" then
    M.updateChase()
  end

end

local function start()
  luaMgr:print("in ai start")
  luaMgr:registerAfterTimer(id, 500, "ai think")
end

local function timer()
  param = luaManager.getTestParamById(id)
  luaMgr:print("in ai timer param:" .. param.timer.callbackMsg)
  if param.timer.callbackMsg == "ai think" then
    luaMgr:print("ai think")
    M.think()
  end
end

local function think()
  param = luaManager.getTestParamById(id)
  local per = math.random()
  luaMgr:print("think per:" .. per .. " time:" .. luaInf:getNowTime())
  finish = false
  if per > attackPer then
    if M.chase() then
      finish = true
    end
  end
  if not finish and per > avoidPer then
    if M.avoidEaten() then
      finish  = true
    end
  end 

  if not finish and per > attackPer then
    if M.chase() then
      finish = true
    end
  end

  if not finish then
    M.moveDirChange()
  end
end

local function moveDirChange()
    param = luaManager.getTestParamById(id)
    luaMgr:print("in move dir change")
    local per = math.random()*2 -1
    local speed = luaInterface.LuaVector2()
    speed.x = math.cos(per*math.pi)
    speed.y = math.sin(per*math.pi)

    luaMgr:print("in move dir id:" .. id .. "speed:" .. speed.x .. ", " .. speed.y .. " rand time:" .. (math.random()*(moveDirTimeMax - moveDirTimeMin) + moveDirTimeMin))
    luaInf:setSpeed(id, speed)
    luaMgr:registerAfterTimer(id, math.random()*(moveDirTimeMax - moveDirTimeMin) + moveDirTimeMin, "ai think")
    param.basic.status = "move dir change"
end


local function avoidEaten()
    param = luaManager.getTestParamById(id)
    luaMgr:print("in avoid eaten")
    idSet = luaInf:getAroundEnemy(id)

    minValue = 1000
    minId = -1
    found = false

    for idIndex = 0, idSet:size()-1 do 
      threat = luaInf:getThreatValue(idSet[idIndex], id)
      if threat > 0 then
        if minValue > threat then
          minValue = threat
          minId = idSet[idIndex]
          found = true
        end
      end
    end

    if not found then
      return false
    end

    param.basic.status = "avoid eaten"
    param.basic.target = minId
    luaInf:fleeOrChase(minId, id, true)

    luaMgr:registerAfterTimer(id, math.random()*(avoidTimeMax - avoidTimeMin) + avoidTimeMin, "ai think")

    return true

end

local function updateAvoidEaten()
  param = luaManager.getTestParamById(id)
  luaMgr:print("in update avoid eaten, target:" .. param.basic.target)

  if not luaInf:isPlayerExist(param.basic.target) then
    return false
  end

  luaInf:fleeOrChase(param.basic.target, id, true)

  return true

end


local function chase()
  param = luaManager.getTestParamById(id)
  luaMgr:print("in chase")
  idSet = luaInf:getAroundEnemy(id)

  luaMgr:print("idset size:" .. idSet:size())

  minValue = 1000
  minId = -1
  found = false


  for idIndex = 0, idSet:size()-1 do 
    threat = luaInf:getThreatValue(id, idSet[idIndex])
    if threat > 0 then
      if minValue > threat then
        minValue = threat
        minId = idSet[idIndex]
        found = true
      end
    end
  end

  luaMgr:print("minValue:" .. minValue .. " minId:" .. minId)

  if not found then
    return false
  end

  param.basic.status = "chase"
  param.basic.target = minId
  if luaInf:splitIfCanEat(id, minId) then
  else
    luaInf:fleeOrChase(id, minId, false)
  end

  luaMgr:registerAfterTimer(id, math.random()*(chaseTimeMax - chaseTimeMin) + chaseTimeMin, "ai think")

  return true;

end

local function updateChase()
  param = luaManager.getTestParamById(id)
  luaMgr:print("in update chase, target:" .. param.basic.target)

  if not luaInf:isPlayerExist(param.basic.target) then
    return false;
  end

  if luaInf:splitIfCanEat(id, param.basic.target) then
    return true
  else
    luaInf:fleeOrChase(id, param.basic.target, false)
  end
  return true

end


M.main = main
M.start = start
M.update = update
M.think = think
M.timer = timer
M.moveDirChange = moveDirChange
M.avoidEaten = avoidEaten
M.updateAvoidEaten = updateAvoidEaten
M.chase = chase
M.updateChase = updateChase


return M
