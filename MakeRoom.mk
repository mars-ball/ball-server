CURR_DIR = $(shell pwd)

export CC=g++
export CPPFLAGS=-std=c++11 
export LDFLAGS = -L$(CURR_DIR)/bullet/ -lprotobuf -lz -lhiredis -pthread -lBulletDynamics -lBulletCollision -lLinearMath -llua -ldl
export OPTIMIZATION?=-g -O0 -fno-inline 
export WARNINGS=-Wall -W 
export DEBUG?= -g -ggdb
SUB_DIRS = common net user protobuf hredis database room lua
OUTPUT_PATH = Output/room
TARGET = roomServer

all: $(SUB_DIRS)
	cd $(OUTPUT_PATH) && $(CC) -o $(TARGET) $(CPPFLAGS) $(DEBUG) $(WARNINGS) $(shell sh -c 'ls $(OUTPUT_PATH) | grep "\.o" | grep -v "$(TARGET)" | grep -v "\.xml" ') $(LDFLAGS)

common:aaa
	cd common && $(MAKE) -f SubDir.mk 
	cp common/*.o $(OUTPUT_PATH)/

net:aaa
	cd net && $(MAKE) -f SubDir.mk 
	cp net/*.o $(OUTPUT_PATH)/

hredis:aaa
	cd hredis && $(MAKE) -f SubDir.mk 
	cp hredis/*.o $(OUTPUT_PATH)/

user:aaa
	cd user && $(MAKE) -f SubDir.mk 
	cp user/*.o $(OUTPUT_PATH)/

protobuf:aaa
	cd protobuf && $(MAKE) -f SubDir.mk 
	cp protobuf/*.o $(OUTPUT_PATH)/

database:aaa
	cd database && $(MAKE) -f SubDir.mk 
	cp database/*.o $(OUTPUT_PATH)/

room:aaa
	cd room && $(MAKE) -f SubDir.mk 
	cp room/*.o $(OUTPUT_PATH)/
lua:aaa
	cd lua && $(MAKE) -f SubDir.mk 
	cp lua/*.o $(OUTPUT_PATH)/


aaa:
	echo "start compile"

.PHONY: clean all
clean:
	-cd $(OUTPUT_PATH) && rm $(TARGET) *.o
	-cd common && $(MAKE) clean -f SubDir.mk
	-cd net && $(MAKE) clean -f SubDir.mk
	-cd hredis && $(MAKE) clean -f SubDir.mk
	-cd user && $(MAKE) clean -f SubDir.mk
	-cd protobuf && $(MAKE) clean -f SubDir.mk
	-cd room && $(MAKE) clean -f SubDir.mk
	-cd database && $(MAKE) clean -f SubDir.mk
	-cd lua && $(MAKE) clean -f SubDir.mk

